$(document).ready(function() {
	// Website Tour For Dashboard
    var dashboard_tour = new Tour({
        name: "stu-dashbord-tour",
        backdrop: true,
        steps: [
        {
            element: ".stu-education-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to your Dashboard"
        },
        {
            element: ".stu-profile-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to check your Profile and change your password"
        },
        {
            element: ".stu-lesson-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to check your driving lessons"
        },
        {
            element: ".stu-invoices-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to check your invoice"
        },
        {
            element: ".stu-chat-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to check your chat history"
        },
        {
            element: ".stu-notification-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to check your notification"
        },
        {
            element: ".stu-outstanding-reservations-t",
            title: "Dashboard Tour",
            placement: "top",
            content: "These are your outstanding reservations."
        },
    ]});
    if(jQuery(".stu-dashbord-tour").length > 0)
    {
	    // Initialize the tour
	    dashboard_tour.init();
	    // Start the tour
	    dashboard_tour.start(); 
	}

    // Website Tour For Profile
    var stu_profile_tour = new Tour({
        name: "stu-profile-tour",
        backdrop: true,
        steps: [
        {
            element: ".stu-information-t",
            title: "Profile Tour",
            placement: "bottom",
            content: "These are your profile detail"
        },
        {
            element: ".stu-changepass-t",
            title: "Profile Tour",
            placement: "bottom",
            content: "You can change your password"
        },
    ]});
    if(jQuery(".stu-profile-tour").length > 0)
    {
        // Initialize the tour
        stu_profile_tour.init();
        // Start the tour
        stu_profile_tour.start(); 
    }
});