
// student form load

$(document).ready(function() {
    $("#student-section").on("click","#profile",function(){
        var profile = Routing.generate('driving_school_student_profile');
        getSectionContentOf(profile);
    });
   
    // student image preview & upload
    $("body").on("click", ".click-change-img", function () {
        $("#student_logo_upload").trigger("click");
    });
    $("#student_logo_upload").change(function () {
        var file_data = $(this).prop("files")[0];
        var form_data = new FormData();
        var url_path = Routing.generate('driving_school_student_getimagepreview');
        form_data.append("file", file_data);
        $.ajax({
            method: "POST",
            url: url_path,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.click-change-img').find('img').attr('src', data.filename);
            }
        });
    });
/*
    $("[id^='share-']").fancybox();*/

    $("body").on("click","[id^='share-']",function(){
        var elementId = this.id;
        var socialtype = elementId.replace("share-","");
        $("[name='socialtype']").val(socialtype);
    });

    $("body").on("submit","#social-share-form",function(event){
        event.preventDefault();
        var url_path = $(this).attr('action');
        $.ajax({
            method: "POST",
            url: url_path,
            data: $(this).serialize(),
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                
            }
        });
    });

});

function getSectionContentOf(url,data) {
    if (typeof data === "undefined") {
        data = {};
    }
    $("#openModal").show();
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json"
    }).success(function (data) {
        if(data.redirect != undefined) {
            document.location = data.url;
        }
        $("#menu-content").html(data.response);
        $("#openModal").hide();
    });
}

