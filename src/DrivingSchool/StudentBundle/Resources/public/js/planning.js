
// student form load

$(document).ready(function() {

    $('#planning_form_enddate_date').hide();
    $('#planning_form_enddate_time').hide();

    var logic = function(currentDateTime){
        var selectedDate = currentDateTime;
        $('input[id="planning_form_enddate_date"]').val($('#planning_form_startdate_date').val());
    }

    $('#planning_form_startdate_date').datetimepicker({
        onChangeDateTime:logic,
        timepicker:false,
        minDate: '0',
        format: 'd-m-Y'
    });

    $('#planning_form_startdate_time').datetimepicker({
        minTime: new Date().getHours()+":"+new Date().getMinutes(),
        datepicker:false,
        step: 5,
        format: 'H:i'
    });

    $('#planning_form_enddate_time').datetimepicker({
        minTime: jQuery('#planning_form_startdate_time').val()?jQuery('#planning_form_startdate_time').val():false,
        datepicker:false,
        step: 5,
        format: 'H:i'
    });

    $('body').on('change',"#planning_form_pricePackage",function(){
        var trainingSessionList = Routing.generate('driving_school_student_planning_list_trainingsession');
        var pricePackageSelector = $(this);
        $.ajax({
            type: "POST",
            url: trainingSessionList,
            data: { price_package_id: $(pricePackageSelector).val() },
            dataType: "JSON",
        }).success(function (responseArray) {

            var trainingSessions = responseArray.trainingSessions;
            var vehicles = responseArray.vehicles;
            var instructors = responseArray.instructors;
            console.log(trainingSessions);

            var trainingSessionSelect = $("#planning_form_trainingSession");
            var vehicleSelect = $("#planning_form_vehicle");
            var instructorSelect = $("#planning_form_instructor");


            // Remove current options
            trainingSessionSelect.html('');
            vehicleSelect.html('');
            instructorSelect.html('');
            
            // Empty value ...
            trainingSessionSelect.append('<option value> Select Training of ' + pricePackageSelector.find("option:selected").text() + '</option>');
            vehicleSelect.append('<option value> Select Vehicle of ' + pricePackageSelector.find("option:selected").text() + '</option>');
            instructorSelect.append('<option value> Select Instructor of ' + pricePackageSelector.find("option:selected").text() + '</option>');
            
            $.each(trainingSessions, function (key, trainingSession) {
                trainingSessionSelect.append('<option value="' + trainingSession.id + '">' + trainingSession.name + '</option>');
            });
            $.each(vehicles, function (key, vehicle) {
                vehicleSelect.append('<option value="' + vehicle.id + '">' + vehicle.name + '</option>');
            });
            $.each(instructors, function (key, instructor) {
                instructorSelect.append('<option value="' + instructor.id + '">' + instructor.name + '</option>');
            });

            $('#planning_form_vehicle').removeClass('changeselectorcolor');
            $('#planning_form_instructor').removeClass('changeselectorcolor');
        });
    });

    $('body').on('change',"#duration-selector",function(event){
        var minutes = $(this).val();
        var startTime = $('#planning_form_startdate_time').val();
        if(minutes == "custom_duration") {
            $(this).hide();
            $('#planning_form_enddate_time').show();
        } else {
            var convertedTime = moment.utc(startTime,'hh:mm').add(minutes,'minutes').format('HH:mm');
            $(this).parent().find('#planning_form_enddate_time').val(convertedTime);
        }
    });
});
