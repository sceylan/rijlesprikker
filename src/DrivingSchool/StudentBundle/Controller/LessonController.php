<?php

namespace DrivingSchool\StudentBundle\Controller;

use DrivingSchool\AdminBundle\Entity\StudentEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class LessonController extends Controller
{
    public function lessionsAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $lessonsquery = $connection->prepare("SELECT planning.*,pap.id as priceid, pap.name as pricename, pi.icon_path, ts.id as trainingid, ts.training_type as trainingtype, ts.name as trainingname, ts.appointment_label, i.instructor_name as instructorname FROM planning LEFT JOIN price_and_packages as pap ON pap.id = planning.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id LEFT JOIN training_sessions as ts ON ts.id = planning.training_session_id LEFT JOIN instructor as i ON i.id = planning.instructor_id WHERE planning.student_id = ".$userId." ORDER BY planning.id DESC");
        $lessonsquery->execute();
        $lessons = $lessonsquery->fetchAll();

        $data = [
            'lessons' => $lessons,
        ];

        return $this->render('DrivingSchoolStudentBundle:Student:lessions.html.twig', $data);
    }

    public function lessionDetailAction(Request $request, $planningid, UserInterface $user)
    {
        // echo $planningid;exit;

        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $detailquery = $connection->prepare("SELECT planning.*, ts.id as trainingid, ts.training_type as trainingtype, ts.name as trainingname, i.instructor_name as instructorname FROM planning LEFT JOIN training_sessions as ts ON ts.id = planning.training_session_id LEFT JOIN instructor as i ON i.id = planning.instructor_id WHERE planning.student_id = ".$userId." AND planning.id = ".$planningid);
        $detailquery->execute();
        $lessonDetail = $detailquery->fetch();

        if(empty($lessonDetail)) 
        {
            return $this->redirectToRoute('driving_school_student_lessons');
        }

        return $this->render('DrivingSchoolStudentBundle:Student:lessionDetail.html.twig', array('lessonDetail' => $lessonDetail));
    }

    public function lessionReportAction(Request $request, $planningid, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();

        // get planning detail
        $planning = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('id' => $planningid, 'student' => $userId));
        if(empty($planning))
        {
            return $this->redirectToRoute('driving_school_student_lessons');
        }

        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $userId));
        $DrivingSchool = $student->getDrivingSchool();

        $lessonModules = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->findBy(array('lessonId' => $planning->getPricePackage()));

        $lessonModuless = [];
        foreach ($lessonModules as $lessonModule) {
            $totalScored=0;
            $totalCounts=0;
            // get assessment method
            $assessments = $entityManager->getRepository('DrivingSchoolAdminBundle:AssessmentMethodEntity')->findBy(array('DrivingSchool' => $DrivingSchool));

            // get lesson roles
            $lessonRoles = $entityManager->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->findBy(array('Module' => $lessonModule->getId()));

            $data = [];
            $scorecards = [];
            foreach ($lessonRoles as $lessonrole) {
                $rolesid = $lessonrole->getId();

                $scoreCardNotes = $entityManager->getRepository('DrivingSchoolAdminBundle:ScoreCardNotesEntity')->findOneBy(array('DrivingSchool' => $DrivingSchool, 'student' => $userId, 'lessonRoles' => $rolesid));

                if(!empty($scoreCardNotes)) {
                    $progressbarValue = $scoreCardNotes->getProgressbarValue();
                } else {
                    $progressbarValue= '';
                }
        
                $count_assessment = count($assessments);
                $totalScored+= $progressbarValue;
                $totalCounts+= $count_assessment;
                $per = ($totalScored * 100)/ $totalCounts;
            }
            $lessonModule->percentage = $per;
            $lessonModuless[] = $lessonModule;
        }
        
        $data = [
            'lessonsModules' => $lessonModuless,
            'planningid' => $planningid,
        ];

        return $this->render('DrivingSchoolStudentBundle:Student:lessonReport.html.twig', $data);
    }

    public function lessionRolesAction(Request $request, $planningid, $moduleid, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();

        // get planning detail
        $planning = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('id' => $planningid, 'student' => $userId));
        if(empty($planning))
        {
            return $this->redirectToRoute('driving_school_student_lessons');
        }

        // get student detail
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $userId));
        $DrivingSchool = $student->getDrivingSchool();

        // get package wise module detail
        $lessonModules = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->findOneBy(array('id' => $moduleid, 'lessonId' => $planning->getPricePackage()));
        $lessonModuleName = $lessonModules->getModuleName();

        // get assessment method
        $assessments = $entityManager->getRepository('DrivingSchoolAdminBundle:AssessmentMethodEntity')->findBy(array('DrivingSchool' => $DrivingSchool));

        // get lesson roles
        $lessonRoles = $entityManager->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->findBy(array('Module' => $moduleid));

        $data = [];
        $scorecards = [];
        foreach ($lessonRoles as $lessonrole) {
            $rolesid = $lessonrole->getId();

            $scoreCardNotes = $entityManager->getRepository('DrivingSchoolAdminBundle:ScoreCardNotesEntity')->findOneBy(array('DrivingSchool' => $DrivingSchool, 'student' => $userId, 'lessonRoles' => $rolesid));

            if(!empty($scoreCardNotes)) {
                $data['lessonRolesId'] = $rolesid;
                $data['lessonRolesName'] = $lessonrole->getName();
                $data['progressbarValue'] = $scoreCardNotes->getProgressbarValue();
                $scorecards[] = $data;
            } else {
                $data['lessonRolesId'] = $rolesid;
                $data['lessonRolesName'] = $lessonrole->getName();
                $data['progressbarValue'] = '';
                $scorecards[] = $data;
            }
        }
        $count_assessment = count($assessments);

        $assessmentsarr[0]['name'] = '';
        foreach ($assessments as $key => $assessment) {
            $array = [];
            $array['name'] = $assessment->getName();
            $assessmentsarr[] = $array;
        }  

        $data = [
            'lessonModuleId' => $lessonModules->getId(),
            'lessonModuleName' => $lessonModuleName,
            'planningid' => $planningid,
            'scorecards' => $scorecards,
            'assessments' => $assessmentsarr,
            'assessment_count' => $count_assessment,
        ];

        return $this->render('DrivingSchoolStudentBundle:Student:lessonRoles.html.twig', $data);
    }
}
