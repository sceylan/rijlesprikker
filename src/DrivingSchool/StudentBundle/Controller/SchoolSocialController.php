<?php

namespace DrivingSchool\StudentBundle\Controller;

use DrivingSchool\AdminBundle\Entity\StudentEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class SchoolSocialController extends Controller
{
    public function connectAction(Request $request, UserInterface $user)
    {
        
        $school = !empty($user->getDrivingSchool()) ? $user->getDrivingSchool() : "";
        $data = [
            'school' => $school,
        ];

        return $this->render('DrivingSchoolStudentBundle:SchoolSocial:connect.html.twig', $data);
    }

    public function shareAction(Request $request, UserInterface $user)
    {
        $data = $request->request->all();
        $fromEmail  = !empty($user->getEmail()) ? $user->getEmail() : "";
        $toemails = $request->request->get('shareemails');

        dump($data);
        exit();
        $school = !empty($user->getDrivingSchool()) ? $user->getDrivingSchool() : "";
        $data = [
            'school' => $school,
        ];
        
        return $this->render('DrivingSchoolStudentBundle:SchoolSocial:connect.html.twig', $data);
    }
}
