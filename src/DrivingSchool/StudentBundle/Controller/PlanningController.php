<?php

namespace DrivingSchool\StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use DrivingSchool\StudentBundle\Form\PlanningForm;
use DrivingSchool\AdminBundle\Entity\PlanningEntity;
use DrivingSchool\AdminBundle\Entity\NotificationEntity;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlanningController extends Controller
{
    public function appointmentAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $event = new PlanningEntity();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(PlanningForm::class, $event, array('userId' => $userId, 'entity_manager' => $entityManager));
        $form->handleRequest($request);

        $data = [
            'form' => $form->createView()
        ];
        
        return $this->render('DrivingSchoolStudentBundle:Planning:createplanning.html.twig', $data);
    }


    public function listTrainingSessionOfPricePackagesAction(Request $request, UserInterface $user)
    {
        $userId         = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $trainingSessionRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:TrainingSessionsEntity");
        $vehicleRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:VehicleEntity");
        $instructorRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:InstructorEntity");


        // Search the Training Session that belongs to the Price and Package with the given id as GET parameter "pricePackageId"
        
        $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $userId);
        

        $trainingSessions = $trainingSessionRepository->createQueryBuilder("q")
            ->innerJoin('q.pricePackageId', 'p')
            ->innerJoin('p.student', 's')
            ->where("q.pricePackageId = :pricePackageId")
            ->andWhere("s.id = :userId")
            ->andWhere("q.type IN ( 0,1 )")
            ->setParameters($params)
            ->getQuery()
            ->getResult();
            
        $trainingSessionResponseArray = array();
        foreach($trainingSessions as $trainingSession){
            $trainingSessionResponseArray[] = array(
                "id" => $trainingSession->getId(),
                "name" => $trainingSession->getName()
            );
        }



        
        $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $userId);
        
        $vehicles = $vehicleRepository->createQueryBuilder("q")
            ->innerJoin('q.suitableTrainingCourses', 'p')
            ->innerJoin('p.student', 's')
            ->where("p.id = :pricePackageId")
            ->andWhere("s.id = :userId")
            ->setParameters($params)
            ->getQuery()
            ->getResult();

        $vehicleResponseArray = array();
        foreach($vehicles as $vehicle){
            $vehicleResponseArray[] = array(
                "id" => $vehicle->getId(),
                "name" => $vehicle->getLicensePlate()
            );
        }



        $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $userId);
        
        $instructors = $instructorRepository->createQueryBuilder("q")
            ->innerJoin('q.authorizedFor', 'p')
            ->innerJoin('p.student', 's')
            ->where("p.id = :pricePackageId")
            ->andWhere("s.id = :userId")
            ->setParameters($params)
            ->getQuery()
            ->getResult();
        


        $instructorResponseArray = array();
        foreach($instructors as $instructor){
            $instructorResponseArray[] = array(
                "id" => $instructor->getId(),
                "name" => $instructor->getInstructorName()
            );
        }

        $responseArray = array(
            "trainingSessions" => $trainingSessionResponseArray,
            "vehicles" => $vehicleResponseArray,
            "instructors" => $instructorResponseArray
        );

        return new JsonResponse($responseArray);
    }

    public function appointmentSaveAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $event = new PlanningEntity();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(PlanningForm::class, $event, array('userId' => $userId, 'entity_manager' => $entityManager));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $data->setSchool($user->getDrivingSchool());
            $data->setStudent($user);
            $data->setIsReservation(true);
            $entityManager->persist($data);
            $entityManager->flush();
            $usertypeid = '3';
            $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);

            /* Start notification */
                $notification = new NotificationEntity();
                $notification->setSchool($user->getDrivingSchool());
                $notification->setStudent($user);
                $notification->setInstructor($event->getInstructor());
                $notification->setPackageName($event->getPricePackage()->getName());
                $notification->setTrainingSessionName($event->getTrainingSession()->getName());
                $notification->setEventStartDate($event->getStartDate());
                $notification->setEventEndDate($event->getEndDate());
                $notification->setUserId($userId);
                $notification->setUserType($usertype);
                $notification->setAction('Added');
                $notification->setSchoolReadStatus(0);
                $notification->setInstructorReadStatus(0);
                $notification->setStudentReadStatus(0);
                $notification->setNotificationType('Event');
                $entityManager->persist($notification);
                $entityManager->flush();
            /* End Notification */

            /* Start Mail Send */
            $mailer = $this->get('mailer');
            $packagename = $event->getPricePackage()->getName();
            $coursename = $event->getTrainingSession()->getName();
            $eventdate = date('d M, Y', $event->getStartDate()->getTimestamp());
            $eventtime = date('g:i, A', $event->getStartDate()->getTimestamp()).' - '.date('g i, A', $event->getEndDate()->getTimestamp());
            $createdatetime = date('M d', $event->getCreation()->getTimestamp()).' at '.date('g:i A', $event->getCreation()->getTimestamp());

            $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '5'));
            $subject = $emailtmp->getSubject();
            $content = $emailtmp->getContent();
            $tempsubject = $this->get('twig')->createTemplate($subject);
            $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

            $template = $this->get('twig')->createTemplate($content);

            $names = array(0 => array("name" => $user->getDrivingSchool()->getSchoolName(), "email" => $user->getDrivingSchool()->getEmail()),1 => array("name" => $event->getInstructor()->getInstructorName(), "email" => $event->getInstructor()->getEmail()), 2 => array("name" => $user->getFirstName(), "email" => $user->getEmail()));
            foreach($names as $data){
                $mailcontent = $template->render(array('name'=> $data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $event->getInstructor()->getInstructorName(), 'schoolname' => $user->getDrivingSchool()->getSchoolName(), 'schooladdress' => $user->getDrivingSchool()->getSchoolAddress(), 'schoolphone' => $user->getDrivingSchool()->getSchoolPhone()));
                
                $message = (new \Swift_Message($mailsubject))
                ->setFrom('admin@drivingschool.com')
                ->setTo($data['email'])
                ->setBody($mailcontent,'text/html');
                $mailer->send($message);
            } 
            /* End Mail Send */
        }
        return $this->redirectToRoute('driving_school_student_appointmentlist');
    }

    public function appointmentListAction(Request $request, UserInterface $user)
    {
        $userId         = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $planningAppointments = array();
        if(!empty($user)) {
            $planningAppointments = $entityManager->getRepository("DrivingSchoolAdminBundle:PlanningEntity")->findBy(array("student" => $user, "isReservation" => false));
        }

        return $this->render('DrivingSchoolStudentBundle:Planning:list.html.twig', array('appointments' => $planningAppointments));
    }

    public function appointmentDetailAction(Request $request, UserInterface $user)
    {
        $userId         = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $appointmentId = $request->get("appointmentid");
        $planningAppointmentDetail = array();
        if(!empty($user) && !empty($appointmentId)) {
            $planningAppointmentDetail = $entityManager->getRepository("DrivingSchoolAdminBundle:PlanningEntity")->findOneBy(array("student" => $user, "id" => $appointmentId));
            if(!empty($planningAppointmentDetail)) {
                return $this->render('DrivingSchoolStudentBundle:Planning:detail.html.twig', array('appointmentdetail' => $planningAppointmentDetail));
            }
        }

        return $this->redirectToRoute('driving_school_student_appointmentlist');
    }
}
