<?php

namespace DrivingSchool\StudentBundle\Controller;

use DrivingSchool\AdminBundle\Entity\StudentEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class ChatController extends Controller
{
    public function chatAction(Request $request, UserInterface $user)
    {
        return $this->render('DrivingSchoolStudentBundle:Student:chat.html.twig');
    }
}
