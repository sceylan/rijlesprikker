<?php

namespace DrivingSchool\StudentBundle\Controller;

use DrivingSchool\StudentBundle\Form\StudentLoginForm;
use DrivingSchool\AdminBundle\Entity\StudentEntity;
use DrivingSchool\AdminBundle\Entity\NotificationEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use Application\Sonata\MediaBundle\Entity\Media;

class StudentController extends Controller
{
    public function loginAction(Request $request)
    {
        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
           return $this->redirectToRoute('driving_school_student_profile'); 
        }

        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        
        $student = new StudentEntity();
        $form = $this->createForm(StudentLoginForm::class, $student);

        return $this->render(
            'DrivingSchoolStudentBundle:Student:login.html.twig',
                array(
                    'form'          => $form->createView(),
                    'error'         => $error,
                )
        );
    }
    
    public function dashboardAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $query = $connection->prepare("SELECT ds.schoolName as schoolName,s.username as studentname FROM student AS s LEFT JOIN driving_school AS ds ON ds.id = s.drivingschool_id WHERE s.id = ".$userId);

        $query->execute();
        $student = $query->fetch();
        $media = $user->getStudentImage();
        
        if(!empty($media)) {
            $mediaManager = $this->get('sonata.media.pool');
            $provider = $mediaManager->getProvider($media->getProviderName());
            $format = $provider->getFormatName($media, 'default_small');
            $image_path = $provider->generatePublicUrl($media, $format);
            $format_big = $provider->getFormatName($media, 'default_big');
            $big_image_path = $provider->generatePublicUrl($media, $format_big);
        }

        // Start Outstanding reservations
        $reservationQuery = $connection->prepare("SELECT p.*,ts.name as coursesname, ts.appointment_label as coursecolor, i.instructor_name, v.license_plate, s.first_name as studentname, pi.icon_path FROM planning AS p LEFT JOIN training_sessions as ts ON ts.id = p.training_session_id LEFT JOIN instructor as i ON i.id = p.instructor_id LEFT JOIN vehicle as v ON v.id = p.vehicle_id LEFT JOIN student as s ON s.id = p.student_id LEFT JOIN price_and_packages as pap ON pap.id = p.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE p.student_id = ".$userId." AND is_reservation='1' AND student_id !='NULL'");
        $reservationQuery->execute();
        $reservations = $reservationQuery->fetchAll();
        // End Outstanding reservations

        return $this->render('DrivingSchoolStudentBundle:Student:dashboard.html.twig', array('imagepath' => $big_image_path, 'student' => $student, 'reservations' => $reservations));
    }

    public function getimagepreviewAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($userId);
        $mediaManager = $this->get('sonata.media.manager.media');
        foreach ($request->files as $uploadedFile) {
            if(!empty($uploadedFile)) {
                $media = new Media();
                $media->setBinaryContent($uploadedFile);
                $media->setContext("default");
                $media->setProviderName('sonata.media.provider.image');
                $mediaManager->save($media);
                $student->setStudentImage($media);
            }
        }
        $entityManager->persist($student);
        $entityManager->flush();

        $request = Request::createFromGlobals();
        $resp = [];
        foreach ($request->files as $uploadedFile) {
            $fileData = file_get_contents($uploadedFile->openFile()->getPathName());
        }
        
        $enocoded_data = base64_encode($fileData);
        $resp['filename'] = 'data:image/png;base64, '.$enocoded_data;

        return new Response(json_encode($resp));
    }

    public function reservationAction(Request $request, UserInterface $user)
    {
        // echo "<pre>";
        // print_r($request->request->all());exit;
        $userId = $user->getId();

        $data = $request->request->all();
        $reservations_id = $data['reservationchk'];
        $entityManager = $this->getDoctrine()->getManager();

        if(isset($data['submitaccept'])) 
        {    
            $editreservation   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(['id' => $reservations_id]);
            $usertypeid = '3';
            $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
         
            if(!empty($editreservation))
            {
                foreach ($editreservation as $reservation) {
                    $reservation->setIsReservation(0);
                    $entityManager->persist($reservation);

                    /* Start notification */
                    $notification = new NotificationEntity();
                    $notification->setSchool($reservation->getSchool());
                    $notification->setStudent($reservation->getStudent());
                    $notification->setInstructor($reservation->getInstructor());
                    $notification->setPackageName($reservation->getPricePackage()->getName());
                    $notification->setTrainingSessionName($reservation->getTrainingSession()->getName());
                    $notification->setEventStartDate($reservation->getStartDate());
                    $notification->setEventEndDate($reservation->getEndDate());
                    $notification->setUserId($userId);
                    $notification->setUserType($usertype);
                    $notification->setAction('Accepted');
                    $notification->setSchoolReadStatus(0);
                    $notification->setInstructorReadStatus(0);
                    $notification->setStudentReadStatus(0);
                    $notification->setNotificationType('Event');
                    $entityManager->persist($notification);
                    /* End Notification */

                    /* Start Mail Send */
                    $mailer = $this->get('mailer');
                    $packagename = $reservation->getPricePackage()->getName();
                    $coursename = $reservation->getTrainingSession()->getName();
                    $eventdate = date('d M, Y', $reservation->getStartDate()->getTimestamp());
                    $eventtime = date('g:i, A', $reservation->getStartDate()->getTimestamp()).' - '.date('g i, A', $reservation->getEndDate()->getTimestamp());
                    $createdatetime = date('M d', $reservation->getCreation()->getTimestamp()).' at '.date('g:i A', $reservation->getCreation()->getTimestamp());

                    $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '8'));
                    $subject = $emailtmp->getSubject();
                    $content = $emailtmp->getContent();
                    $tempsubject = $this->get('twig')->createTemplate($subject);
                    $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));
                    
                    $names = array(0 => array("name" => $reservation->getSchool()->getSchoolName(), "email" => $reservation->getSchool()->getEmail()),1 => array("name" => $reservation->getInstructor()->getInstructorName(), "email" => $reservation->getInstructor()->getEmail()), 2 => array("name" => $reservation->getStudent()->getFirstName(), "email" => $reservation->getStudent()->getEmail()));
                    
                    foreach($names as $data){
                        $template = $this->get('twig')->createTemplate($content);
                        $mailcontent = $template->render(array('name'=>$data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $reservation->getInstructor()->getInstructorName(), 'schoolname' => $reservation->getSchool()->getSchoolName(), 'schooladdress' => $reservation->getSchool()->getSchoolAddress(), 'schoolphone' => $reservation->getSchool()->getSchoolPhone()));

                        $message = (new \Swift_Message($mailsubject))
                        ->setFrom('admin@drivingschool.com')
                        ->setTo($data['email'])
                        ->setBody($mailcontent,'text/html');
                        $mailer->send($message);
                    }

                    /* End Mail Send */
                }       
                $entityManager->flush();
            }
        } elseif (isset($data['submitreject']))
        {
            $editreservation   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(['id' => $reservations_id]);
            $usertypeid = '3';
            $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);

            if(!empty($editreservation))
            {
                foreach ($editreservation as $reservation) {
                    /* Start notification */
                    $notification = new NotificationEntity();
                    $notification->setSchool($reservation->getSchool());
                    $notification->setStudent($reservation->getStudent());
                    $notification->setInstructor($reservation->getInstructor());
                    $notification->setPackageName($reservation->getPricePackage()->getName());
                    $notification->setTrainingSessionName($reservation->getTrainingSession()->getName());
                    $notification->setEventStartDate($reservation->getStartDate());
                    $notification->setEventEndDate($reservation->getEndDate());
                    $notification->setUserId($userId);
                    $notification->setUserType($usertype);
                    $notification->setAction('Rejected');
                    $notification->setSchoolReadStatus(0);
                    $notification->setInstructorReadStatus(0);
                    $notification->setStudentReadStatus(0);
                    $notification->setNotificationType('Event');
                    $entityManager->persist($notification);
                    /* End Notification */

                    /* Start Mail Send */
                    $mailer = $this->get('mailer');
                    $packagename = $reservation->getPricePackage()->getName();
                    $coursename = $reservation->getTrainingSession()->getName();
                    $eventdate = date('d M, Y', $reservation->getStartDate()->getTimestamp());
                    $eventtime = date('g:i, A', $reservation->getStartDate()->getTimestamp()).' - '.date('g i, A', $reservation->getEndDate()->getTimestamp());
                    $createdatetime = date('M d', $reservation->getCreation()->getTimestamp()).' at '.date('g:i A', $reservation->getCreation()->getTimestamp());

                    $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '9'));
                    $subject = $emailtmp->getSubject();
                    $content = $emailtmp->getContent();
                    $tempsubject = $this->get('twig')->createTemplate($subject);
                    $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

                    $names = array(0 => array("name" => $reservation->getSchool()->getSchoolName(), "email" => $reservation->getSchool()->getEmail()),1 => array("name" => $reservation->getInstructor()->getInstructorName(), "email" => $reservation->getInstructor()->getEmail()), 2 => array("name" => $reservation->getStudent()->getFirstName(), "email" => $reservation->getStudent()->getEmail()));
                    
                    foreach($names as $data){
                        $template = $this->get('twig')->createTemplate($content);
                        $mailcontent = $template->render(array('name'=>$data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $reservation->getInstructor()->getInstructorName(), 'schoolname' => $reservation->getSchool()->getSchoolName(), 'schooladdress' => $reservation->getSchool()->getSchoolAddress(), 'schoolphone' => $reservation->getSchool()->getSchoolPhone()));

                        $message = (new \Swift_Message($mailsubject))
                        ->setFrom('admin@drivingschool.com')
                        ->setTo($data['email'])
                        ->setBody($mailcontent,'text/html');
                        $mailer->send($message);
                    }

                    /* End Mail Send */

                    $reservation->setStudent(NULL);
                    $entityManager->persist($reservation);
                }       
                $entityManager->flush();
            }
        }
        return $this->redirectToRoute('driving_school_student_dashboard');
    }
}
