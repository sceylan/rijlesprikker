<?php

namespace DrivingSchool\StudentBundle\Controller;

use DrivingSchool\AdminBundle\Entity\StudentEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class ProfileController extends Controller
{
    public function profileAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($userId);

        return $this->render('DrivingSchoolStudentBundle:Student:profile.html.twig',array("student" => $student));
    }

    public function drivingSchoolAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $query = $connection->prepare("SELECT s.username as studentname, ds.schoolName as schoolName, ds.logo_id as schoollogo, ds.email as schoolemail, ds.phone as schoolphone, ds.schoolAddress FROM student AS s LEFT JOIN driving_school AS ds ON ds.id = s.drivingschool_id WHERE s.id = ".$userId);

        $query->execute();
        $schoolprofile = $query->fetch();
		$media = $schoolprofile['schoollogo'];

        $entityManager = $this->getDoctrine()->getManager();
        $mediaObj = $entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findOneBy(array('id' => $media));

        if(!empty($mediaObj)) {
            $mediaManager = $this->get('sonata.media.pool');
            $provider = $mediaManager->getProvider($mediaObj->getProviderName());
            $format = $provider->getFormatName($mediaObj, 'default_small');
            $image_path = $provider->generatePublicUrl($mediaObj, $format);
            $format_big = $provider->getFormatName($mediaObj, 'default_big');
            $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
        }

    	return $this->render('DrivingSchoolStudentBundle:Student:school_profile.html.twig', array('schoolprofile' => $schoolprofile, 'imagepath' => $big_image_path));
    }

    public function changepasswordAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
    	$entityManager = $this->getDoctrine()->getManager();
    	$student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($userId);
    	$oldpass = $student->getPassword();
    	$oldsalt = $student->getSalt();

    	$data = $request->request->all();
    	if(isset($data['submit'])) {
    		$password = $data['current_password'];
    		$encoder = new MessageDigestPasswordEncoder();
            $currentpass = $encoder->encodePassword($password, $oldsalt);

            if($oldpass == $currentpass)
            {
            	$confirm_password = $data['confirm_password'];
            	$newpass = $encoder->encodePassword($confirm_password, $oldsalt);
            	$student->setPassword($newpass);

            	$entityManager->persist($student);
        		$entityManager->flush();

        		$this->addFlash('successpass','successfully password updated');
            } else {
				$this->addFlash('errcurrentpass','Old password and current password does not match!');
			}
    	}
    	
    	return $this->render('DrivingSchoolStudentBundle:Student:change_password.html.twig');
    }
}
