<?php

namespace DrivingSchool\StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DrivingSchoolStudentBundle:Default:index.html.twig');
    }
}
