<?php
namespace DrivingSchool\StudentBundle\Form;

use DrivingSchool\AdminBundle\Entity\PlanningEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityManagerInterface;
use DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PlanningForm extends AbstractType
{
	private $userId;

    private $em;
	
    private $authorizationChecker=null;
    private $authorizationToken=null;

    public function __construct(AuthorizationChecker $authorizationChecker, TokenStorage $authorizationToken)
    {
      $this->authorizationChecker = $authorizationChecker;
      $this->authorizationToken = $authorizationToken;
    }

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->userId = $options['userId'];
		$this->em = $options['entity_manager'];
		$builder
			//->add('title', null,['required' => 'true','label' => 'Title'])
			->add('description', null,['required' => 'true','attr' => array('placeholder' => 'Notice Insert Wear Insertor')])
            ->add('location', null,['required' => 'true','attr' => array('placeholder' => 'Location')])
            ->add('startdate', 'datetime',['label' => 'Start Date/Time', 'date_widget' => 'single_text', 'time_widget' => 'single_text', 'date_format' => 'dd-MM-yyyy', 'required' => 'true', 'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 ]])
            ->add('enddate', 'datetime',['label' => 'Length','date_widget' => 'single_text', 'time_widget' => 'single_text', 'date_format' => 'dd-MM-yyyy', 'required' => 'true', 'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 ]])
		;

		$builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
	}

	protected function addElements(FormInterface $form, PriceAndPackagesEntity $pricePackage = null) {
        // 4. Add the province element
        $form->add('pricePackage', 'entity', [
            'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
            'query_builder' => function(EntityRepository $repository) {
                $qb = $repository->createQueryBuilder('u');
                // the function returns a QueryBuilder object
                return $qb
                    ->innerJoin('u.student', 's')
                    ->where("s.id = :userId")
                    ->setParameter('userId', $this->userId)
                ;
            },
            'multiple' => false,
            'expanded' => false,
            'placeholder'=> 'Select Package',
            'choice_label' => 'name',
            'label' => 'Package'
        ]);
        
        
        // Training Session empty, unless there is a selected Package (Edit View)
        $trainingSession = array();
        
        // If there is a Package stored in the Planning entity, load the Training Session of it
        if ($pricePackage) {
            // Training Session of the Package if there's a selected Package

            $repoTrainingSession = $this->em->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity');

            $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $this->userId);
            
            $trainingSession = $repoTrainingSession->createQueryBuilder("q")
                ->innerJoin('q.pricePackageId', 'p')
                ->innerJoin('p.student', 's')
                ->where("q.pricePackageId = :pricePackageId")
                ->andWhere("s.id = :userId")
                ->andWhere("q.type IN ( 0,1 )")
                ->setParameters($parameters)
                ->getQuery()
                ->getResult();
        }
        
        // Add the Training Session field with the properly data
        $form->add('trainingSession', EntityType::class, array(
            'required' => true,
            'placeholder' => 'Select a Price Package first',
            'class' => 'DrivingSchoolAdminBundle:TrainingSessionsEntity',
            'choices' => $trainingSession
        ));


        // Vehicle empty, unless there is a selected Package (Edit View)
        $vehicle = array();
        
        // If there is a Package stored in the Planning entity, load the Training Session of it
        if ($pricePackage) {
            // Vehicle of the Package if there's a selected Package

            $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $this->userId);
            

            $vehicleRepository = $this->em->getRepository("DrivingSchoolAdminBundle:VehicleEntity");
            
            $vehicle = $vehicleRepository->createQueryBuilder("q")
	            ->innerJoin('q.suitableTrainingCourses', 'p')
                ->innerJoin('p.student', 's')
	            ->where("p.id = :pricePackageId")
	            ->andWhere("s.id = :userId")
	            ->setParameters($parameters)
	            ->getQuery()
	            ->getResult();
        }
        
        // Add the Vehicle field with the properly data
        $form->add('vehicle', EntityType::class, array(
            'required' => true,
            'placeholder' => 'Select a Price Package first',
            'class' => 'DrivingSchoolAdminBundle:VehicleEntity',
            'choices' => $vehicle
        ));



        // Instructor empty, unless there is a selected Package (Edit View)
        $instructor = array();
        
        // If there is a Package stored in the Planning entity, load the Training Session of it
        if ($pricePackage) {
            // Instructor of the Package if there's a selected Package

            $instructorRepository = $this->em->getRepository("DrivingSchoolAdminBundle:InstructorEntity");
            $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $this->userId);
            $instructor = $instructorRepository->createQueryBuilder("q")
                ->innerJoin('q.authorizedFor', 'p')
                ->innerJoin('p.student', 's')
                ->where("p.id = :pricePackageId")
                ->andWhere("s.id = :userId")
                ->setParameters($parameters)
                ->getQuery()
                ->getResult();
            
        }
        
        // Add the Instructor field with the properly data
        $form->add('instructor', EntityType::class, array(
            'required' => true,
            'placeholder' => 'Select a Price Package first',
            'class' => 'DrivingSchoolAdminBundle:InstructorEntity',
            'choices' => $instructor
        ));
    }

	function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Search for selected Training Session and convert it into an Entity
        $pricePackage = $this->em->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($data['pricePackage']);
        
        $this->addElements($form, $pricePackage);
    }

    function onPreSetData(FormEvent $event) {
        $planning = $event->getData();
        $form = $event->getForm();

        $pricePackage = $planning->getPricePackage() ? $planning->getPricePackage() : null;
        
        $this->addElements($form, $pricePackage);
    }

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => PlanningEntity::class,
		));
		$resolver->setRequired('userId');
		$resolver->setRequired('entity_manager');
	}
}