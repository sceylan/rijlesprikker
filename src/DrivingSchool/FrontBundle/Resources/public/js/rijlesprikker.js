jQuery(document).ready(function() {
	jQuery('.post').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated fadeIn', // Class to add to the elements when they are visible
		offset: 100    
	}); 
	var owl = jQuery('.owl-carousel');
	  owl.owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		autoplayTimeout: 1500,
		autoplaySpeed: 2000,
	})
	 var owl = jQuery('.testi_slider .owl-carousel');
	  owl.owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		nav: true,
		autoplayTimeout: 150,
		autoplaySpeed: 200,
	})
});  

jQuery(window).scroll(function() {
	var scroll = $(window).scrollTop();

	if (scroll >= 1) {
	jQuery("header").addClass("fix_head");
		} else {
	jQuery("header").removeClass("fix_head");
	}
	if($(this).scrollTop() == 0) {
		var imgPath = $('.hd_logo a img').attr('src');
		imgPath = imgPath.replace("Rijlesprikker-logo-v12.svg","Rijlesprikker-logo-v12-white.svg");
		$('.hd_logo a img')   
		.attr('src',imgPath);
	}
	if($(this).scrollTop() > 1) {
		var imgPath = $('.hd_logo a img').attr('src');
		imgPath = imgPath.replace("Rijlesprikker-logo-v12-white.svg","Rijlesprikker-logo-v12.svg");
		$('.hd_logo a img')
		.attr('src',imgPath);
	}
});	

jQuery('#menu').mmenu({
	extensions  : ["fx-menu-slide"],
	searchfield : false,
	counters    : false,
	offCanvas: {
				  position : 'right',
				}
});

var API = jQuery('#menu').data('mmenu'); 

jQuery('#nav-icon1').click(function() {   
  API.close();  
});  