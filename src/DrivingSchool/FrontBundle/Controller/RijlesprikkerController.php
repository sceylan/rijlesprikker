<?php

namespace DrivingSchool\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\SecurityContextInterface;

class RijlesprikkerController extends Controller
{
    public function homepageAction(Request $request)
    {
        return $this->render(
            'DrivingSchoolFrontBundle:Rijlesprikker:homepage.html.twig'
        );
    }
}
