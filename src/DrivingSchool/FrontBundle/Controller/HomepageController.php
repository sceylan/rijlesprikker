<?php

namespace DrivingSchool\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends Controller
{
    public function indexAction()
    {
        return $this->render('DrivingSchoolFrontBundle:Homepage:index.html.twig');
    }
}
