<?php

namespace DrivingSchool\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\SecurityContextInterface;

class CmsController extends Controller
{
    public function loadCmsPageAction(Request $request)
    {
        $path = str_replace("/", "", $request->getPathInfo());
        
        if(empty($path)) {
            $path = "homepage";
        }

        $entityManager = $this->getDoctrine()->getManager();

        // Client Reviews
        $cmsPage = $entityManager->getRepository('DrivingSchoolAdminBundle:CmsPagesEntity')->findOneBy(array('urlKey'=>$path));
        
        if(!$cmsPage) {
            throw $this->createNotFoundException();
        } else {
            if($path == "homepage") {
                $entityManager = $this->getDoctrine()->getManager();
                $clientReviews = $entityManager->getRepository('DrivingSchoolAdminBundle:ClientReviewEntity')->findAll();
                $Allreviews;
                foreach ($clientReviews as $key => $review) {
                    $mediaObj = $review->getImage();
                    if(!empty($mediaObj)) {
                        $mediaManager = $this->get('sonata.media.pool');
                        $provider = $mediaManager->getProvider($mediaObj->getProviderName());
                        $format = $provider->getFormatName($mediaObj, 'default_small');
                        $image_path = $provider->generatePublicUrl($mediaObj, $format);
                        $format_big = $provider->getFormatName($mediaObj, 'default_big');
                        $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
                    }
                    $review->imageurl = $big_image_path;
                    $Allreviews[] = $review; 
                }
                
                $template = $this->get('twig')->createTemplate($cmsPage->getContent());
                $cmsPageContent = $template->render(array('clientReviews'=> $Allreviews));
                $cmsPage->setContent($cmsPageContent);
            }
            $data = ['page' => $cmsPage];

            return $this->render(
                'DrivingSchoolFrontBundle:Cms:page.html.twig', $data
            );
        }
    }
}
