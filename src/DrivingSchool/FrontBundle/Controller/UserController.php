<?php

namespace DrivingSchool\FrontBundle\Controller;

use DrivingSchool\FrontBundle\Form\UserRegistrationForm;
use DrivingSchool\FrontBundle\Form\UserLoginForm;
use DrivingSchool\AdminBundle\Entity\StudentEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\SecurityContextInterface;

class UserController extends Controller
{
    public function loginAction(Request $request)
    {
        // build the form
        $user = new StudentEntity();
        $form = $this->createForm(UserLoginForm::class, $user);

        // handle the submit (will only happen on POST)
         $form->handleRequest($request);
         if ($form->isSubmitted() && $form->isValid()) {
            $session = $request->getSession();

            // get the login error if there is one
            if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
                $error = $request->attributes->get(
                    SecurityContextInterface::AUTHENTICATION_ERROR
                );
            } else {
                $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
                $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
            }
//     echo "sdfgtdrtgry<br>sdfgtdrtgry<br>sdfgtdrtgry<br>sdfgtdrtgry<br>sdfgtdrtgry";

// exit;

//            return $this->redirectToRoute('driving_school_front_login');
         }

        return $this->render(
            'DrivingSchoolFrontBundle:User:login.html.twig',
            array('form' => $form->createView())
        );
    }

    public function registerAction(Request $request)
    {
    	$passwordEncoder = $this->get('security.password_encoder');
        // 1) build the form
        $user = new StudentEntity();
        $form = $this->createForm(UserRegistrationForm::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('driving_school_front_register');
        }

        return $this->render(
            'DrivingSchoolFrontBundle:User:registration.html.twig',
            array('form' => $form->createView())
        );
    }
}
