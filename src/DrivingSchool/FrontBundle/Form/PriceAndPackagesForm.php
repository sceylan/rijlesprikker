<?php

namespace DrivingSchool\FrontBundle\Form;

use DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity;
use DrivingSchool\AdminBundle\Entity\PriceIconsEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\AdminBundle\Form\Type\ModelType;
// use Doctrine\ORM\EntityManagerInterface;

class PriceAndPackagesForm extends AbstractType
{
    // private $entityManager;

    // public function __construct(EntityManagerInterface $entityManager)
    // {
    //     $this->entityManager = $entityManager;
    // }

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            /*->add('icon', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\PriceIconsEntity',
                    'multiple' => false,
                    'expanded' => true,
                    'choice_label' => 'icon_path',
                    'label' => false,
                ])*/
            ->add('status')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PriceAndPackagesEntity::class,
        ));
    }
}
