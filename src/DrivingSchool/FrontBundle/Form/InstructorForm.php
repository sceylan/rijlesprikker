<?php

namespace DrivingSchool\FrontBundle\Form;

use DrivingSchool\AdminBundle\Entity\InstructorEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\AdminBundle\Form\Type\ModelType;

class InstructorForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('instructorName', TextType::class)
            ->add('codeCRB', TextType::class, array('label' => 'Code CBR'))
            // ->add('authorizedFor', ModelType::class, [
            //                 'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
            //                 'property' => 'name',
            //                 'multiple' => true,
            //                 'expanded' => true
            //             ]
            //         )
            ->add('publish_calendar', 'date')
            ->add('status')
            ->add('monday_start', 'time')
            ->add('monday_end', 'time')
            ->add('tuesday_start', 'time')
            ->add('tuesday_end', 'time')
            ->add('wednesday_start', 'time')
            ->add('wednesday_end', 'time')
            ->add('thursday_start', 'time')
            ->add('thursday_end', 'time')
            ->add('friday_start', 'time')
            ->add('friday_end', 'time')
            ->add('saturday_start', 'time')
            ->add('saturday_end', 'time')
            ->add('sunday_start', 'time')
            ->add('sunday_end', 'time')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => InstructorEntity::class,
        ));
    }
}
