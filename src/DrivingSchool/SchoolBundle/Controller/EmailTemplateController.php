<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\SchoolEmailTemplatesEntity;
use DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity;
use DrivingSchool\SchoolBundle\Form\EmailForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class EmailTemplateController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    { 
        $userId = $user->getId();
    
        $entityManager = $this->getDoctrine()->getManager();
        $emailtmps = $entityManager->getRepository('DrivingSchoolAdminBundle:SchoolEmailTemplatesEntity')->findAll();

        $data = [
        	'emailtmps' => $emailtmps,
        ];

        return $this->render(
            'DrivingSchoolSchoolBundle:EmailTemplates:list.html.twig', $data);
    }

    public function editAction(Request $request, $id, UserInterface $user)
    {
    	$userId = $user->getId();

    	$entityManager = $this->getDoctrine()->getManager();
    	$emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:SchoolEmailTemplatesEntity')->find($id);

    	$EmailContent = $entityManager->getRepository('DrivingSchoolAdminBundle:SchoolEmailContentEntity')->findOneBy(array('DrivingSchool' => $userId, 'template' => $id));

    	$form = $this->createForm(EmailForm::class, $EmailContent);

    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {
    		if($EmailContent != '')
    		{
    			$entityManager->persist($EmailContent);
            	$entityManager->flush();
    		} else {
    			$EmailContentadd = new SchoolEmailContentEntity();

    			$drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
    			$EmailContentadd->setDrivingSchool($drivingschool);

    			$templates = $entityManager->getRepository('DrivingSchoolAdminBundle:SchoolEmailTemplatesEntity')->find($id);
    			$EmailContentadd->setTemplate($templates);

           	    $data = $request->request->all()['email_form'];
           	    $EmailContentadd->setContent($data['content']);

    			$entityManager->persist($EmailContentadd);
            	$entityManager->flush();
    		}            
            $this->addFlash(
                'success',
                'Email template updated succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_email');
        }

    	return $this->render(
            'DrivingSchoolSchoolBundle:EmailTemplates:edit.html.twig',
            array('form' => $form->createView(),
            		'username' => $user->getusername(),
            		'userphone' => $user->getSchoolPhone(),
            		'subject' => $emailtmp->getSubject(),
            		'topcontent' => $emailtmp->getTopContent(),
            		'bottomcontent' => $emailtmp->getBottomContent(),
        	)
        );

    }
}