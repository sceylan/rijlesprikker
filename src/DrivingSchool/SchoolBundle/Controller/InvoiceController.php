<?php
namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\InvoiceEntity;
use DrivingSchool\AdminBundle\Entity\StudentFinancialEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Application\Sonata\MediaBundle\Entity\Media;

class InvoiceController extends Controller
{
	public function generateInvoiceAction(Request $request, UserInterface $user)
	{
		$userId = $user->getId();
		
		$data = $request->request->all();
		$studentid = $data['student_id'];
		$financialid = $data['generateinvoice'];

		$entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $fin_query = $connection->prepare("SELECT sf.id as financial_id, ts.name as cousesname, sf.price_package_id, sf.trainingsession_id, sf.quantity, sf.minutes, sf.price, sf.included_vat, sf.vat, sf.amount, sf.totalincluded_vat FROM student_financial AS sf LEFT JOIN training_sessions AS ts ON ts.id = sf.trainingsession_id WHERE sf.id IN (".implode(",", $financialid).")");
        $fin_query->execute();
        $financials = $fin_query->fetchAll();

        $vatcalc = 0;
        $pricetotal = 0;
        foreach ($financials as $financial) {
        	$included_vat = $financial['included_vat'];
        	$price = $financial['price'];
        	$qty = $financial['quantity'];
        	$vat = $financial['vat'];
        	if($included_vat == 0)
        	{
        		$vatcalc = $vatcalc + (($vat/100)*($price*$qty));
        	}
        	$pricetotal = $pricetotal + ($price*$qty);
        }
        $totalinvoice = $vatcalc+$pricetotal;	

		$jsonfinancial = json_encode($financials);

		// update generate invoice in financial
		$editfinancials   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:StudentFinancialEntity')->findBy(['id' => $financialid]);
		if(!empty($editfinancials))
		{
			foreach ($editfinancials as $editfinancial) {
				$editfinancial->setGeneratedInvoice(1);
				$entityManager->persist($editfinancial);
			}		
			$entityManager->flush();
		}

		$invoice = new InvoiceEntity();
		$invoice->setDrivingSchool($user);

		// Get student object
		$student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($studentid);
        $invoice->setStudentid($student); 

        // set invoice number
        $getinvoice   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:InvoiceEntity')->findOneBy(array('DrivingSchool' => $userId),array('id' => 'DESC'));

		if(empty($getinvoice))
		{
			$invoice->setInvoiceNumber(1);
		} else {
			$invoicenumber = $getinvoice->getInvoiceNumber();
			$countnumber = $invoicenumber+1;
			$invoice->setInvoiceNumber($countnumber);
		}

		$invoice->setInvoiceDetail($jsonfinancial);
		$invoice->setInvoiceTotal($totalinvoice);

		$entityManager->persist($invoice);
		$entityManager->flush();
		$invoicelastid = $invoice->getId();

		return $this->redirectToRoute('driving_school_school_student_invoicelist', array('id' => $invoicelastid));
	}

	public function invoicelistAction(Request $request, $id, UserInterface $user)
	{
		$userId = $user->getId();

		$invoices = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:InvoiceEntity')->find($id);

		// get school detail
		if(!empty($invoices)) {
			$schoolName = $invoices->getDrivingSchool()->getSchoolName();
			$schoolPhone = $invoices->getDrivingSchool()->getSchoolPhone();
			$schoolEmail = $invoices->getDrivingSchool()->getEmail();
			$schoolPaymentTerm = $invoices->getDrivingSchool()->getPaymentTerm();
			$schoolmedia = $invoices->getDrivingSchool()->getLogo();
	        if(!empty($schoolmedia)) {
	            $mediaManager = $this->get('sonata.media.pool');
	            $provider = $mediaManager->getProvider($schoolmedia->getProviderName());
	            $format_big = $provider->getFormatName($schoolmedia, 'default_big');
	            $big_school_img = $provider->generatePublicUrl($schoolmedia, $format_big);
	        }

	        // get student detail
	        $studentid = $invoices->getstudentid()->getId();
	        $studentfname = $invoices->getstudentid()->getFirstName();
	        $studentlname = $invoices->getstudentid()->getLastName();
	        $studentpostalcode = $invoices->getstudentid()->getPostalCode();
	        $studentmedia = $invoices->getstudentid()->getStudentImage();
	        if(!empty($studentmedia)) {
	            $mediaManager = $this->get('sonata.media.pool');
	            $provider = $mediaManager->getProvider($studentmedia->getProviderName());
	            $format_big = $provider->getFormatName($studentmedia, 'default_big');
	            $big_student_img = $provider->generatePublicUrl($studentmedia, $format_big);
	        }

	        // get invoice detail
	        $createdate = $invoices->getCreated();
	        $invoiceId = $invoices->getId();
	        $invoiceNumber = $invoices->getInvoiceNumber();
	        $invoicedetail = $invoices->getInvoiceDetail();
	        $invoicecontent = $invoices->getInvoiceContent();
	        $jsoninvoicedetails = json_decode($invoicedetail);

			$data = [
				'school_name' => $schoolName,
				'school_email' => $schoolEmail,
				'school_phone' => $schoolPhone,
				'school_image' => $big_school_img,
				'school_payment_term' => $schoolPaymentTerm,
				'studentfirstname' => $studentfname,
				'studentlastname' => $studentlname,
				'studentpostalcode' => $studentpostalcode,
				'studentImage' => $big_student_img,
				'createdate' => $createdate,
				'invoiceId' => $invoiceId,
				'invoiceNumber' => $invoiceNumber,
				'invoiceDetails' => $jsoninvoicedetails,
				'invoicecontent' => $invoicecontent,
				'studentid' => $studentid,
			];
		} else {
			$data = [
				'school_name' => '',
				'school_email' => '',
				'school_phone' => '',
				'school_image' => '',
				'school_payment_term' => '',
				'studentfirstname' => '',
				'studentlastname' => '',
				'studentpostalcode' => '',
				'studentImage' => '',
				'createdate' => '',
				'invoiceId' => '',
				'invoiceNumber' => '',
				'invoiceDetails' => '',
				'invoicecontent' => '',
				'studentid' => '',
			];
		}
		return $this->render('DrivingSchoolSchoolBundle:Invoice:invoice.html.twig', $data);
	}

	public function editinvoicecontentAction(Request $request, UserInterface $user)
	{
		$data = $request->request->all();
		$invoiceid = $data['invoiceid'];
		$content = $data['content'];

		$invoices = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:InvoiceEntity')->find($invoiceid);
		$invoices->setInvoiceContent($content);

		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->persist($invoices);
		$entityManager->flush();
		$html = "<span id='responsecontent'>".$content."</span>";
        return new Response(json_encode(array("response" => $html)));
	}
}