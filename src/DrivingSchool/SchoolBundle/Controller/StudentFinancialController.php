<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\StudentForm;
use DrivingSchool\SchoolBundle\Form\StudentProfileForm;
use DrivingSchool\SchoolBundle\Form\StudentNotesForm;
use DrivingSchool\AdminBundle\Entity\StudentEntity;
use DrivingSchool\AdminBundle\Entity\StudentFinancialEntity;
use DrivingSchool\AdminBundle\Entity\StudentNotesEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Application\Sonata\MediaBundle\Entity\Media;

class StudentFinancialController extends Controller
{
    public function indexAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
        {
            $invoicequery = $connection->prepare("SELECT invoice.*,student.first_name FROM invoice LEFT JOIN student ON student.id = invoice.student_id WHERE invoice.drivingschool_id = ".$userId." ORDER BY id DESC");
            $invoicequery->execute();
            $invoices = $invoicequery->fetchAll();

            $total = 0;
            foreach ($invoices as $value) {
                $total = $total+$value['invoice_total'];
            }
        } else if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) 
        {
            $invoicequery = $connection->prepare("SELECT invoice.*,student.first_name FROM invoice LEFT JOIN student ON student.id = invoice.student_id WHERE student.instructor_id = ".$userId." ORDER BY id DESC");
            $invoicequery->execute();
            $invoices = $invoicequery->fetchAll();

            $total = 0;
            foreach ($invoices as $value) {
                $total = $total+$value['invoice_total'];
            }
        }

        $data = [
            'invoices' => $invoices,
            'invoice_total' => $total,
        ];

        return $this->render('DrivingSchoolSchoolBundle:StudentFinancial:index.html.twig', $data);
    }
}
