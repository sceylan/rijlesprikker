<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use DrivingSchool\SchoolBundle\Form\FinancialForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class FinancialController extends Controller
{
    public function financialAction(Request $request, UserInterface $user)
    { 
        $userId = $user->getId();
    
        $entityManager = $this->getDoctrine()->getManager();
        $DrivingSchool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
        $form = $this->createForm(FinancialForm::class, $DrivingSchool);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           
            // save the priceandpackages
            $entityManager->persist($DrivingSchool);
            $entityManager->flush();

            $this->addFlash('success','Financial detail updated successfully');

            return $this->redirectToRoute('driving_school_school_financial');
        }

        return $this->render(
            'DrivingSchoolSchoolBundle:DrivingSchool:financialform.html.twig',
            array('form' => $form->createView(),
                  'id'   => $DrivingSchool->getId()
                )
        );
    }

    public function getImagePreviewAction()
    {
        $request = Request::createFromGlobals();
        $resp = [];
        foreach ($request->files as $uploadedFile) {
            $fileData = file_get_contents($uploadedFile->openFile()->getPathName());
        }

        $enocoded_data = base64_encode($fileData);
        $resp['filename'] = 'data:image/png;base64, '.$enocoded_data;

        return new Response(json_encode($resp));
    }
}