<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\RolesAndPermissionEntity;
use DrivingSchool\AdminBundle\Entity\RolesModuleEntity;
use DrivingSchool\SchoolBundle\Form\RolesAndPermissionForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class RolesAndPermissionController extends Controller
{
    public function permissionformAction(Request $request, UserInterface $user, $id)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $rolemodules = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesModuleEntity')->findAll();

        $data = [];
        $permissions = [];
        foreach ($rolemodules as $module) {
        	$moduleid = $module->getId();
        	$rolepermission = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesAndPermissionEntity')->findOneBy(['DrivingSchool' => $userId, 'Instructor' => $id, 'rolesModule' => $moduleid]);
        	if(!empty($rolepermission)) {
	        	$data['id'] = $rolepermission->getId();
	            $data['DrivingSchool'] = $rolepermission->getDrivingSchool()->getId();
	            $data['Instructor'] = $rolepermission->getInstructor()->getId();
	            $data['modulename'] = $rolepermission->getRolesModule()->getModuleName();
	            $data['moduleid'] = $module->getId();
	            $rolesModule = $rolepermission->getRolesModule()->getId();
	            $data['listrole'] = $rolepermission->getListrole();
	            $data['addrole'] = $rolepermission->getAddrole();
	            $data['updaterole'] = $rolepermission->getUpdaterole();
	            $data['deleterole'] = $rolepermission->getDeleterole();
	            $data['acceptrole'] = $rolepermission->getAcceptrole();
	            $data['rejectrole'] = $rolepermission->getRejectrole();
	            $permissions[$rolesModule] = $data;
        	} else {
        		$data['id'] = "";
	            $data['DrivingSchool'] = $userId;
	            $data['Instructor'] = $id;
	            $data['modulename'] = $module->getModuleName();
	            $data['moduleid'] = $module->getId();
	            $rolesModule = $module->getId();
	            $data['listrole'] = 0;
	            $data['addrole'] = 0;
	            $data['updaterole'] = 0;
	            $data['deleterole'] = 0;
	            $data['acceptrole'] = 0;
	            $data['rejectrole'] = 0;
	            $permissions[$rolesModule] = $data;
        	}
        } 

        $formdata = [
            'rolemodules' => $rolemodules,
            'permissions' => $permissions,
            'id' => $id
        ];

        $reqdata = $request->request->all();
        if(isset($reqdata['permissionsubmit']) && !empty($reqdata['permissionsubmit']))
        {
        	foreach ($reqdata as $key => $value) {
        		if($key != "permissionsubmit") {
	        		$rolepermission = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesAndPermissionEntity')->findOneBy(['DrivingSchool' => $userId, 'Instructor' => $id, 'rolesModule' => $value['moduleid']]);
	        		if(!empty($rolepermission))
	        		{
	        			$rolepermission->setDrivingSchool($user);

						$instructor = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->find($id);
						$rolepermission->setInstructor($instructor);
						
						$module = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesModuleEntity')->find($value['moduleid']);

						$list = (int) (isset($value['listrole'])? $value['listrole']:0);
						$add = (int) (isset($value['addrole'])? $value['addrole']:0);
						$edit = (int) (isset($value['updaterole'])? $value['updaterole']:0);
						$delete = (int) (isset($value['deleterole'])? $value['deleterole']:0);
						$accept = (int) (isset($value['acceptrole'])? $value['acceptrole']:0);
						$reject = (int) (isset($value['rejectrole'])? $value['rejectrole']:0);

						$rolepermission->setRolesModule($module);
						$rolepermission->setListrole($list);
						$rolepermission->setAddrole($add);
						$rolepermission->setUpdaterole($edit);
						$rolepermission->setDeleterole($delete);
						$rolepermission->setAcceptrole($accept);
						$rolepermission->setRejectrole($reject);

						$entityManager->persist($rolepermission);
		                $entityManager->flush();
	        		}
	        		else 
	        		{
		    			$permission = new RolesAndPermissionEntity();

						$permission->setDrivingSchool($user);

						$instructor = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->find($id);
						$permission->setInstructor($instructor);
						
						$module = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesModuleEntity')->find($value['moduleid']);

						$list = (int) (isset($value['listrole'])? $value['listrole']:0);
						$add = (int) (isset($value['addrole'])? $value['addrole']:0);
						$edit = (int) (isset($value['updaterole'])? $value['updaterole']:0);
						$delete = (int) (isset($value['deleterole'])? $value['deleterole']:0);
						$accept = (int) (isset($value['acceptrole'])? $value['acceptrole']:0);
						$reject = (int) (isset($value['rejectrole'])? $value['rejectrole']:0);

						$permission->setRolesModule($module);
						$permission->setListrole($list);
						$permission->setAddrole($add);
						$permission->setUpdaterole($edit);
						$permission->setDeleterole($delete);
						$permission->setAcceptrole($accept);
						$permission->setRejectrole($reject);

						$entityManager->persist($permission);
		                $entityManager->flush();
		            }
	            }
        	}
        	return $this->redirectToRoute('driving_school_school_list_instructor');
        }
        return $this->render('DrivingSchoolSchoolBundle:Instructor:permission.html.twig', $formdata);
    }
}