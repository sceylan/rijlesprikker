<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\StudentForm;
use DrivingSchool\SchoolBundle\Form\StudentProfileForm;
use DrivingSchool\SchoolBundle\Form\StudentNotesForm;
use DrivingSchool\AdminBundle\Entity\StudentEntity;
use DrivingSchool\AdminBundle\Entity\StudentFinancialEntity;
use DrivingSchool\AdminBundle\Entity\StudentNotesEntity;
use DrivingSchool\AdminBundle\Entity\EmailTemplatesEntity;
use DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Application\Sonata\MediaBundle\Entity\Media;

class StudentController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
    	$em = $this->getDoctrine()->getManager();
    	$students = $em->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findBy(array('DrivingSchool' => $userId),array('id' => 'DESC'));

    	$data = [
            'students' => $students,
    	];
        return $this->render('DrivingSchoolSchoolBundle:Student:list.html.twig', $data);
    }

    private function generateRandomString() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomPassword = '';
            
        for ($i = 0; $i < 5; $i++)
            $randomPassword .= $characters[mt_rand(0, 61)]; // Send this password in email

        return $randomPassword;
    }

    public function createAction(Request $request, UserInterface $user)
    {
        $student = new StudentEntity();

        $form = $this->createForm(StudentForm::class, $student);

        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('DrivingSchool' => $user),array('id' => 'DESC'));

        $form->handleRequest($request);

        if($form->isSubmitted()) {
            if($form->isValid()) {
                $userId = $user->getId();

                $student = new StudentEntity();

                $data = $request->request->all()['student_form'];
                
                $firstname = $data['firstName'];
                $student->setFirstName($firstname);

                $lastname = $data['lastName'];
                $student->setLastName($lastname);

                $username = $firstname.$lastname;
                $student->setUserName($username.$students->getId());

                $entityManager = $this->getDoctrine()->getManager();
                $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
                $student->setDrivingSchool($drivingschool);

                $realPassword = $this->generateRandomString();
                $salt = $this->generateRandomString();
                $encoder = new MessageDigestPasswordEncoder();
                $password = $encoder->encodePassword($realPassword, $salt);
                $student->setPassword($password);

                $email = $this->generateRandomString()."@gmail.com";
                $student->setEmail($email);

                $entityManager->persist($student);
                $entityManager->flush();
                $studentid = $student->getId();
                
                /*$this->addFlash(
                    'success',
                    'Registration is completed'
                );*/
                // return $this->redirectToRoute('driving_school_school_students_form', array('id'=> $studentid));
                return new Response(
                    json_encode(array(
                            'redirect' => 1, 
                            'url' => $this->generateUrl('driving_school_school_students_form', array('id'=> $studentid),true)
                        )
                    )
                );
            } 

            $resp =  $this->render(
                'DrivingSchoolSchoolBundle:Student:form_create.html.twig',
                array('form' => $form->createView())
            )->getContent();

            return new Response(json_encode(array('response' => $resp)));
        } else {
            return $this->render(
                'DrivingSchoolSchoolBundle:Student:form_create.html.twig',
                array('form' => $form->createView())
            );
        }
    }

    public function formAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        // $students = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($id);
        $students = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));
        if(empty($students))
        {
            return $this->redirectToRoute('driving_school_school_students_list');
        }

        $icons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->findAll();

        if($students->getPostalCode() == '' || $students->getIcon() == '')
        {
            return $this->render('DrivingSchoolSchoolBundle:Student:form.html.twig', array('firstname' => $students->getFirstName(), 'icons' => $icons, 'id' => $id));
        } else 
        {
            return $this->redirectToRoute('driving_school_school_student_card', array('id'=> $id));
        }
    }

    public function updateAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($id);

        $data = $request->request->all();
        $icon_id = $data['icon_id'];
        $iconId = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->find($icon_id);
        $student->setIcon($iconId);

        $postal_code = $data['postal_code'];
        $student->setPostalCode($postal_code);

        $house_number = $data['house_number'];
        $student->setHouseNumber($house_number);

        $email = $data['email'];
        $student->setEmail($email);

        $phone = $data['phone'];
        $student->setPhone($phone);

        $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
        $student->setDrivingSchool($drivingschool);

        $entityManager->persist($student);
        $entityManager->flush();

        return $this->redirectToRoute('driving_school_school_student_card', array('id'=> $id));
    }

    public function activateAction(Request $request, $id = null, UserInterface $user)
    {
        $userId = $user->getId();
        if(!empty($id)) {
            $entityManager = $this->getDoctrine()->getManager();
            $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));
            if(empty($student))
            {
                return $this->redirectToRoute('driving_school_school_student_card', array('id'=> $id));
            }
        }

        $data = $request->request->all();
        if(isset($data['submit'])) {
            $username = $data['username'];
            $firstname = $student->getFirstName();
            // enable account
            $student->setEnabled('1');
            $student->setUserName($username);
            $student->setEmail($data['email']);

            // generate password
            $realPassword = $this->generateRandomString();
            $salt = $this->generateRandomString();
            $encoder = new MessageDigestPasswordEncoder();
            $password = $encoder->encodePassword($realPassword, $salt);
            $student->setPassword($password);
            $student->setSalt($salt);

            $entityManager->persist($student);
            $entityManager->flush();

            /* Start Send Mail*/
            $mailer = $this->get('mailer');
            
            $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '3'));
            $mailsubject = $emailtmp->getSubject();
            $content = $emailtmp->getContent();

            $template = $this->get('twig')->createTemplate($content);
            $mailcontent = $template->render(array('name'=>$student->getFirstName(), 'username' => $username, 'password' => $realPassword, 'schoolname' => $student->getDrivingSchool()->getSchoolName(), 'schooladdress' => $student->getDrivingSchool()->getSchoolAddress(), 'schoolphone' => $student->getDrivingSchool()->getSchoolPhone()));

            $message = (new \Swift_Message($mailsubject))
            ->setFrom('admin@drivingschool.com')
            ->setTo($data['email'])
            ->setBody($mailcontent,'text/html');

            $mailer->send($message);
            /* Enad Send Mail*/

            $this->addFlash(
                'notice',
                'Hello '.$username.'Username: '.$username. 'password: '.$realPassword
            );
            return $this->redirectToRoute('driving_school_school_student_card', array('id'=> $id));
        }

        return $this->render('DrivingSchoolSchoolBundle:Student:activateform.html.twig', 
            array(
                'id' => $id,
                'firstname' => $student->getFirstName(), 
                'lastname' => $student->getLastName(), 
                'username' => $student->getUsername(), 
                'email' => $student->getEmail(), 
                'enabled' => $student->getEnabled(),
            )
        );
    }

    public function deactivateAction(Request $request, $id = null, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));
        if(empty($student))
        {
            return $this->redirectToRoute('driving_school_school_student_card', array('id'=> $id));
        }

        $data = $request->request->all();
        $username = $data['username'];
        $firstname = $student->getFirstName();
        // enable account
        $student->setEnabled('0');

       /* $mailer = $this->get('mailer');
        $message = (new \Swift_Message('Student Deactivate Account'))
        ->setFrom('admin@drivingschool.com')
        ->setTo($data['email'])
        ->setBody('<p>Hello '.$username.'<p>Username: <b>'.$username. '</b></p><br/><p>Your account is deactivated from driving school</p>','text/html');
        $mailer->send($message);*/

        $entityManager->persist($student);
        $entityManager->flush();

        return $this->redirectToRoute('driving_school_school_student_card', array('id'=> $id));
    }

    public function cardAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $username = $user->getUserName();
        $useremail = $user->getEmail();

        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));
        $studentmedia = $student->getStudentImage();
        if(!empty($studentmedia)) {
            $mediaManager = $this->get('sonata.media.pool');
            $provider = $mediaManager->getProvider($studentmedia->getProviderName());
            $format_big = $provider->getFormatName($studentmedia, 'default_big');
            $imagepath = $provider->generatePublicUrl($studentmedia, $format_big);
        }

        if(empty($student))
        {
            return $this->redirectToRoute('driving_school_school_students_list');
        }

        $papquery = $connection->prepare("SELECT pap.id as priceid, pap.name as pricename, pi.icon_path, ts.id as trainingid, ts.training_type as trainingtype, ts.name as trainingname FROM pricepackage_student as pkgstu LEFT JOIN price_and_packages as pap ON pkgstu.priceandpackages_id = pap.id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id LEFT JOIN training_sessions as ts ON ts.price_package_id = pap.id WHERE pap.drivingschool_id = ".$userId." AND pkgstu.student_id = ".$id." ORDER BY pricename");
        $papquery->execute();
        $packages = $papquery->fetchAll();

        $newPackage = array();
        foreach ($packages as $package) {
                $newPackage[$package['pricename']]['icon_path']=$package['icon_path'];
                $newPackage[$package['pricename']]['priceid']=$package['priceid'];
                unset($package['icon_path']);
                $newPackage[$package['pricename']][] = $package;
        }

        $financialpackquery = $connection->prepare("SELECT sfh.id as historyid, ts.id as trainingid, ts.name as trainingname, sfh.id as financialhistoryid, sfh.quantity, sfh.minutes, sfh.course_type, sfh.student_id, pi.icon_path FROM student_financial_history AS sfh LEFT JOIN training_sessions as ts ON sfh.trainingsession_id = ts.id LEFT JOIN price_and_packages as pap ON sfh.price_package_id = pap.id LEFT JOIN price_icons as pi ON pap.icon_id = pi.id WHERE sfh.student_id = ".$id." AND sfh.drivingschool_id = ".$userId." GROUP BY trainingid");

        $financialpackquery->execute();
        $financialpackages = $financialpackquery->fetchAll();

        $historydata == [];
        foreach ($financialpackages as $key => $value) {

            /* start Quantity calc */
            $increaseqty = $connection->prepare("SELECT SUM(quantity) as calcqty FROM student_financial_history WHERE trainingsession_id = ".$value['trainingid']." AND increase_decrease = 1");
            $increaseqty->execute();
            $increaseqty = $increaseqty->fetchAll();
            $increaseqtyvalue = $increaseqty[0]['calcqty'];

            $decreaseqty = $connection->prepare("SELECT SUM(quantity) as calcqty FROM student_financial_history WHERE trainingsession_id = ".$value['trainingid']." AND increase_decrease = 0");
            $decreaseqty->execute();
            $decreaseqty = $decreaseqty->fetchAll();
            $decreaseqtyvalue = $decreaseqty[0]['calcqty'];

            $calcqty = $increaseqtyvalue - $decreaseqtyvalue;
            /* end Quantity calc */

            /* start minute calc */
            $increaseminute = $connection->prepare("SELECT SUM(minutes) as calcminute FROM student_financial_history WHERE trainingsession_id = ".$value['trainingid']." AND increase_decrease = 1");
            $increaseminute->execute();
            $increaseminute = $increaseminute->fetchAll();
            
            $incrmin = $increaseminute[0]['calcminute'];
            $increaseminutevalue = intdiv($incrmin, 60).':'. ($incrmin % 60);

            $decreaseminute = $connection->prepare("SELECT SUM(minutes) as calcminute FROM student_financial_history WHERE trainingsession_id = ".$value['trainingid']." AND increase_decrease = 0");
            $decreaseminute->execute();
            $decreaseminute = $decreaseminute->fetchAll();

            $decrmin = $decreaseminute[0]['calcminute'];
            $decreaseminutevalue = intdiv($decrmin, 60).':'. ($decrmin % 60);

            $array1 = explode(':', $increaseminutevalue);
            $array2 = explode(':', $decreaseminutevalue);

            $minutes1 = ($array1[0] * 60.0 + $array1[1]);
            $minutes2 = ($array2[0] * 60.0 + $array2[1]);

            $calcminute = $minutes1 - $minutes2;
            /* End minute calc */

            /* Progressbar calculation */
            $pro_minute = $Progressbar[0]['calcminute'];
            $calcprogressminute = 100 - (($decrmin/$incrmin)*100);
            $roundprogmin = round($calcprogressminute, 0);

            $value['calcqty'] = $calcqty;
            $value['calcminute'] = $calcminute;
            $value['calcprogressminute'] = $roundprogmin;
            $historydata[] = $value;
        }

        return $this->render('DrivingSchoolSchoolBundle:Student:card.html.twig', 
            array(
                'student' => $student,
                'username' => $username,
                'useremail' => $useremail,
                'priceandpackages' => $newPackage,
                'financialpackages' => $historydata,
                'imagepath' => $imagepath,
            )
        );
    }

    public function studentprofileAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));
        
        if(!empty($student))
        {
            // dump($data);
            $form = $this->createForm(StudentProfileForm::class, $student, array('userId' => $userId));

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $request->request->all();
                // print_r($data);exit;
                $entityManager->persist($student);
                $entityManager->flush();
                return $this->redirectToRoute('driving_school_school_student_card', array('id' => $id));
            }

            return $this->render('DrivingSchoolSchoolBundle:Student:student_profile.html.twig', 
                array(
                    'form' => $form->createView(),
                    'student' => $student,
                    'username' => $username,
                    'useremail' => $useremail,
                    'studentid' => $id
                )
            );
        } else {
            return $this->redirectToRoute('driving_school_school_student_card', array('id' => $id));
        }
    }

    public function sendmessageAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();

        $data = $request->request->all();
        $id = $data['id'];
        if(isset($data['sendmessage']))
        {
            $to_email = $data['to_email'];
            $fromemail = $user->getEmail();
            $topic = $data['topic'];
            $content = $data['editor1'];
            
            $mailer = $this->get('mailer');
                
            $message = (new \Swift_Message('Welcome to Driving School'))
            ->setFrom($fromemail)
            ->setTo($to_email)
            ->setBody($content);

            $mailer->send($message);
        }
        return $this->redirectToRoute('driving_school_school_student_card', array('id' => $id));
    }

    public function studentfinancialAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        $query = $connection->prepare("SELECT sf.*, ts.name as cousesname FROM student_financial AS sf LEFT JOIN training_sessions AS ts ON ts.id = sf.trainingsession_id WHERE (sf.generated_invoice != 1 OR sf.generated_invoice IS NULL) AND sf.drivingschool_id = ".$userId." AND sf.student_id = ".$id. " AND ts.training_type != 3" );

        $query->execute();
        $financials = $query->fetchAll();

        $invoices = $entityManager->getRepository('DrivingSchoolAdminBundle:InvoiceEntity')->findBy(array('DrivingSchool' => $userId, 'studentid' => $id));

        $data = [
            'financials' => $financials,
            'invoices' => $invoices,
        ];
       // dump($data);exit;
        return $this->render('DrivingSchoolSchoolBundle:Student:student_financial.html.twig', $data);
    }

    public function packageorcreaditAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $financial = new StudentFinancialEntity();

        $data = $request->request->all();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        // get drivingschool id
        $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
        $financial->setDrivingSchool($drivingschool); 

        // get student id
        $studentid = $data['studentid'];
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($studentid);
        $financial->setStudentid($student); 

        // get price and packages id
        $priseandpackages = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($data['priceandpackageid']);
        $financial->setPricePackageId($priseandpackages); 

        // get courses id
        $courses = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->find($data['courses']);
        $financial->setCourses($courses); 
        $type = $courses->getType();
        $coursename = $courses->getName();

        // set quantity 
        $quantity = $data['qty'];
        $financial->setQty($quantity);

        // set minute
        $minute = $data['minute'];
        $financial->setMinutes($minute);

        // set CalcMinuteQty
        $calcMinuteQty = $quantity*$minute;
        $financial->setCalcMinuteQty($calcMinuteQty);

        // set description
        if($minute != '')
        {
            $desc = $minute.' min'; 
            $financial->setDesc($desc);
        }
        // set price
        $price = $data['price'];
        $financial->setPrice($price);

        // set amount
        $amount = $data['amount'];
        $financial->setAmount($amount);

        // set vat
        $vat = $data['vat'];    
        $financial->setVat($vat);

        // set included vat
        $includedvat = $data['includedvat'];
        $financial->setIncludedVat($includedvat);

        // set total calculate val
        $totalvat = $quantity * $amount;
        $financial->setTotalincludedVat($totalvat);

        $entityManager->persist($financial);

        if($type == '0' || $type == '1')
        {
            $fhistory = new StudentFinancialHistoryEntity();

            $fhistory->setDrivingSchool($drivingschool);
            $fhistory->setStudentid($student);
            $fhistory->setPricePackageId($priseandpackages);
            $fhistory->setCourses($courses);
            $fhistory->setCourseType($type);
            $fhistory->setQty($quantity);
            $fhistory->setMinutes($minute);
            if($type == '0') {
                $fhistory->setDesc($quantity.' x '.$minute.' min '.$coursename.' added');
            } else {
                $fhistory->setDesc($quantity.' x '.$coursename.' added');
            }
            $fhistory->setIncreaseDecrease(1);
            $entityManager->persist($fhistory);
        }
        $entityManager->flush();

        $resp = $this->studentfinancialAction($request, $studentid, $user)->getContent();

        return new Response(json_encode(array("response" => $resp)));
    }

    public function financialeditAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();

        $data = $request->request->all();
        $id = $data['id'];
        $studentid = $data['studentid'];

        $entityManager = $this->getDoctrine()->getManager();
        
        $connection = $entityManager->getConnection();

        $query = $connection->prepare("SELECT sf.*, ts.name as cousesname FROM student_financial AS sf LEFT JOIN training_sessions AS ts ON ts.id = sf.trainingsession_id WHERE (sf.generated_invoice != 1 OR sf.generated_invoice IS NULL) AND sf.drivingschool_id = ".$userId." AND sf.student_id = ".$studentid." AND sf.id = ".$id );

        $query->execute();
        $financials = $query->fetchAll();

        if($financials[0]['included_vat'] == 1 ) 
        {
            $checked = 'checked';
        } else {
            $checked = '';
        }

        if($financials[0]['vat'] == '21')
        {
            $selected21 = 'selected';
        } else if($financials[0]['vat'] == '19') {
            $selected19 = 'selected';
        } else if($financials[0]['vat'] == '6') {
            $selected6 = 'selected';
        } else if($financials[0]['vat'] == '0') {
            $selected0 = 'selected';
        } else {
            $selected21 = '';
            $selected19 = '';
            $selected6 = '';
            $selected0 = '';
        }

        $html = "";
        $html.= "<div class=''>";
            $html.= "<form id='financial-submit' method='post' action=''>";
                $html.= "<input type='hidden' name='id' value='".$financials[0]['id']."' id='id'>";
                $html.= "<input type='hidden' name='student_id' value='".$studentid."' id='studentid'>";
                $html.= "<div class='finance_name'>
                            <label>Name</label>
                            <input type='text' name='name' value='".$financials[0]['cousesname']."' id='coursename' disabled>
                        </div>";
                $html.= "<div class='finance_desc'>
                            <label>Description</label>
                            <input type='text' name='description' value='".$financials[0]['description']."' id='desc'>
                        </div>";
                $html.= "<div class='finance_num'>
                            <label>Number</label>
                            <input type='text' name='qty' value='".$financials[0]['quantity']."' id='qty' onkeypress='return isNumberKey(event)' onpaste='return false;'> 
                            <span class='financialcross'>×</span> 
                            <input type='text' name='minutes' value='".$financials[0]['minutes']."' id='minutes' onkeypress='return isNumberKey(event)' onpaste='return false;'><span>Min</span>
                        </div>";
                $html.= "<div class='finance_price'>
                            <label>Price per piece</label>
                            <input type='text' name='price' value='".$financials[0]['price']."' id='price' onkeypress='return isNumberKey(event)' onpaste='return false;'>
                            <div class='finance_chck'><input type='checkbox' name='included_vat'  ".$checked." id='included_vat'><label>incl. VAT</label></div>
                            <select name='vat' id='vat'>
                                <option value='21' ".$selected21.">21% VAT</option>
                                <option value='19' ".$selected19.">19% VAT</option>
                                <option value='6' ".$selected6.">6% VAT</option>
                                <option value='0' ".$selected0.">No Tax</option>
                            </select>
                        </div>";    
                $html.= "<div class='form-row submit-row'>
                            <a href='javascript:void(0);' id='financialcancel'>Cancel</a>
                            <input type='submit' name='submit' id='financialsubmit' value='Update'>
                        </div>";
            $html.= "</form>";
        $html.= "</div>";
        return new Response(json_encode(array("response" => $html)));
    }

    public function financialeditdataAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();

        $data = $request->request->all();
        // print_r($data);exit;
        $id = $data['id'];
        $studentid = $data['studentid'];

        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $financial = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentFinancialEntity')->find($id);

        // set description 
        $desc = $data['desc'];
        $financial->setDesc($desc);

        // set quantity 
        $quantity = $data['qty'];
        $financial->setQty($quantity);

        // set minute
        $minute = $data['minutes'];
        $financial->setMinutes($minute);

        // set CalcMinuteQty
        $calcMinuteQty = $quantity*$minute;
        $financial->setCalcMinuteQty($calcMinuteQty);

        // set price
        $price = $data['price'];
        $financial->setPrice($price);

        // set included vat
        $includedvat = $data['included_vat'];
        $financial->setIncludedVat($includedvat);

        // set vat
        $vat = $data['vat'];    
        $financial->setVat($vat);

        // calculation on vat & set amount
        if($includedvat == '1' )
        {
            $amount = $data['price']; 
        } else if($includedvat == '0' ) {
            $amount = $price+(($vat / 100) * $price);
        }
        $financial->setAmount($amount);

        // set total calculate val
        $totalvat = $quantity * $amount;
        $financial->setTotalincludedVat($totalvat);

        $entityManager->persist($financial);
        $entityManager->flush();    

        $resp = $this->studentfinancialAction($request, $studentid, $user)->getContent();

        return new Response(json_encode(array('response' => $resp)));
    }

    public function financialcancelAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $data = $request->request->all();
        $studentid = $data['studentid'];     
        $resp = $this->studentfinancialAction($request, $studentid, $user)->getContent();

        return new Response(json_encode(array('response' => $resp)));
    }

    public function notesAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $notes = new StudentNotesEntity();
        $form = $this->createForm(StudentNotesForm::class, $notes);

        $entityManager = $this->getDoctrine()->getManager();
        $studentnotes = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentNotesEntity')->findBy(array('DrivingSchool' => $userId, 'studentid' => $id), array('id' => 'DESC'));

        $data = array();
        $newnotes = array('notes');
        foreach ($studentnotes as $note) {
                $media = $note->getNotesImage();
                $image_path = "";
                    if(!empty($media)) {
                        $mediaManager = $this->get('sonata.media.pool');
                        $provider = $mediaManager->getProvider($media->getProviderName());
                        $format = $provider->getFormatName($media, 'default_small');
                        $image_path = $provider->generatePublicUrl($media, $format);
                        $format_big = $provider->getFormatName($media, 'default_big');
                        $big_image_path = $provider->generatePublicUrl($media, $format_big);
                    }
                $data['desc'] = $note->getDesc();
                $data['id'] = $note->getId();
                $data['image_path'] = $image_path;
                $data['big_image_path'] = $big_image_path;
                $data['created'] = $note->getCreated();

                // start timestamp
                    $date = date('Y-m-d', $note->getCreated()->getTimestamp());
                    $time = date('H:i:s', $note->getCreated()->getTimestamp());
                    $newdate=date_create($date." ".$time);
                    $now = date_create(date("Y-m-d H:i:s"));
                    $diff = date_diff($now,$newdate);
                    $html = "";
                    $class = "";
                    if($date == date("Y-m-d"))
                    {
                        $hours = (float) $diff->format("%R%h");
                        if($hours > 0) {
                            $html.="After ".abs($hours)." Hours";
                            $class = "today";
                        } elseif($hours < 0) {
                            $html.= abs($hours)." Hours Ago";
                            $class = "today";
                        } else {
                            $minutes = (float) $diff->format("%R%i");
                            if($minutes > 0) {
                                $html.="After ".abs($minutes)." Minute(s)";
                                $class = "today";
                            } elseif($minutes < 0) {
                                $html.= abs($minutes)." Minutes Ago";
                                $class = "today";
                            } else {
                                $seconds = (float) $diff->format("%R%s");
                                if($seconds > 0) {
                                    $html.="After ".abs($seconds)." Second(s)";
                                    $class = "today";
                                } elseif($seconds < 0) {
                                    $html.= abs($seconds)." Seconds Ago";
                                    $class = "today";
                                }
                            }
                        }
                    } else if($date == date('Y-m-d',strtotime("-1 days")))
                    {
                        $html.= "Yesterday ".date('H:i', $note->getCreated()->getTimestamp());
                        $class = "yesterday";
                    } else
                    {
                        $html.= date('d M', $note->getCreated()->getTimestamp()).' '.date('H:i', $note->getCreated()->getTimestamp());
                        $class = "alldateday";
                    }

                    $data['datetimestamp'] = $html;
                    $data['class'] = $class;
                // end timestamp

                $dateKey = date('d M', $note->getCreated()->getTimestamp());
                $newnotes['notes'][$dateKey][] = $data;
        }
        /*dump($newnotes['notes']);
        exit;*/
        return $this->render('DrivingSchoolSchoolBundle:Student:student_notes.html.twig',
                array('form' => $form->createView(), 'studentid' => $id, 'newnotes' => $newnotes['notes']));
    }

    public function notesinsertAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $notes = new StudentNotesEntity();
        $form = $this->createForm(StudentNotesForm::class, $notes);

        $request = Request::createFromGlobals();
        $resp = [];
        $mediaManager = $this->get('sonata.media.manager.media');
        foreach ($request->files as $uploadedFile) {
            if(!empty($uploadedFile)) {
                $media = new Media();
                $media->setBinaryContent($uploadedFile);
                $media->setContext("default");
                $media->setProviderName('sonata.media.provider.image');
                $mediaManager->save($media);
                $notes->setNotesImage($media);
            }
        }

        $data = $request->request->all();
        $entityManager = $this->getDoctrine()->getManager();
        $description  = isset($data['student_notes_form']['desc']) ? $data['student_notes_form']['desc'] : "";
        $notes->setDesc($description);
        $notes->setDrivingSchool($user);

        $studentid = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($data['studentid']);
        $notes->setStudentid($studentid); 
        
        $entityManager->persist($notes);
        $entityManager->flush();

        // get notes detail
        $studentnotes = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentNotesEntity')->findBy(array('DrivingSchool' => $userId, 'studentid' => $data['studentid']), array('id' => 'DESC'));

        $data = array();
        $newnotes = array('notes');
        foreach ($studentnotes as $note) {
                $media = $note->getNotesImage();
                $image_path = "";
                    if(!empty($media)) {
                        $mediaManager = $this->get('sonata.media.pool');
                        $provider = $mediaManager->getProvider($media->getProviderName());
                        $format = $provider->getFormatName($media, 'default_small');
                        $image_path = $provider->generatePublicUrl($media, $format);
                        $format_big = $provider->getFormatName($media, 'default_big');
                        $big_image_path = $provider->generatePublicUrl($media, $format_big);
                        
                    }
                $data['desc'] = $note->getDesc();
                $data['id'] = $note->getId();
                $data['image_path'] = $image_path;
                $data['big_image_path'] = $big_image_path;
                $data['created'] = $note->getCreated();

                // start timestamp
                    $date = date('Y-m-d', $note->getCreated()->getTimestamp());
                    $time = date('H:i:s', $note->getCreated()->getTimestamp());
                    $newdate=date_create($date." ".$time);
                    $now = date_create(date("Y-m-d H:i:s"));
                    $diff = date_diff($now,$newdate);
                    $html = "";
                    $class = "";
                    if($date == date("Y-m-d"))
                    {
                        $hours = (float) $diff->format("%R%h");
                        if($hours > 0) {
                            $html.="After ".abs($hours)." Hours";
                            $class = "today";
                        } elseif($hours < 0) {
                            $html.= abs($hours)." Hours Ago";
                            $class = "today";
                        } else {
                            $minutes = (float) $diff->format("%R%i");
                            if($minutes > 0) {
                                $html.="After ".abs($minutes)." Minute(s)";
                                $class = "today";
                            } elseif($minutes < 0) {
                                $html.= abs($minutes)." Minutes Ago";
                                $class = "today";
                            } else {
                                $seconds = (float) $diff->format("%R%s");
                                if($seconds > 0) {
                                    $html.="After ".abs($seconds)." Second(s)";
                                    $class = "today";
                                } elseif($seconds < 0) {
                                    $html.= abs($seconds)." Seconds Ago";
                                    $class = "today";
                                }
                            }
                        }
                    } else if($date == date('Y-m-d',strtotime("-1 days")))
                    {
                        $html.= "Yesterday ".date('H:i', $note->getCreated()->getTimestamp());
                        $class = "yesterday";
                    } else
                    {
                        $html.= date('d M', $note->getCreated()->getTimestamp()).' '.date('H:i', $note->getCreated()->getTimestamp());
                        $class = "alldateday";
                    }

                    $data['datetimestamp'] = $html;
                    $data['class'] = $class;
                // end timestamp

                $dateKey = date('d M', $note->getCreated()->getTimestamp());
                $newnotes['notes'][$dateKey][] = $data;
        }
        $resp =  $this->render(
                'DrivingSchoolSchoolBundle:Student:student_notes.html.twig', array('form' => $form->createView(), 'studentid' => $data['studentid'], 'newnotes' => $newnotes['notes'])
            )->getContent();

        return new Response(json_encode(array('response' => $resp)));
    }

    public function getimagepreviewAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $data = $request->request->all();
        $studentid = $data['studentid'];
        
        $entityManager = $this->getDoctrine()->getManager();
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($studentid);
        $mediaManager = $this->get('sonata.media.manager.media');
        foreach ($request->files as $uploadedFile) {
            if(!empty($uploadedFile)) {
                $media = new Media();
                $media->setBinaryContent($uploadedFile);
                $media->setContext("default");
                $media->setProviderName('sonata.media.provider.image');
                $mediaManager->save($media);
                $student->setStudentImage($media);
            }
        }
        $entityManager->persist($student);
        $entityManager->flush();

        $request = Request::createFromGlobals();
        $resp = [];
        foreach ($request->files as $uploadedFile) {
            $fileData = file_get_contents($uploadedFile->openFile()->getPathName());
        }
        
        $enocoded_data = base64_encode($fileData);
        $resp['filename'] = 'data:image/png;base64, '.$enocoded_data;

        return new Response(json_encode($resp));
    }

    public function emailduplicationAction(Request $request)
    {
        $data = $request->request->all();
        $email = $data['email'];

        $entityManager = $this->getDoctrine()->getManager();
        $students = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findAll();
        $email_typechk = array();
        foreach ($students as $student) {
            $email_typechk[] = $student->getEmail();
        }

        if (!in_array($email,$email_typechk))
        {
            return new Response(json_encode(TRUE));
        } else {
            return new Response(json_encode(FALSE));
        }
    }

    public function emailduplicationeditAction(Request $request, $id)
    {
        $data = $request->request->all();
        
        $studentemail = $data['student_profile_form']['email'];

        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $studentquery = $connection->prepare("SELECT email FROM student WHERE id !=".$id );
        $studentquery->execute();
        $students = $studentquery->fetchAll();

        $email_typechk = array();
        foreach ($students as $student) {
            $email_typechk[] = $student['email'];
        }

        if (!in_array($studentemail,$email_typechk))
        {
            return new Response(json_encode(TRUE));
        } else {
            return new Response(json_encode(FALSE));
        }
    }

    public function activateemailduplicationAction(Request $request, $id)
    {
        $data = $request->request->all();
        
        $email = $data['email'];

        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $studentquery = $connection->prepare("SELECT email FROM student WHERE id !=".$id );
        $studentquery->execute();
        $students = $studentquery->fetchAll();

        $email_typechk = array();
        foreach ($students as $student) {
            $email_typechk[] = $student['email'];
        }

        if (!in_array($email,$email_typechk))
        {
            return new Response(json_encode(TRUE));
        } else {
            return new Response(json_encode(FALSE));
        }
    }

    public function studentlessonsAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $lessonsquery = $connection->prepare("SELECT planning.*,pap.id as priceid, pap.name as pricename, pi.icon_path, ts.id as trainingid, ts.training_type as trainingtype, ts.name as trainingname, ts.appointment_label, i.instructor_name as instructorname FROM planning LEFT JOIN price_and_packages as pap ON pap.id = planning.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id LEFT JOIN training_sessions as ts ON ts.id = planning.training_session_id LEFT JOIN instructor as i ON i.id = planning.instructor_id WHERE school_id = ".$userId." AND student_id = ".$id. " ORDER BY planning.id DESC");
        $lessonsquery->execute();
        $lessons = $lessonsquery->fetchAll();

        $data = [
            'lessons' => $lessons,
        ];
        return $this->render('DrivingSchoolSchoolBundle:Student:student_lessons.html.twig', $data);
    }

    public function studentexamsAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $examsquery = $connection->prepare("SELECT planning.*,pap.id as priceid, pap.name as pricename, pi.icon_path, ts.id as trainingid, ts.training_type as trainingtype, ts.name as trainingname, ts.appointment_label, i.instructor_name as instructorname FROM planning LEFT JOIN price_and_packages as pap ON pap.id = planning.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id LEFT JOIN training_sessions as ts ON ts.id = planning.training_session_id LEFT JOIN instructor as i ON i.id = planning.instructor_id WHERE school_id = ".$userId." AND student_id = ".$id);
        $examsquery->execute();
        $exams = $examsquery->fetchAll();
        // echo "<pre>";
        // print_r($exams);exit;

        $data = [
            'exams' => $exams,
        ];
        return $this->render('DrivingSchoolSchoolBundle:Student:student_exams.html.twig', $data);
    }

    public function credithistoryAction(Request $request, UserInterface $user)
    {
        $data = $request->request->all();
        $trainingid = $data['trainingid'];
        // $trainingid = "15";

        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $historyquery = $connection->prepare("SELECT sfh.*, ts.name as trainingname, pi.icon_path FROM student_financial_history AS sfh LEFT JOIN training_sessions as ts ON sfh.trainingsession_id = ts.id LEFT JOIN price_and_packages as pap ON sfh.price_package_id = pap.id LEFT JOIN price_icons as pi ON pap.icon_id = pi.id WHERE sfh.drivingschool_id = ".$userId." AND sfh.trainingsession_id = ".$trainingid);
        $historyquery->execute();
        $history = $historyquery->fetchAll();

        foreach ($history as $key => $value) {
            $iconpath = $value['icon_path'];
            $trainingname = $value['trainingname'];
            if($value['course_type'] == '1')
            {
                $resp = $this->render('DrivingSchoolSchoolBundle:Student:credit_examination_history.html.twig', ['histories' => $history, 'trainingid' => $trainingid, 'iconpath' => $iconpath, 'trainingname' => $trainingname])->getContent();
            } else {
                $resp = $this->render('DrivingSchoolSchoolBundle:Student:credit_lesson_history.html.twig', ['histories' => $history, 'trainingid' => $trainingid, 'iconpath' => $iconpath, 'trainingname' => $trainingname])->getContent();
            }
        }

        return new Response(json_encode(array('response' => $resp)));
    }

    public function addHistoryAction(Request $request, $trainingid, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $history = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentFinancialHistoryEntity')->findOneBy(array('Courses' => $trainingid, 'DrivingSchool' => $userId));  

        $fhistory = new StudentFinancialHistoryEntity();

        $data = $request->request->all();

        // get student id
        $student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($history->getStudentid());
        // get price and packages id
        $priseandpackages = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($history->getPricePackageId());
        // get courses id
        $courses = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->find($history->getCourses());

        $fhistory->setDrivingSchool($user);
        $fhistory->setStudentid($student);
        $fhistory->setPricePackageId($priseandpackages);
        $fhistory->setCourses($courses);
        $fhistory->setCourseType($history->getCourseType());
        if($data['quantity'])
        {
            $fhistory->setQty($data['quantity']);
        }
        if($data['minute'])
        {
            $fhistory->setMinutes($data['minute']);
        }
        $fhistory->setDesc($data['description']);
        $fhistory->setIncreaseDecrease($data['incr_decr']);
        $entityManager->persist($fhistory);
        $entityManager->flush();

        return $this->redirectToRoute('driving_school_school_student_card', array('id' => $student->getId()));
    }
}
