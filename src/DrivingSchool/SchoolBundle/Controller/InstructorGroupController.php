<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\InstructorGroupForm;
use DrivingSchool\AdminBundle\Entity\InstructorGroupEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class InstructorGroupController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
    	$entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
    	// $groups = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorGroupEntity')->findBy(array('DrivingSchool' => $userId));

        /*$query = $connection->prepare("SELECT ig.name as instructorGroupName, GROUP_CONCAT(i.instructor_name) as instructorName FROM instructor_group AS ig LEFT JOIN instructor_instructorgroup AS iig ON ig.id = iig.instructorgruop_id LEFT JOIN instructor AS i ON iig.instructor_id = i.id WHERE ig.drivingschool_id = ".$userId." GROUP BY ig.name");*/

        $query = $connection->prepare("SELECT ig.id as instructorGroupId, ig.name as instructorGroupName ,GROUP_CONCAT(i.instructor_name) as instructorName FROM instructor_group AS ig LEFT JOIN instructor_instructorgroup AS iig ON ig.id = iig.instructorgruop_id LEFT JOIN instructor AS i ON iig.instructor_id = i.id WHERE ig.drivingschool_id =".$userId." GROUP BY instructorGroupName");
        
        $query->execute();
        $groups = $query->fetchAll();

        $data = [
            'groups' => $groups,
        ];

        return $this->render('DrivingSchoolSchoolBundle:InstructorGroup:list.html.twig', $data);
    }

    public function createAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
    
    	$group = new InstructorGroupEntity();
    	$form = $this->createForm(InstructorGroupForm::class, $group, array('userId' => $userId));

    	$form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $group->setDrivingSchool($drivingschool); 

            $entityManager->persist($group);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Instructor group added succesfully.'
            );
            return $this->redirectToRoute('driving_school_school_instructorgroups');
        }

    	return $this->render(
			'DrivingSchoolSchoolBundle:InstructorGroup:create.html.twig',
            array('form' => $form->createView(),
                'title' => 'Create Instructor Group',
                'btn_title' => 'Create'
            )
		);	
    }

    public function editAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $group = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorGroupEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));
        if(!empty($group))
        {
            $form = $this->createForm(InstructorGroupForm::class, $group, array('userId' => $userId));

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                $entityManager->persist($group);
                $entityManager->flush();

                $this->addFlash(
                    'success',
                    'Instructor group updated succesfully.'
                );
                return $this->redirectToRoute('driving_school_school_instructorgroups');
            }

            return $this->render(
                'DrivingSchoolSchoolBundle:InstructorGroup:create.html.twig',
                array('form' => $form->createView(),
                    'title' => 'Edit Instructor Group',
                    'btn_title' => 'Update',
                )
            );
        } else {
            return $this->redirectToRoute('driving_school_school_instructorgroups');
        }
    }

    public function deleteAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $group = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorGroupEntity')->find($id);

        if (!$group) { 
            // no group in the system
            throw $this->createNotFoundException(
                'No price & packages found for id '.$id
            );
        } else {
            $entityManager->remove($group);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Instructor group deleted succesfully.'
            );
            return $this->redirectToRoute('driving_school_school_instructorgroups');
        }
    }
}
