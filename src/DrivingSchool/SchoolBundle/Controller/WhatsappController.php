<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\VehicleEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class WhatsappController extends Controller
{
    public function getUserNumberAction(Request $request, UserInterface $user)
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        $userquery = $connection->prepare("SELECT phone FROM fos_user_user");
        $userquery->execute();
        $userPhone = $userquery->fetchAll();

        $data = ['numbers' => $userPhone];

        return $this->render('DrivingSchoolSchoolBundle:Whatsapp:contanct.html.twig', $data);
    }    
}