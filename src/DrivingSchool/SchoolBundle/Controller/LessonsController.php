<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity;
use DrivingSchool\AdminBundle\Entity\ModuleEntity;
use DrivingSchool\AdminBundle\Entity\LessonRolesEntity;
use DrivingSchool\SchoolBundle\Form\LessonsForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class LessonsController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    {
        // $userId = $request->getSession()->get('user_id');
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $Lessons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(array('DrivingSchool' => $userId));

        $icons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->findAll();

        $data = [
            'Lessons' => $Lessons,
            'icons' => $icons
        ];
        // dump($data);exit;
        return $this->render('DrivingSchoolSchoolBundle:Lessons:list.html.twig', $data);
    }

    public function moduleListAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $priceandpackage = $em->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId));
        
        if(!empty($priceandpackage))
        {
            $modules = $em->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->findBy(['lessonId' => $id]);

            foreach ($modules as $module) {
                $rolesdata = [];
                $roles = $em->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->findBy(['Module' => $module->getId()]);
                $rolesdata[][$module->getModuleName()] = $roles;
                $rolesdata['module_id'] = $module->getId();
                $newModuleRoles[] = $rolesdata;
            } 

            $data = [
                'modules' => $newModuleRoles,
            ];
            return $this->render('DrivingSchoolSchoolBundle:Lessons:modulelist.html.twig', $data);
        } else {
            return $this->redirectToRoute('driving_school_school_list_lessons');
        }
    }

    public function moduleCreateAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();

        $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId));
        if(!empty($priceandpackage))
        {
        	// build the form
            $modules = new ModuleEntity();
            $form = $this->createForm(LessonsForm::class, $modules);

            // get lession name
            $lessons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(['id' => $id]);
            foreach ($lessons as $lesson) {
                $lessonName = $lesson->getName();
            }

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                // lession id save 
                $lesson_id = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($id);
                $modules->setLessonId($lesson_id);

                // save all detail of module
                $entityManager->persist($modules);
                $entityManager->flush();
                $lastModuleId = $modules->getId();

                //get role save
                $getrole = $request->request->get('role');
                // $jsonrole = json_encode($getrole);
                // $modules->setRoles($jsonrole);
                foreach ($getrole as $key => $value) {
                    $roles = new LessonRolesEntity();

                    $moduleid = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->find($lastModuleId);
                    $roles->setModule($moduleid);
                    $roles->setName($value);
                    $entityManager->persist($roles);
                    $entityManager->flush();
                }                

                $this->addFlash(
                    'success',
                    'Module added succesfully.'
                ); 
                return $this->redirectToRoute('driving_school_school_modulelist_lessons', array('id' => $id));
            }

        	return $this->render(
                'DrivingSchoolSchoolBundle:Lessons:modulecreate.html.twig',
                array('form' => $form->createView(),
                      'lessonName' => $lessonName,
                      'title' => 'Create Module',
                      'btn_title' => 'Create',
                      'lessionid' => $id)
            );
        } else {
            return $this->redirectToRoute('driving_school_school_list_lessons');
        }
    }	

    public function moduleEditAction(Request $request, $lessionid, $moduleid, UserInterface $user)
    {
        $userId = $user->getId();
       
        $entityManager = $this->getDoctrine()->getManager();
        $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(array('id' => $lessionid, 'DrivingSchool' => $userId));
        if(!empty($priceandpackage))
        {
            $modules = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->findOneBy(array('lessonId' => $lessionid, 'id' => $moduleid));

            if(!empty($modules))
            {
                // get role detail
                $roles = $entityManager->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->findBy(array('Module' => $moduleid));

                // get lession name
                $lessons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(['id' => $lessionid]);
                foreach ($lessons as $lesson) {
                    $lessonName = $lesson->getName();
                }

                $form = $this->createForm(LessonsForm::class, $modules);

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    // save all detail
                    $entityManager->persist($modules);
                    $entityManager->flush();

                    //get role save
                    $getrole = $request->request->get('role');
                    // echo "<pre>";
                    // print_r($getrole);exit;
                    
                    foreach ($getrole as $key => $value) {
                        if (strpos($key, 'new') !== false) {
                            if(!empty($value))
                            {
                                $roleObj = new LessonRolesEntity();
                                $moduleid = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->find($modules->getId());
                                $roleObj->setModule($moduleid);
                                $roleObj->setName($value);
                                $entityManager->persist($roleObj);
                                $entityManager->flush();
                            }
                        } else {
                            $roles = $entityManager->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->findBy(array('Module' => $moduleid, 'id' => $key));
                            if(!empty($value))
                            {
                                foreach ($roles as $role) {
                                    $moduleid = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->find($modules->getId());
                                    $role->setModule($moduleid);
                                    $role->setName($value);
                                    $entityManager->persist($role);
                                    $entityManager->flush();
                                }
                            } else {
                                foreach ($roles as $role) {
                                    $moduleid = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->find($modules->getId());
                                    $entityManager->remove($role);
                                    $entityManager->flush();
                                }
                            }
                        }
                    }   

                    $this->addFlash(
                        'success',
                        'Module updated succesfully.'
                    ); 
                    return $this->redirectToRoute('driving_school_school_modulelist_lessons', array('id' => $lessionid));
                }

                return $this->render(
                    'DrivingSchoolSchoolBundle:Lessons:modulecreate.html.twig',
                    array('form' => $form->createView(),
                          'id'   => $modules->getId(),
                          'roles' => $roles,
                          'lessonName' => $lessonName,
                          'title' => 'Edit Module',
                          'btn_title' => 'Update',
                          'lessionid' => $lessionid
                        )
                    );
            } else {
                return $this->redirectToRoute('driving_school_school_modulelist_lessons', array('id' => $lessionid));
            }
        } else {
            return $this->redirectToRoute('driving_school_school_list_lessons');
        }
    }
}
