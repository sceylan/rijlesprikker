<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\AssessmentMethodEntity;
use DrivingSchool\AdminBundle\Entity\ModuleEntity;
use DrivingSchool\SchoolBundle\Form\AssessmentMethodForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class AssessmentMethodController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $Assessments = $entityManager->getRepository('DrivingSchoolAdminBundle:AssessmentMethodEntity')->findBy(array('DrivingSchool' => $userId));

        $data = [
            'Assessments' => $Assessments
        ];
        return $this->render('DrivingSchoolSchoolBundle:Lessons:assessmentlist.html.twig', $data);
    }

    public function addAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
       	// build the form
        $assessments = new AssessmentMethodEntity();
        $form = $this->createForm(AssessmentMethodForm::class, $assessments);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $assessments->setDrivingSchool($user);

            // save all detail
            $entityManager->persist($assessments);
            $entityManager->flush();
            
            $this->addFlash(
                'success',
                'Assessment method added succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_list_assessmentmethod');
        }

    	return $this->render(
            'DrivingSchoolSchoolBundle:Lessons:assessmentcreate.html.twig',
            array('form' => $form->createView(),
                  'title' => 'Create Assessment Method',
                  'btn_title' => 'Create')
        );
    }	

    public function editAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
       
        $entityManager = $this->getDoctrine()->getManager();
        $assessments = $entityManager->getRepository('DrivingSchoolAdminBundle:AssessmentMethodEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));

        if(!empty($assessments))
        {
            $form = $this->createForm(AssessmentMethodForm::class, $assessments);

	        $form->handleRequest($request);
	        if ($form->isSubmitted() && $form->isValid()) {
	            // save all detail
	            $entityManager->persist($assessments);
	            $entityManager->flush();
	            
	            $this->addFlash(
	                'success',
	                'Assessment method updated succesfully.'
	            ); 
	            return $this->redirectToRoute('driving_school_school_list_assessmentmethod');
	        }

	        return $this->render('DrivingSchoolSchoolBundle:Lessons:assessmentcreate.html.twig',
	            array('form' => $form->createView(),
	                  'title' => 'Edit Assessment Method',
	                  'btn_title' => 'Update')
	        );
        } else {
            return $this->redirectToRoute('driving_school_school_list_assessmentmethod');
        }
    }

    public function deleteAction(Request $request, $id, UserInterface $user)
    {
    	$userId = $user->getId();
       
        $entityManager = $this->getDoctrine()->getManager();
        $assessments = $entityManager->getRepository('DrivingSchoolAdminBundle:AssessmentMethodEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));

        if (!$assessments) { 
        	dump("Yes");exit;
            // no instructor in the system
            throw $this->createNotFoundException(
                'No assessment found for id '.$id
            );
        } else {
            $entityManager->remove($assessments);
            $entityManager->flush();
        	$this->addFlash(
                'success',
                'Assessment method deleted succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_list_assessmentmethod');
        }
    }
}
