<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\NotificationEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class NotificationController extends Controller
{
    public function indexAction(Request $request, UserInterface $user)
    {
    	return $this->render('DrivingSchoolSchoolBundle:Notification:index.html.twig');
    }

    public function ajaxLoadAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) {
            $userlogedid = "where n.event_school_id = ".$userId;
        } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
            $userlogedid = "where n.event_instructor_id = ".$userId;
        }

        if(isset($_POST['getresult']))
        {
            $no = $_POST['getresult'];
        } else {
            $no = '0';
        }
        $query = $connection->prepare("SELECT n.*, i.username as instructorname, s.schoolName as schoolname, s.logo_id as schoolimage, st.username as studentname, st.student_image_id as studentimage, i.instructor_image_id as instructorimage FROM notification AS n LEFT JOIN instructor AS i ON n.event_instructor_id = i.id LEFT JOIN driving_school AS s ON n.event_school_id = s.id LEFT JOIN student AS st ON n.event_student_id = st.id ".$userlogedid." ORDER BY n.id DESC limit ".$no.",10");
        $query->execute();
        $notifications = $query->fetchAll();
// dump($query);exit;
        $notificationdata = [];
        foreach ($notifications as $key => $notification) {
            if($notification['user_type'] == '1') {
                $schoolimage = $notification['schoolimage'];
                $mediaObj = $entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findOneBy(array('id' => $schoolimage));
                if(!empty($mediaObj)) {
                    $mediaManager = $this->get('sonata.media.pool');
                    $provider = $mediaManager->getProvider($mediaObj->getProviderName());
                    $format = $provider->getFormatName($mediaObj, 'default_small');
                    $image_path = $provider->generatePublicUrl($mediaObj, $format);
                    $format_big = $provider->getFormatName($mediaObj, 'default_big');
                    $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
                }

                $notification['image'] = $big_image_path;
                $notification['name'] = $notification['schoolname'];
            }
            else if($notification['user_type'] == '2') {
                $instructorimage = $notification['instructorimage'];
                $mediaObj = $entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findOneBy(array('id' => $instructorimage));
                if(!empty($mediaObj)) {
                    $mediaManager = $this->get('sonata.media.pool');
                    $provider = $mediaManager->getProvider($mediaObj->getProviderName());
                    $format = $provider->getFormatName($mediaObj, 'default_small');
                    $image_path = $provider->generatePublicUrl($mediaObj, $format);
                    $format_big = $provider->getFormatName($mediaObj, 'default_big');
                    $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
                }

                $notification['image'] = $big_image_path;    
                $notification['name'] = $notification['instructorname'];
            } else if($notification['user_type'] == '3') {
                $studentimage = $notification['studentimage'];
                $mediaObj = $entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findOneBy(array('id' => $studentimage));
                if(!empty($mediaObj)) {
                    $mediaManager = $this->get('sonata.media.pool');
                    $provider = $mediaManager->getProvider($mediaObj->getProviderName());
                    $format = $provider->getFormatName($mediaObj, 'default_small');
                    $image_path = $provider->generatePublicUrl($mediaObj, $format);
                    $format_big = $provider->getFormatName($mediaObj, 'default_big');
                    $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
                }
                
                $notification['image'] = $big_image_path;
                $notification['name'] = $notification['studentname'];
            }
            $notificationdata[] = $notification;
        }

        return $this->render('DrivingSchoolSchoolBundle:Notification:ajaxLoad.html.twig', ['notificationdata' => $notificationdata]);
    }

    public function getNotificationIconAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
    	$entityManager = $this->getDoctrine()->getManager();

        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) {
            $notiData = $entityManager->getRepository('DrivingSchoolAdminBundle:NotificationEntity')->findBy(array('school' => $userId, 'schoolReadStatus' => '0'));
        } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
        	$notiData = $entityManager->getRepository('DrivingSchoolAdminBundle:NotificationEntity')->findBy(array('instructor' => $userId, 'instructorReadStatus' => '0'));
        }       
		$count = count($notiData);
		
    	return $this->render('DrivingSchoolSchoolBundle:Notification:notificationicon.html.twig', ['count' => $count]);
    }

    public function updateReadStatusAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
    	$entityManager = $this->getDoctrine()->getManager();

    	if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) {
        	$notiData = $entityManager->getRepository('DrivingSchoolAdminBundle:NotificationEntity')->findBy(array('school' => $userId, 'schoolReadStatus' => '0'));
            foreach ($notiData as $value) {
            	$value->setSchoolReadStatus('1');
            	$entityManager->persist($value);
            }
        } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
        	$notiData = $entityManager->getRepository('DrivingSchoolAdminBundle:NotificationEntity')->findBy(array('instructor' => $userId, 'instructorReadStatus' => '0'));
        	foreach ($notiData as $value) {
            	$value->setInstructorReadStatus('1');
        		$entityManager->persist($value);
            }
        }
        $entityManager->flush();

        return $this->redirectToRoute('driving_school_school_notification');
    }
}