<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\DrivinglessonsForm;
use DrivingSchool\SchoolBundle\Form\OtherCoursesForm;
use DrivingSchool\SchoolBundle\Form\PackagesForm;
use DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity;
use DrivingSchool\AdminBundle\Entity\PackagesEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Response;

class TrainingSessionsController extends Controller
{
    public function listAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
    
        $entityManager = $this->getDoctrine()->getManager();

        //get price and packages detail
        $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId));
        if(empty($priceandpackage))
        {
            return $this->redirectToRoute('driving_school_school_list_priceandpackages');
        }

        // get driving lessons details
        $driving_lessons = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $id, 'type' => '0'));

        // get Examinations details
        $examinations = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $id, 'type' => '1'));

        // get Other details
        $others = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $id, 'type' => '2'));

        // get packages details
        $packages = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $id, 'type' => '3'));
   
        $data = [
            'drivinglessons' => $driving_lessons,
            'examinations' => $examinations,
            'others' => $others,
            'pricepackageid' => $id,
            'packages' => $packages,
        ];
// dump($data);exit;
        return $this->render('DrivingSchoolSchoolBundle:TrainingSessions:list.html.twig', $data);
    }

    public function drivinglessonsCreateAction(Request $request, $pricepackageid, UserInterface $user)
    {
		 $userId = $user->getId();
        
    	// build the form
        $trainingsession = new TrainingSessionsEntity();
        $form = $this->createForm(DrivinglessonsForm::class, $trainingsession);
        $entityManager = $this->getDoctrine()->getManager();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all()['drivinglessons_form'];

            // get drivingschool id
            $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $trainingsession->setDrivingSchool($drivingschool); 

           // get price&packages detail and set in entity
            $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($pricepackageid);
            $trainingsession->setPricePackageId($priceandpackage); 

            // add type
            $trainingsession->setType('0');

            // calculation on vat
            if($data['includedVat'] == '1' )
            {
            	$vatcalc = $data['price']; 
            } else if($data['includedVat'] == '' ) {
            	$price = $data['price'];
            	$vat = $data['vat'];
            	$vatcalc = $price+(($vat / 100) * $price);
            }
            $trainingsession->setVatCalc($vatcalc);

            if($data['appointmentLabel'] == '')
            {
                $trainingsession->setAppointmentLabel('#a8a3a3');
            } else {
                $trainingsession->setAppointmentLabel($data['appointmentLabel']);
            }

            // get linked to value
            $linkedTo = $request->request->all()['linkedTo'];
            $trainingsession->setLinkedTo($linkedTo); 

            // save the trainingsession
            $entityManager->persist($trainingsession);
            $entityManager->flush();
        	
            $this->addFlash(
                    'success_message','Driving lesson added succesfully.'
                );
            $this->addFlash('lesson_parameter','driving_lesson');

            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }

        $colors = $entityManager->getRepository('DrivingSchoolAdminBundle:ColorEntity')->findAll();

        $trainigsessiondata = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));

        // dump($trainigsessiondata);exit;
        return $this->render(
            'DrivingSchoolSchoolBundle:TrainingSessions:dlessoncreate.html.twig',
            array('form' => $form->createView(),
                    'colors' => $colors,
            		'trainigsessions' => $trainigsessiondata,
                    'pricepackageid' => $pricepackageid,
                    'title' => 'Create Driving lesson',
                    'btn_title' => 'Create',
            	)
        );
    }

    public function drivinglessonsEditAction(Request $request, $pricepackageid, $id, UserInterface $user)
    {   
        $userId = $user->getId();
    	// build the form
        $entityManager = $this->getDoctrine()->getManager();

        $trainingsessions = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid, 'type' => '0'));
        $trainingsession = $trainingsessions[0];
        if(!empty($trainingsession))
        {
            $form = $this->createForm(DrivinglessonsForm::class, $trainingsession);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $data = $request->request->all()['drivinglessons_form'];

                // calculation on vat
                if($data['includedVat'] == '1' )
                {
                    $vatcalc = $data['price']; 
                } else if($data['includedVat'] == '' ) {
                    $price = $data['price'];
                    $vat = $data['vat'];
                    $vatcalc = $price+(($vat / 100) * $price);
                }
                $trainingsession->setVatCalc($vatcalc);

                // get linked to value
                $linkedTo = $request->request->all()['linkedTo'];
                $trainingsession->setLinkedTo($linkedTo); 

                // save the trainingsession
                $entityManager->persist($trainingsession);
                $entityManager->flush();

                $this->addFlash(
                    'success_message','Driving lesson updated succesfully.'
                );
                $this->addFlash('lesson_parameter','driving_lesson');

                return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
            }
            $colors = $entityManager->getRepository('DrivingSchoolAdminBundle:ColorEntity')->findAll();

            $trainigsessiondata = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));

            return $this->render(
                'DrivingSchoolSchoolBundle:TrainingSessions:dlessoncreate.html.twig',
                array('form' => $form->createView(),
                    'colors' => $colors,
                    'trainigsessions' => $trainigsessiondata,
                    'linkedTo' => $trainingsession->getLinkedTo(),
                    'pricepackageid' => $pricepackageid,
                    'title' => 'Edit Driving lesson',
                    'btn_title' => 'Update',
                )
            );
        } else {
            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }
    }

    public function examinationCreateAction(Request $request, $pricepackageid, UserInterface $user)
    {
		$userId = $user->getId();
        
    	// build the form
        $trainingsession = new TrainingSessionsEntity();
        $form = $this->createForm(DrivinglessonsForm::class, $trainingsession);
        $entityManager = $this->getDoctrine()->getManager();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all()['drivinglessons_form'];

            // get drivingschool id
            $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $trainingsession->setDrivingSchool($drivingschool); 

           // get price&packages detail and set in entity
            $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($pricepackageid);
            $trainingsession->setPricePackageId($priceandpackage); 

            // add type
            $trainingsession->setType('1');

            // calculation on vat
            if($data['includedVat'] == '1' )
            {
            	$vatcalc = $data['price']; 
            } else if($data['includedVat'] == '' ) {
            	$price = $data['price'];
            	$vat = $data['vat'];
            	$vatcalc = $price+(($vat / 100) * $price);
            }
            $trainingsession->setVatCalc($vatcalc);

            if($data['appointmentLabel'] == '')
            {
                $trainingsession->setAppointmentLabel('#a8a3a3');
            } else {
                $trainingsession->setAppointmentLabel($data['appointmentLabel']);
            }

            // get linked to value
            $linkedTo = $request->request->all()['linkedTo'];
            $trainingsession->setLinkedTo($linkedTo); 

            // save the trainingsession
            $entityManager->persist($trainingsession);
            $entityManager->flush();
        	
            $this->addFlash(
                    'success_message','Examination and tests added succesfully.'
                );
            $this->addFlash('exam_parameter','examination');

            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }

        $colors = $entityManager->getRepository('DrivingSchoolAdminBundle:ColorEntity')->findAll();
        $trainigsessiondata = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));

        return $this->render(
            'DrivingSchoolSchoolBundle:TrainingSessions:examinationcreate.html.twig',
            array('form' => $form->createView(),
            	'colors' => $colors,
                'trainigsessions' => $trainigsessiondata,
                'pricepackageid' => $pricepackageid,
                'title' => 'Create Examination&Tests',
                'btn_title' => 'Create',
        	)
        );
    }

    public function examinationEditAction(Request $request, $pricepackageid, $id, UserInterface $user)
    {   
        $userId = $user->getId();
    	// build the form
        $entityManager = $this->getDoctrine()->getManager();
        $trainingsessions = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid, 'type' => '1'));
        $trainingsession = $trainingsessions[0];
        if(!empty($trainingsession))
        {
            $form = $this->createForm(DrivinglessonsForm::class, $trainingsession);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $data = $request->request->all()['drivinglessons_form'];

                // calculation on vat
                if($data['includedVat'] == '1' )
                {
                	$vatcalc = $data['price']; 
                } else if($data['includedVat'] == '' ) {
                	$price = $data['price'];
                	$vat = $data['vat'];
                	$vatcalc = $price+(($vat / 100) * $price);
                }
                $trainingsession->setVatCalc($vatcalc);

                // get linked to value
                $linkedTo = $request->request->all()['linkedTo'];
                $trainingsession->setLinkedTo($linkedTo); 

                // save the trainingsession
                $entityManager->persist($trainingsession);
                $entityManager->flush();
            	
                $this->addFlash(
                    'success_message','Examination and tests updated succesfully.'
                );

                $this->addFlash('exam_parameter','examination');

                return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
            }

            $colors = $entityManager->getRepository('DrivingSchoolAdminBundle:ColorEntity')->findAll();
            
            $trainigsessiondata = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));

            return $this->render(
                'DrivingSchoolSchoolBundle:TrainingSessions:examinationcreate.html.twig',
                array('form' => $form->createView(),
                	'colors' => $colors,
                    'trainigsessions' => $trainigsessiondata,
                    'linkedTo' => $trainingsession->getLinkedTo(),
                    'pricepackageid' => $pricepackageid,
                    'title' => 'Edit Examination&Tests',
                    'btn_title' => 'Update',
            	)
            );
        } else {
            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }
    }

    public function otherCreateAction(Request $request, $pricepackageid, UserInterface $user)
    {
    	$userId = $user->getId();
        
    	// build the form
        $trainingsession = new TrainingSessionsEntity();
        $form = $this->createForm(OtherCoursesForm::class, $trainingsession);
        $entityManager = $this->getDoctrine()->getManager();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all()['other_courses_form'];

            // get drivingschool id
            $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $trainingsession->setDrivingSchool($drivingschool); 

           // get price&packages detail and set in entity
            $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($pricepackageid);
            $trainingsession->setPricePackageId($priceandpackage); 

            // add type
            $trainingsession->setType('2');

            // calculation on vat
            if($data['includedVat'] == '1' )
            {
            	$vatcalc = $data['price']; 
            } else if($data['includedVat'] == '' ) {
            	$price = $data['price'];
            	$vat = $data['vat'];
            	$vatcalc = $price+(($vat / 100) * $price);
            }
            $trainingsession->setVatCalc($vatcalc);

            // get linked to value
            $linkedTo = $request->request->all()['linkedTo'];
            $trainingsession->setLinkedTo($linkedTo); 

            // save the trainingsession
            $entityManager->persist($trainingsession);
            $entityManager->flush();

            $this->addFlash(
                'success_message','Other added succesfully.'
            );

            $this->addFlash('other_parameter','other');

            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }
        
        $trainigsessiondata = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));

        return $this->render(
            'DrivingSchoolSchoolBundle:TrainingSessions:othercreate.html.twig',
            array('form' => $form->createView(),
                'trainigsessions' => $trainigsessiondata,
                'pricepackageid' => $pricepackageid,
                'title' => 'Create Other',
                'btn_title' => 'Create',
            )
        );
    }

    public function otherEditAction(Request $request, $pricepackageid, $id, UserInterface $user)
    {   
        $userId = $user->getId();
    	// build the form
        $entityManager = $this->getDoctrine()->getManager();
        $trainingsessions = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid, 'type' => '2'));
        $trainingsession = $trainingsessions[0];
        if(!empty($trainingsession))
        {
            $form = $this->createForm(OtherCoursesForm::class, $trainingsession);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $data = $request->request->all()['other_courses_form'];

                // calculation on vat
                if($data['includedVat'] == '1' )
                {
                	$vatcalc = $data['price']; 
                } else if($data['includedVat'] == '' ) {
                	$price = $data['price'];
                	$vat = $data['vat'];
                	$vatcalc = $price+(($vat / 100) * $price);
                }
                $trainingsession->setVatCalc($vatcalc);

                // get linked to value
                $linkedTo = $request->request->all()['linkedTo'];
                $trainingsession->setLinkedTo($linkedTo); 

                // save the trainingsession
                $entityManager->persist($trainingsession);
                $entityManager->flush();
            	
                $this->addFlash(
                    'success_message','Other updated succesfully.'
                );
                $this->addFlash('other_parameter','other');

                return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
            }
            
            $trainigsessiondata = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));

            return $this->render(
                'DrivingSchoolSchoolBundle:TrainingSessions:othercreate.html.twig',
                array('form' => $form->createView(),
                    'trainigsessions' => $trainigsessiondata,
                    'linkedTo' => $trainingsession->getLinkedTo(),
                    'pricepackageid' => $pricepackageid,
                    'title' => 'Edit Other',
                    'btn_title' => 'Update',
                )
            );
        } else {
            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }
    }

    public function packageCreateAction(Request $request, $pricepackageid, UserInterface $user)
    {
    	$userId = $user->getId();
        
    	// build the form
        $packages = new TrainingSessionsEntity();
        $form = $this->createForm(PackagesForm::class, $packages);

        $entityManager = $this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            // echo "<pre>";
            // print_r($data);exit;
            $sum = 0;
            foreach ($data['data'] as $value) {
                // print_r($value);
                if($value['incl_vat'] == '1')
                {
                    $vatcalc = $value['price']*$value['attribute'];
                }
                else if($value['incl_vat'] == '')
                {    
                    $price = $value['price'];
                    $vat = $value['vat'];
                    $vatcalc = $price+(($vat / 100) * ($price*$value['attribute']));
                }
                $sum+= $vatcalc;
                
            }
            $totalprice = $sum;

            $trainingsession_detail = json_encode($data['data']);
            $packages->setTrainingsessionDetail($trainingsession_detail);

            // get drivingschool id
            $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $packages->setDrivingSchool($drivingschool); 

            // get pricepackages id
            $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($pricepackageid);
            $packages->setPricePackageId($priceandpackage); 

            // add type
            $packages->setType('3');

            // set calculation
            $packages->setPackageCalc($totalprice);

            $entityManager->persist($packages);
            $entityManager->flush();

            $this->addFlash(
                'success_message','Packages added succesfully.'
            );
            $this->addFlash('packages_parameter','packages');

            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }
        
        return $this->render(
            'DrivingSchoolSchoolBundle:TrainingSessions:packagescreate.html.twig',
            array('form' => $form->createView(),
                    'pricepackageid' => $pricepackageid,
                    'title' => 'Create Package',
                    'btn_title' => 'Create',
                )
        );
    }

    public function trainingsessionAjaxcallAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $pricepackageid = $request->request->all()['priceid'];
        $counter = $request->request->all()['counter'];
        // echo $pricepackageid;
        // exit; 

        $entityManager = $this->getDoctrine()->getManager();
        $trainigsessiondatas = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));
        // dump($trainigsessiondatas);exit;
        $html = "";
        $html.= "<div class=''>";
            $html.= "<input type='text' name='data[".$counter."][attribute]' id='qty_".$counter."' required value='1' class='attribute'>";
        $data = array();
        foreach ($trainigsessiondatas as $trainigsessiondata) {
            $data[$trainigsessiondata->getType()][] = $trainigsessiondata;
        }
            
        $html.="<select name='data[".$counter."][linkedto]' id='linkedto' required>";
        $html.="<option value=''>Select</option>";
        foreach ($data as $key => $value) {
            switch ($key) {
                case 0:
                    $html.="<optgroup label='Driving Lesson'>";
                    break;

                case 1:
                    $html.="<optgroup label='Examination And Tests'>";
                    break;

                case 2:
                    $html.="<optgroup label='Other'>";
                    break;
            }
            
            foreach ($value as $trainigsession) {
                if($trainigsession->getType() != '3')
                {
                    $html.="<option value='".$trainigsession->getId()."'>".$trainigsession->getName()."</option>";
                }
            }
            $html.="</optgroup>";
        }
        $html.= "<input type='text' name='data[".$counter."][price]' id='price_".$counter."' required class='price'>";
            $html.= "<div class='rdbutton'>
            <input type='checkbox' name='data[".$counter."][incl_vat]' id='inclvat_".$counter."' class='inclvat' value='1'><label>incl. vat</label></div>";
            $html.= "<select name='data[".$counter."][vat]' class='vattax'><option value='21'>21% VAT</option><option value='19'>19% VAT</option><option value='6'>6% VAT</option><option value='0'>No Tax</option></select>";
            $html.= "<input class='btn btn-danger removeNext' type='button' id='removeNext' value=''/><i class='fa fa-times' aria-hidden='true'></i>";
        $html.= "</div>";
        $html.="</select>";
        $resp = array("response" => $html);
        return new Response(json_encode($resp));
    }

    public function ajaxcallAction(Request $request)
    {
        $id = $request->request->all()['id'];

        $entityManager = $this->getDoctrine()->getManager();
        $trainingsession = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->find($id);

        $array = [];
        $array['price'] = $trainingsession->getPrice();
        $array['includedVat'] = $trainingsession->getIncludedVat();
        $array['vat'] = $trainingsession->getVat();
        $array['vat_calc'] = $trainingsession->getVatCalc();
        $array['per'] = $trainingsession->getQty();
        $array['unit'] = $trainingsession->getUnit();

        echo json_encode($array);exit;
    }

    public function packageEditAction(Request $request, $pricepackageid, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        // build the form
        $entityManager = $this->getDoctrine()->getManager();
        $trainingsessions = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid, 'type' => '3'));
        $trainingsession = $trainingsessions[0];
        if(!empty($trainingsession))
        {
            $packages = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->find($id);
            $trainingsessiondetails = json_decode($packages->getTrainingsessionDetail(), true);

            $form = $this->createForm(PackagesForm::class, $trainingsession);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $request->request->all();
                // echo "<pre>";
                // print_r($data);exit;
                $sum = 0;
                foreach ($data['data'] as $value) {
                    // print_r($value['price']);
                    
                    if($value['incl_vat'] == '1')
                    {
                        $vatcalc = $value['price']*$value['attribute'];
                    }
                    else if($value['incl_vat'] == '')
                    {    
                        $price = $value['price'];
                        $vat = $value['vat'];
                        $vatcalc = $price+(($vat / 100) * ($price*$value['attribute']));
                    }
                    $sum += $vatcalc;
                    
                }
                $totalprice = $sum;

                $trainingsession_detail = json_encode($data['data']);
                $packages->setTrainingsessionDetail($trainingsession_detail);

                $packages->setPackageCalc($totalprice);

                $entityManager->persist($packages);
                $entityManager->flush();

                $this->addFlash(
                    'success_message','Packages updated succesfully.'
                );
                $this->addFlash('packages_parameter','packages');

                return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
            }

            $trainigsessiondata = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findBy(array('DrivingSchool' => $userId, 'pricePackageId' => $pricepackageid));

            return $this->render(
                'DrivingSchoolSchoolBundle:TrainingSessions:packagescreate.html.twig',
                array('form' => $form->createView(),
                        'trainingsessiondetails' => $trainingsessiondetails,
                        'pricepackageid' => $pricepackageid,
                        'trainigsessiondata' => $trainigsessiondata,
                        'title' => 'Edit Package',
                        'btn_title' => 'Update',
                    )
            );
        } else {
            return $this->redirectToRoute('driving_school_school_trainingSessions', array('id'=> $pricepackageid));
        }
    }
}