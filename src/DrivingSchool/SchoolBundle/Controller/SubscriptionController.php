<?php

namespace DrivingSchool\SchoolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use DrivingSchool\SchoolBundle\Form\SubscriptionForm;
use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;

class SubscriptionController extends Controller
{
	public function viewAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
    	$em = $this->getDoctrine()->getManager();
    	$schoolEntity = $em->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
    	$subscriptions = $schoolEntity->getSubscriptions();
    	$subscriptionDate ="";
    	if(!empty($subscriptions)) {
    		$subscriptionDate = $schoolEntity->getSubscriptionDate();
    	}
    	
    	$data = [
            'subscription_plan' => $subscriptions,
            'subscription_date' => $subscriptionDate
    	];

    	return $this->render('DrivingSchoolSchoolBundle:Subscription:view.html.twig', $data);
    }

    public function subscribeAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
	    $em = $this->getDoctrine()->getManager();
    	$drivingSchool = $em->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
    	$packagePrice ="";
    	if(!empty($drivingSchool->getSubscriptions())) {
    		$packagePrice = $drivingSchool->getSubscriptions()->getPackagePrice();
    	}
    	
    	$form = $this->createForm(SubscriptionForm::class, $drivingSchool, array('userId' => $userId));
    	$form->handleRequest($request);
    	if ($form->isSubmitted() && $form->isValid()) {
    		$data = $form->getData();
	    	$em->persist($data);
            $em->flush();

            $this->addFlash(
                'success',
                'Subscription plan upgraded succesfully.'
            ); 
	    	return $this->redirectToRoute('driving_school_school_subscription');
	    }

    	return $this->render('DrivingSchoolSchoolBundle:Subscription:subscribe.html.twig',
    		array('form' => $form->createView(), 'package_price' => $packagePrice));
    }

    public function getPackagePriceAction(Request $request) {
        $packageId = $request->get('package_id');
        if(!empty($packageId)) {
            $em = $this->getDoctrine()->getManager();
            $subscriptionPlan = $em->getRepository('DrivingSchoolAdminBundle:SubscriptionPlanEntity')->find($packageId);
            $packagePrice = $subscriptionPlan->getPackagePrice();
            return new Response(json_encode(array('response' => $packagePrice)));
        }
    }
}