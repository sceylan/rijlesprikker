<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\InstructorForm;
use DrivingSchool\AdminBundle\Entity\InstructorEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Application\Sonata\MediaBundle\Entity\Media;

class InstructorController extends Controller
{
    // public function __construct(Request $request)
    // {
    //     parent::__construct();
    //     echo $request->getSession()->get('user_id');exit;
    // }

    public function listAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
    	$em = $this->getDoctrine()->getManager();
    	$instructors = $em->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->findBy(array('DrivingSchool' => $userId));

    	$data = [
    		'instructors' => $instructors,
    	];
// dump($data);exit;
        return $this->render('DrivingSchoolSchoolBundle:Instructor:list.html.twig', $data);
    }

    private function generateRandomString() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomPassword = '';
            
        for ($i = 0; $i < 5; $i++)
            $randomPassword .= $characters[mt_rand(0, 61)]; // Send this password in email

        return $randomPassword;
    }

    public function createAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
    
    	$instructor = new InstructorEntity();
    	$form = $this->createForm(InstructorForm::class, $instructor, array('userId' => $userId));

    	$form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $instructor->setDrivingSchool($drivingschool); 

            $realPassword = $this->generateRandomString();
            $salt = $this->generateRandomString();
            $encoder = new MessageDigestPasswordEncoder();
            // dump($data['password']['first']);exit;
            $password = $encoder->encodePassword($realPassword, $salt);
            $instructor->setPassword($password);
            $instructor->setSalt($salt);

            $mediaManager = $this->get('sonata.media.manager.media');

            foreach ($request->files as $uploadedFile) {
                if(!empty($uploadedFile['instructorImage']['binaryContent'])) {
                    $media = new Media();
                    $media->setBinaryContent($uploadedFile['instructorImage']['binaryContent']);
                    $media->setContext("default");
                    $media->setProviderName('sonata.media.provider.image');
                    $mediaManager->save($media);
                    $instructor->setInstructorImage($media);
                }
            } 
            $entityManager->persist($instructor);
            $entityManager->flush();

            $schoolusername = $instructor->getDrivingSchool()->getUsername();
            $domainname = str_replace(' ', '', $schoolusername).'.'.$this->container->getParameter('domain').'/login';

            /* Start Send Mail*/
            $mailer = $this->get('mailer');
            $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '4'));
            $mailsubject = $emailtmp->getSubject();
            $content = $emailtmp->getContent();

            $template = $this->get('twig')->createTemplate($content);
            $mailcontent = $template->render(array('name'=>$instructor->getInstructorName(), 'username' => $instructor->getUsername(), 'password' => $realPassword, 'Link' => $domainname, 'schoolname' => $instructor->getDrivingSchool()->getSchoolName(), 'schooladdress' => $instructor->getDrivingSchool()->getSchoolAddress(), 'schoolphone' => $instructor->getDrivingSchool()->getSchoolPhone()));

            $message = (new \Swift_Message($mailsubject))
            ->setFrom('admin@drivingschool.com')
            ->setTo($instructor->getEmail())
            ->setBody($mailcontent,'text/html');

            $mailer->send($message);
            /* Enad Send Mail */

            $this->addFlash(
                'success',
                'Instructor added succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_list_instructor');
        }

    	return $this->render(
			'DrivingSchoolSchoolBundle:Instructor:create.html.twig',
            array('form' => $form->createView(),'title' => 'Create Instructor', 'btn_title' => 'Create')
		);	
    }

    public function editAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $instructor = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->findOneBy(array('id'=>$id, 'DrivingSchool' => $userId));
        if(!empty($instructor))
        {
        	$form = $this->createForm(InstructorForm::class, $instructor, array('userId' => $userId));

        	$form->handleRequest($request);
        	if($form->isSubmitted() && $form->isValid()) {

                $mediaManager = $this->get('sonata.media.manager.media');
                foreach ($request->files as $uploadedFile) {
                    if(!empty($uploadedFile['instructorImage']['binaryContent'])) {
                        $media = new Media();
                        $media->setBinaryContent($uploadedFile['instructorImage']['binaryContent']);
                        $media->setContext("default");
                        $media->setProviderName('sonata.media.provider.image');
                        $mediaManager->save($media);
                        $instructor->setInstructorImage($media);
                    }
                }

        		$entityManager->persist($instructor);
                $entityManager->flush();

                $this->addFlash(
                    'success',
                    'Instructor updated succesfully.'
                ); 
                return $this->redirectToRoute('driving_school_school_list_instructor');
        	}

        	return $this->render(
                'DrivingSchoolSchoolBundle:Instructor:create.html.twig',
                array('form' => $form->createView(),
                      'id'   => $instructor->getId(),
                      'title' => 'Edit Instructor', 
                      'btn_title' => 'Update')
            );   
        } else {
            return $this->redirectToRoute('driving_school_school_list_instructor');
        }
    }

    public function deleteAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $instructor = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->find($id);

        if (!$instructor) { 
            // no instructor in the system
            throw $this->createNotFoundException(
                'No price & packages found for id '.$id
            );
        } else {
            $entityManager->remove($instructor);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Instructor deleted succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_list_instructor');
        }
    }
}
