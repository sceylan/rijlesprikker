<?php

namespace DrivingSchool\SchoolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use DrivingSchool\SchoolBundle\Form\PlanningForm;
use DrivingSchool\AdminBundle\Entity\PlanningEntity;
use DrivingSchool\AdminBundle\Entity\NotificationEntity;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlanningController extends Controller
{
    public function viewPlanningAction(Request $request, UserInterface $user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $instructorsData = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->findBy(array('DrivingSchool' => $user->getId()), array('instructorName' => 'ASC'));
        $permissions = array();

        if(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) 
        {
            $permissions = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesAndPermissionEntity')->findOneBy(array('Instructor' => $user->getId(), 'rolesModule' => '1'));
        }

        return $this->render('DrivingSchoolSchoolBundle:Planning:view.html.twig', array('instructors' => $instructorsData, 'permissions' => $permissions));
    }

    public function getAllEventsAction(Request $request, UserInterface $user)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $instructorsData;
        $calendarData = array();
        $calendarData['resources'] = array();
        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) {
            $instructorIds = $request->get("instructor_ids");
            if($instructorIds) {
                $instructorsData = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->findBy(array('DrivingSchool' => $user->getId(), 'id' => $instructorIds));
            } else {
                $instructorsData = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->findBy(array('DrivingSchool' => $user->getId()));
            }
            foreach ($instructorsData as $instructorData) {
                // resources
                $resourceId     = $instructorData->getId();
                $resourceTitle  = $instructorData->getInstructorName();

                $calendarData['resources'][$resourceId] = array(
                    "id" => $resourceId, 
                    "title" => $resourceTitle
                ); 
            }
        } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
            $instructorsData = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->find($user->getId());
            $resourceId     = $instructorsData->getId();
            $resourceTitle  = $instructorsData->getInstructorName();

            $calendarData['resources'][$resourceId] = array(
                "id" => $resourceId, 
                "title" => $resourceTitle
            ); 
        }


        $eventsData;


        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) {
            $eventsData = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(array('school' => $user->getId()));
        } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
            $eventsData = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(array('instructor' => $user->getId()));
        }

        $calendarData['events'] = array();
        if(!empty($eventsData)) {
            foreach ($eventsData as $eventData) {
                //events
                $eventId            = $eventData->getId();
                $start              = date('Y-m-d\TH:i', $eventData->getStartDate()->getTimestamp());
                $end                = date('Y-m-d\TH:i', $eventData->getEndDate()->getTimestamp());
                $eventDescription   = $eventData->getDescription();
                
                $eventPricePackage  = "";
                if(!empty($eventData->getPricePackage())) {
                    $eventPricePackage  = $eventData->getPricePackage()->getName();
                }
                $eventStudent       = "";
                if(!empty($eventData->getStudent())) {
                    $eventStudent   = $eventData->getStudent()->getFirstName();
                }
                $eventInstructor    = "";
                if(!empty($eventData->getInstructor())) {
            	    $eventRresourceId   = $eventData->getInstructor()->getId();
                    $eventInstructor    = $eventData->getInstructor()->getInstructorName();
                }
                $eventVehicle       = "";
                if(!empty($eventData->getVehicle())) {
                    $eventVehicle   = $eventData->getVehicle()->getLicensePlate();
                }
                $eventBackgroundColor = "";
                if(!empty($eventData->getTrainingSession())) {
                    $eventBackgroundColor = $eventData->getTrainingSession()->getAppointmentLabel();
                }

                $isReservation = $eventData->getIsReservation();
                
    			$eventLocation		= $eventData->getLocation();

    			$calendarData['events'][] = array(
    				"id"=> $eventId, 
    				"resourceId"=> $eventRresourceId, 
    				"start"=> $start, 
    				"end"=> $end, 
    				"description" => $eventDescription, 
                    "price_package" => $eventPricePackage, 
    				"student" => $eventStudent, 
    				"instructor" => $eventInstructor, 
    				"vehicle" => $eventVehicle, 
    				"location" => $eventLocation,
                    "backgroundColor" => $eventBackgroundColor,
                    "is_reservation" => $isReservation,
    			);
            }
        }

        $calendarData['resources'] = array_values($calendarData['resources']);
        $response = array('response' => $calendarData);
        return new Response(json_encode($response));
    }

    public function getEditFormAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $eventId = $request->get("event_id");
        $entityManager = $this->getDoctrine()->getManager();

        if(!empty($eventId)) {
            if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
            {
                $eventsData = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('school' => $userId, 'id' => $eventId));
                $usertypeid = '1';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
                $eventsData = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('instructor' => $userId, 'id' => $eventId));
                $usertypeid = '2';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            }

            if(!empty($eventsData)) {
                $form = $this->createForm(PlanningForm::class, $eventsData, array('userId' => $userId, 'entity_manager' => $entityManager));
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $data = $form->getData();
                    $entityManager->persist($data);
                    $entityManager->flush();

                    /* Start notification */
                    $notification = new NotificationEntity();
                    $notification->setSchool($eventsData->getSchool());
                    $notification->setStudent($eventsData->getStudent());
                    $notification->setInstructor($eventsData->getInstructor());
                    $notification->setPackageName($eventsData->getPricePackage()->getName());
                    $notification->setTrainingSessionName($eventsData->getTrainingSession()->getName());
                    $notification->setEventStartDate($eventsData->getStartDate());
                    $notification->setEventEndDate($eventsData->getEndDate());
                    $notification->setUserId($userId);
                    $notification->setUserType($usertype);
                    $notification->setAction('Updated');
                    $notification->setSchoolReadStatus(0);
                    $notification->setInstructorReadStatus(0);
                    $notification->setStudentReadStatus(0);
                    $notification->setNotificationType('Event');
                    $entityManager->persist($notification);
                    $entityManager->flush();
                    /* End Notification */

                    /* Start Mail Send */
                    $mailer = $this->get('mailer');
                    $packagename = $eventsData->getPricePackage()->getName();
                    $coursename = $eventsData->getTrainingSession()->getName();
                    $eventdate = date('d M, Y', $eventsData->getStartDate()->getTimestamp());
                    $eventtime = date('g:i, A', $eventsData->getStartDate()->getTimestamp()).' - '.date('g i, A', $eventsData->getEndDate()->getTimestamp());
                    $createdatetime = date('M d', $eventsData->getCreation()->getTimestamp()).' at '.date('g:i A', $eventsData->getCreation()->getTimestamp());

                    $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '6'));
                    $subject = $emailtmp->getSubject();
                    $content = $emailtmp->getContent();
                    $tempsubject = $this->get('twig')->createTemplate($subject);
                    $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

                    $names = array(0 => array("name" => $eventsData->getSchool()->getSchoolName(), "email" => $eventsData->getSchool()->getEmail()),1 => array("name" => $eventsData->getInstructor()->getInstructorName(), "email" => $eventsData->getInstructor()->getEmail()), 2 => array("name" => $eventsData->getStudent()->getFirstName(), "email" => $eventsData->getStudent()->getEmail()));
                    foreach($names as $data){
                        $template = $this->get('twig')->createTemplate($content);
                        $mailcontent = $template->render(array('name'=> $data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $eventsData->getInstructor()->getInstructorName(), 'schoolname' => $eventsData->getSchool()->getSchoolName(), 'schooladdress' => $eventsData->getSchool()->getSchoolAddress(), 'schoolphone' => $eventsData->getSchool()->getSchoolPhone()));

                        $message = (new \Swift_Message($mailsubject))
                        ->setFrom('admin@drivingschool.com')
                        ->setTo($data['email'])
                        ->setBody($mailcontent,'text/html');
                        $mailer->send($message);
                    }

                    /* End Mail Send */
                    return $this->getAllEventsAction($request, $user);
                }

                $permissions = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesAndPermissionEntity')->findOneBy(array('Instructor' => $user->getId(), 'rolesModule' => '1'));

                $resp =  $this->render(
                    'DrivingSchoolSchoolBundle:Planning:editplanning.html.twig',
                    array('form' => $form->createView(), 'event_id' => $eventId, 'permissions' => $permissions)
                )->getContent();
                
                return new Response(json_encode(array('response' => $resp)));
            }
        }
        return $this->getAllEventsAction($request, $user);
    }

    public function createAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $event = new PlanningEntity();
        $entityManager = $this->getDoctrine()->getManager();
        if(!empty($request->get("instructor_id"))) {
            $instructorId = $request->get("instructor_id");
            $instructor = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->find($instructorId);
            $event->setInstructor($instructor);
        }

        if(!empty($request->get("startdatetime"))) {
            $startDateTime = $request->get("startdatetime");
            $time = strtotime($startDateTime);
            $startDateTime = date('Y-m-d H:i:s',$time);
            $event->setStartDate(new \DateTime($startDateTime));
        }

        if(!empty($request->get("enddatetime"))) {
            $endDateTime = $request->get("enddatetime");
            $time = strtotime($endDateTime);
            $endDateTime = date('Y-m-d H:i:s',$time);
            $event->setEndDate(new \DateTime($endDateTime));
        }
        
        if(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) 
        {
           $school = $user->getDrivingSchool();
           $usertypeid = '2';
           $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
        } elseif (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) 
        {
            $school = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $usertypeid = '1';
            $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
        }

        $event->setSchool($school);
        $form = $this->createForm(PlanningForm::class, $event, array('userId' => $userId, 'entity_manager' => $entityManager));
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $entityManager->persist($data);
            $entityManager->flush();

            /* Start notification */
                $notification = new NotificationEntity();
                $notification->setSchool($event->getSchool());
                $notification->setStudent($event->getStudent());
                $notification->setInstructor($event->getInstructor());
                $notification->setPackageName($event->getPricePackage()->getName());
                $notification->setTrainingSessionName($event->getTrainingSession()->getName());
                $notification->setEventStartDate($event->getStartDate());
                $notification->setEventEndDate($event->getEndDate());
                $notification->setUserId($userId);
                $notification->setUserType($usertype);
                $notification->setAction('Added');
                $notification->setSchoolReadStatus(0);
                $notification->setInstructorReadStatus(0);
                $notification->setStudentReadStatus(0);
                $notification->setNotificationType('Event');
                $entityManager->persist($notification);
                $entityManager->flush();
            /* End Notification */

            /* Start Mail Send */
            $mailer = $this->get('mailer');
            $packagename = $event->getPricePackage()->getName();
            $coursename = $event->getTrainingSession()->getName();
            $eventdate = date('d M, Y', $event->getStartDate()->getTimestamp());
            $eventtime = date('g:i, A', $event->getStartDate()->getTimestamp()).' - '.date('g i, A', $event->getEndDate()->getTimestamp());
            $createdatetime = date('M d', $event->getCreation()->getTimestamp()).' at '.date('g:i A', $event->getCreation()->getTimestamp());

            $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '5'));
            $subject = $emailtmp->getSubject();
            $content = $emailtmp->getContent();
            $tempsubject = $this->get('twig')->createTemplate($subject);
            $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

            $template = $this->get('twig')->createTemplate($content);

            $names = array(0 => array("name" => $event->getSchool()->getSchoolName(), "email" => $event->getSchool()->getEmail()),1 => array("name" => $event->getInstructor()->getInstructorName(), "email" => $event->getInstructor()->getEmail()), 2 => array("name" => $event->getStudent()->getFirstName(), "email" => $event->getStudent()->getEmail()));
            foreach($names as $data){
                $mailcontent = $template->render(array('name'=> $data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $event->getInstructor()->getInstructorName(), 'schoolname' => $event->getSchool()->getSchoolName(), 'schooladdress' => $event->getSchool()->getSchoolAddress(), 'schoolphone' => $event->getSchool()->getSchoolPhone()));
                
                $message = (new \Swift_Message($mailsubject))
                ->setFrom('admin@drivingschool.com')
                ->setTo($data['email'])
                ->setBody($mailcontent,'text/html');
                $mailer->send($message);
            } 
            /* End Mail Send */

            return $this->getAllEventsAction($request, $user);
        } else {
            $resp =  $this->render(
                'DrivingSchoolSchoolBundle:Planning:createplanning.html.twig',
                array('form' => $form->createView())
            )->getContent();
            
            return new Response(json_encode(array('response' => $resp)));
        }
    }

    public function deleteAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $eventId = $request->get("event_id");

        $entityManager = $this->getDoctrine()->getManager();

        if(!empty($eventId)) {
            if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
            {
                $eventsData = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('school' => $userId, 'id' => $eventId));
                $usertypeid = '1';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
                $eventsData = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('instructor' => $userId, 'id' => $eventId));
                $usertypeid = '2';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            }

            if(!empty($eventsData)) {
                    /* Start notification */
                    $notification = new NotificationEntity();
                    $notification->setSchool($eventsData->getSchool());
                    $notification->setStudent($eventsData->getStudent());
                    $notification->setInstructor($eventsData->getInstructor());
                    $notification->setPackageName($eventsData->getPricePackage()->getName());
                    $notification->setTrainingSessionName($eventsData->getTrainingSession()->getName());
                    $notification->setEventStartDate($eventsData->getStartDate());
                    $notification->setEventEndDate($eventsData->getEndDate());
                    $notification->setUserId($userId);
                    $notification->setUserType($usertype);
                    $notification->setAction('Deleted');
                    $notification->setSchoolReadStatus(0);
                    $notification->setInstructorReadStatus(0);
                    $notification->setStudentReadStatus(0);
                    $notification->setNotificationType('Event');
                    $entityManager->persist($notification);
                    $entityManager->flush();
                    /* End Notification */

                    /* Start Mail Send */
                    $mailer = $this->get('mailer');
                    $packagename = $eventsData->getPricePackage()->getName();
                    $coursename = $eventsData->getTrainingSession()->getName();
                    $eventdate = date('d M, Y', $eventsData->getStartDate()->getTimestamp());
                    $eventtime = date('g:i, A', $eventsData->getStartDate()->getTimestamp()).' - '.date('g i, A', $eventsData->getEndDate()->getTimestamp());
                    $createdatetime = date('M d', $eventsData->getCreation()->getTimestamp()).' at '.date('g:i A', $eventsData->getCreation()->getTimestamp());

                    $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '7'));
                    $subject = $emailtmp->getSubject();
                    $content = $emailtmp->getContent();
                    $tempsubject = $this->get('twig')->createTemplate($subject);
                    $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

                    $names = array(0 => array("name" => $eventsData->getSchool()->getSchoolName(), "email" => $eventsData->getSchool()->getEmail()),1 => array("name" => $eventsData->getInstructor()->getInstructorName(), "email" => $eventsData->getInstructor()->getEmail()), 2 => array("name" => $eventsData->getStudent()->getFirstName(), "email" => $eventsData->getStudent()->getEmail()));
                    foreach($names as $data){
                        $template = $this->get('twig')->createTemplate($content);
                        $mailcontent = $template->render(array('name'=>$data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $eventsData->getInstructor()->getInstructorName(), 'schoolname' => $eventsData->getSchool()->getSchoolName(), 'schooladdress' => $eventsData->getSchool()->getSchoolAddress(), 'schoolphone' => $eventsData->getSchool()->getSchoolPhone()));

                        $message = (new \Swift_Message($mailsubject))
                        ->setFrom('admin@drivingschool.com')
                        ->setTo($data['email'])
                        ->setBody($mailcontent,'text/html');
                        $mailer->send($message);
                    }

                    /* End Mail Send */

                $entityManager->remove($eventsData);
                $entityManager->flush();
            }
        }
        return $this->getAllEventsAction($request, $user);
    }

    public function updateAction(Request $request, UserInterface $user)
    {
        $userId         = $user->getId();
        $eventId        = $request->get("event_id");

        $entityManager = $this->getDoctrine()->getManager();
        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
        {
            $event = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('school' => $userId, 'id' => $eventId));
            $usertypeid = '1';
            $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
        } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
            $event = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('instructor' => $userId, 'id' => $eventId));
            $usertypeid = '2';
            $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
        }

        if(!empty($request->get("start_time"))) {
            $startTime      = $request->get("start_time");
            $time = strtotime($startTime);
            $startTime = date('Y-m-d H:i:s',$time);
            $event->setStartDate(new \DateTime($startTime));
        }

        if(!empty($request->get("end_time"))) {
            $endTime = $request->get("end_time");
            $time = strtotime($endTime);
            $endTime = date('Y-m-d H:i:s',$time);
            $event->setEndDate(new \DateTime($endTime));
        }

        if(!empty($request->get("instructor_id"))) {
            $instructorId   = $request->get("instructor_id");
            $instructor = $entityManager->getRepository('DrivingSchoolAdminBundle:InstructorEntity')->find($instructorId);
            $event->setInstructor($instructor);
        }

        $entityManager->persist($event);
        $entityManager->flush();

        /* Start notification */
        $notification = new NotificationEntity();
        $notification->setSchool($event->getSchool());
        $notification->setStudent($event->getStudent());
        $notification->setInstructor($event->getInstructor());
        $notification->setPackageName($event->getPricePackage()->getName());
        $notification->setTrainingSessionName($event->getTrainingSession()->getName());
        $notification->setEventStartDate($event->getStartDate());
        $notification->setEventEndDate($event->getEndDate());
        $notification->setUserId($userId);
        $notification->setUserType($usertype);
        $notification->setAction('Updated');
        $notification->setSchoolReadStatus(0);
        $notification->setInstructorReadStatus(0);
        $notification->setStudentReadStatus(0);
        $notification->setNotificationType('Event');
        $entityManager->persist($notification);
        $entityManager->flush();
        /* End Notification */

            /* Start Mail Send */
            $mailer = $this->get('mailer');
            $packagename = $event->getPricePackage()->getName();
            $coursename = $event->getTrainingSession()->getName();
            $eventdate = date('d M, Y', $event->getStartDate()->getTimestamp());
            $eventtime = date('g:i, A', $event->getStartDate()->getTimestamp()).' - '.date('g i, A', $event->getEndDate()->getTimestamp());
            $createdatetime = date('M d', $event->getCreation()->getTimestamp()).' at '.date('g:i A', $event->getCreation()->getTimestamp());

            $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '6'));
            $subject = $emailtmp->getSubject();
            $content = $emailtmp->getContent();
            $tempsubject = $this->get('twig')->createTemplate($subject);
            $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

            $names = array(0 => array("name" => $event->getSchool()->getSchoolName(), "email" => $event->getSchool()->getEmail()),1 => array("name" => $event->getInstructor()->getInstructorName(), "email" => $event->getInstructor()->getEmail()), 2 => array("name" => $event->getStudent()->getFirstName(), "email" => $event->getStudent()->getEmail()));
                    
                foreach($names as $data){
                    $template = $this->get('twig')->createTemplate($content);
                    $mailcontent = $template->render(array('name'=>$data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $event->getInstructor()->getInstructorName(), 'schoolname' => $event->getSchool()->getSchoolName(), 'schooladdress' => $event->getSchool()->getSchoolAddress(), 'schoolphone' => $event->getSchool()->getSchoolPhone()));

                    
                    $message = (new \Swift_Message($mailsubject))
                    ->setFrom('admin@drivingschool.com')
                    ->setTo($data['email'])
                    ->setBody($mailcontent,'text/html');
                    $mailer->send($message);
                }

            /* End Mail Send */

        return $this->getAllEventsAction($request, $user);
    }

    public function listTrainingSessionOfPricePackagesAction(Request $request, UserInterface $user)
    {
        $userId         = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $trainingSessionRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:TrainingSessionsEntity");
        $studentRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:StudentEntity");
        $vehicleRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:VehicleEntity");
        $instructorRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:InstructorEntity");


        // Search the Training Session that belongs to the Price and Package with the given id as GET parameter "pricePackageId"
        $params = array();
        if(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) 
        {
            $school = $user->getDrivingSchool();   
            $schoolUserId = $school->getId();
            $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $schoolUserId);
        } elseif (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) 
        {
            $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $userId);
        }

        $trainingSessions = $trainingSessionRepository->createQueryBuilder("q")
            ->where("q.pricePackageId = :pricePackageId")
            ->andWhere("q.DrivingSchool = :userId")
            ->andWhere("q.type IN ( 0,1 )")
            ->setParameters($params)
            ->getQuery()
            ->getResult();
            
        $trainingSessionResponseArray = array();
        foreach($trainingSessions as $trainingSession){
            $trainingSessionResponseArray[] = array(
                "id" => $trainingSession->getId(),
                "name" => $trainingSession->getName()
            );
        }


        $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $userId);
        $students;
        if(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) 
        {
            $students = $studentRepository->createQueryBuilder("q")
                ->innerJoin('q.educations', 'p')
                ->where("p.id = :pricePackageId")
                ->andWhere("q.instructor = :userId")
                ->setParameters($params)
                ->getQuery()
                ->getResult();
        } elseif (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) 
        {
            $students = $studentRepository->createQueryBuilder("q")
                ->innerJoin('q.educations', 'p')
                ->where("p.id = :pricePackageId")
                ->andWhere("q.DrivingSchool = :userId")
                ->setParameters($params)
                ->getQuery()
                ->getResult();
        }


        $studentResponseArray = array();
        foreach($students as $student){
            $studentname = $student->getFirstName().' '.$student->getLastName();
            $studentResponseArray[] = array(
                "id" => $student->getId(),
                "name" => $studentname
            );
        }


        $params = array();
        if(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) 
        {
            $school = $user->getDrivingSchool();   
            $schoolUserId = $school->getId();
            $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $schoolUserId);
        } elseif (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) 
        {
            $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $userId);
        }

        $vehicles = $vehicleRepository->createQueryBuilder("q")
            ->innerJoin('q.suitableTrainingCourses', 'p')
            ->where("p.id = :pricePackageId")
            ->andWhere("q.DrivingSchool = :userId")
            ->setParameters($params)
            ->getQuery()
            ->getResult();

        $vehicleResponseArray = array();
        foreach($vehicles as $vehicle){
            $vehicleResponseArray[] = array(
                "id" => $vehicle->getId(),
                "name" => $vehicle->getLicensePlate()
            );
        }


        $params = array("pricePackageId" => $request->get("price_package_id"), "userId" => $userId);
        $instructors;
        if(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) 
        {
            $instructors = $instructorRepository->createQueryBuilder("q")
                ->innerJoin('q.authorizedFor', 'p')
                ->where("p.id = :pricePackageId")
                ->andWhere("q.id = :userId")
                ->setParameters($params)
                ->getQuery()
                ->getResult();
        } elseif (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) 
        {
            $instructors = $instructorRepository->createQueryBuilder("q")
                ->innerJoin('q.authorizedFor', 'p')
                ->where("p.id = :pricePackageId")
                ->andWhere("q.DrivingSchool = :userId")
                ->setParameters($params)
                ->getQuery()
                ->getResult();
        }


        $instructorResponseArray = array();
        foreach($instructors as $instructor){
            $instructorResponseArray[] = array(
                "id" => $instructor->getId(),
                "name" => $instructor->getInstructorName()
            );
        }

        $responseArray = array(
            "trainingSessions" => $trainingSessionResponseArray,
            "students" => $studentResponseArray,
            "vehicles" => $vehicleResponseArray,
            "instructors" => $instructorResponseArray
        );

        return new JsonResponse($responseArray);
    }

    public function getPlanningLengthAction(Request $request, UserInterface $user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $trainingSessionRepository = $entityManager->getRepository("DrivingSchoolAdminBundle:TrainingSessionsEntity")->find($request->get("training_session_id"));
        $data = ["length"=> "0:30"];
        if(!empty($trainingSessionRepository->getLengthQty()))
        {
            $data["length"]= $trainingSessionRepository->getLengthQty();
        }

        return new JsonResponse($data);
    }
}