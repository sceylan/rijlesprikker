<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\PriceAndPackagesForm;
use DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class PriceAndPackagesController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
    
        $entityManager = $this->getDoctrine()->getManager();
        $PriceAndPackages = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(array('DrivingSchool' => $userId));

        $icons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->findAll();
   
        $data = [
            'PriceAndPackages' => $PriceAndPackages,
            'icons' => $icons
        ];

        return $this->render('DrivingSchoolSchoolBundle:PriceAndPackages:list.html.twig', $data);
    }

    public function createAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
    	// build the form
        $priceandpackages = new PriceAndPackagesEntity();
        $form = $this->createForm(PriceAndPackagesForm::class, $priceandpackages);
        $entityManager = $this->getDoctrine()->getManager();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if($form->isValid()) 
            {
                // get drivingschool id
                $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
                $priceandpackages->setDrivingSchool($drivingschool); 

               // get icon detail and set in entity
                $icon_id = $request->request->get('icon_id');
                try {
                    $priceicon = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->find($icon_id);
                    $priceandpackages->setIcon($priceicon); 

                    // save the priceandpackages
                    $entityManager->persist($priceandpackages);
                    $entityManager->flush();
                } catch (UniqueConstraintViolationException $e) {
                    if(preg_match('/\bunique_name\b/i', $e->getMessage())) {
                        /*dump('Package name "' . $priceandpackages->getName() . '" is already taken');
                        exit;*/
                        $this->addFlash(
                            'error',
                            'Price Package name <b>'.$priceandpackages->getName().'</b> already taken...'
                        ); 
                    } else {
                        $this->addFlash(
                            'error',
                            'Error processing your request'
                        ); 
                    }
                    
                    return $this->redirectToRoute('driving_school_school_create_priceandpackages');
                }

                $this->addFlash(
                    'success',
                    'Price & packages added succesfully.'
                ); 
                return $this->redirectToRoute('driving_school_school_list_priceandpackages');
            }
        }
        $icons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->findAll();
        
        return $this->render(
            'DrivingSchoolSchoolBundle:PriceAndPackages:create.html.twig',
            array('form' => $form->createView(), 'icons' => $icons, 'title' => 'Create Price & Packages', 'btn_title' => 'Create')
        );
    }

    public function editAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
    
        $entityManager = $this->getDoctrine()->getManager();
        $priceandpackage = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId));
        $priceandpackages = $priceandpackage[0];
        if(!empty($priceandpackages))
        {
            $form = $this->createForm(PriceAndPackagesForm::class, $priceandpackages);

            $form->handleRequest($request);
             if ($form->isSubmitted() && $form->isValid()) {

                // get perticular detail and set in entity
                $icon_id1 = $request->request->get('icon_id');
                $priceicon1 = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->find($icon_id1);
                $priceandpackages->setIcon($priceicon1); 

                // Update the priceandpackages
                $entityManager->persist($priceandpackages);
                $entityManager->flush();

                $this->addFlash(
                    'success',
                    'Price & packages updated succesfully.'
                ); 
                return $this->redirectToRoute('driving_school_school_list_priceandpackages');
            }
            $icons = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceIconsEntity')->findAll();

            return $this->render(
                'DrivingSchoolSchoolBundle:PriceAndPackages:create.html.twig',
                array('form' => $form->createView(),
                      'id'   => $priceandpackages->getId(),
                      'oldicon_id'   => $priceandpackages->getIcon(),
                      'icons' => $icons,
                      'title' => 'Edit Price & Packages',
                      'btn_title' => 'Update')
            );
        } else {
            return $this->redirectToRoute('driving_school_school_list_priceandpackages');
        }
    }

    public function deleteAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $priceandpackages = $entityManager->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->findOneBy(array('id' => $id));

        if (!$priceandpackages) { 
            // no priceandpackages in the system
            throw $this->createNotFoundException(
                'No price & packages found for id '.$id
            );
        } else {
            $entityManager->remove($priceandpackages);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Price & packages deleted succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_list_priceandpackages');
        }
    }
}
