<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use DrivingSchool\SchoolBundle\Form\ReservationForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ScheduleController extends Controller
{
    public function indexAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        return $this->render('DrivingSchoolSchoolBundle:Schedule:index.html.twig');
    }

    public function reservationAction(Request $request, UserInterface $user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $DrivingSchool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($user->getId());
        $form = $this->createForm(ReservationForm::class, $DrivingSchool);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
           
            // save the priceandpackages
            $entityManager->persist($DrivingSchool);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Reservation updated succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_schedule');
        }
        return $this->render(
            'DrivingSchoolSchoolBundle:Schedule:reservation.html.twig',
            array('form' => $form->createView())
        );

    }

    public function appointmentAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        $query = $connection->prepare("SELECT pap.name as package_name, pi.icon_path, ts.id as trainingid, ts.name as trainingname, ts.appointment_label FROM price_and_packages AS pap LEFT JOIN training_sessions as ts ON pap.id = ts.price_package_id LEFT JOIN price_icons AS pi ON pi.id = pap.icon_id WHERE ts.training_type IN(0,1) AND ts.drivingschool_id = ".$userId);

        $query->execute();
        $labels = $query->fetchAll();

        $newLabel = array();
        foreach ($labels as $label) {
                $newLabel[$label['package_name']]['icon_path']=$label['icon_path'];
                unset($label['icon_path']);
                $newLabel[$label['package_name']][] = $label;
        }
        // dump($newLabel);
        // exit();

        $colors = $entityManager->getRepository('DrivingSchoolAdminBundle:ColorEntity')->findAll();

        $data = [
            'newLabels' => $newLabel,
            'colors' => $colors,
        ];
        // dump($data);exit;

        return $this->render('DrivingSchoolSchoolBundle:Schedule:appointmentLabel.html.twig', $data);
    }

    public function updatecolorAction(Request $request, UserInterface $user)
    {
        $data = $request->request->all();

        foreach ($data as $key => $value) {
            $id = $value['id'];
            $color = $value['color'];

            $entityManager = $this->getDoctrine()->getManager();

            $trainingsession = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->find($id);
            $trainingsession->setAppointmentLabel($color); 

            $entityManager->persist($trainingsession);
            $entityManager->flush();
        }
        $this->addFlash(
            'success',
            'Appointment label updated succesfully.'
        ); 
        return $this->redirectToRoute('driving_school_school_appointmentlabel'); 
    }
}