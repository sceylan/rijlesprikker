<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\AssessmentMethodEntity;
use DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity;
use DrivingSchool\AdminBundle\Entity\ModuleEntity;
use DrivingSchool\AdminBundle\Entity\LessonRolesEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Response;

class ScoreCardController extends Controller
{
	public function reportAction(Request $request, $planningid, UserInterface $user)
    {
    	$userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();

        // get planning detail
        $planning = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('id' => $planningid, 'school' => $userId));
        if(empty($planning))
        {
        	return $this->redirectToRoute('driving_school_school_students_list');
        }

        // get student detail
    	$student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $planning->getStudent(), 'DrivingSchool' => $userId));
    	$studentname = $student->getFirstName().' '.$student->getLastName();
    	$studentid = $student->getId();

    	// get courses detail
    	$courses = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findOneBy(array('id' => $planning->getTrainingSession()));
		$coursesname = $courses->getName();

        $lessonModules = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->findBy(array('lessonId' => $planning->getPricePackage()));
        /*************************** newd ***************************************/
        $lessonModuless = [];
        foreach ($lessonModules as $lessonModule) {
        $totalScored=0;
        $totalCounts=0;
            // get assessment method
            $assessments = $entityManager->getRepository('DrivingSchoolAdminBundle:AssessmentMethodEntity')->findBy(array('DrivingSchool' => $userId));

            // get lesson roles
            $lessonRoles = $entityManager->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->findBy(array('Module' => $lessonModule->getId()));

            $data = [];
            $scorecards = [];
            foreach ($lessonRoles as $lessonrole) {
                $rolesid = $lessonrole->getId();

                $scoreCardNotes = $entityManager->getRepository('DrivingSchoolAdminBundle:ScoreCardNotesEntity')->findOneBy(array('DrivingSchool' => $userId, 'student' => $studentid, 'lessonRoles' => $rolesid));

                if(!empty($scoreCardNotes)) {
                    $progressbarValue = $scoreCardNotes->getProgressbarValue();
                } else {
                    $progressbarValue= '';
                }
        
                $count_assessment = count($assessments);
                $totalScored+= $progressbarValue;
                $totalCounts+= $count_assessment;
                $per = ($totalScored * 100)/ $totalCounts;
            }
            $lessonModule->percentage = $per;
            $lessonModuless[] = $lessonModule;
        }
        /************************************************************************/

        $data = [
        	'studentid' => $studentid,
        	'studentname' => $studentname,
        	'lessonsModules' => $lessonModuless,
        	'coursesname' => $coursesname,
            'planningid' => $planningid,
        	'assessments' => count($assessments),
        ];

    	return $this->render('DrivingSchoolSchoolBundle:ScoreCard:report.html.twig', $data);
    }

    public function rolesnoteAction(Request $request, $planningid, $moduleid, UserInterface $user)
    {
    	$userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();

    	// get planning detail
        $planning = $entityManager->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findOneBy(array('id' => $planningid, 'school' => $userId));
        if(empty($planning))
        {
        	return $this->redirectToRoute('driving_school_school_students_list');
        }

        // get student detail
    	$student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->findOneBy(array('id' => $planning->getStudent(), 'DrivingSchool' => $userId));
    	$studentname = $student->getFirstName().' '.$student->getLastName();
    	$studentid = $student->getId();

    	// get courses detail
    	$courses = $entityManager->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity')->findOneBy(array('id' => $planning->getTrainingSession()));
		$coursesname = $courses->getName();

    	// get package wise module detail
    	$lessonModules = $entityManager->getRepository('DrivingSchoolAdminBundle:ModuleEntity')->findOneBy(array('id' => $moduleid, 'lessonId' => $planning->getPricePackage()));
    	$lessonModuleName = $lessonModules->getModuleName();

    	// get assessment method
    	$assessments = $entityManager->getRepository('DrivingSchoolAdminBundle:AssessmentMethodEntity')->findBy(array('DrivingSchool' => $userId));

		// get lesson roles
    	$lessonRoles = $entityManager->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->findBy(array('Module' => $moduleid));

    	$data = [];
    	$scorecards = [];
    	foreach ($lessonRoles as $lessonrole) {
    		$rolesid = $lessonrole->getId();

    		$scoreCardNotes = $entityManager->getRepository('DrivingSchoolAdminBundle:ScoreCardNotesEntity')->findOneBy(array('DrivingSchool' => $userId, 'student' => $studentid, 'lessonRoles' => $rolesid));

    		if(!empty($scoreCardNotes)) {
	        	$data['scorecard_id'] = $scoreCardNotes->getId();
	            $data['DrivingSchool'] = $userId;
	            $data['StudentId'] = $studentid;
	            $data['lessonRolesId'] = $rolesid;
	            $data['lessonRolesName'] = $lessonrole->getName();
	            $data['progressbarValue'] = $scoreCardNotes->getProgressbarValue();
	            $data['description'] = $scoreCardNotes->getDescription();
	            $scorecards[] = $data;
        	} else {
        		$data['scorecard_id'] = '';
	            $data['DrivingSchool'] = $userId;
	            $data['StudentId'] = $studentid;
	            $data['lessonRolesId'] = $rolesid;
	            $data['lessonRolesName'] = $lessonrole->getName();
	            $data['progressbarValue'] = '';
	            $data['description'] = '';
	            $scorecards[] = $data;
        	}
    	}
		$count_assessment = count($assessments);

		$assessmentsarr[0]['name'] = '';
		foreach ($assessments as $key => $assessment) {
			$array = [];
			$array['name'] = $assessment->getName();
			$assessmentsarr[] = $array;
		}  
// dump($assessmentsarr);exit;
        $data = [
        	'studentid' => $studentid,
        	'studentname' => $studentname,
        	'lessonModuleName' => $lessonModuleName,
        	'coursesname' => $coursesname,
        	'planningid' => $planningid,
        	'scorecards' => $scorecards,
        	'assessments' => $assessmentsarr,
        	'assessment_count' => $count_assessment,
        ];

    	return $this->render('DrivingSchoolSchoolBundle:ScoreCard:rolesnote.html.twig', $data);
    }

    public function ajaxloadnotesAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $reqdata = $request->request->all();

        $scoreCardNotes = $entityManager->getRepository('DrivingSchoolAdminBundle:ScoreCardNotesEntity')->findOneBy(array('DrivingSchool' => $userId, 'student' => $reqdata['studentid'], 'lessonRoles' => $reqdata['rolesid']));
        if(!empty($scoreCardNotes))
        {
            if(!empty($reqdata['description']))
            {
			    $scoreCardNotes->setDescription($reqdata['description']);
            }
            if($reqdata['progressvalue'] == 'null') {} else {
                $scoreCardNotes->setProgressbarValue($reqdata['progressvalue']);    
            }

			$entityManager->persist($scoreCardNotes);
		    $entityManager->flush();
        } else {
        	$scoreCardNotes = new ScoreCardNotesEntity();
        	$scoreCardNotes->setDrivingSchool($user);

        	$student = $entityManager->getRepository('DrivingSchoolAdminBundle:StudentEntity')->find($reqdata['studentid']);
        	$scoreCardNotes->setStudent($student);

        	$lessonRoles = $entityManager->getRepository('DrivingSchoolAdminBundle:LessonRolesEntity')->find($reqdata['rolesid']);
			$scoreCardNotes->setLessonRoles($lessonRoles);

            if(!empty($reqdata['description']))
            {
                $scoreCardNotes->setDescription($reqdata['description']);
            }
            if($reqdata['progressvalue'] == 'null') {} else {
                $scoreCardNotes->setProgressbarValue($reqdata['progressvalue']);    
            }

			$entityManager->persist($scoreCardNotes);
		    $entityManager->flush();
        }
        return new Response(json_encode(array("response" => $scoreCardNotes)));
    }
}