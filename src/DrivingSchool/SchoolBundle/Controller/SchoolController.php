<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use DrivingSchool\SchoolBundle\Form\DrivingSchoolForm;
use DrivingSchool\SchoolBundle\Form\DrivingSchoolLoginForm;
use DrivingSchool\SchoolBundle\Form\DrivingSchoolRegistrationForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;

class SchoolController extends Controller
{
    public function loginAction(Request $request,$subdomain)
    {
        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL') || TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
           return $this->redirectToRoute('driving_school_school_dashboard'); 
        }

        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();

    	$entityManager = $this->getDoctrine()->getManager();
        
        $DrivingSchool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->findOneBy(array('username' => $subdomain));

        if(null === $DrivingSchool) {
            return $this->redirectToRoute('driving_school_school_register',array('subdomain' => "newuser"));
        }

        $form = $this->createForm(DrivingSchoolLoginForm::class, $DrivingSchool);

        return $this->render(
            'DrivingSchoolSchoolBundle:DrivingSchool:login.html.twig',
                array(
                    'form'          => $form->createView(),
                    'id'            => $DrivingSchool->getId(),
                    'school_name'   => $DrivingSchool->getSchoolName(),
                    'error'         => $error,
                )
        );
        
    }

    private function generateRandomString() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomPassword = '';
            
        for ($i = 0; $i < 5; $i++)
            $randomPassword .= $characters[mt_rand(0, 61)]; // Send this password in email

        return $randomPassword;
    }

    public function registerAction(Request $request, $subdomain)
    {   
        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
           return $this->redirectToRoute('driving_school_school_profile'); 
        }
        
        if($subdomain != "newuser") {
            return $this->redirectToRoute('driving_school_school_register',array('subdomain' => "newuser"));
        }
        $drivingschool = new DrivingSchoolEntity();
        $form = $this->createForm(DrivingSchoolRegistrationForm::class, $drivingschool);

        return $this->render(
            'DrivingSchoolSchoolBundle:DrivingSchool:registration.html.twig',
            array('form' => $form->createView())
        ); 
    }

    public function registerPostAction(Request $request)
    {
        $drivingschool = new DrivingSchoolEntity();
        $form = $this->createForm(DrivingSchoolRegistrationForm::class, $drivingschool);

        $form->handleRequest($request);
        
        if($form->isValid()) {
            $realPassword = $this->generateRandomString();
            $salt = $this->generateRandomString();
            $encoder = new MessageDigestPasswordEncoder();
            // dump($data['password']['first']);exit;
            $password = $encoder->encodePassword($realPassword, $salt);
            $drivingschool->setPassword($password);
            $drivingschool->setSalt($salt);
            $drivingschool->setEnabled(1);

            $data = $request->request->all()['driving_school_registration_form'];
            $username = $data['username'];
            $domainname = str_replace(' ', '', $username).'.'.$this->container->getParameter('domain').'/login';

            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($drivingschool);
            $entityManager->flush();

            /* Start Send Mail*/
            $mailer = $this->get('mailer');
            $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '2'));
            $mailsubject = $emailtmp->getSubject();
            $content = $emailtmp->getContent();

            $template = $this->get('twig')->createTemplate($content);
            $mailcontent = $template->render(array('name'=>$drivingschool->getSchoolName(), 'username' => $username, 'password' => $realPassword, 'Link' => $domainname));

            $message = (new \Swift_Message($mailsubject))
            ->setFrom('admin@drivingschool.com')
            ->setTo($data['email'])
            ->setBody($mailcontent,'text/html');
            $mailer->send($message);
            /* Enad Send Mail */
            
            return $this->redirectToRoute('driving_school_school_login', array('subdomain' => $username));
        } 

        return $this->render(
            'DrivingSchoolSchoolBundle:DrivingSchool:registration.html.twig',
            array('form' => $form->createView())
        );
    }

    public function profileAction(Request $request, UserInterface $user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $DrivingSchool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($user->getId());
        $form = $this->createForm(DrivingSchoolForm::class, $DrivingSchool);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           
            // save the priceandpackages
            $entityManager->persist($DrivingSchool);
            $entityManager->flush();

            $this->addFlash('success','Successfully updated your profile');
        }

    	return $this->render(
			'DrivingSchoolSchoolBundle:DrivingSchool:profile.html.twig',
			array('form' => $form->createView(),
                  'id'   => $DrivingSchool->getId()
                )
		);
    }

    public function getUserImageAction(Request $request, UserInterface $user)
    {
        $media = $user->getLogo();

        $image_path = "";
        if(!empty($media)) {
            $mediaManager = $this->get('sonata.media.pool');
            $provider = $mediaManager->getProvider($media->getProviderName());
            $format = $provider->getFormatName($media, 'default_small');
            $image_path = $provider->generatePublicUrl($media, $format);
        }

        return $this->render(
            'DrivingSchoolSchoolBundle:DrivingSchool:profile_image.html.twig',
            array('imagepath' => $image_path)
        );
    }
}
