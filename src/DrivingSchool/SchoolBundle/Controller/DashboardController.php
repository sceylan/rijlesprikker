<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\VehicleKMForm;
use DrivingSchool\AdminBundle\Entity\VehicleKMEntity;
use DrivingSchool\AdminBundle\Entity\NotificationEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends Controller
{
    public function indexAction(Request $request, UserInterface $user)
    {
        if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL')) {
            
        } elseif(TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
            /*dump("ROLE_INSTRUCTOR mate development chalu 6e...");
            exit;*/
        }

    	$userId = $user->getId();
        
    	$vehicles = new VehicleKMEntity();
    	$form = $this->createForm(VehicleKMForm::class, $vehicles, array('userId' => $userId));

    	$entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        $form->handleRequest($request);
        
            // echo "<pre>";
            // print_r($data);exit;
        if($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all()['vehicle_km_form'];
	        $query = $connection->prepare("SELECT vkm.id as vkmid,vkm.mileage,vkm.date,vkm.time,vkm.Vehicle_id  FROM vehicle AS v LEFT JOIN vehicle_km AS vkm ON v.id = vkm.Vehicle_id WHERE v.drivingschool_id = ".$userId." AND vkm.Vehicle_id =".$data['Vehicle_id']);
	        $query->execute();
	        $vehiclekms = $query->fetchAll();
	        $arr   = end($vehiclekms);
	        $mileageold = $arr['mileage'];
	        $dateold = $arr['date'];
	        $timeold = date("H:i", strtotime($arr['time']));
			foreach ($dataval['vehiclekms'] as $key => $value) {
	            $allmileage += $value['mileage'];
	        } 


            $vehiclekm = new VehicleKMEntity();
            $vehicleid = $entityManager->getRepository('DrivingSchoolAdminBundle:VehicleEntity')->find($data['Vehicle_id']);
            $vehiclekm->setVehicleId($vehicleid); 

            $mileage = $data['mileage'];
            $vehiclekm->setMileage($mileage);

            $date = date("Y-m-d");
            $vehiclekm->setDate(new \DateTime($date)); 

            //$time = $data['time'];
            $time = date("H:i:s");
            $matchtime = date("H:i");
            $vehiclekm->setTime(new \DateTime($time)); 

            $note = $data['note'];
            $vehiclekm->setNote($note);

            if($mileageold < $mileage)
            {
                $totalmileage = $allmileage+$mileage;
                if($totalmileage < 10000 && ($timeold != $matchtime || $dateold != $date))
                {
                    $entityManager->persist($vehiclekm);
                    $entityManager->flush();
                    $this->addFlash(
			            'success',
			            'You have been succesfully mileage added.'
			        ); 
                    return $this->redirectToRoute('driving_school_school_dashboard');
                } else {
                    $this->addFlash(
                        'notice',
                        '2 errors meant that mileage could not be saved:
                            Mileage is invalid
                            Vehicle can not have driven more than 10,000 km per day.'
                    );
                }
            } else {
                $this->addFlash(
                    'notice',
                    '1 error meant that mileage could not be saved: Mileage must be more than the previous position.'
                );
            }
        }

        $todaydate = date("Y-m-d");
        $currenttime = date('H:i:s');

            // Start Right now lessons
            if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
            {
                $currentlessonsquery = $connection->prepare("SELECT p.*,ts.name as coursesname, ts.appointment_label as coursecolor, i.instructor_name, v.license_plate, s.first_name as studentname, pi.icon_path FROM planning AS p LEFT JOIN training_sessions as ts ON ts.id = p.training_session_id LEFT JOIN instructor as i ON i.id = p.instructor_id LEFT JOIN vehicle as v ON v.id = p.vehicle_id LEFT JOIN student as s ON s.id = p.student_id LEFT JOIN price_and_packages as pap ON pap.id = p.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE p.school_id = ".$userId." AND DATE(p.start_date) = '".$todaydate."' AND TIME(p.start_date) <= '".$currenttime."' AND TIME(p.end_date) >= '".$currenttime."'");
            } else {
                $currentlessonsquery = $connection->prepare("SELECT p.*,ts.name as coursesname, ts.appointment_label as coursecolor, i.instructor_name, v.license_plate, s.first_name as studentname, pi.icon_path FROM planning AS p LEFT JOIN training_sessions as ts ON ts.id = p.training_session_id LEFT JOIN instructor as i ON i.id = p.instructor_id LEFT JOIN vehicle as v ON v.id = p.vehicle_id LEFT JOIN student as s ON s.id = p.student_id LEFT JOIN price_and_packages as pap ON pap.id = p.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE p.instructor_id = ".$userId." AND DATE(p.start_date) = '".$todaydate."' AND TIME(p.start_date) <= '".$currenttime."' AND TIME(p.end_date) >= '".$currenttime."'");
            }

            $currentlessonsquery->execute();
            $currentlessons = $currentlessonsquery->fetchAll();
            // End Right now lessons

            // Start Today lessons
            if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
            {
                $todaylessonsquery = $connection->prepare("SELECT p.*,ts.name as coursesname, ts.appointment_label as coursecolor, i.instructor_name, v.license_plate, s.first_name as studentname, pi.icon_path FROM planning AS p LEFT JOIN training_sessions as ts ON ts.id = p.training_session_id LEFT JOIN instructor as i ON i.id = p.instructor_id LEFT JOIN vehicle as v ON v.id = p.vehicle_id LEFT JOIN student as s ON s.id = p.student_id LEFT JOIN price_and_packages as pap ON pap.id = p.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE p.school_id = ".$userId." AND DATE(p.start_date) = '".$todaydate."'");

            } else {
                $todaylessonsquery = $connection->prepare("SELECT p.*,ts.name as coursesname, ts.appointment_label as coursecolor, i.instructor_name, v.license_plate, s.first_name as studentname, pi.icon_path FROM planning AS p LEFT JOIN training_sessions as ts ON ts.id = p.training_session_id LEFT JOIN instructor as i ON i.id = p.instructor_id LEFT JOIN vehicle as v ON v.id = p.vehicle_id LEFT JOIN student as s ON s.id = p.student_id LEFT JOIN price_and_packages as pap ON pap.id = p.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE p.instructor_id = ".$userId." AND DATE(p.start_date) = '".$todaydate."'");
            }
            $todaylessonsquery->execute();
            $todaylessons = $todaylessonsquery->fetchAll();
            // End Today lessons

            // start Outstanding reservations
            $permissions = "";
            if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
            {
                $reservationQuery = $connection->prepare("SELECT p.*,ts.name as coursesname, ts.appointment_label as coursecolor, i.instructor_name, v.license_plate, s.first_name as studentname, pi.icon_path FROM planning AS p LEFT JOIN training_sessions as ts ON ts.id = p.training_session_id LEFT JOIN instructor as i ON i.id = p.instructor_id LEFT JOIN vehicle as v ON v.id = p.vehicle_id LEFT JOIN student as s ON s.id = p.student_id LEFT JOIN price_and_packages as pap ON pap.id = p.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE p.school_id = ".$userId." AND is_reservation='1' AND student_id !='NULL'");
            } else {
                $reservationQuery = $connection->prepare("SELECT p.*,ts.name as coursesname, ts.appointment_label as coursecolor, i.instructor_name, v.license_plate, s.first_name as studentname, pi.icon_path FROM planning AS p LEFT JOIN training_sessions as ts ON ts.id = p.training_session_id LEFT JOIN instructor as i ON i.id = p.instructor_id LEFT JOIN vehicle as v ON v.id = p.vehicle_id LEFT JOIN student as s ON s.id = p.student_id LEFT JOIN price_and_packages as pap ON pap.id = p.price_package_id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE p.instructor_id = ".$userId." AND is_reservation='1' AND student_id !='NULL'");
                $permissions = $entityManager->getRepository('DrivingSchoolAdminBundle:RolesAndPermissionEntity')->findOneBy(array('Instructor' => $userId, 'rolesModule' => '1'));
            }
            $reservationQuery->execute();
            $reservations = $reservationQuery->fetchAll();
            // end Outstanding reservations

            // Start Notification 
            $notificationQuery = $connection->prepare("SELECT n.*, i.username as instructorname, s.schoolName as schoolname, s.logo_id as schoolimage, st.username as studentname, st.student_image_id as studentimage, i.instructor_image_id as instructorimage FROM notification AS n LEFT JOIN instructor AS i ON n.event_instructor_id = i.id LEFT JOIN driving_school AS s ON n.event_school_id = s.id LEFT JOIN student AS st ON n.event_student_id = st.id ORDER BY n.id DESC limit 0,5");
            $notificationQuery->execute();
            $notifications = $notificationQuery->fetchAll();
                $notificationdata = [];
                foreach ($notifications as $key => $notification) {
                    if($notification['user_type'] == '1') {
                        $schoolimage = $notification['schoolimage'];
                        $mediaObj = $entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findOneBy(array('id' => $schoolimage));
                        if(!empty($mediaObj)) {
                            $mediaManager = $this->get('sonata.media.pool');
                            $provider = $mediaManager->getProvider($mediaObj->getProviderName());
                            $format = $provider->getFormatName($mediaObj, 'default_small');
                            $image_path = $provider->generatePublicUrl($mediaObj, $format);
                            $format_big = $provider->getFormatName($mediaObj, 'default_big');
                            $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
                        }

                        $notification['image'] = $big_image_path;
                        $notification['name'] = $notification['schoolname'];
                    }
                    else if($notification['user_type'] == '2') {
                        $instructorimage = $notification['instructorimage'];
                        $mediaObj = $entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findOneBy(array('id' => $instructorimage));
                        if(!empty($mediaObj)) {
                            $mediaManager = $this->get('sonata.media.pool');
                            $provider = $mediaManager->getProvider($mediaObj->getProviderName());
                            $format = $provider->getFormatName($mediaObj, 'default_small');
                            $image_path = $provider->generatePublicUrl($mediaObj, $format);
                            $format_big = $provider->getFormatName($mediaObj, 'default_big');
                            $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
                        }

                        $notification['image'] = $big_image_path;    
                        $notification['name'] = $notification['instructorname'];
                    } else if($notification['user_type'] == '3') {
                        $studentimage = $notification['studentimage'];
                        $mediaObj = $entityManager->getRepository('ApplicationSonataMediaBundle:Media')->findOneBy(array('id' => $studentimage));
                        if(!empty($mediaObj)) {
                            $mediaManager = $this->get('sonata.media.pool');
                            $provider = $mediaManager->getProvider($mediaObj->getProviderName());
                            $format = $provider->getFormatName($mediaObj, 'default_small');
                            $image_path = $provider->generatePublicUrl($mediaObj, $format);
                            $format_big = $provider->getFormatName($mediaObj, 'default_big');
                            $big_image_path = $provider->generatePublicUrl($mediaObj, $format_big);
                        }
                        
                        $notification['image'] = $big_image_path;
                        $notification['name'] = $notification['studentname'];
                    }
                    $notificationdata[] = $notification;
                }
            // End Notification 

    	return $this->render('DrivingSchoolSchoolBundle:Dashboard:index.html.twig', array('form' => $form->createView(),'currentlessons' => $currentlessons, 'todaylessons' => $todaylessons, 'reservations' => $reservations, 'permissions' => $permissions, 'notificationdata' => $notificationdata));
    }

    public function getmileageAction(Request $request, UserInterface $user)
    {
    	$userId = $user->getId();
     
    	$data = $request->request->all();
    	$vahicleid = $data['vahicleval'];

    	$entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        // $query = $connection->prepare("SELECT vkm.mileage as maxmileage FROM vehicle AS v LEFT JOIN vehicle_km AS vkm ON v.id = vkm.Vehicle_id WHERE v.drivingschool_id = ".$userId." AND vkm.Vehicle_id =".$vahicleid);
        $query = $connection->prepare("SELECT MAX(vkm.mileage) as maxmileage FROM vehicle AS v LEFT JOIN vehicle_km AS vkm ON v.id = vkm.Vehicle_id WHERE v.drivingschool_id = ".$userId." AND vkm.Vehicle_id =".$vahicleid);
	        
	        $query->execute();
	        $vehiclekms = $query->fetch();
	        echo $vehiclekms['maxmileage'];
    		exit;
    }

    public function reservationAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();

        $data = $request->request->all();
        $reservations_id = $data['reservationchk'];
        $entityManager = $this->getDoctrine()->getManager();

        if(isset($data['submitaccept'])) 
        {
            if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
            {
                $editreservation   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(['id' => $reservations_id]);
                $usertypeid = '1';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            } elseif (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
                $editreservation   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(['id' => $reservations_id]);
                $usertypeid = '2';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            }

            if(!empty($editreservation))
            {
                foreach ($editreservation as $reservation) {
                    $reservation->setIsReservation(0);
                    $entityManager->persist($reservation);

                    /* Start notification */
                    $notification = new NotificationEntity();
                    $notification->setSchool($reservation->getSchool());
                    $notification->setStudent($reservation->getStudent());
                    $notification->setInstructor($reservation->getInstructor());
                    $notification->setPackageName($reservation->getPricePackage()->getName());
                    $notification->setTrainingSessionName($reservation->getTrainingSession()->getName());
                    $notification->setEventStartDate($reservation->getStartDate());
                    $notification->setEventEndDate($reservation->getEndDate());
                    $notification->setUserId($userId);
                    $notification->setUserType($usertype);
                    $notification->setAction('Accepted');
                    $notification->setSchoolReadStatus(0);
                    $notification->setInstructorReadStatus(0);
                    $notification->setStudentReadStatus(0);
                    $notification->setNotificationType('Event');
                    $entityManager->persist($notification);
                    /* End Notification */

                    /* Start Mail Send */
                    $mailer = $this->get('mailer');
                    $packagename = $reservation->getPricePackage()->getName();
                    $coursename = $reservation->getTrainingSession()->getName();
                    $eventdate = date('d M, Y', $reservation->getStartDate()->getTimestamp());
                    $eventtime = date('g:i, A', $reservation->getStartDate()->getTimestamp()).' - '.date('g i, A', $reservation->getEndDate()->getTimestamp());
                    $createdatetime = date('M d', $reservation->getCreation()->getTimestamp()).' at '.date('g:i A', $reservation->getCreation()->getTimestamp());

                    $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '8'));
                    $subject = $emailtmp->getSubject();
                    $content = $emailtmp->getContent();
                    $tempsubject = $this->get('twig')->createTemplate($subject);
                    $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

                    $names = array(0 => array("name" => $reservation->getSchool()->getSchoolName(), "email" => $reservation->getSchool()->getEmail()),1 => array("name" => $reservation->getInstructor()->getInstructorName(), "email" => $reservation->getInstructor()->getEmail()), 2 => array("name" => $reservation->getStudent()->getFirstName(), "email" => $reservation->getStudent()->getEmail()));
                    
                    foreach($names as $data){
                        $template = $this->get('twig')->createTemplate($content);
                        $mailcontent = $template->render(array('name'=>$data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $reservation->getInstructor()->getInstructorName(), 'schoolname' => $reservation->getSchool()->getSchoolName(), 'schooladdress' => $reservation->getSchool()->getSchoolAddress(), 'schoolphone' => $reservation->getSchool()->getSchoolPhone()));

                        $message = (new \Swift_Message($mailsubject))
                        ->setFrom('admin@drivingschool.com')
                        ->setTo($data['email'])
                        ->setBody($mailcontent,'text/html');
                        $mailer->send($message);
                    }

                    /* End Mail Send */
                }       
                $entityManager->flush();
            }
        } elseif (isset($data['submitreject']))
        {
            if (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_SCHOOL'))
            {
                $editreservation   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(['id' => $reservations_id]);
                $usertypeid = '1';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            } elseif (TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_INSTRUCTOR')) {
                $editreservation   = $this->getDoctrine()->getRepository('DrivingSchoolAdminBundle:PlanningEntity')->findBy(['id' => $reservations_id]);
                $usertypeid = '2';
                $usertype = $entityManager->getRepository('DrivingSchoolAdminBundle:UserTypeEntity')->find($usertypeid);
            }

            if(!empty($editreservation))
            {
                foreach ($editreservation as $reservation) {
                    /* Start notification */
                    $notification = new NotificationEntity();
                    $notification->setSchool($reservation->getSchool());
                    $notification->setStudent($reservation->getStudent());
                    $notification->setInstructor($reservation->getInstructor());
                    $notification->setPackageName($reservation->getPricePackage()->getName());
                    $notification->setTrainingSessionName($reservation->getTrainingSession()->getName());
                    $notification->setEventStartDate($reservation->getStartDate());
                    $notification->setEventEndDate($reservation->getEndDate());
                    $notification->setUserId($userId);
                    $notification->setUserType($usertype);
                    $notification->setAction('Rejected');
                    $notification->setSchoolReadStatus(0);
                    $notification->setInstructorReadStatus(0);
                    $notification->setStudentReadStatus(0);
                    $notification->setNotificationType('Event');
                    $entityManager->persist($notification);
                    /* End Notification */

                    /* Start Mail Send */
                    $mailer = $this->get('mailer');
                    $packagename = $reservation->getPricePackage()->getName();
                    $coursename = $reservation->getTrainingSession()->getName();
                    $eventdate = date('d M, Y', $reservation->getStartDate()->getTimestamp());
                    $eventtime = date('g:i, A', $reservation->getStartDate()->getTimestamp()).' - '.date('g i, A', $reservation->getEndDate()->getTimestamp());
                    $createdatetime = date('M d', $reservation->getCreation()->getTimestamp()).' at '.date('g:i A', $reservation->getCreation()->getTimestamp());

                    $emailtmp = $entityManager->getRepository('DrivingSchoolAdminBundle:EmailTemplatesEntity')->findOneBy(array('id' => '9'));
                    $subject = $emailtmp->getSubject();
                    $content = $emailtmp->getContent();
                    $tempsubject = $this->get('twig')->createTemplate($subject);
                    $mailsubject = $tempsubject->render(array('packagename' => $packagename, 'course' => $coursename, 'datetime' => $createdatetime));

                    $names = array(0 => array("name" => $reservation->getSchool()->getSchoolName(), "email" => $reservation->getSchool()->getEmail()),1 => array("name" => $reservation->getInstructor()->getInstructorName(), "email" => $reservation->getInstructor()->getEmail()), 2 => array("name" => $reservation->getStudent()->getFirstName(), "email" => $reservation->getStudent()->getEmail()));
                    
                    foreach($names as $data){
                        $template = $this->get('twig')->createTemplate($content);
                        $mailcontent = $template->render(array('name'=>$data['name'], 'packagename' => $packagename, 'coursename' => $coursename, 'date' => $eventdate, 'time' => $eventtime, 'instructorname' => $reservation->getInstructor()->getInstructorName(), 'schoolname' => $reservation->getSchool()->getSchoolName(), 'schooladdress' => $reservation->getSchool()->getSchoolAddress(), 'schoolphone' => $reservation->getSchool()->getSchoolPhone()));

                        $message = (new \Swift_Message($mailsubject))
                        ->setFrom('admin@drivingschool.com')
                        ->setTo($data['email'])
                        ->setBody($mailcontent,'text/html');
                        $mailer->send($message);
                    }

                    /* End Mail Send */

                    $reservation->setStudent(NULL);
                    $entityManager->persist($reservation);
                }       
                $entityManager->flush();
            }
        }
        return $this->redirectToRoute('driving_school_school_dashboard');
    }
}