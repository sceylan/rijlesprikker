<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\VehicleForm;
use DrivingSchool\AdminBundle\Entity\VehicleEntity;
use DrivingSchool\AdminBundle\Entity\VehicleKMEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class VehicleController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
    	$entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();
        // $query = $connection->prepare("SELECT v.id, v.license_plate, GROUP_CONCAT(pi.icon_path) AS icon_path FROM vehicle AS v LEFT JOIN pricepackage_vehicle AS pv ON v.id = pv.vehicle_id LEFT JOIN price_and_packages AS pap ON pv.training_courses_id = pap.id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id WHERE v.drivingschool_id = ".$userId." GROUP BY v.license_plate");

        /*$query = $connection->prepare("SELECT v.id, v.license_plate, MAX(vkm.mileage) as max_mileage, GROUP_CONCAT(DISTINCT pi.icon_path) AS icon_path FROM vehicle AS v LEFT JOIN pricepackage_vehicle AS pv ON v.id = pv.vehicle_id LEFT JOIN price_and_packages AS pap ON pv.training_courses_id = pap.id LEFT JOIN price_icons AS pi ON pap.icon_id = pi.id LEFT JOIN vehicle_km AS vkm ON vkm.Vehicle_id = v.id WHERE v.drivingschool_id = ".$userId." GROUP BY v.license_plate");*/

        /*$query = $connection->prepare("SELECT t2.*, t1.date, t1.time FROM vehicle_km AS t1 RIGHT JOIN (SELECT v.id, v.license_plate, vkm.Vehicle_id, MAX(vkm.mileage) as max_mileage, GROUP_CONCAT(DISTINCT pi.icon_path) AS icon_path FROM vehicle AS v JOIN pricepackage_vehicle AS pv ON v.id = pv.vehicle_id JOIN price_and_packages AS pap ON pv.training_courses_id = pap.id JOIN price_icons AS pi ON pap.icon_id = pi.id LEFT JOIN vehicle_km AS vkm ON vkm.Vehicle_id = v.id WHERE v.drivingschool_id = ".$userId." GROUP BY v.license_plate) AS t2 ON t1.Vehicle_id = t2.Vehicle_id AND t1.mileage = t2.max_mileage ORDER BY t1.date DESC, t1.time DESC");*/
        $query = $connection->prepare("SELECT v.id, v.license_plate, GROUP_CONCAT(pi.icon_path) AS icon_path
                                    FROM vehicle AS v 
                                    JOIN pricepackage_vehicle AS pv ON v.id = pv.vehicle_id
                                    JOIN price_and_packages AS pap ON pap.id = pv.training_courses_id
                                    JOIN price_icons AS pi ON pi.id = pap.icon_id
                                    WHERE v.drivingschool_id = ".$userId."
                                    GROUP BY v.license_plate, v.id");


        // temp disabled
//        RIGHT JOIN (
//        SELECT vv.id as vehicle_id_2 , vv.license_plate as licence_plate2, vkm.Vehicle_id, MAX(vkm.mileage) AS max_mileage, vkm.date as vehicle_date, vkm.time
//            FROM vehicle AS vv
//                JOIN vehicle_km AS vkm ON vv.id = vkm.Vehicle_id
//
//            ) AS m ON v.id = m.vehicle_id_2



        $query->execute();
        $vehicleList = $query->fetchAll();

//        foreach ($vehicleList as $key => $vehicle) {
//
//            var_dump($vehicle);
//            exit;
//            $date=date_create($vehicle['date']." ".$vehicle['time']);
//            $now = date_create(date("Y-m-d H:i:s"));
//            $diff = date_diff($now,$date);
//            $html = "";
//            $years = (float) $diff->format("%R%y");
//            if($years > 0) {
//                $html.="After ".abs($years)." Year(s)";
//            } elseif($years < 0) {
//                $html.=abs($years)." Year(s) Ago";
//            } else {
//                $months = (float) $diff->format("%R%m");
//                if($months > 0) {
//                    $html.="After ".abs($months)." Month(s)";
//                } elseif($months < 0) {
//                    $html.=abs($months)." Month(s) Ago";
//                } else {
//                    $days = (float) $diff->format("%R%a");
//                    if($days > 0) {
//                        $html.="After ".abs($days)." Day(s)";
//                    } elseif($days < 0) {
//                        $html.= abs($days)." Day(s) ago";
//                    } else {
//                        $hours = (float) $diff->format("%R%h");
//                        if($hours > 0) {
//                            $html.="After ".abs($hours)." Hour(s)";
//                        } elseif($hours < 0) {
//                            $html.= abs($hours)." Hour(s) Ago";
//                        } else {
//                            $minutes = (float) $diff->format("%R%i");
//                            if($minutes > 0) {
//                                $html.="After ".abs($minutes)." Minute(s)";
//                            } elseif($minutes < 0) {
//                                $html.= abs($minutes)." Minute(s) Ago";
//                            } else {
//                                $seconds = (float) $diff->format("%R%s");
//                                if($seconds > 0) {
//                                    $html.="After ".abs($seconds)." Second(s)";
//                                } elseif($seconds < 0) {
//                                    $html.= abs($seconds)." Second(s) Ago";
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            $vehicleList[$key]['time'] = $html;
//        }
    	$data = [
    		'vehicles' => $vehicleList,
    	];

        return $this->render('DrivingSchoolSchoolBundle:Vehicle:list.html.twig', $data);
    }

    public function createAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
    	$vehicles = new VehicleEntity();
    	$form = $this->createForm(VehicleForm::class, $vehicles, array('userId' => $userId));

    	$form->handleRequest($request);

    	if ($form->isSubmitted()) {
            if($form->isValid()) 
            {
                $entityManager = $this->getDoctrine()->getManager();

                $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
                $vehicles->setDrivingSchool($drivingschool); 

                try {
                    $entityManager->persist($vehicles);
                    $entityManager->flush();
                } catch (UniqueConstraintViolationException $e) {
                    if(preg_match('/\bunique_name\b/i', $e->getMessage())) {
                        /*dump('Vehicle licence plate <b>'.$vehicles->getLicensePlate().'</b> already taken...');exit;*/
                        $this->addFlash(
                            'error',
                            'Vehicle licence plate <b>'.$vehicles->getLicensePlate().'</b> already taken...'
                        ); 
                    } else {
                        $this->addFlash(
                            'error',
                            'Error processing your request'
                        ); 
                    }
                    
                    return $this->redirectToRoute('driving_school_school_create_vehicle');
                }

                $this->addFlash(
                    'success',
                    'Vehicle added successfully.'
                ); 
                return $this->redirectToRoute('driving_school_school_list_vehicle');
            }
        }

    	return $this->render(
    		'DrivingSchoolSchoolBundle:Vehicle:create.html.twig',
    		array('form' => $form->createView(), 'title' => 'Create Vehicle',
                    'btn_title' => 'Create')
    	); 
    }

    public function editAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
    	$entityManager = $this->getDoctrine()->getManager();
    	$vehicles = $entityManager->getRepository('DrivingSchoolAdminBundle:VehicleEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId));
        $vehicle = $vehicles[0];
        if(!empty($vehicle))
        {
        	$form = $this->createForm(VehicleForm::class, $vehicle, array('userId' => $userId));

        	$form->handleRequest($request);

        	if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($vehicle);
                $entityManager->flush();

                $this->addFlash(
                    'success',
                    'Vehicle updated successfully.'
                ); 
                return $this->redirectToRoute('driving_school_school_list_vehicle');
            }
        	return $this->render(
        		'DrivingSchoolSchoolBundle:Vehicle:create.html.twig',
        		array('form' => $form->createView(), 
                    'title' => 'Edit Vehicle',
                    'btn_title' => 'Update',)
        	);   
        } else {
            return $this->redirectToRoute('driving_school_school_list_vehicle');
        }  
    }

    public function deleteAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
       
        $entityManager = $this->getDoctrine()->getManager();
        $Vehicle = $entityManager->getRepository('DrivingSchoolAdminBundle:VehicleEntity')->find($id);

        if (!$Vehicle) { 
            // no instructor in the system
            throw $this->createNotFoundException(
                'No price & packages found for id '.$id
            );
        } else {
            $entityManager->remove($Vehicle);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Vehicle deleted successfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_list_vehicle');
        }
    }

    public function mileageAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
    
        $entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        $vehicles = $entityManager->getRepository('DrivingSchoolAdminBundle:VehicleEntity')->findBy(array('id' => $id, 'DrivingSchool' => $userId));
        if(!empty($vehicles))
        {
            /* $query = $connection->prepare("SELECT * FROM vehicle_km WHERE Vehicle_id =".$id);*/
            $query = $connection->prepare("SELECT vkm.id as vkmid,vkm.mileage,vkm.date,vkm.time,vkm.Vehicle_id  FROM vehicle AS v LEFT JOIN vehicle_km AS vkm ON v.id = vkm.Vehicle_id WHERE v.drivingschool_id = ".$userId." AND vkm.Vehicle_id =".$id." ORDER BY vkmid DESC");
            $query->execute();
            $vehiclekms = $query->fetchAll();

            $vehicles = $entityManager->getRepository('DrivingSchoolAdminBundle:VehicleEntity')->findOneBy(array('id' => $id));

            $dataval = [
                'vehiclekms' => $vehiclekms,
                'licenseplate' => $vehicles->getLicensePlate(),
                'id' => $id
            ];
            // dump($dataval);
            // exit();

            $arr   = $dataval['vehiclekms'][0];
            $mileageold = $arr['mileage'];
            $dateold = $arr['date'];
            $timeold = date("H:i", strtotime($arr['time']));
            $allmileage = 0;
            foreach ($dataval['vehiclekms'] as $key => $value) {
                $allmileage += $value['mileage'];
            } 
            
            $data = $request->request->all();
            if(isset($data['submit'])) {
                $vehiclekm = new VehicleKMEntity();
                $vehicleid = $entityManager->getRepository('DrivingSchoolAdminBundle:VehicleEntity')->find($id);
                $vehiclekm->setVehicleId($vehicleid); 

                $mileage = $data['mileage'];
                $vehiclekm->setMileage($mileage);

                $date = date("Y-m-d", strtotime($data['date']));
                $vehiclekm->setDate(new \DateTime($date)); 

                //$time = $data['time'];
                $time = date("H:i:s", strtotime($data['time']));
                $matchtime = date("H:i", strtotime($data['time']));
                $vehiclekm->setTime(new \DateTime($time)); 

                if($mileage == '0')
                {
                    $this->addFlash(
                        'notice',
                        'Mileage should not be 0. Please add another mileage.'
                    );
                } else {
                    if($mileageold < $mileage)
                    {
                        $totalmileage = $allmileage+$mileage;
                        if($totalmileage < 10000 && ($timeold != $matchtime || $dateold != $date))
                        {
                            $entityManager->persist($vehiclekm);
                            $entityManager->flush();

                            $this->addFlash(
                                'success',
                                'Mileage added successfully.'
                            ); 
                            return $this->redirectToRoute('driving_school_school_vehicle_mileage', array('id' => $id));
                        } else {
                            $this->addFlash(
                                'notice',
                                '2 errors meant that mileage could not be saved:
                                    Mileage is invalid
                                    Vehicle can not have driven more than 10,000 km per day.'
                            );
                        }
                    } else {
                        $this->addFlash(
                            'notice',
                            '1 error meant that mileage could not be saved: Mileage must be more than the previous position.'
                        );
                    } 
                }
            }

            return $this->render('DrivingSchoolSchoolBundle:Vehicle:mileage.html.twig', $dataval);
        } else {
            return $this->redirectToRoute('driving_school_school_list_vehicle');
        }
    }

    public function mileagedeleteAction(Request $request, $vehicleid, $id, UserInterface $user)
    {
        $userId = $user->getId();
       
        $entityManager = $this->getDoctrine()->getManager();
        $Vehicle = $entityManager->getRepository('DrivingSchoolAdminBundle:VehicleKMEntity')->find($id);

        if (!$Vehicle) { 
            // no instructor in the system
            throw $this->createNotFoundException(
                'No price & packages found for id '.$id
            );
        } else {
            $entityManager->remove($Vehicle);
            $entityManager->flush();
            return $this->redirectToRoute('driving_school_school_vehicle_mileage', array('id' => $vehicleid));
        }
    }
}