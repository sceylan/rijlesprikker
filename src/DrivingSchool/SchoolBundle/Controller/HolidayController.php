<?php

namespace DrivingSchool\SchoolBundle\Controller;

use DrivingSchool\SchoolBundle\Form\HolidayForm;
use DrivingSchool\AdminBundle\Entity\HolidayEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class HolidayController extends Controller
{
    public function listAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        
    	$entityManager = $this->getDoctrine()->getManager();
        $connection = $entityManager->getConnection();

        $query = $connection->prepare("SELECT h.id as holidayId, h.name as holidayName ,GROUP_CONCAT(i.instructor_name) as instructorName, h.date as holidaydate, h.until as holidayuntildate FROM holiday AS h LEFT JOIN instructor_holiday AS ih ON h.id = ih.holiday_id LEFT JOIN instructor AS i ON ih.instructor_id = i.id WHERE h.drivingschool_id =".$userId." GROUP BY holidayName");
        
        $query->execute();
        $holidays = $query->fetchAll();

        $data = [
            'holidays' => $holidays,
        ];

        return $this->render('DrivingSchoolSchoolBundle:Holiday:list.html.twig', $data);
    }

    public function createAction(Request $request, UserInterface $user)
    {
        $userId = $user->getId();
    
    	$holiday = new HolidayEntity();
    	$form = $this->createForm(HolidayForm::class, $holiday, array('userId' => $userId));

    	$form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $drivingschool = $entityManager->getRepository('DrivingSchoolAdminBundle:DrivingSchoolEntity')->find($userId);
            $holiday->setDrivingSchool($drivingschool); 

            $entityManager->persist($holiday);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Holiday added succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_holidays');
        }

    	return $this->render(
			'DrivingSchoolSchoolBundle:Holiday:create.html.twig',
            array('form' => $form->createView(),
                'title' => 'Create Holiday',
                'btn_title' => 'Create',
            )
		);	
    }

    public function editAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $holiday = $entityManager->getRepository('DrivingSchoolAdminBundle:HolidayEntity')->findOneBy(array('id' => $id, 'DrivingSchool' => $userId));
        if(!empty($holiday))
        {
            $form = $this->createForm(HolidayForm::class, $holiday, array('userId' => $userId));

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                $entityManager->persist($holiday);
                $entityManager->flush();

                $this->addFlash(
                    'success',
                    'Holiday updated succesfully.'
                ); 
                return $this->redirectToRoute('driving_school_school_holidays');
            }

            return $this->render(
                'DrivingSchoolSchoolBundle:Holiday:create.html.twig',
                array('form' => $form->createView(),
                    'title' => 'Edit Holiday',
                    'btn_title' => 'Update',
                )
            );
        } else {
            return $this->redirectToRoute('driving_school_school_holidays');
        }
    }

    public function deleteAction(Request $request, $id, UserInterface $user)
    {
        $userId = $user->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $holiday = $entityManager->getRepository('DrivingSchoolAdminBundle:HolidayEntity')->find($id);

        if (!$holiday) { 
            // no holiday in the system
            throw $this->createNotFoundException(
                'No price & packages found for id '.$id
            );
        } else {
            $entityManager->remove($holiday);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Holiday deleted succesfully.'
            ); 
            return $this->redirectToRoute('driving_school_school_holidays');
        }
    }
}
