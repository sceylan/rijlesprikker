<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\ORM\EntityRepository;

class DrivinglessonsForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('attr' => array('placeholder' => 'Name')))
            ->add('price', TextType::class, array('attr' => array('placeholder' => 'Price')))
            ->add('includedVat')
            ->add('vat', 'choice', array(
                    'choices' => array(
                        '21% VAT' => "21",
                        '19% VAT' => "19",
                        '6% VAT' => "6",
                        'No tax' => "0",
                    )
                ))
            ->add('qty', TextType::class, array('required' => false, 'attr' => array('maxlength' => '5')))
            ->add('unit', 'choice', array(
                    'choices' => array(
                        'minutes' => "minutes",
                        'broken' => "broken"
                    )
                ))
            ->add('appointmentLabel', 'hidden')
            ->add('schedule', 'choice', array(
                    'choices' => array(
                        'Yes, I want to schedule this' => "Yes, I want to schedule this",
                        'No, I do not want to schedule this' => "No, I do not want to schedule this"
                    ),
                ))  
            ->add('lengthQty', 'choice', array(
                    'choices' => array(
                      '0:30' => "30",
                      '0:45' => "45",
                      '0:50' => "50",
                      '0:55' => "55",
                      '1:00' => "60",
                      '1:15' => "75",
                      '1:30' => "90",
                      '1:40' => "100",
                      '1:45' => "105",
                      '2:00' => "120",
                      '2:30' => "150",
                      '3:00' => "180",
                      '3:20' => "200",
                      '6:00' => "360",
                      '7:00' => "420",
                      '7:30' => "450",
                      '8:00' => "480",
                    ),
                ))

            ->add('exam', 'choice', array(
                    'choices' => array(
                        'Yes, This is a test or exam' => "Yes, This is a test or exam",
                        'No, This is not a test or exam' => "No, This is not a test or exam"
                    ),
                ))  
            ->add('accountingCode')
            ->add('category', 'choice', array(
                    'choices' => array(
                        'Driving lesson or exam' => "Driving lesson or exam",
                        'Remaining' => "Remaining"
                    ),
                ))
            ->add('codeCbr')
            // ->add('linkedTo', 'choice', array(
            //         'choices' => [
            //             'Not Linked' => '0'
            //         ]
                        // 'Main Statuses' => array(
                            // 'class' => 'DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity',
                            // 'query_builder' => function(EntityRepository $repository) {
                            //     $qb = $repository->createQueryBuilder('u');
                            //     // the function returns a QueryBuilder object
                            //     return $qb
                            //         ->where($qb->expr()->eq('u.type', '?1'))
                            //         ->setParameter('1', '0')
                            //     ;
                            // },
                            // 'choice_label' => 'name',
                        // ),
                        // 'Out of Stock Statuses' => array(
                        //     'Backordered' => 'stock_backordered',
                        //     'Discontinued' => 'stock_discontinued',
                        // )
                    // ))
            ->add('status')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => TrainingSessionsEntity::class,
        ));
    }
}
