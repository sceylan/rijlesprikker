<?php
namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\VehicleKMEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Doctrine\ORM\EntityRepository;

class VehicleKMForm extends AbstractType
{
	private $userId;

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->userId = $options['userId'];
		
		$builder
			->add('Vehicle_id', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\VehicleEntity',
                    'query_builder' => function(EntityRepository $repository) {
				        $qb = $repository->createQueryBuilder('u');
				        // the function returns a QueryBuilder object
				        return $qb
				            ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
				            ->setParameter('1', $this->userId)
				        ;
				    },
                    'choice_label' => 'licensePlate',
                    'label' => 'Vehicle',
                    'placeholder'=>'Choose a vehicle..',
                ])
			->add('mileage', 'integer')
			->add('note')
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => VehicleKMEntity::class,
		));
		$resolver->setRequired('userId');
	}
}