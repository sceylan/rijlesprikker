<?php
namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\StudentEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;

class StudentProfileForm extends AbstractType
{
	private $userId;
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->userId = $options['userId'];
		$builder
			->add('firstName', TextType::class)
			->add('lastName', TextType::class)
			->add('initials', TextType::class)
			->add('houseNumber', TextType::class)
			->add('postalCode', TextType::class)
			->add('place', TextType::class)
			->add('email', TextType::class)
			->add('phone', TextType::class)
			->add('dateOfBirth', 'datetime',['label' => 'Date of birth', 'widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'required' => false, 'attr' => array('readonly' => 'readonly')])
			->add('gender', 'choice', array(
				'choices' => array(
					'Male' => '1',
					'Female' => '2',
				)
			))
			->add('birthPlace', TextType::class)
			->add('bsn', TextType::class)
			->add('driversLicense', TextType::class)
			->add('candidateNumber', TextType::class)
			->add('instructorsNote', 'textarea')
			->add('note', 'textarea')
			->add('instructor', 'entity', [
                    'class' => 'DrivingSchool\AdminBundle\Entity\InstructorEntity',
                    'query_builder' => function(EntityRepository $repository) {
				        $qb = $repository->createQueryBuilder('u');
				        // the function returns a QueryBuilder object
				        return $qb
				            ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
				            ->setParameter('1', $this->userId)
				        ;
				    },
                    'choice_label' => 'instructorName',
                    'placeholder' => 'No Preference'
                ]
            )
			->add('educations', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                    'query_builder' => function(EntityRepository $repository) {
				        $qb = $repository->createQueryBuilder('u');
				        // the function returns a QueryBuilder object
				        return $qb
				            ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
				            ->setParameter('1', $this->userId)
				        ;
				    },
                    'multiple' => true,
                    'expanded' => true,
                    'choice_label' => 'name',
                ]
            )
			->add('authorization')
			->add('healthInformation')
			->add('tooGood')
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => StudentEntity::class,
		));
		$resolver->setRequired('userId');
	}
}