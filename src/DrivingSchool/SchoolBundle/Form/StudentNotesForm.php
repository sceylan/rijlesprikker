<?php
namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\StudentNotesEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class StudentNotesForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('desc', 'textarea')
			->add('notesImage', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'default', 
                'attr' => array('accept' => 'image/*'),
            ))
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => StudentNotesEntity::class,
		));
	}
}