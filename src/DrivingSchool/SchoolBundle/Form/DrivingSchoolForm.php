<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Validator\Constraints\NotBlank;

class DrivingSchoolForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('schoolName', TextType::class, array('trim'   => true))
            ->add('schoolAddress', TextType::class, array('label' => 'Address', 'constraints' => array(new NotBlank())))
            ->add('email', 'text', array('label' => 'Email'))
            ->add('schoolPhone', TextType::class, array('label' => 'Phone', 'constraints' => array(new NotBlank())))
            ->add('facebookUrl', UrlType::class, array('label' => 'Facebook','required' => false))
            ->add('instagramUrl', UrlType::class, array('label' => 'Instagram','required' => false))
            ->add('linkedinUrl', UrlType::class, array('label' => 'Linkedin','required' => false))
            ->add('twitterUrl', UrlType::class, array('label' => 'Twitter','required' => false))
            ->add('schoolWebsite', UrlType::class, array('label' => 'Website','required' => false))
            ->add('schoolCCNNumber', 'text', array('label' => 'Chamber of Commerce Number','required' => false))
            ->add('schoolVatNumber', 'text', array('label' => 'VAT Number','required' => false))
            ->add('schoolBankActNumber', 'text', array('label' => 'Bank Account Number','required' => false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DrivingSchoolEntity::class,
        ));
    }
}
