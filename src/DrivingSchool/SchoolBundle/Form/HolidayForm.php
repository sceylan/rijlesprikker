<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\HolidayEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\ORM\EntityRepository;

class HolidayForm extends AbstractType
{
	private $userId;

	public function buildForm(FormBuilderInterface $builder, array $options )
    {
    	$this->userId = $options['userId'];
        $currentDate = date('d-m-Y');
        $builder
            ->add('name', TextType::class)
            // ->add('date', 'text', array('required' => false))
            ->add('date', 'datetime',['label' => 'Start Date', 'widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'required' => false, 'attr' => array('readonly' => 'readonly', 'value' => $currentDate)])
            // ->add('until', 'text', array('required' => false))
            ->add('until', 'datetime',['label' => 'End Date', 'widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'required' => false, 'attr' => array('readonly' => 'readonly', 'value' => $currentDate)])
            ->add('instructor', 'entity', [
                    'class' => 'DrivingSchool\AdminBundle\Entity\InstructorEntity',
                    'query_builder' => function(EntityRepository $repository) {
				        $qb = $repository->createQueryBuilder('u');
				        // the function returns a QueryBuilder object
				        return $qb
				            ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
				            ->setParameter('1', $this->userId)
				        ;
				    },
                    'choice_label' => 'instructorName',
                    'multiple' => true,
                    'expanded' => true,
                    'required' => false
                ]
            )
            ->add('status')
         
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => HolidayEntity::class,
        ));
    	$resolver->setRequired('userId');
    }
}
