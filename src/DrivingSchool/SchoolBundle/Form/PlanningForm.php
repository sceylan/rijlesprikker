<?php
namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\PlanningEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityManagerInterface;
use DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PlanningForm extends AbstractType
{
	private $userId;

    private $em;
	
    private $authorizationChecker=null;
    private $authorizationToken=null;

    public function __construct(AuthorizationChecker $authorizationChecker, TokenStorage $authorizationToken)
    {
      $this->authorizationChecker = $authorizationChecker;
      $this->authorizationToken = $authorizationToken;
    }

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->userId = $options['userId'];
		$this->em = $options['entity_manager'];
		$builder
			//->add('title', null,['required' => 'true','label' => 'Title'])
			->add('description', null,['required' => 'true','attr' => array('placeholder' => 'Notice Insert Wear Insertor')])
            ->add('location', null,['required' => 'true','attr' => array('placeholder' => 'Location')])
            ->add('isReservation', null,array('label' => 'This is a reservation'))
            ->add('startdate', 'datetime',['label' => 'Start Date/Time', 'date_widget' => 'single_text', 'time_widget' => 'single_text', 'date_format' => 'dd-MM-yyyy', 'required' => 'true', 'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 ]])
            ->add('enddate', 'datetime',['label' => 'Length','date_widget' => 'single_text', 'time_widget' => 'single_text', 'date_format' => 'dd-MM-yyyy', 'required' => 'true', 'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 ]])
			/*->add('school', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity',
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder'=> 'Select School',
                    'choice_label' => 'schoolName',
                    'label' => 'School'
                ])*/
            /*->add('pricePackage', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                    'query_builder' => function(EntityRepository $repository) {
	                    $qb = $repository->createQueryBuilder('u');
	                    // the function returns a QueryBuilder object
	                    return $qb
	                        ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
	                        ->setParameter('1', $this->userId)
	                    ;
	                },
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder'=> 'Select Package',
                    'choice_label' => 'name',
                    'label' => 'Package'
                ])*/
            /*->add('trainingSession', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity',
                    'query_builder' => function(EntityRepository $repository) {
	                    $qb = $repository->createQueryBuilder('u');
	                    // the function returns a QueryBuilder object
	                    return $qb
	                        ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
	                        ->setParameter('1', $this->userId)
	                    ;
	                },
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder'=> 'Select Training Session',
                    'choice_label' => 'name',
                    'label' => 'Training Session'
                ])*/
			/*->add('student', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\StudentEntity',
                    'query_builder' => function(EntityRepository $repository) {
	                    $qb = $repository->createQueryBuilder('u');
	                    // the function returns a QueryBuilder object
	                    return $qb
	                        ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
	                        ->setParameter('1', $this->userId)
	                    ;
	                },
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder'=> 'Select Student',
                    'choice_label' => 'firstName',
                    'label' => 'Student'
                ])*/
			/*->add('vehicle', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\VehicleEntity',
                    'query_builder' => function(EntityRepository $repository) {
	                    $qb = $repository->createQueryBuilder('u');
	                    // the function returns a QueryBuilder object
	                    return $qb
	                        ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
	                        ->setParameter('1', $this->userId)
	                    ;
	                },
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder'=> 'Select Vehicle',
                    'choice_label' => 'licensePlate',
                    'label' => 'Vehicle'
                ])*/
			/*->add('instructor', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\InstructorEntity',
                    'query_builder' => function(EntityRepository $repository) {
	                    $qb = $repository->createQueryBuilder('u');
	                    // the function returns a QueryBuilder object
	                    return $qb
	                        ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
	                        ->setParameter('1', $this->userId)
	                    ;
	                },
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder'=> 'Select Instructor',
                    'choice_label' => 'instructorName',
                    'label' => 'Instructor'
                ])*/
		;

		$builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
	}

	protected function addElements(FormInterface $form, PriceAndPackagesEntity $pricePackage = null) {
        // 4. Add the province element

        if (TRUE === $this->authorizationChecker->isGranted('ROLE_SCHOOL'))
        {
            $form->add('pricePackage', 'entity', [
                'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                'query_builder' => function(EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('u');
                    // the function returns a QueryBuilder object
                    return $qb
                        ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
                        ->setParameter('1', $this->userId)
                    ;
                },
                'multiple' => false,
                'expanded' => false,
                'placeholder'=> 'Select Package',
                'choice_label' => 'name',
                'label' => 'Package'
            ]);
        } elseif(TRUE === $this->authorizationChecker->isGranted('ROLE_INSTRUCTOR')) {
            $form->add('pricePackage', 'entity', [
                'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                'query_builder' => function(EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('u');
                    // the function returns a QueryBuilder object
                    return $qb
                        ->leftJoin('u.instructor','i')
                        ->where($qb->expr()->eq('i.id', '?1'))
                        ->setParameter('1', $this->userId)
                    ;
                },
                'multiple' => false,
                'expanded' => false,
                'placeholder'=> 'Select Package',
                'choice_label' => 'name',
                'label' => 'Package'
            ]);
        }
        
        // Training Session empty, unless there is a selected Package (Edit View)
        $trainingSession = array();
        
        // If there is a Package stored in the Planning entity, load the Training Session of it
        if ($pricePackage) {
            // Training Session of the Package if there's a selected Package

            $repoTrainingSession = $this->em->getRepository('DrivingSchoolAdminBundle:TrainingSessionsEntity');

            if (TRUE === $this->authorizationChecker->isGranted('ROLE_SCHOOL')){
                $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $this->userId);
            } elseif(TRUE === $this->authorizationChecker->isGranted('ROLE_INSTRUCTOR')) {
                $user  = $this->authorizationToken->getToken()->getUser();
                $schoolId = $user->getDrivingSchool()->getId();
                $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $schoolId);
            }
            
            
            $trainingSession = $repoTrainingSession->createQueryBuilder("q")
                ->where("q.pricePackageId = :pricePackageId")
                ->andWhere("q.DrivingSchool = :userId")
                ->andWhere("q.type IN ( 0,1 )")
                ->setParameters($parameters)
                ->getQuery()
                ->getResult();
        }
        
        // Add the Training Session field with the properly data
        $form->add('trainingSession', EntityType::class, array(
            'required' => true,
            'placeholder' => 'Select a Price Package first',
            'class' => 'DrivingSchoolAdminBundle:TrainingSessionsEntity',
            'choices' => $trainingSession
        ));




        // Student empty, unless there is a selected Package (Edit View)
        $student = array();
        
        // If there is a Package stored in the Planning entity, load the Training Session of it

        if ($pricePackage) {
            // Student of the Package if there's a selected Package
            $studentRepository = $this->em->getRepository("DrivingSchoolAdminBundle:StudentEntity");
        	$parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $this->userId);
            if (TRUE === $this->authorizationChecker->isGranted('ROLE_SCHOOL'))
            {
                $student = $studentRepository->createQueryBuilder("q")
    	            ->innerJoin('q.educations', 'p')
    	            ->where("p.id = :pricePackageId")
    	            ->andWhere("q.DrivingSchool = :userId")
    	            ->setParameters($parameters)
    	            ->getQuery()
    	            ->getResult();
            } elseif(TRUE === $this->authorizationChecker->isGranted('ROLE_INSTRUCTOR')) {
                $student = $studentRepository->createQueryBuilder("q")
                    ->innerJoin('q.educations', 'p')
                    ->where("p.id = :pricePackageId")
                    ->andWhere("q.instructor = :userId")
                    ->setParameters($parameters)
                    ->getQuery()
                    ->getResult();
            }
            
        }
        
        // Add the Student field with the properly data
        $form->add('student', EntityType::class, array(
            'required' => true,
            'placeholder' => 'Select a Price Package first',
            'class' => 'DrivingSchoolAdminBundle:StudentEntity',
            'choices' => $student
        ));



        // Vehicle empty, unless there is a selected Package (Edit View)
        $vehicle = array();
        
        // If there is a Package stored in the Planning entity, load the Training Session of it
        if ($pricePackage) {
            // Vehicle of the Package if there's a selected Package

            if (TRUE === $this->authorizationChecker->isGranted('ROLE_SCHOOL')){
                $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $this->userId);
            } elseif(TRUE === $this->authorizationChecker->isGranted('ROLE_INSTRUCTOR')) {
                $user  = $this->authorizationToken->getToken()->getUser();
                $schoolId = $user->getDrivingSchool()->getId();
                $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $schoolId);
            }

            $vehicleRepository = $this->em->getRepository("DrivingSchoolAdminBundle:VehicleEntity");
            
            $vehicle = $vehicleRepository->createQueryBuilder("q")
	            ->innerJoin('q.suitableTrainingCourses', 'p')
	            ->where("p.id = :pricePackageId")
	            ->andWhere("q.DrivingSchool = :userId")
	            ->setParameters($parameters)
	            ->getQuery()
	            ->getResult();
        }
        
        // Add the Vehicle field with the properly data
        $form->add('vehicle', EntityType::class, array(
            'required' => true,
            'placeholder' => 'Select a Price Package first',
            'class' => 'DrivingSchoolAdminBundle:VehicleEntity',
            'choices' => $vehicle
        ));



        // Instructor empty, unless there is a selected Package (Edit View)
        $instructor = array();
        
        // If there is a Package stored in the Planning entity, load the Training Session of it
        if ($pricePackage) {
            // Instructor of the Package if there's a selected Package

            $instructorRepository = $this->em->getRepository("DrivingSchoolAdminBundle:InstructorEntity");

            $parameters = array("pricePackageId" => $pricePackage->getId() , "userId" => $this->userId);
            if (TRUE === $this->authorizationChecker->isGranted('ROLE_SCHOOL')){
                $instructor = $instructorRepository->createQueryBuilder("q")
    	            ->innerJoin('q.authorizedFor', 'p')
    	            ->where("p.id = :pricePackageId")
    	            ->andWhere("q.DrivingSchool = :userId")
    	            ->setParameters($parameters)
    	            ->getQuery()
    	            ->getResult();
            } elseif(TRUE === $this->authorizationChecker->isGranted('ROLE_INSTRUCTOR')) {
                $instructor = $instructorRepository->createQueryBuilder("q")
                    ->innerJoin('q.authorizedFor', 'p')
                    ->where("p.id = :pricePackageId")
                    ->andWhere("q.id = :userId")
                    ->setParameters($parameters)
                    ->getQuery()
                    ->getResult();
            }
        }
        
        // Add the Instructor field with the properly data
        $form->add('instructor', EntityType::class, array(
            'required' => true,
            'placeholder' => 'Select a Price Package first',
            'class' => 'DrivingSchoolAdminBundle:InstructorEntity',
            'choices' => $instructor
        ));
    }

	function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Search for selected Training Session and convert it into an Entity
        $pricePackage = $this->em->getRepository('DrivingSchoolAdminBundle:PriceAndPackagesEntity')->find($data['pricePackage']);
        
        $this->addElements($form, $pricePackage);
    }

    function onPreSetData(FormEvent $event) {
        $planning = $event->getData();
        $form = $event->getForm();

        $pricePackage = $planning->getPricePackage() ? $planning->getPricePackage() : null;
        
        $this->addElements($form, $pricePackage);
    }

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => PlanningEntity::class,
		));
		$resolver->setRequired('userId');
		$resolver->setRequired('entity_manager');
	}
}