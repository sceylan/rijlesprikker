<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class FinancialForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('window_on_envelope', 'choice', [
                    'choices' => [
                        'Window is on the left' => '0',
                        'Window is on the right' => '1'
                    ],
                    'multiple' => false,
                    'expanded' => true,
                ])
            ->add('payment_term', 'choice', [
                'choices' => [
                    '5 days' => '5 days',
                    '7 days' => '7 days',
                    '10 days' => '10 days',
                    '14 days' => '14 days',
                    '21 days' => '21 days',
                    '30 days' => '30 days',
                    '45 days' => '45 days',
                    '60 days' => '60 days',
                ],
                'required' => false
            ])
            ->add('logo', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'default',
            ))
            ->add('footer_for_invoice', 'textarea', array('attr' => array('placeholder' => 'We kindly request that you transfer the amount due within ... days to our bank account stating the invoice number.'),'required' => false))
            ->add('footer_with_direct_debit', 'textarea', array('attr' => array('placeholder' => 'The guilty amount automatically enters the door of the bookkeeping bank.'),'required' => false))
            ->add('footer_1st_reminder', 'textarea',array('attr' => array('placeholder' => 'Footer 1st Reminder'),'required' => false))
            ->add('footer_2nd_memory', 'textarea',array('attr' => array('placeholder' => 'Footer 2nd Memory'),'required' => false))
            ->add('footer_with_credit_invoice', 'textarea', array('attr' => array('placeholder' => 'The guilty thank you will be transferred z.s.m. to your bank account.'),'required' => false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DrivingSchoolEntity::class,
        ));
    }
}
