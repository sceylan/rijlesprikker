<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class DrivingSchoolLoginForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array('attr' => array('placeholder' => 'User Name')))
            ->add('password', PasswordType::class, array('attr' => array('placeholder' => 'Password')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DrivingSchoolEntity::class,
        ));
    }
}
