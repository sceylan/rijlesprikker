<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\InstructorEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Doctrine\ORM\EntityRepository;

class InstructorForm extends AbstractType
{
    private $userId;

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->userId = $options['userId'];
        $builder
            ->add('instructorName', TextType::class)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('codeCRB', TextType::class, array('label' => 'Code CBR', 'required' => false))
            ->add('instructorEmail', EmailType::class)
            ->add('phoneNumber', TextType::class)
            ->add('authorizedFor', 'entity',[
                'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                'query_builder' => function(EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('u');
                    // the function returns a QueryBuilder object
                    return $qb
                        ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
                        ->setParameter('1', $this->userId)
                    ;
                },
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
            ])
            ->add('instructorImage', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'default',
                'required' => false
            ))
            // ->add('publish_calendar', 'date')
            ->add('status')
            ->add('monday_start', 'time', array('label' => 'Start Time','required' => false))
            ->add('monday_end', 'time', array('label' => 'End Time','required' => false))
            ->add('tuesday_start', 'time', array('label' => 'Start Time','required' => false))
            ->add('tuesday_end', 'time', array('label' => 'End Time','required' => false))
            ->add('wednesday_start', 'time', array('label' => 'Start','required' => false))
            ->add('wednesday_end', 'time', array('label' => 'End','required' => false))
            ->add('thursday_start', 'time', array('label' => 'Start','required' => false))
            ->add('thursday_end', 'time', array('label' => 'End','required' => false))
            ->add('friday_start', 'time', array('label' => 'Start','required' => false))
            ->add('friday_end', 'time', array('label' => 'End','required' => false))
            ->add('saturday_start', 'time', array('label' => 'Start','required' => false))
            ->add('saturday_end', 'time', array('label' => 'End','required' => false))
            ->add('sunday_start', 'time', array('label' => 'Start','required' => false))
            ->add('sunday_end', 'time', array('label' => 'End','required' => false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => InstructorEntity::class,
        ));
        $resolver->setRequired('userId');
    }
}
