<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class OtherCoursesForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('attr' => array('placeholder' => 'Name')))
            ->add('price')
            ->add('includedVat')
            ->add('vat', 'choice', array(
                    'choices' => array(
                        '21% VAT' => "21",
                        '19% VAT' => "19",
                        '6% VAT' => "6",
                        'No tax' => "0",
                    )
                ))
            ->add('qty', TextType::class, array('required' => false, 'attr' => array('maxlength' => '5')))
            ->add('unit', 'choice', array(
                    'choices' => array(
                        'minutes' => "minutes",
                        'broken' => "broken"
                    )
                ))
            ->add('schedule', 'choice', array(
                    'choices' => array(
                        'Yes, I want to schedule this' => "Yes, I want to schedule this",
                        'No, I do not want to schedule this' => "No, I do not want to schedule this"
                    ),
                ))  
            ->add('exam', 'choice', array(
                    'choices' => array(
                        'Yes, This is a test or exam' => "Yes, This is a test or exam",
                        'No, This is not a test or exam' => "No, This is not a test or exam"
                    ),
                ))  
            ->add('accountingCode')
            ->add('category', 'choice', array(
                    'choices' => array(
                        'Driving lesson or exam' => "Driving lesson or exam",
                        'Remaining' => "Remaining"
                    ),
                ))
            ->add('codeCbr')
            ->add('status')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => TrainingSessionsEntity::class,
        ));
    }
}
