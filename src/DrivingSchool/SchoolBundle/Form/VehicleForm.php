<?php
namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\VehicleEntity;
use DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Doctrine\ORM\EntityRepository;

class VehicleForm extends AbstractType
{
	private $userId;

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->userId = $options['userId'];
		$currentyear = date("Y");
        $last40year = date("Y",strtotime("-50 year"));
        $year = array();
        for ($i = $currentyear; $i >= $last40year; $i--) {
            $year[$i] = $i;
        }

		$builder
			->add('licensePlate')
			->add('cbrCode')
			->add('brand')
			->add('FashionModel')
			->add('construction_year', 'choice', array('choices' => $year))
			->add('suitable_training_courses', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                    'query_builder' => function(EntityRepository $repository) {
				        $qb = $repository->createQueryBuilder('u');
				        // the function returns a QueryBuilder object
				        return $qb
				            ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
				            ->setParameter('1', $this->userId)
				        ;
				    },
                    'multiple' => true,
                    'expanded' => true,
                    'choice_label' => 'name',
                    'label' => 'Suitable Training Courses',
                    'required' => false
                ])
			->add('status')
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => VehicleEntity::class,
		));
		$resolver->setRequired('userId');
	}
}