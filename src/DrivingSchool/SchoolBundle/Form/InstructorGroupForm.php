<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\InstructorGroupEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\ORM\EntityRepository;

class InstructorGroupForm extends AbstractType
{
	private $userId;

	public function buildForm(FormBuilderInterface $builder, array $options )
    {
    	$this->userId = $options['userId'];
    	/*dump($userId);
    	exit();*/
        $builder
            ->add('name', TextType::class)
            /*->add('instructor', 'entity', [
                    'class' => 'DrivingSchool\AdminBundle\Entity\InstructorEntity',
                    'choice_label' => 'instructorName',
                    'multiple' => true,
                    'expanded' => true,
                ]
            )*/
            ->add('instructor', 'entity', [
                    'class' => 'DrivingSchool\AdminBundle\Entity\InstructorEntity',
                    'query_builder' => function(EntityRepository $repository) {
				        $qb = $repository->createQueryBuilder('u');
				        // the function returns a QueryBuilder object
				        return $qb
				            ->where($qb->expr()->eq('u.DrivingSchool', '?1'))
				            ->setParameter('1', $this->userId)
				        ;
				    },
                    'choice_label' => 'instructorName',
                    'multiple' => true,
                    'expanded' => true,
                    'required' => false
                ]
            )
            ->add('status')
         
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => InstructorGroupEntity::class,
        ));
    	$resolver->setRequired('userId');
    }
}
