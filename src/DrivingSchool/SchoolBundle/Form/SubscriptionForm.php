<?php
namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Doctrine\ORM\EntityRepository;

class SubscriptionForm extends AbstractType
{
	private $userId;

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$this->userId = $options['userId'];
		$builder
			->add('schoolCCNNumber', null,['required' => 'true','label' => 'Chamber of Commerce number'])
			->add('schoolVatNumber', null,['required' => 'true','label' => 'VAT number'])
			->add('schoolBankActNumber', null,['required' => 'true','label' => 'IBAN'])
			->add('bic', null,['required' => 'true','label' => 'BIC'])
			->add('inNameOf', null,['required' => 'true','label' => 'In the name of'])
			->add('subscriptions', 'entity',[
                    'class' => 'DrivingSchool\AdminBundle\Entity\SubscriptionPlanEntity',
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder'=> 'Select your package',
                    'choice_label' => 'packageName',
                    'label' => 'Package'
                ])
			->add('subscriptionDate', 'datetime',[
                    'data' => new \DateTime()
                ])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => DrivingSchoolEntity::class,
		));
		$resolver->setRequired('userId');
	}
}