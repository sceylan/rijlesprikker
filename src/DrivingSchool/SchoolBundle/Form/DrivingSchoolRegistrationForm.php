<?php

namespace DrivingSchool\SchoolBundle\Form;

use DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class DrivingSchoolRegistrationForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('schoolName', TextType::class, array('attr' => array('placeholder' => 'School Name')))
            ->add('username', TextType::class, array('attr' => array('placeholder' => 'User Name')))
            ->add('schoolContactPerson', TextType::class, array('attr' => array('placeholder' => 'Contact Person')))
            ->add('email', TextType::class, array('attr' => array('placeholder' => 'Email')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DrivingSchoolEntity::class,
        ));
    }
}
