$(document).ready(function() {
	// Website Tour For Dashboard
    var tour = new Tour({
        name: "dashboard-tour",
        class: 'dashboard-tour',
        backdrop: true,
        steps: [
        {
            element: ".dashboard-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to your Dashboard"
        },
        {
            element: ".plannning-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Navigate to check your Events"
        },
        {
            element: ".students-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Way to manage your pupils."
        },
        {
            element: ".financial-t",
            title: "Dashboard Tour",
            placement: "bottom",
            content: "Manage your transactions and financials."
        },
        {
            element: "#notification-click",
            title: "Dashboard Tour",
            placement: "left",
            content: "You can check all your notification here."
        },
        {
            element: "#menudrpdown",
            title: "Dashboard Tour",
            placement: "left",
            content: "This is a Gateway to Manage all your resources"
        },
        {
            element: ".rightnow-lessions-t",
            title: "Dashboard Tour",
            content: "These are your Ongoing lessions."
        },
        {
            element: ".today-lessions-t",
            title: "Dashboard Tour",
            placement: "left",
            content: "These are your Today's lessions."
        },
        {
            element: ".mileage-form-t",
            title: "Dashboard Tour",
            content: "You can make new entry for mileage using this form."
        },
        {
            element: ".outstanding-reservations-t",
            title: "Dashboard Tour",
            content: "These are your outstanding reservations."
        },
        {
            element: ".whatclick-t",
            title: "Dashboard Tour",
            placement: "left",
            content: "These are your outstanding reservations."
        }
    ]});
    if(jQuery(".dashboard-tour").length > 0)
    {
	    // Initialize the tour
	    tour.init();
	    // Start the tour
	    tour.start(); 
	}

	// Website Tour For Planning
	var planning_tour = new Tour({
        name: "planning-tour",
        class: "planning-tour",
        backdrop: true,
        steps: [
        {
            element: ".instructor-selector-t",
            title: "Planning Tour",
            placement: "bottom",
            content: "You can select your instructor"
        },
        {
            element: ".planningprev-t",
            title: "Planning Tour",
            placement: "bottom",
            content: "Changed previous date of planning"
        },
        {
            element: ".planningnext-t",
            title: "Planning Tour",
            placement: "bottom",
            content: "Changed next date of planning"
        },
        {
            element: ".planningtoday-t",
            title: "Planning Tour",
            placement: "bottom",
            content: "Select today planning"
        },
        {
            element: ".planningday-t",
            title: "Planning Tour",
            placement: "left",
            content: "Select Day wise planning"
        },
        {
            element: ".planningweek-t",
            title: "Planning Tour",
            placement: "left",
            content: "Select Week wise planning"
        },
        {
            element: ".planningmonth-t",
            title: "Planning Tour",
            placement: "left",
            content: "Select Month wise planning"
        },
        {
            element: ".planningcalender-t",
            title: "Planning Tour",
            placement: "top",
            content: "You can make new event for calender using this form"
        }
    ]});
	if(jQuery(".planning-tour").length > 0)
    {
    	// Initialize the planning tour
	    planning_tour.init();
	    // Start the planning tour
	    planning_tour.start(); 	
    }

    // Website Tour For Financial
	var financial_tour = new Tour({
        name: "financial-tour",
        backdrop: true,
        steps: [
        {
            element: ".financial-search-t",
            title: "Financial Tour",
            placement: "bottom",
            content: "You can search date wise your invoice"
        },
        {
            element: ".invoice-detail-t",
            title: "Financial Tour",
            placement: "bottom",
            content: "These are your invoice detail."
        },
        {
            element: ".financial-reset-t",
            title: "Financial Tour",
            placement: "bottom",
            content: "You can refresh page here."
        },
        {
            element: ".finacial-invoice-num-t",
            title: "Financial Tour",
            placement: "bottom",
            content: "You can check your invoice number and detail."
        },
    ]});
    if(jQuery(".financial-tour").length > 0)
    {
	    // Initialize the financial tour
	    financial_tour.init();
	    // Start the financial tour
	    financial_tour.start(); 
	}

    // Website Tour For student card
	var studentcard_tour = new Tour({
        name: "studentcard-tour",
        backdrop: true,
        steps: [
        {
            element: ".card-profile-view-t",
            title: "Student Card Tour",
            placement: "top",
            content: "These are student profile detail."
        },
        {
            element: ".card-activate-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "You can Activate & Deactivate Student Account"
        },
        {
            element: ".card-profile-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "You can Changed Student Profile"
        },
        {
            element: ".card-message-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "You can Send Message to Student"
        },
        {
            element: ".card-credit-history-t",
            title: "Student Card Tour",
            placement: "top",
            content: "These are Student credit financial history."
        },
        {
            element: ".card-packageorcreadit-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "You Can make new student package or credit detail using assigned packages."
        },
        {
            element: ".card-creditlist-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "These are displayed student package or credit listing and all invoices."
        },
        {
            element: ".card-lesson-overview-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "These are student lesson overview."
        },
        {
            element: ".card-exams-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "These are student exams detail."
        },
        {
            element: ".card-financial-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "These are student selected financial & invoice detail."
        },
        {
            element: ".card-notes-t",
            title: "Student Card Tour",
            placement: "bottom",
            content: "You can make student new notes with images."
        },
    ]});
    if(jQuery(".studentcard-tour").length > 0)
    {
	    // Initialize the student card tour
	    studentcard_tour.init();
	    // Start the student card tour
	    studentcard_tour.start(); 
	}

    // Website Tour For instructor
	var instructor_tour = new Tour({
        name: "instructor-tour",
        backdrop: true,
        steps: [
        {
            element: ".instructor-new-t",
            title: "Instructor Tour",
            placement: "bottom",
            content: "You can create new instructor account"
        },
        {
            element: ".instructor-name-t",
            title: "Instructor Tour",
            placement: "bottom",
            content: "These are your instructor name"
        },
        {
            element: ".instructor-action-t",
            title: "Instructor Tour",
            placement: "bottom",
            content: "You can update,delete & set permission to relevant instructor."
        },
    ]});
    if(jQuery(".instructor-tour").length > 0)
    {
	    // Initialize the instructor tour
	    instructor_tour.init();
	    // Start the instructor tour
	    instructor_tour.start(); 
	}

    // Website Tour For price and packages
	var pricepackage_tour = new Tour({
        name: "PriceAndPackages-tour",
        backdrop: true,
        steps: [
        {
            element: ".pricepackage-new-t",
            title: "Price And Packages Tour",
            placement: "bottom",
            content: "You can make new packages"
        },
    ]});
    if(jQuery(".PriceAndPackages-tour").length > 0)
    {
	    // Initialize the price and packages tour
	    pricepackage_tour.init();
	    // Start the price and packages tour
	    pricepackage_tour.start(); 
	}

    // Website Tour For package cources
	var course_tour = new Tour({
        name: "course-tour",
        backdrop: true,
        steps: [
        {
            element: ".course-new-t",
            title: "Driving Lessons Tour",
            placement: "bottom",
            content: "You can make new driving lessons"
        },
        {
            element: ".package-update-t",
            title: "Driving Lessons Tour",
            placement: "bottom",
            content: "You can update relevant price and packages"
        },
        {
            element: ".cources-action-t",
            title: "Driving Lessons Tour",
            placement: "bottom",
            content: "You can update relevant cources"
        },
        
    ]});
    if(jQuery(".course-tour").length > 0)
    {
	    // Initialize the package cources tour
	    course_tour.init();
	    // Start the package cources tour
	    course_tour.start(); 
	}

    // Website Tour For schedule
	var schedule_tour = new Tour({
        name: "schedule-tour",
        backdrop: true,
        steps: [
        {
            element: ".schedule-reservation-t",
            title: "Schedule Tour",
            placement: "bottom",
            content: "You can update your reservation"
        },
        {
            element: ".schedule-appointment-t",
            title: "Schedule Tour",
            placement: "bottom",
            content: "You can update your cources appointment color"
        },
        {
            element: ".schedule-group-t",
            title: "Schedule Tour",
            placement: "bottom",
            content: "You can manage your instructor group"
        },
        {
            element: ".schedule-holiday-t",
            title: "Schedule Tour",
            placement: "bottom",
            content: "You can manage your holidays"
        },    
    ]});
    if(jQuery(".schedule-tour").length > 0)
    {
	    // Initialize the schedule tour
	    schedule_tour.init();
	    // Start the schedule tour
	    schedule_tour.start(); 
	}

	// Website Tour For instructor group
	var instgroup_tour = new Tour({
        name: "instructor-group-tour",
        backdrop: true,
        steps: [
        {
            element: ".instructor-group-new-t",
            title: "Instructor Group Tour",
            placement: "bottom",
            content: "You can make new instructor group"
        },
        {
            element: ".instructor-group-action-t",
            title: "Instructor Group Tour",
            placement: "bottom",
            content: "You can update & delete instructor group"
        },
    ]});
    if(jQuery(".instgroup-tour").length > 0)
    {
	    // Initialize the instructor group tour
	    instgroup_tour.init();
	    // Start the instructor group tour
	    instgroup_tour.start(); 
	}

    // Website Tour For holidays
	var holiday_tour = new Tour({
        name: "holiday-tour",
        backdrop: true,
        steps: [
        {
            element: ".holidays-new-t",
            title: "Holidays Tour",
            placement: "bottom",
            content: "You can make new holidays"
        },
        {
            element: ".holidays-action-t",
            title: "Holidays Tour",
            placement: "bottom",
            content: "You can update & delete holidays"
        },
    ]});
    if(jQuery(".holiday-tour").length > 0)
    {
	    // Initialize the Holidays tour
	    holiday_tour.init();
	    // Start the Holidays tour
	    holiday_tour.start(); 
	}

    // Website Tour For lessons module
	var lessonsModule_tour = new Tour({
        name: "lessons-module-tour",
        backdrop: true,
        steps: [
        {
            element: ".module-new-t",
            title: "Lesson Module Tour",
            placement: "left",
            content: "You can make new module and roles"
        },
    ]});
    if(jQuery(".lessonsModule-tour").length > 0)
    {
	    // Initialize the lesson module tour
	    lessonsModule_tour.init();
	    // Start the lesson module tour
	    lessonsModule_tour.start(); 
	}

    // Website Tour For vehicle
	var vehicle_tour = new Tour({
        name: "vehicle-tour",
        backdrop: true,
        steps: [
        {
            element: ".vehicle-new-t",
            title: "Vehicle Tour",
            placement: "bottom",
            content: "You can make new vehicle"
        },
        {
            element: ".vehicle-action-t",
            title: "Vehicle Tour",
            placement: "bottom",
            content: "You can update, delete relevant vehicle and make new relevant vehicle mileage"
        },
    ]});
    if(jQuery(".vehicle-tour").length > 0)
    {
	    // Initialize the vehicle tour
	    vehicle_tour.init();
	    // Start the vehicle tour
	    vehicle_tour.start(); 
	}
});