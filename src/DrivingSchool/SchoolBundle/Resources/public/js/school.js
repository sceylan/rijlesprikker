
// student form load

$(document).ready(function() {
    var url = Routing.generate('driving_school_school_subscription_getpackageprice');
    
    $('body').on('change',"#subscription_form_subscriptions",function(event){
        if(this.value.trim() != "") {
            var packageId = this.value;
            $.ajax({
                type: "POST",
                url: url,
                data: {package_id: packageId},
                dataType: "json"
            }).success(function (data) {
                if(data.response != undefined || data.response.trim() != "") {
                    $("#price").html(data.response);
                }
            });
        }
    });

    // Student registration popup
	$('body').on('submit',"[name='student_form']",function(event){
		var username = $("#student_form_firstName").val() + $("#student_form_lastName").val();
		$("#student_form_username").val(username);
		event.preventDefault();
		$.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: "json"
        }).success(function (data) {
            if(data.redirect != undefined) {
                document.location = data.url;
            }
            $("#createform").html(data.response);
        });
	});

    // vehicle KM get maximum mileage (dashboard)
    $('#vehicle_km_form_Vehicle_id').change(function() {
        var vahicleval = $('#vehicle_km_form_Vehicle_id').val();
        var url_path = Routing.generate('driving_school_school_dashboard_getmileage');
        $.ajax({
            method: "POST",
            url: url_path,
            data: {vahicleval: vahicleval},
            success: function (result) {
                $("#vehicle_km_form_mileage").attr('placeholder', result);
            }
        });
    });

    // Appointment label
    $('.new_colors').hide();
    $(".clicklebel").click(function(e) {
        var dataid = $(this).attr('data-id');
        $('.hidebodyclickbox').hide();  
        e.preventDefault(); 
        e.stopPropagation();
        $('#colordisplaylabel'+dataid).toggle();
            $('#colordisplaylabel'+dataid).click( function(e) {
                e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
            });
    });
    $(".labelcolorbox").click(function () {
        var color = $(this).attr('id');
        var data_id = $(this).attr('data-id');
        $('#appointmentlabel'+data_id).val(color);
        $('#getcolorlabel'+data_id).css({"background-color": color});
        $('#colordisplaylabel'+data_id).hide();
        $('.overlay_bluer').removeClass('show');
    });
    $('body, .overlay_bluer').click(function() {
        $('.hidebodyclickbox').hide();
        $('.overlay_bluer').removeClass('show');
    });

    $('.colorclick').click(function() {
        $('.new_colors').toggleClass('add_new_color');
        $('.overlay_bluer').toggleClass('show');
    })

    // courses color picker
    $("#advanced").hide();
    $("#advancedclick").click(function () {
        $("#advanced").toggle();
    });

    $('#togglecolor').hide();
    $(".colorclick").click(function(e) {
        e.preventDefault(); 
        e.stopPropagation();
        $('#togglecolor').toggle();
    });
    $('#togglecolor').click( function(e) {
        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
    });
    $(".color-box").click(function () {
        var color = $(this).attr('id');
        $('#drivinglessons_form_appointmentLabel').val(color);
        $('#getcolor').css({"background-color": color});
        $('#togglecolor').hide();
        $('.overlay_bluer').toggleClass('show');
    });
    var getcolorval = $("#drivinglessons_form_appointmentLabel").val();
    if(getcolorval != '')
    {
        $('#getcolor').css({"background-color": getcolorval});    
    }
    $('body').click(function() {
        $('#togglecolor').hide();
    });

    // lesson module add role
    var counter = 1;
    $("#showNext").click(function() {
        if($(rolesdisplay).children().length > 0) {
            var objId = $(rolesdisplay).children().last().find(".rolesvalid").attr("id");
            counter = parseInt(objId.replace("role_","")) + 1;
          }
        var html = "<div class='role_input'><input type='text' name='role[new"+counter+"]' id='role_"+counter+"' required class='rolesvalid new"+counter+"'><input class='btn btn-danger removeNext' type='button' id='removeNext' value='' /><i class='fa fa-times' aria-hidden='true'></i></div>";
                // Append data
                $('#rolesdisplay').append(html);
        
        jQuery.validator.addMethod("noSpace", function(value, element) { 
            return value.trim() != ""; 
        }, "No space please and don't leave it empty");

        $("#rolesdisplay").find('.new'+counter).rules("add", 
            {
                required: true,
                noSpace: true,
                messages: {
                    required: "This value should not be blank.",
                  }
            });
        $("#modulefrm").validate();
        counter++;  
    });
    $("#rolesdisplay").on("click","#removeNext", function(e){ 
        counter--;
        e.preventDefault();
        $(this).prev().val("");
        $(this).parent('div').hide();
    }); 

    // packages add more and delete more (courses)
    var counter = 1;
    $("#addmore").click(function() {
        var priceid = $('#pricepackageid').val();
          if($(packagesdisplay).children().length > 0) {
            var objId = $(packagesdisplay).children().last().find(".attribute").attr("id");
            counter = parseInt(objId.replace("qty_","")) + 1;
          }

          // var url_path = "{{ absolute_url(path('driving_school_school_trainingSessions_trainingsessionajaxcall')) }}";
          var url_path = Routing.generate('driving_school_school_trainingSessions_trainingsessionajaxcall');

          $.ajax({
              method: "POST",
              url: url_path,
              dataType: "json",
              data: {priceid: priceid, counter: counter},
              success: function (result) {
                $('#packagesdisplay').append(result.response);
              }
          });
    });
    $('body').on('change','#linkedto', function() {
        var id = this.value;
        var object = this;
        var url_path = Routing.generate('driving_school_school_trainingSessions_ajaxcall');

        $.ajax({  
            url:  url_path,
            type: 'POST',   
            data: {id : id}, 
            success: function(data) {  
                var obj = JSON.parse(data);
                var priceval = obj.price
                $(object).parent().find(".price").val(priceval);
                var includedVat = obj.includedVat;
                if(includedVat==true)
                {
                  $(object).parent().find(".inclvat").prop('checked',true);
                } else {
                  $(object).parent().find(".inclvat").prop('checked',false);
                }
                var vat = obj.vat;
                $(object).parent().find(".vattax").val(vat).change();
            }
        });
    });
    $("#packagesdisplay").on("click","#removeNext", function(e){ 
        counter--;
        e.preventDefault();
        $(this).parent('div').remove();
    });

    // fancybox
    $(".fancybox").fancybox({
        afterLoad:function(){
            setTimeout(function(){
                if (CKEDITOR.instances['editor1']) {
                    CKEDITOR.instances['editor1'].destroy();
                    delete CKEDITOR.instances['editor1'];
                }
                CKEDITOR.replace('editor1');
            }, 1000);
        }
    });

    // tab
    $("#tabs").tabs();
    $("#packagetabs").tabs();
    $("#packagetabs").show();
    $("#financialtabs").tabs();
    $("#financialtabs").show();
    $("#trainingtab").tabs();

    // student card add package or creadit
    $('.courses').change(function() {
        var id = $(this).val();
        var object = this;

        var data_id = $(this).find('option:selected').attr('id');
        if(data_id == '3')
        {
            $(this).siblings("#quantityhide").hide();
        } else {
            $(this).siblings("#quantityhide").show();
        }
        var url_path = Routing.generate('driving_school_school_trainingSessions_ajaxcall');
        $.ajax({  
            url:  url_path,  
            type: 'POST',   
            data: {id : id}, 
            success: function(data) { 
                // alert(data); 
                var obj = JSON.parse(data);
                var perval = obj.per;
                if(obj.unit == 'minutes')
                {
                    // $(".percentage").val(perval);
                    $(object).parent().find(".percentage").val(perval);
                }
                var priceval = obj.price;
                // $('.price').val(priceval);
                $(object).parent().find(".price").val(priceval);

                var vatval = obj.vat;
                // $('.price').val(priceval);
                $(object).parent().find(".vat").val(vatval);

                var vat_calc = obj.vat_calc;
                // $('.price').val(priceval);
                $(object).parent().find(".amount").val(vat_calc);

                var includedVat = obj.includedVat;
                if(includedVat==true)
                {
                  $(object).parent().find(".includedvat").val(1);
                    /*$(".inclvat").attr('checked', 'checked');*/
                } else {
                  $(object).parent().find(".includedvat").val(0);
                }
            }
        });
    });

    $("body").on("click", ".click-change", function () {
        $("#financial_form_logo_binaryContent").trigger("click");
    });
    $("#financial_form_logo_binaryContent").change(function () {
        var file_data = $(this).prop("files")[0];
        var form_data = new FormData();
        var url_path = Routing.generate('driving_school_school_financial_getimagepreview');
        form_data.append("file", file_data);
        $.ajax({
            method: "POST",
            url: url_path,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                console.log($('#user-image'));
                $('.click-change').find('img').attr('src', data.filename);
            }
        });
    });

    // student financial tab
    $('body').on('click',".editfinancial",function(event){
        var studentid = $(this).attr('data-id');
        var id = $(this).attr('id');

        var url_path = Routing.generate('driving_school_school_students_financialedit');
        event.preventDefault();
        $.ajax({
            method: "POST",
            url: url_path,
            dataType: "json",
            data: {studentid: studentid, id: id},
            success: function (result) {
                $("#financial-content").html(result.response);
            }
        });
    });
    $('#financial').on("submit","#financial-submit",function(event){
        event.preventDefault();
        
        var id = $('#id').val();
        var studentid = $('#studentid').val();
        var desc = $('#desc').val();
        var qty = $('#qty').val();
        var minutes = $('#minutes').val();
        var price = $('#price').val();
        var included_vat = $('#included_vat').is(':checked') ? 1 : 0;
        var vat = $('#vat').val();

        var url_path = Routing.generate('driving_school_school_students_financialeditdata');

        $.ajax({
            method: "POST",
            url: url_path,
            dataType: "json",
            data: {id: id, studentid: studentid, desc: desc, qty: qty, minutes: minutes, price: price, included_vat: included_vat, vat: vat},
            success: function (result) {
                $("#financial-content").html(result.response);
                $('#example').DataTable();
            }
        });
    });
    $('body').on('click',"#financialcancel",function(event){
        event.preventDefault();
        var studentid = $('#studentid').val();
        var url_path = Routing.generate('driving_school_school_students_financialcancel');
        $.ajax({
            method: "POST",
            url: url_path,
            dataType: "json",
            data: {studentid: studentid},
            success: function (result) {
                $("#financial-content").html(result.response);
                $('#example').DataTable();
            }
        });
    });

    // package or creadit insert detail
    $('body').on("submit","#creaditsubmit",function(event){
        event.preventDefault();
        var data = $(this).serialize();
        var url_path = Routing.generate('driving_school_school_students_packageorcreadit');
        $.ajax({
            method: "POST",
            url: url_path,
            data: data,
            success: function (result) {
                var result = JSON.parse(result);
                    $.fancybox.close();
                    $("#packagetabs").tabs("option", "active", 2);
                    $("#financial-content").html(result.response);
                    $('#example').DataTable();
            }
        });
    });

    // Notes
    $('body').on("submit","form[name='student_notes_form']",function(event){
        event.preventDefault();
        var formData = new FormData();
        var fileData = $('#student_notes_form_notesImage_binaryContent').prop("files")[0];
        formData.append("file", fileData);
        formData.append("student_notes_form[desc]", $("[name='student_notes_form[desc]']").val());
        formData.append("studentid", $("[name='studentid']").val());
        var url_path = Routing.generate('driving_school_school_students_notesinsert');
        $.ajax({
            method: "POST",
            url: url_path,
            data: formData,
            cache: false,
            processData: false,  
            contentType: false, 
            dataType: 'json',
            success: function (result) {
                $("#packagetabs").tabs("option", "active", 3);
                $("#notes-content").html(result.response);

                // fancybox
                $(".fancybox").fancybox({});
            }
        });
    });

    // activate and financial part and profile
    $('#account_content').hide();
    $('#studet_profile_form').hide();
    $('#accountactivation').click(function() {
        $('#packagetabs').hide();
        $('#studet_profile_form').hide();
        $('#account_content').show();
    });
    $('#studentprofileupdate').click(function() {
        $('#packagetabs').hide();
        $('#studet_profile_form').show();
        $('#account_content').hide();
    });

    // display all creadit
    $("body").on("click", "#displayallcreadit", function () {
        $('#packagetabs').show();
        $('#account_content').hide();
        $('#studet_profile_form').hide();
        $("#packagetabs").tabs("option", "active", 2);
    });

    // sidebar scroll
    (function($){
        $(window).on("load",function(){
            
            $("a[rel='load-content']").click(function(e){
                e.preventDefault();
                var url=$(this).attr("href");
                $.get(url,function(data){
                    $(".content .mCSB_container").append(data); //load new content inside .mCSB_container
                    //scroll-to appended content 
                    $(".content").mCustomScrollbar("scrollTo","h2:last");
                });
            });
            
            $(".content").delegate("a[href='top']","click",function(e){
                e.preventDefault();
                $(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
            });
            
        });
    })(jQuery);

    // finacial invoice checkbox
    $("body").on("click","#selectall",function(){
        if($(this).is(':checked'))
        {
            $('.generateinvoice').each(function() { 
                this.checked = true; 
            });
            $('#btngenerateinvoice').show();
        } else {
            $('.generateinvoice').each(function() { 
                this.checked = false; 
            });
            $('#btngenerateinvoice').hide();
        }
    });
    $("body").on("click",".generateinvoice",function(){
        if($('.generateinvoice').is(':checked'))
        {
            $('#btngenerateinvoice').show();
            $("#selectall").prop("checked", false);        
        } else {
            $('#btngenerateinvoice').hide();
        }
    });

    // student image preview & upload
    $("body").on("click", ".click-change-img", function () {
        $("#student_logoimg_upload").trigger("click");
    });
    $("#student_logoimg_upload").change(function () {
        var file_data = $(this).prop("files")[0];
        var form_data = new FormData();
        var url_path = Routing.generate('driving_school_school_student_getimagepreview');
        form_data.append("file", file_data);
        form_data.append("studentid", $("[name='studentid']").val());
        $.ajax({
            method: "POST",
            url: url_path,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.click-change-img').find('img').attr('src', data.filename);
            }
        });
    });

    // header right menu toggle
    $('.new_drp').hide();
    $('#menudrpdown').click(function(e) {
        e.preventDefault(); 
        e.stopPropagation();
        $('.new_drp').toggle();
    });
    $('.new_drp').click( function(e) {
        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
    });
    $('body').click(function() {
        $('.new_drp').hide();
        $('.hamburger-2').removeClass("is-active");
    });

    $(document).ready(function(){
      $(".hamburger").click(function(){
        $(this).toggleClass("is-active");
        $('.navigation').slideToggle('slow');
      });
    });
    $(document).ready(function(){
      $(".hamburger-2").click(function(){
        $(this).toggleClass("is-active");
      });
    });

    // Data table
    $('#example').DataTable({
        "aaSorting": [],
    });

    // invoice content
    $(document).ready(function() {
        $('#invoicecontentform').hide();
        $('#invoicecontent').click(function() {
            $('#invoicecontent').hide();
            $('#invoicecontentform').show();
        });
        $('#cancelinvoicecontentform').click(function() {
            $('#invoicecontentform').hide();
            $('#invoicecontent').show();
        })
        $('body').on('submit',"[name='submiitinvoicecontent']",function(event){
            event.preventDefault();
            var data = $(this).serialize();
            var url_path = Routing.generate('driving_school_school_student_invoicecontent');

            $.ajax({
                method: "post",
                url: url_path,
                data: data,
                dataType: "json"
            }).success(function (data) {
                $('#invoicecontent').show();
                $('#invoicecontent').html(data.response);
                $('#invoicecontentform').hide();
            });
        });
    });

    // whats-app popup
    // jQuery("#close_whatsapp").click(function(){
    //     jQuery("#myPopup").removeClass('show');
    // }); 
    $('.whatclick').click(function() {
        $('.popuptext').addClass('show');
            setTimeout(function () {
                $('.popuptext').addClass('active');
            }, 500);
        });

        $('#close_whatsapp').click(function() { 
            $('.popuptext').removeClass('active');
            setTimeout(function () {
          $('.popuptext').removeClass('show');
        }, 500);
    });

    // student card package credit history
    $('#creadithistory').hide();
    $('.stucreadithistory').click(function() {
        var dataid = $(this).attr('data-id');
        var url_path = Routing.generate('driving_school_school_students_credithistory');
        $.ajax({
            method: "post",
            url: url_path,
            data: {trainingid: dataid},
            dataType: "json"
        }).success(function (data) {
            $("#creadithistory").html(data.response);
            $(".hiddenhistoryclick").fancybox();
            $("[href='#creadithistory']").trigger("click");
            $("#creadithistory").show();
        });
    });

    // mouse-wheel
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop:false,
        nav:false,
        margin:0,
        dots: false,
        fitToView: false,
        autoSize: false,
        width: 600,
        height: 'auto',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },            
            960:{
                items:3
            },
            1200:{
                items:5
            },
            1500:{
                items:6
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY>0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });

    setTimeout(function(){ 
        $('.flash-success').fadeOut();
        $('.flash-danger').fadeOut();
    }, 3000); 
    
});

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 32 && (charCode < 48 || charCode > 57) && charCode != 43)
        return false;
    return true;
}
function isNumberKeydot(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 32 && (charCode < 46 || charCode > 57) && charCode != 43)
        return false;
    return true;
}
function isNumberKeyTextNot(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 40 || charCode > 57) && charCode != 32)
        return false;
    return true;
}
function isNumberKeyDesh(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 32 && (charCode < 45 || charCode > 57))
        return false;
    return true;
}
function WhatsAppPopup() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}   
