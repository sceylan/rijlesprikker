var userType;
document.addEventListener('DOMContentLoaded', function() {
    var userRating = document.querySelector('.js-user-type');
    userType = userRating.dataset.roleType;
});
function renderCalendar(fc_events, fc_resources) 
{
  $('#calendar').fullCalendar({
    defaultView: 'agendaDay',
    header: false,
    defaultDate: moment(),
    editable: true,
    selectable: true,
    snapDuration: '00:05:00',
    allDaySlot: false,
    height: "auto",
    eventLimit: true, // allow "more" link when too many events
    selectConstraint:{
      start: '00:01', 
      end: '23:59', 
    },
    /*customButtons: {
      instructorSelecter: {
        text: 'Calendars',
        click: function() {
          alert('Clicked the instructor selectors');
        }
      }
    },*/
    /*header: {
      left: 'instructorSelecter prev,next today',
      center: 'title',
      right: 'agendaDay,agendaTwoDay,agendaWeek,month'
    },*/
    views: {
      agendaTwoDay: {
        type: 'agenda',
        duration: { days: 2 },

        // views that are more than a day will NOT do this behavior by default
        // so, we need to explicitly enable it
        groupByResource: true

        //// uncomment this line to group by day FIRST with resources underneath
        //groupByDateAndResource: true
      }
    },

    //// uncomment this line to hide the all-day slot
    //allDaySlot: false,
    resources:fc_resources,
    events : fc_events,

    select: function(start, end, jsEvent, view, resource) {
      if(userType == "INSTRUCTOR") {
        var addrole = $('#instructoraddrole').val();
        if(addrole == 0)
        {
            alert("You don't have permission please contact admin");
            return true;
        } 
      }
      var url = Routing.generate('driving_school_school_planning_create');
      var startDateTime = start.format();
      var endDateTime = end.format();
      var instructorId = resource.id;
      $('#calendar').css('pointer-events', 'none');
      $.ajax({
          type: "POST",
          url: url,
          dataType: "json",
          data: {instructor_id : instructorId, startdatetime: startDateTime, enddatetime: endDateTime}
      }).success(function (data) {
          if(data.response != undefined || data.response.trim() != "") {
              $("#edit-event").html(data.response);
              $(".fancybox").fancybox();
              $("[href='#edit-event']").trigger("click");
          }

          $('#planning_form_enddate_date').hide();
          $('#planning_form_enddate_time').hide();
          
          $('#planning_form_startdate_date').datetimepicker({
            timepicker:false,
            minDate: '0',
            format: 'd-m-Y'
          });

          $('#planning_form_startdate_time').datetimepicker({
            minTime: new Date().getHours()+":"+new Date().getMinutes(),
            datepicker:false,
            step: 5,
            format: 'H:i'
          });

          $('#planning_form_enddate_time').datetimepicker({
            minTime: jQuery('#planning_form_startdate_time').val()?jQuery('#planning_form_startdate_time').val():false,
            datepicker:false,
            step: 5,
            format: 'H:i'
          });
          

          $('#calendar').removeAttr('style');
      });
    },
    dayClick: function(date, jsEvent, view, resource) {
      /*var url = Routing.generate('driving_school_school_planning_create');
      var startDateTime = date.format();
      var instructorId = resource.id;
      $.ajax({
          type: "POST",
          url: url,
          dataType: "json",
          data: {instructor_id : instructorId, startdatetime: startDateTime}
      }).success(function (data) {
          if(data.response != undefined || data.response.trim() != "") {
              $("#edit-event").html(data.response);
              $(".fancybox").fancybox();
              $("[href='#edit-event']").trigger("click");
          }
      });*/
    },
    eventRender: function(event, element) {
      // console.log(event);
        var description = (event.description !== undefined) ? event.description : "";
        var pricePackage= (event.price_package !== undefined) ? event.price_package : "";
        var student     = (event.student !== undefined) ? event.student : "";
        var instructor  = (event.instructor !== undefined) ? event.instructor : "";
        var vehicle     = (event.vehicle !== undefined) ? event.vehicle : "";
        var location    = (event.location !== undefined) ? event.location : "";
        var isReservation    = (event.is_reservation !== undefined) ? event.is_reservation : "";

        element.find(".fc-content")
                  /*.append("<div class='fc-description'>" + description + "</div>")*/
                  /*.append("<div class='fc-price_package'>" + pricePackage + "</div>")*/
                  .append("<div class='fc-student'>" + student + "</div>")
                  .append("<div class='fc-instructor'>" + instructor + "</div>")
                  .append("<div class='fc-vehicle'>" + vehicle + "</div>");
                  /*.append("<div class='fc-location'>" + location + "</div>");*/
        
        if(isReservation == true)
        {
            element.find(".fc-content").parents('a').addClass("strip-bg");
        } else {
            element.find(".fc-content").parents('a').removeClass("strip-bg");
        }
    },
    eventClick:  function(event, jsEvent, view) {  //This is the editing function
      if(userType == "INSTRUCTOR") {
        var editrole = $('#instructorupdaterole').val();
        if(editrole == 0)
        {
            alert("You don't have permission please contact admin");
            return true;
        } 
      }
      var eventId = event.id;
      var url = Routing.generate('driving_school_school_planning_geteditform');
      $('#calendar').css('pointer-events', 'none');
      $.ajax({
          type: "POST",
          url: url,
          dataType: "json",
          data: {event_id:eventId}
      }).success(function (data) {
          if(data.response != undefined || data.response.trim() != "") {
              $("#edit-event").html(data.response);
              if($.fancybox.getInstance() == false){
                $(".fancybox").fancybox();
                $("[href='#edit-event']").trigger("click");
              }
          }

          $('#planning_form_enddate_date').hide();
          $('#planning_form_enddate_time').hide();

          $('#planning_form_startdate_date').datetimepicker({
            timepicker:false,
            minDate: '0',
            format: 'd-m-Y'
          });

          $('#planning_form_startdate_time').datetimepicker({
            minTime: new Date().getHours()+":"+new Date().getMinutes(),
            datepicker:false,
            step: 5,
            format: 'H:i'
          });

          $('#planning_form_enddate_time').datetimepicker({
            minTime: jQuery('#planning_form_startdate_time').val()?jQuery('#planning_form_startdate_time').val():false,
            datepicker:false,
            step: 5,
            format: 'H:i'
          });

          $('#calendar').removeAttr('style');
      });

    },
    eventDrop: function(event, delta, revertFunc) {
      if (!confirm("Are you sure about this change?")) {
        revertFunc();
      } else {
        $('#calendar').css('pointer-events', 'none');
        var url = Routing.generate('driving_school_school_planning_updateevents');
        var eventId = event.id;
        var instructorId = event.resourceId;
        var startDateTime = event.start.format();
        var endDateTime = event.end.format();

        $.ajax({
          type: "POST",
          url: url,
          dataType: "json",
          data: {event_id: eventId, instructor_id: instructorId, start_time: startDateTime, end_time: endDateTime }
        }).success(function (data) {
            if(data.response != undefined || data.response.trim() !=  "") {
              $('#calendar').fullCalendar('removeEvents');
              $('#calendar').fullCalendar('addEventSource', data.response.events);
              $('#calendar').fullCalendar('rerenderEvents');
            }
            $('#calendar').removeAttr('style');
        });  
      }
    },
    eventResize: function(event, delta, revertFunc) {
      if (!confirm("Are you sure about this change?")) {
        revertFunc();
      } else {
        var url = Routing.generate('driving_school_school_planning_updateevents');
        var eventId = event.id;
        var instructorId = event.resourceId;
        var startDateTime = event.start.format();
        var endDateTime = event.end.format();
        $('#calendar').css('pointer-events', 'none');
        $.ajax({
          type: "POST",
          url: url,
          dataType: "json",
          data: {event_id: eventId, instructor_id: instructorId, start_time: startDateTime, end_time: endDateTime }
        }).success(function (data) {
            if(data.response != undefined || data.response.trim() != "") {
              $('#calendar').fullCalendar('removeEvents');
              $('#calendar').fullCalendar('addEventSource', data.response.events);
              $('#calendar').fullCalendar('rerenderEvents');
            }
            $('#calendar').removeAttr('style');
        });  
      }
    }
  });
}

function removeActiveClass(obj) 
{
  $(obj).find("button").removeClass("fc-state-active");
}

function updateTitle()
{
  var title = $('#calendar').fullCalendar('getView').title;
  $("#calander_title").html(title);
}

function refetchInstructors(instructorIds)
{
  var url = Routing.generate('driving_school_school_planning_getallevents');
  $.ajax({
      type: "POST",
      url: url,
      data: {instructor_ids:instructorIds},
      dataType: "json"
  }).success(function (data) {
      if(data.response != undefined || data.response.trim() != "") {
          var events = data.response.events;
          var newResources = data.response.resources;

          var resources = $('#calendar').fullCalendar('getResources');

          for (var i = 0; i <= resources.length - 1; i++) {
            var resource = resources[i];
            $('#calendar').fullCalendar('removeResource',resource.id);
          }
          
          for (var i = 0; i <= newResources.length - 1; i++) {
            var resource = newResources[i];
            $('#calendar').fullCalendar('addResource',{ id:resource.id, title: resource.title});
          }

          $('#calendar').fullCalendar('removeEvents');
          $('#calendar').fullCalendar('addEventSource', events);
          $('#calendar').fullCalendar('rerenderEvents');

          
      }
  });
}

$(document).ready(function() {
  var url = Routing.generate('driving_school_school_planning_getallevents');
  $.ajax({
      type: "GET",
      url: url,
      dataType: "json"
  }).success(function (data) {
      if(data.response != undefined || data.response.trim() != "") {
          renderCalendar(data.response.events,data.response.resources);
          updateTitle();
      }
  });

  $('#instructor-selector option').each(function() {
    $(this).addClass('selected');
    $(this).prop('selected', true);
  });

  $('body').on('submit',"[name='planning_form']",function(event){
    event.preventDefault();
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json"
    }).success(function (data) {
        if(data.response != undefined || data.response.trim() != "") {
            $('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('addEventSource', data.response.events);
            $('#calendar').fullCalendar('rerenderEvents');
        }
    });
    $.fancybox.close();
  });

  $('body').on('click',"[name='planning_form'] input[name='delete']",function(event){
    if (confirm("Are you sure about removing event?")) {
      var eventDeleteUrl = Routing.generate('driving_school_school_planning_deleteevents');
      var eventId = $("[name='event_id']").val();
      $.ajax({
          type: "POST",
          url: eventDeleteUrl,
          data: {event_id:eventId},
          dataType: "json"
      }).success(function (data) {
          if(data.response != undefined || data.response.trim() != "") {
              $('#calendar').fullCalendar('removeEvents');
              $('#calendar').fullCalendar('addEventSource', data.response.events);
              $('#calendar').fullCalendar('rerenderEvents');
          }
      });
      $.fancybox.close();
    } 
  });


  $('body').on('change',"#planning_form_pricePackage",function(){
    var trainingSessionList = Routing.generate('driving_school_school_planning_list_trainingsession');
    var pricePackageSelector = $(this);
    $.ajax({
        type: "POST",
        url: trainingSessionList,
        data: { price_package_id: $(pricePackageSelector).val() },
        dataType: "JSON"
    }).success(function (responseArray) {

        var trainingSessions = responseArray.trainingSessions;
        var students = responseArray.students;
        var vehicles = responseArray.vehicles;
        var instructors = responseArray.instructors;


        var trainingSessionSelect = $("#planning_form_trainingSession");
        var studentSelect = $("#planning_form_student");
        var vehicleSelect = $("#planning_form_vehicle");
        var instructorSelect = $("#planning_form_instructor");


        // Remove current options
        trainingSessionSelect.html('');
        studentSelect.html('');
        vehicleSelect.html('');
        instructorSelect.html('');
        
        // Empty value ...
        trainingSessionSelect.append('<option value> Select Training of ' + pricePackageSelector.find("option:selected").text() + '</option>');
        studentSelect.append('<option value> Select Student of ' + pricePackageSelector.find("option:selected").text() + '</option>');
        vehicleSelect.append('<option value> Select Vehicle of ' + pricePackageSelector.find("option:selected").text() + '</option>');
        instructorSelect.append('<option value> Select Instructor of ' + pricePackageSelector.find("option:selected").text() + '</option>');
        
        $.each(trainingSessions, function (key, trainingSession) {
            trainingSessionSelect.append('<option value="' + trainingSession.id + '">' + trainingSession.name + '</option>');
        });
        $.each(students, function (key, student) {
            studentSelect.append('<option value="' + student.id + '">' + student.name + '</option>');
        });
        $.each(vehicles, function (key, vehicle) {
            vehicleSelect.append('<option value="' + vehicle.id + '">' + vehicle.name + '</option>');
        });
        $.each(instructors, function (key, instructor) {
            instructorSelect.append('<option value="' + instructor.id + '">' + instructor.name + '</option>');
        });

        $('#planning_form_student').removeClass('changeselectorcolor');
        $('#planning_form_vehicle').removeClass('changeselectorcolor');
        $('#planning_form_instructor').removeClass('changeselectorcolor');
    });
  });

  $('body').on('change',"#planning_form_trainingSession",function(){
    var planningLengthUrl = Routing.generate('driving_school_school_planning_length');
    var trainingSessionSelector = $(this);
    $.ajax({
        type: "POST",
        url: planningLengthUrl,
        data: { "training_session_id": $(trainingSessionSelector).val() },
        dataType: "JSON"
    }).success(function (response) {
        var length = response.length;
        $("#duration-selector").val(length);
    });
  });


  $('body').on('change',"#planning_form_student",function(){
      var studentclass = $('#planning_form_student').val();
      if(studentclass!='')
      {
        $('#planning_form_student').addClass('changeselectorcolor');
      } else if (studentclass=='') {
        $('#planning_form_student').removeClass('changeselectorcolor');
      }
  });
  $('body').on('change',"#planning_form_vehicle",function(){
      var vehicleclass = $('#planning_form_vehicle').val();
      if(vehicleclass!='')
      {
        $('#planning_form_vehicle').addClass('changeselectorcolor');
      } else if (vehicleclass=='') {
        $('#planning_form_vehicle').removeClass('changeselectorcolor');
      }
  });
  $('body').on('change',"#planning_form_instructor",function(){
      var instructorclass = $('#planning_form_instructor').val();
      if(instructorclass!='')
      {
        $('#planning_form_instructor').addClass('changeselectorcolor');
      } else if (instructorclass=='') {
        $('#planning_form_instructor').removeClass('changeselectorcolor');
      }
  });

  $('body').on('click',"#instructor-selector-btn",function(){
      $('#instructor-selector').toggle();
  });

  $('body').on('click',"#fc-prev-button",function(){
      $('#calendar').fullCalendar('prev');
      updateTitle();
  });

  $('body').on('click',"#fc-next-button",function(){
      $('#calendar').fullCalendar('next');
      updateTitle();
  });

  $('body').on('click',"#fc-today-button",function(){
      $('#calendar').fullCalendar('today');
      updateTitle();
  });

  $('body').on('click',"#fc-agendaDay-button",function(){
      $('#calendar').fullCalendar('changeView','agendaDay'); 
      updateTitle();
      var obj = $(this).parent();
      removeActiveClass(obj);
      $(this).addClass("fc-state-active");
  });

  $('body').on('click',"#fc-agendaTwoDay-button",function(){
      $('#calendar').fullCalendar('changeView','agendaTwoDay'); 
      updateTitle();
      var obj = $(this).parent();
      removeActiveClass(obj);
      $(this).addClass("fc-state-active");
  });
  
  $('body').on('click',"#fc-agendaWeek-button",function(){
      $('#calendar').fullCalendar('changeView','agendaWeek'); 
      updateTitle();
      var obj = $(this).parent();
      removeActiveClass(obj);
      $(this).addClass("fc-state-active");
  });

  $('body').on('click',"#fc-month-button",function(){
      $('#calendar').fullCalendar('changeView','month'); 
      updateTitle();
      var obj = $(this).parent();
      removeActiveClass(obj);
      $(this).addClass("fc-state-active");
  });

  $('body').on('mousedown',"#instructor-selector option",function(event){
      event.preventDefault();
      $(this).toggleClass('selected');
      $(this).prop('selected', !$(this).prop('selected'));
      var selectedValues = [];
      $('#instructor-selector option:selected').each(function() {
        selectedValues.push($(this).val());
      });
      
      refetchInstructors(selectedValues);
      return false;
  });

  $('body').on('touchend',"#instructor-selector option",function(event){
      event.preventDefault();
      $(this).toggleClass('selected');
      $(this).prop('selected', !$(this).prop('selected'));
      var selectedValues = [];
      $('#instructor-selector option:selected').each(function() {
        selectedValues.push($(this).val());
      });

      return false;
  });

  $('body').on('change',"#duration-selector",function(event){
      var minutes = $(this).val();
      var startTime = $('#planning_form_startdate_time').val();
      if(minutes == "custom_duration") {
        $(this).hide();
        $('#planning_form_enddate_time').show();
      } else {
        var convertedTime = moment.utc(startTime,'hh:mm').add(minutes,'minutes').format('HH:mm');
        $(this).parent().find('#planning_form_enddate_time').val(convertedTime);
      }
  });

});