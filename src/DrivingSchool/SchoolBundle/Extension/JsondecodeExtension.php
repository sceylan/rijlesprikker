<?php

namespace DrivingSchool\SchoolBundle\Extension;

class JsondecodeExtension extends \Twig_Extension
{
	public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('json_decode', array($this, 'jsonDecode')),
        );
    }

    public function jsonDecode($string)
    {
        $string = json_decode($string);

        return $string;
    }
}
