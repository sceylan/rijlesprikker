<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;

class DrivingSchoolAdmin extends AbstractAdmin {

    
    /**
     * @param FormMapper $formMapper
     */
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('Driving Data', ['class' => 'col-md-6'])->end()
                ->with('Company Details', ['class' => 'col-md-6'])->end()
                ->with('Contact Details', ['class' => 'col-md-6'])->end()
                ->with('Financial Settings', ['class' => 'col-md-6'])->end()
            ->end()
        ;

        $formMapper
            ->tab('General')
                ->with('Driving Data')
                    ->add('schoolName', 'text', array('label' => 'Name of driving school')) 
                    ->add('schoolAddress', 'textarea', array('label' => 'Address'))
                    ->add('username', 'text', array('label' => 'Username'))
                    ->add('plainPassword', TextType::class, [
                        'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                    ])
                    ->add('enabled', null, ['required' => false])
                ->end()
                ->with('Contact Details')
                    ->add('schoolContactPerson', 'text', array('label' => 'Contact Person'))
                    ->add('schoolPhone', null, array('label' => 'Phone Number'))
                    ->add('email', 'text', array('label' => 'Email'))
                    ->add('schoolWebsite', UrlType::class, array('label' => 'Website'))
                ->end()
                ->with('Company Details')
                    ->add('schoolCCNNumber', 'text', array('label' => 'Chamber of Commerce Number'))
                    ->add('schoolVatNumber', 'text', array('label' => 'VAT Number'))
                    ->add('schoolBankActNumber', 'text', array('label' => 'International Bank Account Number'))
                    ->add('bic', 'text', array('label' => 'BIC Code'))
                    ->add('inNameOf', 'text', array('label' => 'In The Name Of'))
                    ->add('subscriptionDate', 'datetime', array('label' => 'Subscribed On'))
                    ->add('subscriptions', ModelType::class, [
                            'class' => 'DrivingSchool\AdminBundle\Entity\SubscriptionPlanEntity',
                            'property' => 'packageName',
                            'multiple' => false,
                            'expanded' => false,
                            'placeholder'=> 'Select your package'
                        ]
                    )
                ->end()
                ->with('Financial Settings')
                    ->add('window_on_envelope', 'choice', [
                        'choices' => [
                            'Window is on the left' => '0',
                            'Window is on the right' => '1'
                        ],
                        'multiple' => false,
                        'expanded' => true,
                    ])
                    ->add('payment_term', 'choice', [
                        'choices' => [
                            '5 days' => '5 days',
                            '7 days' => '7 days',
                            '10 days' => '10 days',
                            '14 days' => '14 days',
                            '21 days' => '21 days',
                            '30 days' => '30 days',
                            '45 days' => '45 days',
                            '60 days' => '60 days',
                        ],
                    ])
                    ->add('logo', 'sonata_media_type', array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'default',
                    ))
                    ->add('footer_for_invoice', 'textarea')
                    ->add('footer_with_direct_debit', 'textarea')
                    ->add('footer_1st_reminder', 'textarea')
                    ->add('footer_2nd_memory', 'textarea')
                    ->add('footer_with_credit_invoice', 'textarea')
                ->end()
            ->end()
        ;
    }


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        		->add('schoolName', null, array('label' => 'Name'))
                ->add('schoolAddress', null, array('label' => 'Address'))
                ->add('username')
                ->add('schoolContactPerson', null, array('label' => 'Contact Person'))
                ->add('schoolPhone', null, array('label' => 'Phone Number'))
                ->add('email', null, array('label' => 'Email'))
                ->add('enabled')
                ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        		->add('schoolName', null, array('label' => 'Name'))
                ->add('schoolAddress', null, array('label' => 'Address'))
                ->add('username')
                ->add('schoolContactPerson', null, array('label' => 'Contact Person'))
                ->add('schoolPhone', null, array('label' => 'Phone Number'))
                ->add('email', null, array('label' => 'Email'))
                ->add('enabled' ,null, ['editable' => true])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                        'vehicle' => array('template' => "DrivingSchoolAdminBundle:Default:index.html.twig"),
                        'instructor' => array('template' => "DrivingSchoolAdminBundle:Default:instructorlist.html.twig"),
                         'priceandpackages' => array('template' => "DrivingSchoolAdminBundle:Default:priceandpackageslist.html.twig"),
                    ),
                ))
				;
    }

    
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('General')
                ->with('Driving Data')
                    ->add('schoolName', null, array('label' => 'Name'))
                    ->add('schoolAddress', null, array('label' => 'Address'))
                    ->add('username')
                    ->add('enabled')
                ->end()
                ->with('Contact Details')
                    ->add('schoolContactPerson', null, array('label' => 'Contact Person'))
                    ->add('schoolPhone', null, array('label' => 'Phone Number'))
                    ->add('email', null, array('label' => 'Email'))
                    ->add('schoolWebsite', null, array('label' => 'Website'))
                ->end()
                ->with('Company Details')
                    ->add('schoolCCNNumber', null, array('label' => 'CCN Number'))
                    ->add('schoolVatNumber', null, array('label' => 'Vat Number'))
                    ->add('schoolBankActNumber', null, array('label' => 'International Bank Acount Number'))
                    ->add('bic', null, array('label' => 'BIC Code'))
                    ->add('inNameOf', null,  array('label' => 'In The Name Of'))
                    ->add('subscriptionDate', null, array('label' => 'Subscribed On'))
                    ->add('subscriptions.packageName', null, array('label' => 'Subscription Pack'))
                ->end()
                ->with('Financial Settings')
                    ->add('window_on_envelope', 'choice', [
                        'choices' => [
                            '0' => 'Window is on the left',
                            '1' => 'Window is on the right'
                        ]
                    ])
                    ->add('payment_term', 'choice', [
                        'choices' => [
                            '5 days' => '5 days',
                            '7 days' => '7 days',
                            '10 days' => '10 days',
                            '14 days' => '14 days',
                            '21 days' => '21 days',
                            '30 days' => '30 days',
                            '45 days' => '45 days',
                            '60 days' => '60 days',
                        ],
                    ])
                    ->add('logo')
                    ->add('footer_for_invoice')
                    ->add('footer_with_direct_debit')
                    ->add('footer_1st_reminder')
                    ->add('footer_2nd_memory')
                    ->add('footer_with_credit_invoice')
                ->end()
            ->end()
        ;
    }
}