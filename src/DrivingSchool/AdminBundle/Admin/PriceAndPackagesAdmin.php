<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

class PriceAndPackagesAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('DrivingSchool', ModelType::class,[
                    'class' => 'DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity',
                    'property' => 'schoolName',
                    'label' => 'Driving School'
                ])
                ->add('name', 'text')
                ->add('amountOfLessons', 'text')
                ->add('lessonDuration', 'text')
                ->add('totalPrice', 'text')
                ->add('includedVat')
                ->add('vat', 'choice', array(
                    'choices' => array(
                        '21% VAT' => "21",
                        '19% VAT' => "19",
                        '6% VAT' => "6",
                        'No tax' => "0",
                    )
                ))
                ->add('icon', null, [
                            'multiple' => false,
                            'expanded' => true,
                            'choice_label' => function($icon, $key, $value) {
                                /** @var PriceIconsEntity $icon */
                                return '<img src="'. $icon->getIconPath() .'" />';
                            }
                        ]
                    )
                ->add('status')
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        		->add('name')
                ->add('icon')
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
                ->add('DrivingSchool.id',null,array('label' => 'Driving School ID'))
                ->add('status')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        		->add('name')
                ->add('amountOfLessons', 'text')
                ->add('lessonDuration', 'text')
                ->add('totalPrice', 'text')
                ->add('includedVat')
                ->add('vat', 'choice', array(
                    'choices' => array(
                        '21' => "21% VAT",
                        '19' => "19% VAT",
                        '6' => "6% VAT",
                        '0' => "No tax",
                    )
                ))
                ->add('icon.iconTitle',null,array('label' => 'Icon'))
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
                ->add('status', 'choice', [
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                    'editable' => true,
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
                ;
    }

     /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('icon')
            ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
            ->add('status', 'choice', [
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
            ;
    }
}