<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use DrivingSchool\AdminBundle\Entity\ClientReviewEntity;

class ClientReviewAdmin extends AbstractAdmin {
    /** @var int */
    protected $maxPerPage = 10;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name', 'text', array('label' => 'Client Name'))
                ->add('image', 'sonata_media_type', array(
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'default',
                    'label' => 'Image',
                ))
                ->add('designation', 'text', array('label' => 'Designation'))
                ->add('text', 'textarea', array('label' => 'Text', 'attr' => array('maxlength' => '200'), 'help' => 'Enter maximum 200 character'))
                ;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('name',null,array('label' => 'Client Name'))
                ->addIdentifier('image', null, array(
                    'template' => 'DrivingSchoolAdminBundle:ClientReview:picture.html.twig',
                    'label' => 'Image',
                ))
                ->add('designation',null,array('label' => 'Designation'))
                ->add('text')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                    ),
                ))
				;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            // ->remove('edit')
            ->remove('delete')
            ;
    }
}