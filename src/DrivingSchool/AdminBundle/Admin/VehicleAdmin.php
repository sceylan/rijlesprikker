<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

class VehicleAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $currentyear = date("Y");
        $last40year = date("Y",strtotime("-50 year"));
        $year = array();
        for ($i = $currentyear; $i >= $last40year; $i--) {
            $year[$i] = $i;
        }

        $formMapper
                ->add('DrivingSchool', ModelType::class,[
                    'class' => 'DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity',
                    'property' => 'schoolName',
                    'label' => 'Driving School'
                ])
                ->add('license_plate', 'text')
                ->add('cbr_code', 'text')
                ->add('brand', 'text')
                ->add('fashion_model', 'text')
                ->add('construction_year', 'choice', array('choices' => $year))
                ->add('suitable_training_courses', ModelType::class, [
                        'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                        'property' => 'name',
                        'multiple' => true,
                        'expanded' => true
                    ]
                )
                ->add('status')
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        		->add('licensePlate')
                ->add('cbrCode')
                ->add('brand')
                ->add('FashionModel')
                ->add('constructionYear')
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School Name'))
                ->add('DrivingSchool.id',null,array('label' => 'Driving School ID'))
                ->add('status')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
        		->add('licensePlate')
                ->add('cbrCode')
                ->add('brand')
                ->add('FashionModel')
                ->add('constructionYear')
                ->add('suitableTrainingCourses', null, array('label' => 'Suitable Training Courses', 'associated_property' => 'name'))
                ->add('status', 'choice', [
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
                ->add('licensePlate')
                ->add('cbrCode')
                ->add('brand')
                ->add('FashionModel')
                ->add('constructionYear')
                ->add('suitableTrainingCourses', null, array('label' => 'Suitable Training Courses', 'associated_property' => 'name'))
                ->add('status', 'choice', [
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ;
        ;
    }
}