<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;

class BannerAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('banner_title', 'text', array('label' => 'Title'))
                ->add('banner_image', 'sonata_media_type', array(
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'default',
                    'label' => 'Image',
                ))
                ->add('banner_description', 'textarea', array('label' => 'Description'))
                ->add('status')
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        		->add('bannerTitle', null, array('label' => 'Title'))
                ->add('bannerImage', null, array('label' => 'Image'))
                ->add('bannerDescription', null, array('label' => 'Description'))
                ->add('status')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        		->add('bannerTitle', null, array('label' => 'Title'))
        		->addIdentifier('bannerImage', null, array(
                    'template' => 'DrivingSchoolAdminBundle:Banner:picture.html.twig',
                    'label' => 'Image',
                ))
        		->add('bannerDescription', 'html', [
                    'truncate' => [
                        'length' => 100,
                    ],
                    'label' => 'Description',
                ])
                ->add('status', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('bannerTitle', null, array('label' => 'Title'))
                ->add('bannerImage', null, array('label' => 'Image'))
                ->add('bannerDescription', null, array('label' => 'Description'))
                ->add('status', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ;
        ;
    }
}