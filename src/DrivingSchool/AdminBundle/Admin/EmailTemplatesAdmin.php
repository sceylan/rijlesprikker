<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

class EmailTemplatesAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('title', 'text')
                ->add('subject', 'text')
                ->add('content', 'textarea', array('attr' => array('class' => 'ckeditor')))
                ->add('status')
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('status')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('title')
                ->add('subject')
                ->add('status', 'choice', [
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('status', 'choice', [
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ;
        ;
    }
}