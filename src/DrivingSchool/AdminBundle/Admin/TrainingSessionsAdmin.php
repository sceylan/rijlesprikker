<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity;

class TrainingSessionsAdmin extends AbstractAdmin {
    /** @var int */
    protected $maxPerPage = 10;
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
                ->add('PriceAndPackages.name',null,array('label' => 'Price & Packages'))
                ->add('type', 'choice', array(
                    'choices' => [
                        '0' => 'Driving Lesson',
                        '1' => 'Examination and Test',
                        '2' => 'Others',
                    ]
                ))
                ->add('name')
                ->add('price', null, array('label' => 'Price (in Euro)'))
                ->add('includedVat')
                ->add('vat')
                ->add('vatCalc')
                ->add('qty')
                ->add('unit')
                ->add('appointmentLabel')
                ->add('status', 'choice', [
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive'
                    ],
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                    ),
                ))
				;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $query->select('o');
        $query->andWhere('o.type != 3');

        return $query;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
            ->add('pricePackageId.name',null,array('label' => 'Price & Packages'))
            ->add('type', 'choice', array(
                'choices' => [
                    '0' => 'Driving Lesson',
                    '1' => 'Examination and Test',
                    '2' => 'Others',
                ]
            ))
            ->add('name')
            ->add('price', null, array('label' => 'Price (in Euro)'))
            ->add('includedVat')
            ->add('vat')
            ->add('vatCalc')
            ->add('qty')
            ->add('unit')
            ->add('appointmentLabel')
            ->add('schedule')
            ->add('lengthQty')
            ->add('exam')
            ->add('accountingCode')
            ->add('category')
            ->add('codeCbr')
            ->add('status', 'choice', [
                'choices' => [
                    1 => 'Active',
                    0 => 'Inactive'
                ],
            ])
            ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('edit')
            ->remove('delete')
            ;
    }
}