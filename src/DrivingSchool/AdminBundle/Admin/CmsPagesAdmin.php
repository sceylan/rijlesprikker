<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use DrivingSchool\AdminBundle\Entity\ClientReviewEntity;

class CmsPagesAdmin extends AbstractAdmin {
    /** @var int */
    protected $maxPerPage = 10;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('title', 'text', array('label' => 'Title'))
                ->add('content', 'textarea', array('label' => 'Content', 'attr' => array('class' => 'ckeditor')))
                ->add('urlKey', 'text', array('label' => 'Url Key'))
                ->add('metaTitle', 'text')
                ->add('metaKeyword', 'textarea')
                ->add('metaDescription', 'textarea')
                ;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        	->add('title')
            ->add('urlKey')
            ->add('metaTitle')
            ->add('metaKeyword')
            ->add('metaDescription')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                ),
            ))
            ;                
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            // ->remove('edit')
            ->remove('delete')
            ;
    }
}