<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class SubscriptionPlanAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('package_name', 'text', array('label' => 'Name'))
                ->add('package_price', 'number', array('scale' => 2, 'label' => 'Price (in Euro)'))
                // ->add('package_price', NumberType::class, array('scale' => 2))
                ->add('maximum_number_of_students', 'integer')
                ->add('maximum_number_of_instructors', 'integer')
                ->add('package_validity', 'integer', ['label' => 'Validity (in days)'])
                ->add('status')
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        		->add('packageName', null, array('label' => 'Name'))
                ->add('packagePrice', null, array('label' => 'Price (in Euro)'))
                ->add('maximumNumberOfStudents')
                ->add('maximumNumberOfInstructors')
                ->add('packageValidity', null, array('label' => 'Validity (in days)'))
                ->add('status')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        		->add('packageName', null, array('label' => 'Name'))
                ->add('packagePrice', null, array('label' => 'Price (in Euro)'))
                ->add('maximumNumberOfStudents')
                ->add('maximumNumberOfInstructors')
                ->add('packageValidity', null, array('label' => 'Validity (in days)'))
                ->add('status', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('packageName', null, array('label' => 'Name'))
                ->add('packagePrice', null, array('label' => 'Price (in Euro)'))
                ->add('maximumNumberOfStudents')
                ->add('maximum_number_of_instructors')
                ->add('packageValidity', null, array('label' => 'Validity (in days)'))
                ->add('status', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ;
        ;
    }
}