<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;

class BlogAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('blog_name', 'text', array('label' => 'Name'))
                ->add('blog_image', 'sonata_media_type', array(
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'default',
                    'label' => 'Image'
                ))
                ->add('blog_description', 'textarea', array('label' => 'Description'))
                ->add('status')
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        		->add('blogName', null, array('label' => 'Name'))
                ->add('blogImage', null, array('label' => 'Image'))
                ->add('blogDescription', null, array('label' => 'Description'))
                ->add('status')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        		->add('blogName', null, array('label' => 'Name'))
        		->addIdentifier('blogImage', null, array(
                    'template' => 'DrivingSchoolAdminBundle:Blog:picture.html.twig',
                ))
        		->add('blogDescription', 'html', [
                    'truncate' => [
                        'length' => 100,
                    ],
                    'label' => 'Description',
                ])
                ->add('status', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('blogName', null, array('label' => 'Name'))
                ->add('blogImage', null, array('label' => 'Image'))
                ->add('blogDescription', null, array('label' => 'Description'))
                ->add('status', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ;
        ;
    }
}