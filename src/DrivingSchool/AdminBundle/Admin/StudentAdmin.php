<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;

class StudentAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('General Information', array('class' => 'col-md-6'))
                    ->add('DrivingSchool', ModelType::class,[
                        'class' => 'DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity',
                        'property' => 'schoolName',
                        'label' => 'Driving School'
                    ])
                    ->add('firstName', 'text')
                    ->add('lastName', 'text')
                    ->add('initials', 'text')
                    ->add('phone', null, array('label' => 'Phone Number'))
                    ->add('student_age', 'text', array('label' => 'Age'))
                    ->add('username', 'text', array('label' => 'Username'))
                    ->add('email', 'text', array('label' => 'Email'))
                    ->add('plainPassword', TextType::class, [
                        'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                    ])
                    ->add('gender', 'choice', [
                        'choices' => [
                            'Male' => 1,
                            'Female' => 2,
                        ],
                        'expanded' => true,
                        'multiple' => false
                    ])
                    ->add('date_of_birth', 'date')
                    ->add('birth_place', 'text')
                    ->add('enabled', null, ['required' => false])
                    ->add('authorization')
                    ->add('healthInformation')
                    ->add('tooGood')
                ->end()
                ->with('Profile', array('class' => 'col-md-6'))
                    ->add('student_image', 'sonata_media_type', array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'default',
                        'label' => 'Image'
                    ))
                    ->add('student_residence', 'text')
                    ->add('house_number', 'text')
                    ->add('postal_code', 'text')
                    ->add('place', 'text')
                    ->add('bsn', 'text')
                    ->add('drivers_license', 'text')
                    ->add('candidate_number', 'text')
                    ->add('instructors_note', 'text')
                    ->add('note', 'text')
                    ->add('educations', ModelType::class, [
                            'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                            'property' => 'name',
                            'multiple' => true,
                            'expanded' => true
                        ]
                    )
                ->end()
                ->with('Payment Details')
                    ->add('debtor_number', 'text')
                    ->add('invoice_name', 'text')
                    ->add('invoice_surname', 'text')
                    ->add('billing_address', 'text')
                    ->add('invoice_zip_code', 'text')
                    ->add('invoice_place', 'text')
                    ->add('invoice_email', 'text')
                ->end()
            ->end()
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School Name'))
                ->add('firstName', null)
                ->add('lastName', null)
                ->add('phone', null, array('label' => 'Phone Number'))
                ->add('username')
                ->add('studentResidence', null, array('label' => 'Residence'))
                ->add('email', null, array('label' => 'Email'))
                ->add('enabled')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School Name'))
                ->add('firstName', null)
                ->add('lastName', null)
                ->add('phone', null, array('label' => 'Phone Number'))
                ->add('studentResidence', null, array('label' => 'Residence'))
                ->add('email', null, array('label' => 'Email'))
                ->add('username')
                ->add('enabled', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                    'editable' => true,
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

     /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General Information')
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School Name'))
                ->add('firstName', null)
                ->add('lastName', null)
                ->add('initials', null)
                ->add('email', null, array('label' => 'Email'))
                ->add('username')
                ->add('phone', null, array('label' => 'Phone Number'))
                ->add('studentAge', null, array('label' => 'Age'))
                ->add('enabled', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
            ->end()
            ->with('Profile')->add('studentResidence')
                ->add('student_image', null, array('label' => 'Image'))
                ->add('student_residence', null)
                ->add('house_number', null)
                ->add('postal_code', null)
                ->add('place', null)
                ->add('date_of_birth', 'date')
                ->add('birth_place', null)
                ->add('gender', 'choice', [
                        'choices' => [
                            1 => 'Male',
                            2 => 'Female',
                        ],
                    ])
                ->add('bsn', null)
                ->add('drivers_license', null)
                ->add('candidate_number', null)
                ->add('instructors_note', null)
                ->add('note', null)
                ->add('authorization', null, array('label' => 'Authorization'))
                ->add('healthInformation', null, array('label' => 'Health Information'))
                ->add('tooGood', null, array('label' => 'Too Good'))
            ->end()
            ->with('Payment Details')
                ->add('debtorNumber', null, array('label' => 'Debtor Number'))
                ->add('invoiceName')
                ->add('invoiceSurname')
                ->add('billingAddress')
                ->add('invoiceZipCode')
                ->add('invoicePlace')
                ->add('InvoiceEmail')
            ->end()
            ;
    }
}