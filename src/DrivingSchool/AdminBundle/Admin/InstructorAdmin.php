<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

class InstructorAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('Profile', ['class' => 'col-md-6'])
                    ->add('DrivingSchool', ModelType::class,[
                        'class' => 'DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity',
                        'property' => 'schoolName',
                        'label' => 'Driving School'
                    ])
                    ->add('instructor_name', 'text', array('label' => 'Name'))
                    ->add('email', 'text', array('label' => 'Email'))
                    ->add('instructor_email', 'text', array('label' => 'Instructor Email'))
                    ->add('username', 'text', array('label' => 'Username'))
                    ->add('plainPassword', 'text', [
                        'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                    ])
                    ->add('phone_number', 'text', array('label' => 'Phone'))
                    ->add('code_crb', 'text', array('label' => 'Code CBR'))
                    ->add('instructorImage', 'sonata_media_type', array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'default',
                        'label' => 'Logo',
                    ))
                    ->add('authorizedFor', ModelType::class, [
                            'class' => 'DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity',
                            'property' => 'name',
                            'multiple' => true,
                            'expanded' => true
                        ]
                    )
                    // ->add('publish_calendar', 'date')
                    ->add('status')
                ->end()
                ->with('Working Hours', ['class' => 'col-md-6'])
                    ->add('monday_start', 'time')
                    ->add('monday_end', 'time')
                    ->add('tuesday_start', 'time')
                    ->add('tuesday_end', 'time')
                    ->add('wednesday_start', 'time')
                    ->add('wednesday_end', 'time')
                    ->add('thursday_start', 'time')
                    ->add('thursday_end', 'time')
                    ->add('friday_start', 'time')
                    ->add('friday_end', 'time')
                    ->add('saturday_start', 'time')
                    ->add('saturday_end', 'time')
                    ->add('sunday_start', 'time')
                    ->add('sunday_end', 'time')
                ->end()
            ->end()
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('instructorName', null, array('label' => 'Name'))
                ->add('email', null, array('label' => 'Email'))
                ->add('username', null, array('label' => 'Username'))
                ->add('codeCRB')
                ->add('publishCalendar')
                ->add('DrivingSchool.schoolName',null,array('label' => 'Driving School'))
                ->add('status')
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('instructorName', null, array('label' => 'Name'))
                ->add('email', null, array('label' => 'Email'))
                ->add('username', null, array('label' => 'Username'))
                ->add('codeCRB')
                ->add('authorizedFor',null, array('label' => 'Package Name', 'associated_property' => 'name'))
                ->add('publishCalendar')
                ->add('DrivingSchool.schoolName', null, array('label' => 'School Name'))
                ->add('instructorImage', null, array(
                    'template' => 'DrivingSchoolAdminBundle:Instructor:picture.html.twig',
                ))
                ->add('status', 'choice', [
                    'class' => 'Vendor\ExampleBundle\Entity\ExampleStatus',
                    'choices' => [
                        1 => 'Active',
                        0 => 'Inactive',
                    ],
                ])
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('General')
                ->with('Profile')
                    ->add('instructorName', null, array('label' => 'Name'))
                    ->add('email', null, array('label' => 'Email'))
                    ->add('username', null, array('label' => 'Username'))
                    ->add('phoneNumber', null, array('label' => 'Phone'))
                    ->add('codeCRB')
                    ->add('authorizedFor',null, array('label' => 'Package Name', 'associated_property' => 'name'))
                    ->add('DrivingSchool.schoolName', null, array('label' => 'School Name'))
                    ->add('instructorImage', null, array(
                        'template' => 'DrivingSchoolAdminBundle:Instructor:picture.html.twig',
                        'label' => 'Logo'
                    ))
                    ->add('status', 'choice', [
                        'choices' => [
                            1 => 'Active',
                            0 => 'Inactive',
                        ],
                    ])
                ->end()
                ->with('Working Hours')
                    ->add('mondayStart')
                    ->add('mondayEnd')
                    ->add('tuesdayStart')
                    ->add('tuesdayEnd')
                    ->add('wednesdayStart')
                    ->add('wednesdayEnd')
                    ->add('thursdayStart')
                    ->add('thursdayEnd')
                    ->add('fridayStart')
                    ->add('fridayEnd')
                    ->add('saturdayStart')
                    ->add('saturdayEnd')
                    ->add('sundayStart')
                    ->add('sundayEnd')
                ->end()
            ->end()
        ;
    }
}