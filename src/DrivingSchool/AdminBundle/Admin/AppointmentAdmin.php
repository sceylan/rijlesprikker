<?php
namespace DrivingSchool\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

class AppointmentAdmin extends AbstractAdmin {
	protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('appointment_type', ChoiceType::class, array(
                        'choices' => array(
                                'Appointment' => '0',
                                'Reservation' => '1',
                                'Other' => '2',
                        ),
                    ))
                ->add('DrivingSchool', ModelType::class,[
                    'class' => 'DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity',
                    'property' => 'schoolName',
                    'label' => 'Driving School'
                ])
                ->add('vehicleType', ModelType::class,[
                    'class' => 'DrivingSchool\AdminBundle\Entity\VehicleEntity',
                    'property' => 'licensePlate',
                    'label' => 'Vehicle Type'
                ])
                ->add('instructor', ModelType::class,[
                    'class' => 'DrivingSchool\AdminBundle\Entity\InstructorEntity',
                    'property' => 'instructorName',
                    'label' => 'Instructor Type'
                ])
                ->add('Student', ModelType::class,[
                    'class' => 'DrivingSchool\AdminBundle\Entity\StudentEntity',
                    'property' => 'studentName',
                    'label' => 'Student Name'
                ])
                ->add('preferabledatetime', 'datetime', array('label' => 'Preferable Date & Time'))
                ->add('length', 'text', array('label' => 'Length [in mins]'))
                ->add('status', ChoiceType::class, array(
                        'choices' => array(
                                'Pending' => '0',
                                'Approved' => '1',
                                'Rejected' => '2',
                        ),
                    ))
                ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('DrivingSchool.schoolName', null,array('label' => 'School'))
                ->add('Student.studentName', null,array('label' => 'Student'))
                ->add('preferabledatetime', null, array('label' => 'Preferable Date & Time'))
                ->add('status', null, array(), 'choice', array(
                    'choices' => [
                        'Pending' => '0',
                        'Approved' => '1',
                        'Rejected' => '2',
                    ]
                ))
                ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('DrivingSchool.schoolName', null, array('label' => 'School'))
                ->add('Student.studentName', null, array('label' => 'Student'))
                ->add('preferabledatetime', null, array('label' => 'Preferable Date & Time'))
                ->add('status', 'choice', [
                    'multiple' => true, 
                    'choices' => [
                        0 => 'Pending',
                        1 => 'Approved',
                        2 => 'Rejected'
                    ],
                ])
                // ->add('status', null, array(
                //     'template' => 'DrivingSchoolAdminBundle:Appiontment:Status.html.twig'
                // ))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
				;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('DrivingSchool.schoolName', null, array('label' => 'School'))
                ->add('Student.studentName', null, array('label' => 'Student'))
                ->add('preferabledatetime', null, array('label' => 'Preferable Date & Time'))
                ->add('status', 'choice', [
                    'choices' => [
                        0 => 'Pending',
                        1 => 'Approved',
                        2 => 'Rejected'
                    ],
                ])
                ;
        ;
    }
}