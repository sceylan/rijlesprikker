<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * SubscriptionPlanEntity
 *
 * @ORM\Table(name="SubscriptionPlan")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\SubscriptionPlanRepository")
 */
class SubscriptionPlanEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="package_name", type="string", length=255, nullable=true)
     */
    private $packageName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $packagePrice;


    /**
     * @var int|null
     *
     * @ORM\Column(name="maximum_number_of_students", type="integer", length = 11)
     */
    private $maximumNumberOfStudents;

    /**
     * @var string|null
     *
     * @ORM\Column(name="maximum_number_of_instructors", type="integer", length = 11)
     */
    private $maximumNumberOfInstructors;

    /**
     * @var string|null
     *
     * @ORM\Column(name="package_validity", type="integer", length=11, nullable=true)
     */
    private $packageValidity;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="DrivingSchoolEntity", mappedBy="subscriptions", cascade={"persist"})
     */
    private $drivingSchools;

    public function __construct()
    {
        $this->drivingSchools = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set packageName.
     *
     * @param string|null $packageName
     *
     * @return SubscriptionPlan
     */
    public function setPackageName($packageName = null)
    {
        $this->packageName = $packageName;

        return $this;
    }

    /**
     * Get packageName.
     *
     * @return string|null
     */
    public function getPackageName()
    {
        return $this->packageName;
    }

    /**
     * Set packagePrice.
     *
     * @param string|null $packagePrice
     *
     * @return SubscriptionPlan
     */
    public function setPackagePrice($packagePrice = null)
    {
        $this->packagePrice = $packagePrice;

        return $this;
    }

    /**
     * Get packagePrice.
     *
     * @return int|null
     */
    public function getPackagePrice()
    {
        return $this->packagePrice;
    }

    /**
     * Set maximumNumberOfStudents.
     *
     * @param int|null $maximumNumberOfStudents
     *
     * @return SubscriptionPlan
     */
    public function setMaximumNumberOfStudents($maximumNumberOfStudents = null)
    {
        $this->maximumNumberOfStudents = $maximumNumberOfStudents;

        return $this;
    }

    /**
     * Get maximumNumberOfStudents.
     *
     * @return string|null
     */
    public function getMaximumNumberOfStudents()
    {
        return $this->maximumNumberOfStudents;
    }

    /**
     * Set maximumNumberOfInstructors.
     *
     * @param string|null $maximumNumberOfInstructors
     *
     * @return SubscriptionPlan
     */
    public function setMaximumNumberOfInstructors($maximumNumberOfInstructors = null)
    {
        $this->maximumNumberOfInstructors = $maximumNumberOfInstructors;

        return $this;
    }

    /**
     * Get maximumNumberOfInstructors.
     *
     * @return string|null
     */
    public function getMaximumNumberOfInstructors()
    {
        return $this->maximumNumberOfInstructors;
    }

    /**
     * Set packageValidity.
     *
     * @param string|null $packageValidity
     *
     * @return SubscriptionPlan
     */
    public function setPackageValidity($packageValidity = null)
    {
        $this->packageValidity = $packageValidity;

        return $this;
    }

    /**
     * Get packageValidity.
     *
     * @return string|null
     */
    public function getPackageValidity()
    {
        return $this->packageValidity;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return SubscriptionPlan
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @return Collection|DrivingSchoolEntity[]
     */
    public function getDrivingSchools(): Collection
    {
        return $this->drivingSchools;
    }

    /**
     * Add drivingSchool.
     *
     * @param \DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $drivingSchool
     *
     * @return SubscriptionPlanEntity
     */
    public function addDrivingSchool(\DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $drivingSchool)
    {
        $this->drivingSchools[] = $drivingSchool;

        return $this;
    }

    /**
     * Remove drivingSchool.
     *
     * @param \DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $drivingSchool
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDrivingSchool(\DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $drivingSchool)
    {
        return $this->drivingSchools->removeElement($drivingSchool);
    }
}
