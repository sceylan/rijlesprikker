<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * VehicleEntity
 *
 * @ORM\Table(name="vehicle", 
 * uniqueConstraints={@ORM\UniqueConstraint(name="unique_name",columns={
 *      "license_plate", "drivingschool_id"
 *  })}
 * )
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\VehicleRepository")
 * @UniqueEntity(fields={"licensePlate","DrivingSchool"}, message="Licence already taken !")
 */
class VehicleEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="vehicle")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $DrivingSchool;

    /**
     * @var string|null
     *
     * @ORM\Column(name="license_plate", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "License plate should not be blank.")
     */
    private $licensePlate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cbr_code", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "CBR code should not be blank.")
     */
    private $cbrCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="brand", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Brand should not be blank.")
     */
    private $brand;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fashion_model", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Fashion model should not be blank.")
     */
    private $FashionModel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="construction_year", type="string", length=255, nullable=true)
     */
    private $constructionYear;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="PriceAndPackagesEntity", inversedBy="vehicle")
     * @ORM\JoinTable(name="pricepackage_vehicle",
     *      joinColumns={@ORM\JoinColumn(name="vehicle_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_courses_id", referencedColumnName="id")}
     *      )
     */
    private $suitableTrainingCourses;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="VehicleKMEntity", mappedBy="vehicleId", cascade={"remove"})
     */
    protected $vehiclekm;

    /**
     * @ORM\OneToMany(targetEntity="PlanningEntity", mappedBy="vehicle", cascade={"remove"})
     */
    protected $planning;

    public function __construct()
    {
        $this->suitableTrainingCourses = new ArrayCollection();
        $this->appointment = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getLicensePlate();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return Vehicle
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set licensePlate.
     *
     * @param string|null $licensePlate
     *
     * @return Vehicle
     */
    public function setLicensePlate($licensePlate = null)
    {
        $this->licensePlate = $licensePlate;

        return $this;
    }

    /**
     * Get licensePlate.
     *
     * @return string|null
     */
    public function getLicensePlate()
    {
        return $this->licensePlate;
    }

    /**
     * Set cbrCode.
     *
     * @param string|null $cbrCode
     *
     * @return Vehicle
     */
    public function setCbrCode($cbrCode = null)
    {
        $this->cbrCode = $cbrCode;

        return $this;
    }

    /**
     * Get cbrCode.
     *
     * @return string|null
     */
    public function getCbrCode()
    {
        return $this->cbrCode;
    }  

    /**
     * Set brand.
     *
     * @param string|null $brand
     *
     * @return Vehicle
     */
    public function setBrand($brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return string|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set FashionModel.
     *
     * @param string|null $FashionModel
     *
     * @return Vehicle
     */
    public function setFashionModel($FashionModel = null)
    {
        $this->FashionModel = $FashionModel;

        return $this;
    }

    /**
     * Get FashionModel.
     *
     * @return string|null
     */
    public function getFashionModel()
    {
        return $this->FashionModel;
    }

    /**
     * Set constructionYear.
     *
     * @param string|null $constructionYear
     *
     * @return Vehicle
     */
    public function setConstructionYear($constructionYear = null)
    {
        $this->constructionYear = $constructionYear;

        return $this;
    }

    /**
     * Get constructionYear.
     *
     * @return string|null
     */
    public function getConstructionYear()
    {
        return $this->constructionYear;
    }

    /**
     * Set suitableTrainingCourses.
     *
     * @param int|null $suitableTrainingCourses
     *
     * @return Vehicle
     */
    public function setSuitableTrainingCourses(PriceAndPackagesEntity $suitableTrainingCourses = null)
    {
        $this->suitableTrainingCourses = $suitableTrainingCourses;

        return $this;
    }

    /**
     * Get suitableTrainingCourses.
     *
     * @return int|null
     */
    public function getSuitableTrainingCourses()
    {
        return $this->suitableTrainingCourses;
    }

    /**
     * Add SuitableTrainingCourses
     *
     * @return PriceAndPackagesEntity $suitableTrainingCourses
     */
    public function addSuitableTrainingCourses(PriceAndPackagesEntity $suitableTrainingCourses)
    {
        $this->suitableTrainingCourses[] = $suitableTrainingCourses;

        return $this;
    }

    /**
     * Remove SuitableTrainingCourses
     *
     * @param PriceAndPackagesEntity $suitableTrainingCourses
     */
    public function removeSuitableTrainingCourses(PriceAndPackagesEntity $suitableTrainingCourses)
    {
        $this->suitableTrainingCourses->removeElement($suitableTrainingCourses);
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Vehicle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    // Remove appointment table vehicleType
    public function remove($appointment)
    {
        $this->getAppointment()->remove($appointment);
    }

    /**
     * Add suitableTrainingCourse.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $suitableTrainingCourse
     *
     * @return VehicleEntity
     */
    public function addSuitableTrainingCourse(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $suitableTrainingCourse)
    {
        $this->suitableTrainingCourses[] = $suitableTrainingCourse;

        return $this;
    }

    /**
     * Remove suitableTrainingCourse.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $suitableTrainingCourse
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSuitableTrainingCourse(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $suitableTrainingCourse)
    {
        return $this->suitableTrainingCourses->removeElement($suitableTrainingCourse);
    }

    /**
     * Add vehiclekm.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleKMEntity $vehiclekm
     *
     * @return VehicleEntity
     */
    public function addVehiclekm(\DrivingSchool\AdminBundle\Entity\VehicleKMEntity $vehiclekm)
    {
        $this->vehiclekm[] = $vehiclekm;

        return $this;
    }

    /**
     * Remove vehiclekm.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleKMEntity $vehiclekm
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehiclekm(\DrivingSchool\AdminBundle\Entity\VehicleKMEntity $vehiclekm)
    {
        return $this->vehiclekm->removeElement($vehiclekm);
    }

    /**
     * Get vehiclekm.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehiclekm()
    {
        return $this->vehiclekm;
    }

    /**
     * Add planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return VehicleEntity
     */
    public function addPlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        $this->planning[] = $planning;

        return $this;
    }

    /**
     * Remove planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        return $this->planning->removeElement($planning);
    }

    /**
     * Get planning.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanning()
    {
        return $this->planning;
    }
}
