<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use FOS\UserBundle\Model\User as User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * InstructorEntity
 *
 * @ORM\Table(name="instructor")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\InstructorRepository")
 * @UniqueEntity(fields={"username"}, message="Username already taken !")
 * @UniqueEntity(fields={"email"}, message="Email already registered with us !")
 */
class InstructorEntity extends User implements UserInterface, EquatableInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="instructor")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var string|null
     *
     * @ORM\Column(name="instructor_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Instructor name should not be blank.")
     */
    private $instructorName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="instructor_email", type="string", length=255, nullable=true,  unique=true)
     * @Assert\NotBlank(message = "Instructor Email should not be blank.")
     * @Assert\Email(
     *     message = "The email {{ value }} is not a valid email.",
     *     checkMX = true
     * )
     */
    private $instructorEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Phone number should not be blank.")
     */
    private $phoneNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_crb", type="string", length=255, nullable=true)
     */
    private $codeCRB;

    /**
     * @var array|null
     * @ORM\ManyToMany(targetEntity="PriceAndPackagesEntity", inversedBy="instructor")
     * @ORM\JoinTable(name="pricepackage_instructor",
     *      joinColumns={@ORM\JoinColumn(name="instructor_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="price_packages_id", referencedColumnName="id")}
     *      )
     */
    private $authorizedFor;

    /**
     * @var array
     */
    protected $roles;

    /**
     * @var string|null
     *
     * @ORM\Column(name="publish_calendar", type="date", nullable=true)
     */
    private $publishCalendar;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    private $instructorImage;

    /**
     * @var time|null
     *
     * @ORM\Column(name="monday_start", type="time", nullable=true)
     */
    private $mondayStart;

    /**
     * @var time|null
     *
     * @ORM\Column(name="monday_end", type="time", nullable=true)
     */
    private $mondayEnd;

    /**
     * @var time|null
     *
     * @ORM\Column(name="tuesday_start", type="time", nullable=true)
     */
    private $tuesdayStart;

    /**
     * @var time|null
     *
     * @ORM\Column(name="tuesday_end", type="time", nullable=true)
     */
    private $tuesdayEnd;

     /**
     * @var time|null
     *
     * @ORM\Column(name="wednesday_start", type="time", nullable=true)
     */
    private $wednesdayStart;

    /**
     * @var time|null
     *
     * @ORM\Column(name="wednesday_end", type="time", nullable=true)
     */
    private $wednesdayEnd;

    /**
     * @var time|null
     *
     * @ORM\Column(name="thursday_start", type="time", nullable=true)
     */
    private $thursdayStart;

    /**
     * @var time|null
     *
     * @ORM\Column(name="thursday_end", type="time", nullable=true)
     */
    private $thursdayEnd;

    /**
     * @var time|null
     *
     * @ORM\Column(name="friday_start", type="time", nullable=true)
     */
    private $fridayStart;

    /**
     * @var time|null
     *
     * @ORM\Column(name="friday_end", type="time", nullable=true)
     */
    private $fridayEnd;

     /**
     * @var time|null
     *
     * @ORM\Column(name="saturday_start", type="time", nullable=true)
     */
    private $saturdayStart;

     /**
     * @var time|null
     *
     * @ORM\Column(name="saturday_end", type="time", nullable=true)
     */
    private $saturdayEnd;

     /**
     * @var time|null
     *
     * @ORM\Column(name="sunday_start", type="time", nullable=true)
     */
    private $sundayStart;

     /**
     * @var time|null
     *
     * @ORM\Column(name="sunday_end", type="time", nullable=true)
     */
    private $sundayEnd;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="HolidayEntity", mappedBy="instructor", cascade={"persist","remove"})
     */
    protected $holiday;

    /**
     * @ORM\ManyToMany(targetEntity="InstructorGroupEntity", mappedBy="instructor", cascade={"persist","remove"})
     */
    protected $instructorGroup;

    /**
     * @ORM\OneToMany(targetEntity="PlanningEntity", mappedBy="instructor", cascade={"remove"})
     */
    protected $planning;

    /**
     * @ORM\OneToMany(targetEntity="StudentEntity", mappedBy="instructor", cascade={"remove"})
     */
    protected $StudentEntity;
    
    /**
     * @ORM\OneToMany(targetEntity="NotificationEntity", mappedBy="instructor", cascade={"remove"})
     */
    protected $notification;
    
    public function __construct()
    {
        parent::__construct();
        $this->authorizedFor = new ArrayCollection();
        $this->appointment = new ArrayCollection();
        $this->roles = array();
        $this->enabled = true;
        $this->setInstructorAdmin(true);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getInstructorName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return Vehicle
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set instructorName.
     *
     * @param string|null $instructorName
     *
     * @return Instructor
     */
    public function setInstructorName($instructorName = null)
    {
        $this->instructorName = $instructorName;

        return $this;
    }

    /**
     * Get instructorName.
     *
     * @return string|null
     */
    public function getInstructorName()
    {
        return $this->instructorName;
    }

    /**
     * Set instructorEmail.
     *
     * @param string|null $instructorEmail
     *
     * @return Instructor
     */
    public function setInstructorEmail($instructorEmail = null)
    {
        $this->instructorEmail = $instructorEmail;

        return $this;
    }

    /**
     * Get instructorEmail.
     *
     * @return string|null
     */
    public function getInstructorEmail()
    {
        return $this->instructorEmail;
    }

    /**
     * Set phoneNumber.
     *
     * @param string|null $phoneNumber
     *
     * @return Instructor
     */
    public function setPhoneNumber($phoneNumber = null)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber.
     *
     * @return string|null
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set codeCRB.
     *
     * @param string|null $codeCRB
     *
     * @return Instructor
     */
    public function setCodeCRB($codeCRB = null)
    {
        $this->codeCRB = $codeCRB;

        return $this;
    }

    /**
     * Get codeCRB.
     *
     * @return string|null
     */
    public function getCodeCRB()
    {
        return $this->codeCRB;
    }

    /**
     * Set authorizedFor.
     *
     * @param int|null $authorizedFor
     *
     * @return Instructor
     */
    public function setAuthorizedFor(PriceAndPackagesEntity $authorizedFor)
    {
        $this->authorizedFor = $authorizedFor;

        return $this;
    }

    /**
     * Get authorizedFor.
     *
     * @return int|null
     */
    public function getAuthorizedFor()
    {
        return $this->authorizedFor;
    }


    public function addAuthorizedFor(PriceAndPackagesEntity $authorizedFor)
    {
        $this->authorizedFor[] = $authorizedFor;

        return $this;
    }

    public function removeAuthorizedFor(PriceAndPackagesEntity $authorizedFor)
    {
        $this->authorizedFor->removeElement($authorizedFor);
    }


    /**
     * Set publishCalendar.
     *
     * @param date $publishCalendar
     *
     * @return Instructor
     */
    public function setPublishCalendar($publishCalendar = null)
    {
        $this->publishCalendar = $publishCalendar;

        return $this;
    }

    /**
     * Get publishCalendar.
     *
     * @return date
     */
    public function getPublishCalendar()
    {
        return $this->publishCalendar;
    }

    /**
     * Set instructorImage.
     *
     * @param string|null $instructorImage
     *
     * @return Student
     */
    public function setInstructorImage($instructorImage = null)
    {
        $this->instructorImage = $instructorImage;

        return $this;
    }

    /**
     * Get instructorImage.
     *
     * @return string|null
     */
    public function getInstructorImage()
    {
        return $this->instructorImage;
    }

    /**
     * Set mondayStart.
     *
     * @param time|null $mondayStart
     *
     * @return Instructor
     */
    public function setMondayStart($mondayStart = null)
    {
        $this->mondayStart = $mondayStart;

        return $this;
    }

    /**
     * Get mondayStart.
     *
     * @return time|null
     */
    public function getMondayStart()
    {
        return $this->mondayStart;
    }

    /**
     * Set mondayEnd.
     *
     * @param time|null $mondayEnd
     *
     * @return Instructor
     */
    public function setMondayEnd($mondayEnd = null)
    {
        $this->mondayEnd = $mondayEnd;

        return $this;
    }

    /**
     * Get mondayEnd.
     *
     * @return time|null
     */
    public function getMondayEnd()
    {
        return $this->mondayEnd;
    }

    /**
     * Set tuesdayStart.
     *
     * @param time|null $tuesdayStart
     *
     * @return Instructor
     */
    public function setTuesdayStart($tuesdayStart = null)
    {
        $this->tuesdayStart = $tuesdayStart;

        return $this;
    }

    /**
     * Get tuesdayStart.
     *
     * @return time|null
     */
    public function getTuesdayStart()
    {
        return $this->tuesdayStart;
    }

    /**
     * Set tuesdayEnd.
     *
     * @param time|null $tuesdayEnd
     *
     * @return Instructor
     */
    public function setTuesdayEnd($tuesdayEnd = null)
    {
        $this->tuesdayEnd = $tuesdayEnd;

        return $this;
    }

    /**
     * Get tuesdayEnd.
     *
     * @return time|null
     */
    public function getTuesdayEnd()
    {
        return $this->tuesdayEnd;
    }

    /**
     * Set wednesdayStart.
     *
     * @param time|null $wednesdayStart
     *
     * @return Instructor
     */
    public function setWednesdayStart($wednesdayStart = null)
    {
        $this->wednesdayStart = $wednesdayStart;

        return $this;
    }

    /**
     * Get wednesdayStart.
     *
     * @return time|null
     */
    public function getWednesdayStart()
    {
        return $this->wednesdayStart;
    }

    /**
     * Set wednesdayEnd.
     *
     * @param time|null $wednesdayEnd
     *
     * @return Instructor
     */
    public function setWednesdayEnd($wednesdayEnd = null)
    {
        $this->wednesdayEnd = $wednesdayEnd;

        return $this;
    }

    /**
     * Get wednesdayEnd.
     *
     * @return time|null
     */
    public function getWednesdayEnd()
    {
        return $this->wednesdayEnd;
    }

    /**
     * Set thursdayStart.
     *
     * @param time|null $thursdayStart
     *
     * @return Instructor
     */
    public function setThursdayStart($thursdayStart = null)
    {
        $this->thursdayStart = $thursdayStart;

        return $this;
    }

    /**
     * Get thursdayStart.
     *
     * @return time|null
     */
    public function getThursdayStart()
    {
        return $this->thursdayStart;
    }

    /**
     * Set thursdayEnd.
     *
     * @param time|null $thursdayEnd
     *
     * @return Instructor
     */
    public function setThursdayEnd($thursdayEnd = null)
    {
        $this->thursdayEnd = $thursdayEnd;

        return $this;
    }

    /**
     * Get thursdayEnd.
     *
     * @return time|null
     */
    public function getThursdayEnd()
    {
        return $this->thursdayEnd;
    }

    /**
     * Set fridayStart.
     *
     * @param time|null $fridayStart
     *
     * @return Instructor
     */
    public function setFridayStart($fridayStart = null)
    {
        $this->fridayStart = $fridayStart;

        return $this;
    }

    /**
     * Get fridayStart.
     *
     * @return time|null
     */
    public function getFridayStart()
    {
        return $this->fridayStart;
    }

    /**
     * Set fridayEnd.
     *
     * @param time|null $fridayEnd
     *
     * @return Instructor
     */
    public function setFridayEnd($fridayEnd = null)
    {
        $this->fridayEnd = $fridayEnd;

        return $this;
    }

    /**
     * Get fridayEnd.
     *
     * @return time|null
     */
    public function getFridayEnd()
    {
        return $this->fridayEnd;
    }

    /**
     * Set saturdayStart.
     *
     * @param time|null $saturdayStart
     *
     * @return Instructor
     */
    public function setSaturdayStart($saturdayStart = null)
    {
        $this->saturdayStart = $saturdayStart;

        return $this;
    }

    /**
     * Get saturdayStart.
     *
     * @return time|null
     */
    public function getSaturdayStart()
    {
        return $this->saturdayStart;
    }

    /**
     * Set saturdayEnd.
     *
     * @param time|null $saturdayEnd
     *
     * @return Instructor
     */
    public function setSaturdayEnd($saturdayEnd = null)
    {
        $this->saturdayEnd = $saturdayEnd;

        return $this;
    }

    /**
     * Get saturdayEnd.
     *
     * @return time|null
     */
    public function getSaturdayEnd()
    {
        return $this->saturdayEnd;
    }

    /**
     * Set sundayStart.
     *
     * @param time|null $sundayStart
     *
     * @return Instructor
     */
    public function setSundayStart($sundayStart = null)
    {
        $this->sundayStart = $sundayStart;

        return $this;
    }

    /**
     * Get sundayStart.
     *
     * @return time|null
     */
    public function getSundayStart()
    {
        return $this->sundayStart;
    }

    /**
     * Set sundayEnd.
     *
     * @param time|null $sundayEnd
     *
     * @return Instructor
     */
    public function setSundayEnd($sundayEnd = null)
    {
        $this->sundayEnd = $sundayEnd;

        return $this;
    }

    /**
     * Get sundayEnd.
     *
     * @return time|null
     */
    public function getSundayEnd()
    {
        return $this->sundayEnd;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Instructor
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    // Remove appointment table instructor
    public function remove($appointment)
    {
        $this->getAppointment()->remove($appointment);
    }


    /**
     * {@inheritdoc}
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * {@inheritdoc}
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setInstructorAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole("ROLE_INSTRUCTOR");
        } else {
            $this->removeRole("ROLE_INSTRUCTOR");
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        $roles = $this->roles;
        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }
        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    

    /**
     * {@inheritdoc}
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;

        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        if (13 === count($data)) {
            // Unserializing a User object from 1.3.x
            unset($data[4], $data[5], $data[6], $data[9], $data[10]);
            $data = array_values($data);
        } elseif (11 === count($data)) {
            // Unserializing a User from a dev version somewhere between 2.0-alpha3 and 2.0-beta1
            unset($data[4], $data[7], $data[8]);
            $data = array_values($data);
        }

        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical
        ) = $data;
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof InstructorEntity) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    /**
     * Add holiday.
     *
     * @param \DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday
     *
     * @return InstructorEntity
     */
    public function addHoliday(\DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday)
    {
        $this->holiday[] = $holiday;

        return $this;
    }

    /**
     * Remove holiday.
     *
     * @param \DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeHoliday(\DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday)
    {
        return $this->holiday->removeElement($holiday);
    }

    /**
     * Get holiday.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHoliday()
    {
        return $this->holiday;
    }

    /**
     * Add instructorGroup.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup
     *
     * @return InstructorEntity
     */
    public function addInstructorGroup(\DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup)
    {
        $this->instructorGroup[] = $instructorGroup;

        return $this;
    }

    /**
     * Remove instructorGroup.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInstructorGroup(\DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup)
    {
        return $this->instructorGroup->removeElement($instructorGroup);
    }

    /**
     * Get instructorGroup.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstructorGroup()
    {
        return $this->instructorGroup;
    }

    /**
     * Add planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return InstructorEntity
     */
    public function addPlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        $this->planning[] = $planning;

        return $this;
    }

    /**
     * Remove planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        return $this->planning->removeElement($planning);
    }

    /**
     * Get planning.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanning()
    {
        return $this->planning;
    }

    /**
     * Add notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return InstructorEntity
     */
    public function addNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        $this->notification[] = $notification;

        return $this;
    }

    /**
     * Remove notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        return $this->notification->removeElement($notification);
    }

    /**
     * Get notification.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Add studentEntity.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity
     *
     * @return InstructorEntity
     */
    public function addStudentEntity(\DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity)
    {
        $this->StudentEntity[] = $studentEntity;

        return $this;
    }

    /**
     * Remove studentEntity.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentEntity(\DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity)
    {
        return $this->StudentEntity->removeElement($studentEntity);
    }

    /**
     * Get studentEntity.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentEntity()
    {
        return $this->StudentEntity;
    }
}
