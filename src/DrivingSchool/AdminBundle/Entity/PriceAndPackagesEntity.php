<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PriceAndPackagesEntity
 *
 * @ORM\Table(name="price_and_packages",
 *  uniqueConstraints={@ORM\UniqueConstraint(name="unique_name",columns={
 *      "name", "drivingschool_id"
 *  })}
 *)
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\PriceAndPackagesRepository")
 * @UniqueEntity(fields={"name","DrivingSchool"}, message="name already taken !")
 */
class PriceAndPackagesEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="priceAndPackages")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $DrivingSchool;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Name should not be blank.")
     */
    private $name;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="PriceIconsEntity", inversedBy="priceAndPackages")
     * @ORM\JoinColumn(name="icon_id", referencedColumnName="id", nullable=true)
     */
    private $icon;

    /**
     * @var int|null
     *
     * @ORM\Column(name="amount_of_lessons", type="integer", length=11, nullable=true)
     */
    private $amountOfLessons;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lesson_duration", type="integer", length=11, nullable=true)
     */
    private $lessonDuration;

    /**
     * @var string|null
     *
     * @ORM\Column(name="total_price", type="float", nullable=true)
     */
    private $totalPrice;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="included_vat", type="boolean", nullable=true)
     */
    private $includedVat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vat", type="string", length=255, nullable=true)
     */
    private $vat;

    /** 
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="VehicleEntity", mappedBy="suitableTrainingCourses", cascade={"remove"})
     */ 
    protected $vehicle;

    /**
     * @ORM\ManyToMany(targetEntity="InstructorEntity", mappedBy="authorizedFor", cascade={"persist","remove"})
     */
    protected $instructor;

    /**
     * @ORM\ManyToMany(targetEntity="StudentEntity", mappedBy="educations", cascade={"persist","remove"})
     */
    protected $student;

    /**
     * @ORM\OneToMany(targetEntity="ModuleEntity", mappedBy="lessonId", cascade={"remove"})
     */
    protected $module;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialEntity", mappedBy="pricePackageId", cascade={"remove"})
     */
    protected $studentFinancial;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialHistoryEntity", mappedBy="pricePackageId", cascade={"remove"})
     */
    protected $studentFinancialHistory;

    /**
     * @ORM\OneToMany(targetEntity="TrainingSessionsEntity", mappedBy="pricePackageId", cascade={"remove"})
     */
    protected $trainingSessions;

    /**
     * @ORM\OneToMany(targetEntity="PlanningEntity", mappedBy="pricePackage", cascade={"remove"})
     */
    protected $planning;

    public function __toString()
    {
        return strval($this->id);
    }

    /**
     * Get instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Set instructor.
     *
     * @param int|null $instructor
     *
     * @return PriceAndPackages
     */
    public function setInstructor(InstructorEntity $instructor = null)
    {
        $this->instructor = $instructor;

        return $this;
    }

    public function addInstructor(InstructorEntity $instructor)
    {
        $this->instructor[] = $instructor;

        return $this;
    }

    public function removeInstructor(InstructorEntity $instructor)
    {
        $this->instructor->removeElement($instructor);
    }

    /**
     * Get student.
     *
     * @return int|null
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set student.
     *
     * @param int|null $student
     *
     * @return PriceAndPackages
     */
    public function setStudent(StudentEntity $student = null)
    {
        $this->student = $student;

        return $this;
    }

    public function addStudent(StudentEntity $student)
    {
        $this->student[] = $student;

        return $this;
    }

    public function removeStudent(StudentEntity $student)
    {
        $this->student->removeElement($student);
    }
    
    public function getVehicle()
    {
        return $this->vehicle;
    }

    public function getLessonId()
    {
        return $this->lessonId;
    }

    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return PriceAndPachages
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return PriceAndPackages
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set icon.
     *
     * @param int|null $icon
     *
     * @return PriceAndPackages
     */
    public function setIcon($icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return int|null
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set amountOfLessons.
     *
     * @param string|null $amountOfLessons
     *
     * @return PriceAndPachages
     */
    public function setAmountOfLessons($amountOfLessons = null)
    {
        $this->amountOfLessons = $amountOfLessons;

        return $this;
    }

    /**
     * Get amountOfLessons.
     *
     * @return string|null
     */
    public function getAmountOfLessons()
    {
        return $this->amountOfLessons;
    }

    /**
     * Set lessonDuration.
     *
     * @param string|null $lessonDuration
     *
     * @return PriceAndPachages
     */
    public function setLessonDuration($lessonDuration = null)
    {
        $this->lessonDuration = $lessonDuration;

        return $this;
    }

    /**
     * Get lessonDuration.
     *
     * @return string|null
     */
    public function getLessonDuration()
    {
        return $this->lessonDuration;
    }

    /**
     * Set totalPrice.
     *
     * @param string|null $totalPrice
     *
     * @return PriceAndPachages
     */
    public function setTotalPrice($totalPrice = null)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice.
     *
     * @return string|null
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set includedVat.
     *
     * @param bool $includedVat
     *
     * @return PriceAndPachages
     */
    public function setIncludedVat($includedVat)
    {
        $this->includedVat = $includedVat;

        return $this;
    }

    /**
     * Get includedVat.
     *
     * @return bool
     */
    public function getIncludedVat()
    {
        return $this->includedVat;
    }

    /**
     * Set vat.
     *
     * @param string|null $vat
     *
     * @return PriceAndPachages
     */
    public function setVat($vat = null)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat.
     *
     * @return string|null
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return PriceAndPackages
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicle = new \Doctrine\Common\Collections\ArrayCollection();
        $this->instructor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->student = new \Doctrine\Common\Collections\ArrayCollection();
        $this->module = new \Doctrine\Common\Collections\ArrayCollection();
        $this->studentFinancial = new \Doctrine\Common\Collections\ArrayCollection();
        $this->studentFinancialHistory = new \Doctrine\Common\Collections\ArrayCollection();
        $this->trainingSessions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->planning = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vehicle.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle
     *
     * @return PriceAndPackagesEntity
     */
    public function addVehicle(\DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle)
    {
        $this->vehicle[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehicle(\DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle)
    {
        return $this->vehicle->removeElement($vehicle);
    }

    /**
     * Add module.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ModuleEntity $module
     *
     * @return PriceAndPackagesEntity
     */
    public function addModule(\DrivingSchool\AdminBundle\Entity\ModuleEntity $module)
    {
        $this->module[] = $module;

        return $this;
    }

    /**
     * Remove module.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ModuleEntity $module
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeModule(\DrivingSchool\AdminBundle\Entity\ModuleEntity $module)
    {
        return $this->module->removeElement($module);
    }

    /**
     * Get module.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Add studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return PriceAndPackagesEntity
     */
    public function addStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        $this->studentFinancial[] = $studentFinancial;

        return $this;
    }

    /**
     * Remove studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        return $this->studentFinancial->removeElement($studentFinancial);
    }

    /**
     * Get studentFinancial.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancial()
    {
        return $this->studentFinancial;
    }

    /**
     * Add studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return PriceAndPackagesEntity
     */
    public function addStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        $this->studentFinancialHistory[] = $studentFinancialHistory;

        return $this;
    }

    /**
     * Remove studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        return $this->studentFinancialHistory->removeElement($studentFinancialHistory);
    }

    /**
     * Get studentFinancialHistory.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancialHistory()
    {
        return $this->studentFinancialHistory;
    }

    /**
     * Add trainingSession.
     *
     * @param \DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession
     *
     * @return PriceAndPackagesEntity
     */
    public function addTrainingSession(\DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession)
    {
        $this->trainingSessions[] = $trainingSession;

        return $this;
    }

    /**
     * Remove trainingSession.
     *
     * @param \DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrainingSession(\DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession)
    {
        return $this->trainingSessions->removeElement($trainingSession);
    }

    /**
     * Get trainingSessions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingSessions()
    {
        return $this->trainingSessions;
    }

    /**
     * Add planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return PriceAndPackagesEntity
     */
    public function addPlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        $this->planning[] = $planning;

        return $this;
    }

    /**
     * Remove planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        return $this->planning->removeElement($planning);
    }

    /**
     * Get planning.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanning()
    {
        return $this->planning;
    }
}
