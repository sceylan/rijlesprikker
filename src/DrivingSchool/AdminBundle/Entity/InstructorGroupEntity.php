<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * InstructorGroupEntity
 *
 * @ORM\Table(name="instructor_group", indexes={@ORM\Index(name="drivingschool_id", columns={"drivingschool_id"})})
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\InstructorGroupRepository")
 */
class InstructorGroupEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="instructorGroup")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Name should not be blank.")
     */
    private $name;
    
    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="InstructorEntity", inversedBy="instructorGroup")
     * @ORM\JoinTable(name="instructor_instructorgroup",
     *      joinColumns={@ORM\JoinColumn(name="instructorgruop_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="instructor_id", referencedColumnName="id")}
     *      )
     */
    private $instructor;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    public function __construct()
    {
        
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return Vehicle
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InstructorGroup
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set instructor.
     *
     * @param int|null $instructor
     *
     * @return InstructorGroup
     */
    public function setInstructor(InstructorEntity $instructor = null)
    {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Add instructor
     *
     * @return InstructorEntity $instructor
     */
    public function addInstructor(InstructorEntity $instructor)
    {
        $this->instructor[] = $instructor;

        return $this;
    }

    /**
     * Remove instructor
     *
     * @param InstructorEntity $instructor
     */
    public function removeInstructor(instructorEntity $instructor)
    {
        $this->instructor->removeElement($instructor);
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Vehicle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
}
