<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentFinancialEntity
 *
 * @ORM\Table(name="student_financial")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\StudentFinancialRepository")
 */
class StudentFinancialEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="studentFinancial")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="studentFinancial")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=true)
     */
    private $studentid;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="PriceAndPackagesEntity", inversedBy="studentFinancial")
     * @ORM\JoinColumn(name="price_package_id", referencedColumnName="id", nullable=true)
     */
    private $pricePackageId;
    
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="TrainingSessionsEntity", inversedBy="studentFinancial")
     * @ORM\JoinColumn(name="trainingsession_id", referencedColumnName="id", nullable=true)
     */
    private $Courses;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="float", nullable=true)
     */
    private $qty;

    /**
     * @var int|null
     *
     * @ORM\Column(name="minutes", type="integer", nullable=true)
     */
    private $minutes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="calc_minute_qty", type="float", nullable=true)
     */
    private $calcMinuteQty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;
   
    /**
     * @var string|null
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="totalincluded_vat", type="float", nullable=true)
     */
    private $totalincludedVat;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="included_vat", type="boolean", nullable=true)
     */
    private $includedVat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vat", type="string", length=255, nullable=true)
     */
    private $vat;

    /**
     * @var text|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $desc;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="generated_invoice", type="boolean", nullable=true)
     */
    private $generatedInvoice;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return StudentFinancial
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set studentid.
     *
     * @param int|null $studentid
     *
     * @return StudentFinancial
     */
    public function setStudentid(StudentEntity $studentid = null)
    {
        $this->studentid = $studentid;

        return $this;
    }

    /**
     * Get studentid.
     *
     * @return int|null
     */
    public function getStudentid()
    {
        return $this->studentid;
    }

    /**
     * Set pricePackageId.
     *
     * @param int|null $pricePackageId
     *
     * @return StudentFinancial
     */
    public function setPricePackageId(PriceAndPackagesEntity $pricePackageId = null)
    {
        $this->pricePackageId = $pricePackageId;

        return $this;
    }

    /**
     * Get pricePackageId.
     *
     * @return int|null
     */
    public function getPricePackageId()
    {
        return $this->pricePackageId;
    }

    /**
     * Set Courses.
     *
     * @param int|null $Courses
     *
     * @return StudentFinancial
     */
    public function setCourses(TrainingSessionsEntity $Courses = null)
    {
        $this->Courses = $Courses;

        return $this;
    }

    /**
     * Get Courses.
     *
     * @return int|null
     */
    public function getCourses()
    {
        return $this->Courses;
    }

    /**
     * Set qty.
     *
     * @param int|null $qty
     *
     * @return StudentFinancial
     */
    public function setQty($qty = null)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty.
     *
     * @return int|null
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set minutes.
     *
     * @param int|null $minutes
     *
     * @return StudentFinancial
     */
    public function setMinutes($minutes = null)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes.
     *
     * @return int|null
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set price.
     *
     * @param string|null $price
     *
     * @return StudentFinancial
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set calcMinuteQty.
     *
     * @param string|null $calcMinuteQty
     *
     * @return StudentFinancial
     */
    public function setCalcMinuteQty($calcMinuteQty = null)
    {
        $this->calcMinuteQty = $calcMinuteQty;

        return $this;
    }

    /**
     * Get calcMinuteQty.
     *
     * @return string|null
     */
    public function getCalcMinuteQty()
    {
        return $this->calcMinuteQty;
    }

    /**
     * Set amount.
     *
     * @param string|null $amount
     *
     * @return StudentFinancial
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return string|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set totalincludedVat.
     *
     * @param string|null $totalincludedVat
     *
     * @return StudentFinancial
     */
    public function setTotalincludedVat($totalincludedVat = null)
    {
        $this->totalincludedVat = $totalincludedVat;

        return $this;
    }

    /**
     * Get totalincludedVat.
     *
     * @return string|null
     */
    public function getTotalincludedVat()
    {
        return $this->totalincludedVat;
    }

    /**
     * Set includedVat.
     *
     * @param bool $includedVat
     *
     * @return StudentFinancial
     */
    public function setIncludedVat($includedVat)
    {
        $this->includedVat = $includedVat;

        return $this;
    }

    /**
     * Get includedVat.
     *
     * @return bool
     */
    public function getIncludedVat()
    {
        return $this->includedVat;
    }

    /**
     * Set vat.
     *
     * @param string|null $vat
     *
     * @return StudentFinancial
     */
    public function setVat($vat = null)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat.
     *
     * @return string|null
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set desc.
     *
     * @param string|null $desc
     *
     * @return StudentFinancial
     */
    public function setDesc($desc = null)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc.
     *
     * @return string|null
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set generatedInvoice.
     *
     * @param bool $generatedInvoice
     *
     * @return StudentFinancial
     */
    public function setGeneratedInvoice($generatedInvoice)
    {
        $this->generatedInvoice = $generatedInvoice;

        return $this;
    }

    /**
     * Get generatedInvoice.
     *
     * @return bool
     */
    public function getGeneratedInvoice()
    {
        return $this->generatedInvoice;
    }
}
