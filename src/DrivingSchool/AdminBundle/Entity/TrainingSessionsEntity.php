<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TrainingSessionsEntity
 *
 * @ORM\Table(name="training_sessions")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\TrainingSessionsRepository")
 */
class TrainingSessionsEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="trainingSessions")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     *
     * @ORM\Column(name="training_type", type="integer", options={"comment":"0 = Driving Lesson, 1 = Examination and Test, 2 = Other, 3 = Packages"})
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Name should not be blank.")
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     * @Assert\NotBlank(message = "Price should not be blank.")
     * @Assert\Regex(pattern="/\b\d{1,3}(?:,?\d{3})*(?:\.\d{2})?\b/", message="Price number of boxes must be a number")
     */
    private $price;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="included_vat", type="boolean", nullable=true)
     */
    private $includedVat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vat", type="string", length=255, nullable=true)
     */
    private $vat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vat_calc", type="string", length=255, nullable=true)
     */
    private $vatCalc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="qty", type="integer", nullable=true)
     * @Assert\Regex(pattern="/\b\d{1,3}(?:,?\d{3})*(?:\.\d{2})?\b/", message="Qty per minute must be a number")
     * @Assert\LessThan(1441)
     * @Assert\GreaterThan(0)
     */
    private $qty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="minutes", type="string", length=255, nullable=true)
     */
    private $minutes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="unit", type="string", length=255, nullable=true)
     */
    private $unit;

    /**
     * @var string|null
     *
     * @ORM\Column(name="appointment_label", type="string", length=255, nullable=true)
     */
    private $appointmentLabel;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="PriceAndPackagesEntity", inversedBy="trainingSessions")
     * @ORM\JoinColumn(name="price_package_id", referencedColumnName="id", nullable=true)
     */
    private $pricePackageId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="schedule", type="string", length=255, nullable=true)
     */
    private $schedule;

    /**
     * @var int|null
     *
     * @ORM\Column(name="length_qty", type="string", length=255, nullable=true)
     */
    private $lengthQty;
   
    /**
     * @var string|null
     *
     * @ORM\Column(name="exam", type="string", length=255, nullable=true)
     */
    private $exam;

    /**
     * @var string|null
     *
     * @ORM\Column(name="accounting_code", type="string", length=255, nullable=true)
     */
    private $accountingCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=true)
     */
    private $category;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="code_cbr", type="string", length=255, nullable=true)
     */
    private $codeCbr;

    /**
     * @var int|null
     *
     * @ORM\Column(name="linked_to", type="integer", nullable=true)
     */
    private $linkedTo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="trainingsession_detail", type="text", nullable=true)
     */
    private $trainingsessionDetail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="package_calc", type="string", length=255, nullable=true)
     */
    private $packageCalc;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialEntity", mappedBy="Courses", cascade={"remove"})
     */
    protected $studentFinancial;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialHistoryEntity", mappedBy="Courses", cascade={"remove"})
     */
    protected $studentFinancialHistory;

    /**
     * @ORM\OneToMany(targetEntity="PlanningEntity", mappedBy="trainingSession", cascade={"remove"})
     */
    protected $planning;

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param int $type
     *
     * @return TrainingSessions
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return PriceAndPackages
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return TrainingSessions
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price.
     *
     * @param string|null $price
     *
     * @return TrainingSessions
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set includedVat.
     *
     * @param bool $includedVat
     *
     * @return TrainingSessions
     */
    public function setIncludedVat($includedVat)
    {
        $this->includedVat = $includedVat;

        return $this;
    }

    /**
     * Get includedVat.
     *
     * @return bool
     */
    public function getIncludedVat()
    {
        return $this->includedVat;
    }

    /**
     * Set vat.
     *
     * @param string|null $vat
     *
     * @return TrainingSessions
     */
    public function setVat($vat = null)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat.
     *
     * @return string|null
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set vatCalc.
     *
     * @param string|null $vatCalc
     *
     * @return TrainingSessions
     */
    public function setVatCalc($vatCalc = null)
    {
        $this->vatCalc = $vatCalc;

        return $this;
    }

    /**
     * Get vatCalc.
     *
     * @return string|null
     */
    public function getVatCalc()
    {
        return $this->vatCalc;
    }

    /**
     * Set qty.
     *
     * @param int|null $qty
     *
     * @return TrainingSessions
     */
    public function setQty($qty = null)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty.
     *
     * @return int|null
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set minutes.
     *
     * @param string|null $minutes
     *
     * @return TrainingSessions
     */
    public function setMinutes($minutes = null)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes.
     *
     * @return string|null
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set unit.
     *
     * @param string|null $unit
     *
     * @return TrainingSessions
     */
    public function setUnit($unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit.
     *
     * @return string|null
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set appointmentLabel.
     *
     * @param string|null $appointmentLabel
     *
     * @return TrainingSessions
     */
    public function setAppointmentLabel($appointmentLabel = null)
    {
        $this->appointmentLabel = $appointmentLabel;

        return $this;
    }

    /**
     * Get appointmentLabel.
     *
     * @return string|null
     */
    public function getAppointmentLabel()
    {
        return $this->appointmentLabel;
    }


    /**
     * Set pricePackageId.
     *
     * @param int|null $pricePackageId
     *
     * @return TrainingSessions
     */
    public function setPricePackageId(PriceAndPackagesEntity $pricePackageId = null)
    {
        $this->pricePackageId = $pricePackageId;

        return $this;
    }

    /**
     * Get pricePackageId.
     *
     * @return int|null
     */
    public function getPricePackageId()
    {
        return $this->pricePackageId;
    }

    /**
     * Set schedule.
     *
     * @param string|null $schedule
     *
     * @return TrainingSessions
     */
    public function setSchedule($schedule = null)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule.
     *
     * @return string|null
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set lengthQty.
     *
     * @param string|null $lengthQty
     *
     * @return TrainingSessions
     */
    public function setLengthQty($lengthQty = null)
    {
        $this->lengthQty = $lengthQty;

        return $this;
    }

    /**
     * Get lengthQty.
     *
     * @return string|null
     */
    public function getLengthQty()
    {
        return $this->lengthQty;
    }

    /**
     * Set exam.
     *
     * @param string|null $exam
     *
     * @return TrainingSessions
     */
    public function setExam($exam = null)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam.
     *
     * @return string|null
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * Set accountingCode.
     *
     * @param string|null $accountingCode
     *
     * @return TrainingSessions
     */
    public function setAccountingCode($accountingCode = null)
    {
        $this->accountingCode = $accountingCode;

        return $this;
    }

    /**
     * Get accountingCode.
     *
     * @return string|null
     */
    public function getAccountingCode()
    {
        return $this->accountingCode;
    }

    /**
     * Set category.
     *
     * @param string|null $category
     *
     * @return TrainingSessions
     */
    public function setCategory($category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return string|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set codeCbr.
     *
     * @param string|null $codeCbr
     *
     * @return TrainingSessions
     */
    public function setCodeCbr($codeCbr = null)
    {
        $this->codeCbr = $codeCbr;

        return $this;
    }

    /**
     * Get codeCbr.
     *
     * @return string|null
     */
    public function getCodeCbr()
    {
        return $this->codeCbr;
    }

    /**
     * Set linkedTo.
     *
     * @param int|null $linkedTo
     *
     * @return TrainingSessions
     */
    public function setLinkedTo($linkedTo = null)
    {
        $this->linkedTo = $linkedTo;

        return $this;
    }

    /**
     * Get linkedTo.
     *
     * @return int|null
     */
    public function getLinkedTo()
    {
        return $this->linkedTo;
    }

    /**
     * Set trainingsessionDetail.
     *
     * @param string|null $trainingsessionDetail
     *
     * @return Packages
     */
    public function setTrainingsessionDetail($trainingsessionDetail = null)
    {
        $this->trainingsessionDetail = $trainingsessionDetail;

        return $this;
    }

    /**
     * Get trainingsessionDetail.
     *
     * @return string|null
     */
    public function getTrainingsessionDetail()
    {
        return $this->trainingsessionDetail;
    }

    /**
     * Set packageCalc.
     *
     * @param string|null $packageCalc
     *
     * @return Packages
     */
    public function setPackageCalc($packageCalc = null)
    {
        $this->packageCalc = $packageCalc;

        return $this;
    }

    /**
     * Get packageCalc.
     *
     * @return string|null
     */
    public function getPackageCalc()
    {
        return $this->packageCalc;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return TrainingSessions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->studentFinancial = new \Doctrine\Common\Collections\ArrayCollection();
        $this->studentFinancialHistory = new \Doctrine\Common\Collections\ArrayCollection();
        $this->planning = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return TrainingSessionsEntity
     */
    public function addStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        $this->studentFinancial[] = $studentFinancial;

        return $this;
    }

    /**
     * Remove studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        return $this->studentFinancial->removeElement($studentFinancial);
    }

    /**
     * Get studentFinancial.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancial()
    {
        return $this->studentFinancial;
    }

    /**
     * Add studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return TrainingSessionsEntity
     */
    public function addStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        $this->studentFinancialHistory[] = $studentFinancialHistory;

        return $this;
    }

    /**
     * Remove studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        return $this->studentFinancialHistory->removeElement($studentFinancialHistory);
    }

    /**
     * Get studentFinancialHistory.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancialHistory()
    {
        return $this->studentFinancialHistory;
    }

    /**
     * Add planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return TrainingSessionsEntity
     */
    public function addPlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        $this->planning[] = $planning;

        return $this;
    }

    /**
     * Remove planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        return $this->planning->removeElement($planning);
    }

    /**
     * Get planning.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanning()
    {
        return $this->planning;
    }
}
