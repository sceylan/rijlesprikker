<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserTypeEntity
 *
 * @ORM\Table(name="user_type")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\UserTypeRepository")
 */
class UserTypeEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="NotificationEntity", mappedBy="userType", cascade={"remove"})
     */
    protected $notification;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Banner
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notification = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return UserTypeEntity
     */
    public function addNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        $this->notification[] = $notification;

        return $this;
    }

    /**
     * Remove notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        return $this->notification->removeElement($notification);
    }

    /**
     * Get notification.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotification()
    {
        return $this->notification;
    }
}
