<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModuleEntity
 *
 * @ORM\Table(name="module", indexes={@ORM\Index(name="lesson_id", columns={"lesson_id"})})
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\ModuleRepository")
 */
class ModuleEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="PriceAndPackagesEntity", inversedBy="module")
     * @ORM\JoinColumn(name="lesson_id", referencedColumnName="id", nullable=true)
     */
    private $lessonId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="module_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Module name should not be blank.")
     */
    private $moduleName;

    /**
     * @ORM\OneToMany(targetEntity="LessonRolesEntity", mappedBy="Module", cascade={"remove"})
     */
    protected $LessonRoles;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lessonId.
     *
     * @param int|null $lessonId
     *
     * @return Module
     */
    public function setLessonId(PriceAndPackagesEntity $lessonId = null)
    {
        $this->lessonId = $lessonId;

        return $this;
    }

    /**
     * Get lessonId.
     *
     * @return int|null
     */
    public function getLessonId()
    {
        return $this->lessonId;
    }

    /**
     * Set moduleName.
     *
     * @param string|null $moduleName
     *
     * @return Module
     */
    public function setModuleName($moduleName = null)
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get moduleName.
     *
     * @return string|null
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->LessonRoles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessonRole.
     *
     * @param \DrivingSchool\AdminBundle\Entity\LessonRolesEntity $lessonRole
     *
     * @return ModuleEntity
     */
    public function addLessonRole(\DrivingSchool\AdminBundle\Entity\LessonRolesEntity $lessonRole)
    {
        $this->LessonRoles[] = $lessonRole;

        return $this;
    }

    /**
     * Remove lessonRole.
     *
     * @param \DrivingSchool\AdminBundle\Entity\LessonRolesEntity $lessonRole
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLessonRole(\DrivingSchool\AdminBundle\Entity\LessonRolesEntity $lessonRole)
    {
        return $this->LessonRoles->removeElement($lessonRole);
    }

    /**
     * Get lessonRoles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonRoles()
    {
        return $this->LessonRoles;
    }
}
