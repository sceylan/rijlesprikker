<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RolesModuleEntity
 *
 * @ORM\Table(name="roles_module")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\RolesModuleRepository")
 */
class RolesModuleEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="module_name", type="string", length=255, nullable=true)
     */
    private $moduleName;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moduleName.
     *
     * @param string|null $moduleName
     *
     * @return RolesModule
     */
    public function setModuleName($moduleName = null)
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get moduleName.
     *
     * @return string|null
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }
}
