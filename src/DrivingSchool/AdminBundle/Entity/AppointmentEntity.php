<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppointmentEntity
 *
 * @ORM\Table(name="appointment", indexes={@ORM\Index(name="drivingschool_id", columns={"drivingschool_id"}), @ORM\Index(name="vehicle_type", columns={"vehicle_type"})})
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\AppointmentRepository")
 */
class AppointmentEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="appointment")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="appointment")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=true)
     */
    private $Student;

    /**
     * @var int|null
     *
     * @ORM\Column(name="appointment_type", type="integer", options={"comment":"0 = Appointment, 1 = Reservation, 2 = Other"}, nullable=true)
     */
    private $appointmentType;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="VehicleEntity", inversedBy="appointment")
     * @ORM\JoinColumn(name="vehicle_type", referencedColumnName="id", nullable=true)
     */
    private $vehicleType;

     /**
     * @var int
     * @ORM\ManyToOne(targetEntity="InstructorEntity", inversedBy="appointment")
     * @ORM\JoinColumn(name="instructor_id", referencedColumnName="id", nullable=true)
     */
    private $instructor;

     /**
     * @var datetime|null
     *
     * @ORM\Column(name="length", type="text", length = 255, nullable=true)
     */
    private $length;

    /**
     * @var datetime|null
     *
     * @ORM\Column(name="preferabledatetime", type="datetime", nullable=true)
     */
    private $preferabledatetime;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", options={"comment":"0 = Pending, 1 = Approved, 2 = Rejected"})
     */
    private $status;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return Apointment
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set Student.
     *
     * @param int|null $Student
     *
     * @return Apointment
     */
    public function setStudent(StudentEntity $Student = null)
    {
        $this->Student = $Student;

        return $this;
    }

    /**
     * Get Student.
     *
     * @return int|null
     */
    public function getStudent()
    {
        return $this->Student;
    }

    /**
     * Set appointmentType.
     *
     * @param string|null $appointmentType
     *
     * @return Apointment
     */
    public function setAppointmentType($appointmentType = null)
    {
        $this->appointmentType = $appointmentType;

        return $this;
    }

    /**
     * Get appointmentType.
     *
     * @return string|null
     */
    public function getAppointmentType()
    {
        return $this->appointmentType;
    }

    /**
     * Set vehicleType.
     *
     * @param int|null $vehicleType
     *
     * @return Apointment
     */
    public function setVehicleType(VehicleEntity $vehicleType = null)
    {
        $this->vehicleType = $vehicleType;

        return $this;
    }

    /**
     * Get vehicleType.
     *
     * @return int|null
     */
    public function getVehicleType()
    {
        return $this->vehicleType;
    }

    /**
     * Set instructor.
     *
     * @param int|null $instructor
     *
     * @return Apointment
     */
    public function setInstructor(InstructorEntity $instructor = null)
    {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Set length.
     *
     * @param string|null $length
     *
     * @return Apointment
     */
    public function setLength($length = null)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length.
     *
     * @return string|null
     */
    public function getLength()
    {
        return $this->length;
    }


    /**
     * Set preferabledatetime.
     *
     * @param string|null $preferabledatetime
     *
     * @return Apointment
     */
    public function setPreferabledatetime($preferabledatetime = null)
    {
        $this->preferabledatetime = $preferabledatetime;

        return $this;
    }

    /**
     * Get preferabledatetime.
     *
     * @return string|null
     */
    public function getPreferabledatetime()
    {
        return $this->preferabledatetime;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return Apointment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}
