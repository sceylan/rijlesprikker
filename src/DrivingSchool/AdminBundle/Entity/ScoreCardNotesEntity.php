<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ScoreCardNotesEntity
 *
 * @ORM\Table(name="score_card_notes")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\ScoreCardNotesRepository")
 */
class ScoreCardNotesEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="ScoreCardNotes")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="ScoreCardNotes")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $student;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="LessonRolesEntity", inversedBy="ScoreCardNotes")
     * @ORM\JoinColumn(name="roles_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $lessonRoles;

    /**
     * @var int|null
     *
     * @ORM\Column(name="progressbar_value", type="integer", nullable=true)
     */
    private $progressbarValue;

    /**
     * @var text|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return ScoreCardNotesEntity
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set student.
     *
     * @param int|null $student
     *
     * @return ScoreCardNotesEntity
     */
    public function setStudent(StudentEntity $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return int|null
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set lessonRoles.
     *
     * @param int|null $lessonRoles
     *
     * @return ScoreCardNotesEntity
     */
    public function setLessonRoles(LessonRolesEntity $lessonRoles = null)
    {
        $this->lessonRoles = $lessonRoles;

        return $this;
    }

    /**
     * Get lessonRoles.
     *
     * @return int|null
     */
    public function getLessonRoles()
    {
        return $this->lessonRoles;
    }

    /**
     * Set progressbarValue.
     *
     * @param int|null $progressbarValue
     *
     * @return AssessmentMethod
     */
    public function setProgressbarValue($progressbarValue = null)
    {
        $this->progressbarValue = $progressbarValue;

        return $this;
    }

    /**
     * Get progressbarValue.
     *
     * @return int|null
     */
    public function getProgressbarValue()
    {
        return $this->progressbarValue;
    }

    /**
     * Set description.
     *
     * @param text|null $description
     *
     * @return AssessmentMethod
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return text|null
     */
    public function getDescription()
    {
        return $this->description;
    }
}
