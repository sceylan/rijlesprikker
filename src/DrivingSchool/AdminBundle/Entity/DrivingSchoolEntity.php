<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use FOS\UserBundle\Model\User as User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DrivingSchoolEntity
 *
 * @ORM\Table(name="driving_school")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\DrivingSchoolEntityRepository")
 * @UniqueEntity(fields={"username"}, message="Username already taken !")
 * @UniqueEntity(fields={"email"}, message="Email already registered with us !")
 */
class DrivingSchoolEntity extends User implements UserInterface, EquatableInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolName", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "School name should not be blank.")
     */
    private $schoolName;

    /**
     * @Assert\NotBlank(message = "Email should not be blank.")
     * @Assert\Email(
     *     message = "The email {{ value }} is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     * @Assert\Regex(pattern="/^[0-9\-\(\)\/\+\s]*$/", message="The number of boxes must be a number")
     */
    private $schoolPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolAddress", type="text", nullable=true)
     */
    private $schoolAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolContactPerson", type="string", length=255, nullable=true)
     */
    private $schoolContactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolWebsite", type="string", length=255, nullable=true)
     */
    private $schoolWebsite;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolCCNNumber", type="string", length=255, nullable=true)
     */
    private $schoolCCNNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolVatNumber", type="string", length=255, nullable=true)
     */
    private $schoolVatNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolBankActNumber", type="string", length=255, nullable=true)
     */
    private $schoolBankActNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="string", length=255, nullable=true)
     */
    private $bic;

    /**
     * @var string
     *
     * @ORM\Column(name="in_name_of", type="string", length=255, nullable=true)
     */
    private $inNameOf;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    private $logo;

    /**
     * @var int
     *
     * @ORM\Column(name="window_on_envelope", type="integer",  nullable=true, options={"comment":"0 = Window is on the left, 1 = Window is on the right"})
     */
    private $windowOnEnvelope;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_term", type="string", length=255, nullable=true)
     */
    private $paymentTerm;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_for_invoice", type="text", nullable=true)
     */
    private $footerForInvoice;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_with_direct_debit", type="text", nullable=true)
     */
    private $footerWithDirectDebit;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_1st_reminder", type="text", nullable=true)
     */
    private $footer1stReminder;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_2nd_memory", type="text", nullable=true)
     */
    private $footer2ndMemory;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_with_credit_invoice", type="text", nullable=true)
     */
    private $footerWithCreditInvoice;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_url", type="text", nullable=true)
     */
    private $facebookUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram_url", type="text", nullable=true)
     */
    private $instagramUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin_url", type="text", nullable=true)
     */
    private $linkedinUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_url", type="text", nullable=true)
     */
    private $twitterUrl;

    /** 
     * @var bool
     *
     * @ORM\Column(name="notification_off", type="boolean", nullable=true)
     */
    private $notificationOff;

    /** 
     * @var bool
     *
     * @ORM\Column(name="to_reserve", type="boolean", nullable=true)
     */
    private $toReserve;

    /** 
     * @var bool
     *
     * @ORM\Column(name="confirmation_required", type="boolean", nullable=true)
     */
    private $confirmationRequired;

    /** 
     * @var bool
     *
     * @ORM\Column(name="unknown_instructor", type="boolean", nullable=true)
     */
    private $unknownInstructor;

    /** 
     * @var int
     *
     * @ORM\Column(name="reservation_possible_from", type="integer", nullable=true)
     */
    private $reservationPossibleFrom;

    /** 
     * @var int
     *
     * @ORM\Column(name="reservation_possible_until", type="integer", nullable=true)
     */
    private $reservationPossibleUntil;

    /**
     * @ORM\ManyToOne(targetEntity="SubscriptionPlanEntity", inversedBy="drivingSchools", cascade={"persist"})
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $subscriptions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_date", type="datetime", nullable=true)
     */
    private $subscriptionDate;

    /**
     * @ORM\OneToMany(targetEntity="InstructorEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $instructor;

    /**
     * @ORM\OneToMany(targetEntity="InstructorGroupEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $instructorGroup;

    /**
     * @ORM\OneToMany(targetEntity="VehicleEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $vehicle;

    /**
     * @ORM\OneToMany(targetEntity="PriceAndPackagesEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $priceAndPackages;

    /**
     * @ORM\OneToMany(targetEntity="AssessmentMethodEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $AssessmentMethod;

    /**
     * @ORM\OneToMany(targetEntity="ScoreCardNotesEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $ScoreCardNotes;

    /**
     * @ORM\OneToMany(targetEntity="HolidayEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $Holiday;

    /**
     * @ORM\OneToMany(targetEntity="SchoolEmailContentEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $SchoolEmailContent;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $studentFinancial;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialHistoryEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $studentFinancialHistory;

    /**
     * @ORM\OneToMany(targetEntity="StudentEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $StudentEntity;

    /**
     * @ORM\OneToMany(targetEntity="InvoiceEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $invoice;

    /**
     * @ORM\OneToMany(targetEntity="StudentNotesEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $studentNotes;

    /**
     * @ORM\OneToMany(targetEntity="TrainingSessionsEntity", mappedBy="DrivingSchool", cascade={"remove"})
     */
    protected $trainingSessions;

    /**
     * @ORM\OneToMany(targetEntity="PlanningEntity", mappedBy="school", cascade={"remove"})
     */
    protected $planning;

    /**
     * @ORM\OneToMany(targetEntity="NotificationEntity", mappedBy="school", cascade={"remove"})
     */
    protected $notification;

    /**
     * @var array
     */
    protected $roles;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     */
    protected $password;
    
    /**
     * The salt to use for hashing.
     *
     * @var string
     */
    protected $salt;

    /**
     * @var string
     */
    protected $username;


    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;


    public function __construct() {
        parent::__construct();
        $this->roles = array();
        $this->enabled = true;
        $this->setSchoolAdmin(true);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUsername();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set schoolName.
     *
     * @param string $schoolName
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolName($schoolName)
    {
        $this->schoolName = $schoolName;

        return $this;
    }

    /**
     * Get schoolName.
     *
     * @return string
     */
    public function getSchoolName()
    {
        return $this->schoolName;
    }

    /**
     * Set schoolPhone.
     *
     * @param string $schoolPhone
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolPhone($schoolPhone)
    {
        $this->schoolPhone = $schoolPhone;

        return $this;
    }

    /**
     * Get schoolPhone.
     *
     * @return string
     */
    public function getSchoolPhone()
    {
        return $this->schoolPhone;
    }

    /**
     * Set schoolAddress.
     *
     * @param string $schoolAddress
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolAddress($schoolAddress)
    {
        $this->schoolAddress = $schoolAddress;

        return $this;
    }

    /**
     * Get schoolAddress.
     *
     * @return string
     */
    public function getSchoolAddress()
    {
        return $this->schoolAddress;
    }

    /**
     * Set schoolContactPerson.
     *
     * @param string $schoolContactPerson
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolContactPerson($schoolContactPerson)
    {
        $this->schoolContactPerson = $schoolContactPerson;

        return $this;
    }

    /**
     * Get schoolContactPerson.
     *
     * @return string
     */
    public function getSchoolContactPerson()
    {
        return $this->schoolContactPerson;
    }

    /**
     * Set schoolWebsite.
     *
     * @param string $schoolWebsite
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolWebsite($schoolWebsite)
    {
        $this->schoolWebsite = $schoolWebsite;

        return $this;
    }

    /**
     * Get schoolWebsite.
     *
     * @return string
     */
    public function getSchoolWebsite()
    {
        return $this->schoolWebsite;
    }

    /**
     * Set schoolCCNNumber.
     *
     * @param string $schoolCCNNumber
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolCCNNumber($schoolCCNNumber)
    {
        $this->schoolCCNNumber = $schoolCCNNumber;

        return $this;
    }

    /**
     * Get schoolCCNNumber.
     *
     * @return string
     */
    public function getSchoolCCNNumber()
    {
        return $this->schoolCCNNumber;
    }

    /**
     * Set schoolVatNumber.
     *
     * @param string $schoolVatNumber
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolVatNumber($schoolVatNumber)
    {
        $this->schoolVatNumber = $schoolVatNumber;

        return $this;
    }

    /**
     * Get schoolVatNumber.
     *
     * @return string
     */
    public function getSchoolVatNumber()
    {
        return $this->schoolVatNumber;
    }

    /**
     * Set schoolBankActNumber.
     *
     * @param string $schoolBankActNumber
     *
     * @return DrivingSchoolEntity
     */
    public function setSchoolBankActNumber($schoolBankActNumber)
    {
        $this->schoolBankActNumber = $schoolBankActNumber;

        return $this;
    }

    /**
     * Get schoolBankActNumber.
     *
     * @return string
     */
    public function getSchoolBankActNumber()
    {
        return $this->schoolBankActNumber;
    }

    /**
     * Set bic.
     *
     * @param string $bic
     *
     * @return DrivingSchoolEntity
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bic.
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set inNameOf.
     *
     * @param string $inNameOf
     *
     * @return DrivingSchoolEntity
     */
    public function setInNameOf($inNameOf)
    {
        $this->inNameOf = $inNameOf;

        return $this;
    }

    /**
     * Get inNameOf.
     *
     * @return string
     */
    public function getInNameOf()
    {
        return $this->inNameOf;
    }

    /**
     * Set logo.
     *
     * @param string|null $logo
     *
     * @return Banner
     */
    public function setLogo($logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return string|null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set windowOnEnvelope.
     *
     * @param int $windowOnEnvelope
     *
     * @return DrivingSchoolEntity
     */
    public function setWindowOnEnvelope($windowOnEnvelope)
    {
        $this->windowOnEnvelope = $windowOnEnvelope;

        return $this;
    }

    /**
     * Get windowOnEnvelope.
     *
     * @return int
     */
    public function getWindowOnEnvelope()
    {
        return $this->windowOnEnvelope;
    }

    /**
     * Set paymentTerm.
     *
     * @param string $paymentTerm
     *
     * @return DrivingSchoolEntity
     */
    public function setPaymentTerm($paymentTerm)
    {
        $this->paymentTerm = $paymentTerm;

        return $this;
    }

    /**
     * Get paymentTerm.
     *
     * @return string
     */
    public function getPaymentTerm()
    {
        return $this->paymentTerm;
    }

    /**
     * Set footerForInvoice.
     *
     * @param string $footerForInvoice
     *
     * @return DrivingSchoolEntity
     */
    public function setFooterForInvoice($footerForInvoice)
    {
        $this->footerForInvoice = $footerForInvoice;

        return $this;
    }

    /**
     * Get footerForInvoice.
     *
     * @return string
     */
    public function getFooterForInvoice()
    {
        return $this->footerForInvoice;
    }

    /**
     * Set footerWithDirectDebit.
     *
     * @param string $footerWithDirectDebit
     *
     * @return DrivingSchoolEntity
     */
    public function setFooterWithDirectDebit($footerWithDirectDebit)
    {
        $this->footerWithDirectDebit = $footerWithDirectDebit;

        return $this;
    }

    /**
     * Get footerWithDirectDebit.
     *
     * @return string
     */
    public function getFooterWithDirectDebit()
    {
        return $this->footerWithDirectDebit;
    }

    /**
     * Set footer1stReminder.
     *
     * @param string $footer1stReminder
     *
     * @return DrivingSchoolEntity
     */
    public function setFooter1stReminder($footer1stReminder)
    {
        $this->footer1stReminder = $footer1stReminder;

        return $this;
    }

    /**
     * Get footer1stReminder.
     *
     * @return string
     */
    public function getFooter1stReminder()
    {
        return $this->footer1stReminder;
    }

    /**
     * Set footer2ndMemory.
     *
     * @param string $footer2ndMemory
     *
     * @return DrivingSchoolEntity
     */
    public function setFooter2ndMemory($footer2ndMemory)
    {
        $this->footer2ndMemory = $footer2ndMemory;

        return $this;
    }

    /**
     * Get footer2ndMemory.
     *
     * @return string
     */
    public function getFooter2ndMemory()
    {
        return $this->footer2ndMemory;
    }

    /**
     * Set footerWithCreditInvoice.
     *
     * @param string $footerWithCreditInvoice
     *
     * @return DrivingSchoolEntity
     */
    public function setFooterWithCreditInvoice($footerWithCreditInvoice)
    {
        $this->footerWithCreditInvoice = $footerWithCreditInvoice;

        return $this;
    }

    /**
     * Get footerWithCreditInvoice.
     *
     * @return string
     */
    public function getFooterWithCreditInvoice()
    {
        return $this->footerWithCreditInvoice;
    }


    /**
     * Set facebookUrl.
     *
     * @param string $facebookUrl
     *
     * @return DrivingSchoolEntity
     */
    public function setFacebookUrl($facebookUrl)
    {
        $this->facebookUrl = $facebookUrl;

        return $this;
    }

    /**
     * Get facebookUrl.
     *
     * @return string
     */
    public function getFacebookUrl()
    {
        return $this->facebookUrl;
    }


    /**
     * Set instagramUrl.
     *
     * @param string $instagramUrl
     *
     * @return DrivingSchoolEntity
     */
    public function setInstagramUrl($instagramUrl)
    {
        $this->instagramUrl = $instagramUrl;

        return $this;
    }

    /**
     * Get instagramUrl.
     *
     * @return string
     */
    public function getInstagramUrl()
    {
        return $this->instagramUrl;
    }


    /**
     * Set linkedinUrl.
     *
     * @param string $linkedinUrl
     *
     * @return DrivingSchoolEntity
     */
    public function setLinkedinUrl($linkedinUrl)
    {
        $this->linkedinUrl = $linkedinUrl;

        return $this;
    }

    /**
     * Get linkedinUrl.
     *
     * @return string
     */
    public function getLinkedinUrl()
    {
        return $this->linkedinUrl;
    }


    /**
     * Set twitterUrl.
     *
     * @param string $twitterUrl
     *
     * @return DrivingSchoolEntity
     */
    public function setTwitterUrl($twitterUrl)
    {
        $this->twitterUrl = $twitterUrl;

        return $this;
    }

    /**
     * Get twitterUrl.
     *
     * @return string
     */
    public function getTwitterUrl()
    {
        return $this->twitterUrl;
    }

    /**
     * Set notificationOff.
     *
     * @param bool $notificationOff
     *
     * @return DrivingSchoolEntity
     */
    public function setNotificationOff($notificationOff)
    {
        $this->notificationOff = $notificationOff;

        return $this;
    }

    /**
     * Get notificationOff.
     *
     * @return bool
     */
    public function getNotificationOff()
    {
        return $this->notificationOff;
    }

    /**
     * Set toReserve.
     *
     * @param bool $toReserve
     *
     * @return DrivingSchoolEntity
     */
    public function setToReserve($toReserve)
    {
        $this->toReserve = $toReserve;

        return $this;
    }

    /**
     * Get toReserve.
     *
     * @return bool
     */
    public function getToReserve()
    {
        return $this->toReserve;
    }

    /**
     * Set confirmationRequired.
     *
     * @param bool $confirmationRequired
     *
     * @return DrivingSchoolEntity
     */
    public function setConfirmationRequired($confirmationRequired)
    {
        $this->confirmationRequired = $confirmationRequired;

        return $this;
    }

    /**
     * Get confirmationRequired.
     *
     * @return bool
     */
    public function getConfirmationRequired()
    {
        return $this->confirmationRequired;
    }

    /**
     * Set unknownInstructor.
     *
     * @param bool $unknownInstructor
     *
     * @return DrivingSchoolEntity
     */
    public function setUnknownInstructor($unknownInstructor)
    {
        $this->unknownInstructor = $unknownInstructor;

        return $this;
    }

    /**
     * Get unknownInstructor.
     *
     * @return bool
     */
    public function getUnknownInstructor()
    {
        return $this->unknownInstructor;
    }

    /**
     * Set reservationPossibleFrom.
     *
     * @param int $reservationPossibleFrom
     *
     * @return DrivingSchoolEntity
     */
    public function setReservationPossibleFrom($reservationPossibleFrom)
    {
        $this->reservationPossibleFrom = $reservationPossibleFrom;

        return $this;
    }

    /**
     * Get reservationPossibleFrom.
     *
     * @return int
     */
    public function getReservationPossibleFrom()
    {
        return $this->reservationPossibleFrom;
    }

    /**
     * Set reservationPossibleUntil.
     *
     * @param int $reservationPossibleUntil
     *
     * @return DrivingSchoolEntity
     */
    public function setReservationPossibleUntil($reservationPossibleUntil)
    {
        $this->reservationPossibleUntil = $reservationPossibleUntil;

        return $this;
    }

    /**
     * Get reservationPossibleUntil.
     *
     * @return int
     */
    public function getReservationPossibleUntil()
    {
        return $this->reservationPossibleUntil;
    }

    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    public function setSubscriptions(SubscriptionPlanEntity $subscription)
    {
        $this->subscriptions = $subscription;
        return $this;
    }

    /**
     * Add subscription.
     *
     * @param \DrivingSchool\AdminBundle\Entity\SubscriptionPlanEntity $subscription
     *
     * @return DrivingSchoolEntity
     */
    public function addSubscription(\DrivingSchool\AdminBundle\Entity\SubscriptionPlanEntity $subscription)
    {
        $this->subscriptions[] = $subscription;

        return $this;
    }

    /**
     * Remove subscription.
     *
     * @param \DrivingSchool\AdminBundle\Entity\SubscriptionPlanEntity $subscription
     */
    public function removeSubscription(\DrivingSchool\AdminBundle\Entity\SubscriptionPlanEntity $subscription)
    {
        $this->subscriptions->removeElement($subscription);
    }

    /**
     * Set subscriptionDate.
     *
     * @param \DateTime $subscriptionDate
     *
     * @return DrivingSchoolEntity
     */
    public function setSubscriptionDate($subscriptionDate)
    {
        $this->subscriptionDate = $subscriptionDate;

        return $this;
    }

    /**
     * Get subscriptionDate.
     *
     * @return \DateTime
     */
    public function getSubscriptionDate()
    {
        return $this->subscriptionDate;
    }

    // Remove appointment table DrivingSchool
    public function remove($appointment, $instructor, $vehicle, $priceAndPackages)
    {
        $this->getAppointment()->remove($appointment);
        $this->getInstructor()->remove($instructor);
        $this->getVehicle()->remove($vehicle);
        $this->getPriceAndPackages()->remove($priceAndPackages);
    }


    /**
     * {@inheritdoc}
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * {@inheritdoc}
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSchoolAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole("ROLE_SCHOOL");
        } else {
            $this->removeRole("ROLE_SCHOOL");
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        $roles = $this->roles;
        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }
        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    

    /**
     * {@inheritdoc}
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;

        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        if (13 === count($data)) {
            // Unserializing a User object from 1.3.x
            unset($data[4], $data[5], $data[6], $data[9], $data[10]);
            $data = array_values($data);
        } elseif (11 === count($data)) {
            // Unserializing a User from a dev version somewhere between 2.0-alpha3 and 2.0-beta1
            unset($data[4], $data[7], $data[8]);
            $data = array_values($data);
        }

        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical
        ) = $data;
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof DrivingSchoolEntity) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    /**
     * Add instructor.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor
     *
     * @return DrivingSchoolEntity
     */
    public function addInstructor(\DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor)
    {
        $this->instructor[] = $instructor;

        return $this;
    }

    /**
     * Remove instructor.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInstructor(\DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor)
    {
        return $this->instructor->removeElement($instructor);
    }

    /**
     * Get instructor.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Add instructorGroup.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup
     *
     * @return DrivingSchoolEntity
     */
    public function addInstructorGroup(\DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup)
    {
        $this->instructorGroup[] = $instructorGroup;

        return $this;
    }

    /**
     * Remove instructorGroup.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInstructorGroup(\DrivingSchool\AdminBundle\Entity\InstructorGroupEntity $instructorGroup)
    {
        return $this->instructorGroup->removeElement($instructorGroup);
    }

    /**
     * Get instructorGroup.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstructorGroup()
    {
        return $this->instructorGroup;
    }

    /**
     * Add vehicle.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle
     *
     * @return DrivingSchoolEntity
     */
    public function addVehicle(\DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle)
    {
        $this->vehicle[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeVehicle(\DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle)
    {
        return $this->vehicle->removeElement($vehicle);
    }

    /**
     * Get vehicle.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Add priceAndPackage.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage
     *
     * @return DrivingSchoolEntity
     */
    public function addPriceAndPackage(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage)
    {
        $this->priceAndPackages[] = $priceAndPackage;

        return $this;
    }

    /**
     * Remove priceAndPackage.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePriceAndPackage(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage)
    {
        return $this->priceAndPackages->removeElement($priceAndPackage);
    }

    /**
     * Get priceAndPackages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriceAndPackages()
    {
        return $this->priceAndPackages;
    }

    /**
     * Add assessmentMethod.
     *
     * @param \DrivingSchool\AdminBundle\Entity\AssessmentMethodEntity $assessmentMethod
     *
     * @return DrivingSchoolEntity
     */
    public function addAssessmentMethod(\DrivingSchool\AdminBundle\Entity\AssessmentMethodEntity $assessmentMethod)
    {
        $this->AssessmentMethod[] = $assessmentMethod;

        return $this;
    }

    /**
     * Remove assessmentMethod.
     *
     * @param \DrivingSchool\AdminBundle\Entity\AssessmentMethodEntity $assessmentMethod
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAssessmentMethod(\DrivingSchool\AdminBundle\Entity\AssessmentMethodEntity $assessmentMethod)
    {
        return $this->AssessmentMethod->removeElement($assessmentMethod);
    }

    /**
     * Get assessmentMethod.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssessmentMethod()
    {
        return $this->AssessmentMethod;
    }

    /**
     * Add scoreCardNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote
     *
     * @return DrivingSchoolEntity
     */
    public function addScoreCardNote(\DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote)
    {
        $this->ScoreCardNotes[] = $scoreCardNote;

        return $this;
    }

    /**
     * Remove scoreCardNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeScoreCardNote(\DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote)
    {
        return $this->ScoreCardNotes->removeElement($scoreCardNote);
    }

    /**
     * Get scoreCardNotes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScoreCardNotes()
    {
        return $this->ScoreCardNotes;
    }

    /**
     * Add holiday.
     *
     * @param \DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday
     *
     * @return DrivingSchoolEntity
     */
    public function addHoliday(\DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday)
    {
        $this->Holiday[] = $holiday;

        return $this;
    }

    /**
     * Remove holiday.
     *
     * @param \DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeHoliday(\DrivingSchool\AdminBundle\Entity\HolidayEntity $holiday)
    {
        return $this->Holiday->removeElement($holiday);
    }

    /**
     * Get holiday.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHoliday()
    {
        return $this->Holiday;
    }

    /**
     * Add schoolEmailContent.
     *
     * @param \DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent
     *
     * @return DrivingSchoolEntity
     */
    public function addSchoolEmailContent(\DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent)
    {
        $this->SchoolEmailContent[] = $schoolEmailContent;

        return $this;
    }

    /**
     * Remove schoolEmailContent.
     *
     * @param \DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSchoolEmailContent(\DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent)
    {
        return $this->SchoolEmailContent->removeElement($schoolEmailContent);
    }

    /**
     * Get schoolEmailContent.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchoolEmailContent()
    {
        return $this->SchoolEmailContent;
    }

    /**
     * Add studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return DrivingSchoolEntity
     */
    public function addStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        $this->studentFinancial[] = $studentFinancial;

        return $this;
    }

    /**
     * Remove studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        return $this->studentFinancial->removeElement($studentFinancial);
    }

    /**
     * Get studentFinancial.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancial()
    {
        return $this->studentFinancial;
    }

    /**
     * Add studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return DrivingSchoolEntity
     */
    public function addStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        $this->studentFinancialHistory[] = $studentFinancialHistory;

        return $this;
    }

    /**
     * Remove studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        return $this->studentFinancialHistory->removeElement($studentFinancialHistory);
    }

    /**
     * Get studentFinancialHistory.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancialHistory()
    {
        return $this->studentFinancialHistory;
    }

    /**
     * Add invoice.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice
     *
     * @return DrivingSchoolEntity
     */
    public function addInvoice(\DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice)
    {
        $this->invoice[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInvoice(\DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice)
    {
        return $this->invoice->removeElement($invoice);
    }

    /**
     * Get invoice.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Add studentNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote
     *
     * @return DrivingSchoolEntity
     */
    public function addStudentNote(\DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote)
    {
        $this->studentNotes[] = $studentNote;

        return $this;
    }

    /**
     * Remove studentNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentNote(\DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote)
    {
        return $this->studentNotes->removeElement($studentNote);
    }

    /**
     * Get studentNotes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentNotes()
    {
        return $this->studentNotes;
    }

    /**
     * Add trainingSession.
     *
     * @param \DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession
     *
     * @return DrivingSchoolEntity
     */
    public function addTrainingSession(\DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession)
    {
        $this->trainingSessions[] = $trainingSession;

        return $this;
    }

    /**
     * Remove trainingSession.
     *
     * @param \DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrainingSession(\DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession)
    {
        return $this->trainingSessions->removeElement($trainingSession);
    }

    /**
     * Get trainingSessions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingSessions()
    {
        return $this->trainingSessions;
    }

    /**
     * Add planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return DrivingSchoolEntity
     */
    public function addPlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        $this->planning[] = $planning;

        return $this;
    }

    /**
     * Remove planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        return $this->planning->removeElement($planning);
    }

    /**
     * Get planning.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanning()
    {
        return $this->planning;
    }

    /**
     * Add notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return DrivingSchoolEntity
     */
    public function addNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        $this->notification[] = $notification;

        return $this;
    }

    /**
     * Remove notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        return $this->notification->removeElement($notification);
    }

    /**
     * Get notification.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Add student.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $student
     *
     * @return DrivingSchoolEntity
     */
    public function addStudent(\DrivingSchool\AdminBundle\Entity\StudentEntity $student)
    {
        $this->student[] = $student;

        return $this;
    }

    /**
     * Remove student.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $student
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudent(\DrivingSchool\AdminBundle\Entity\StudentEntity $student)
    {
        return $this->student->removeElement($student);
    }

    /**
     * Get student.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Add studentEntity.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity
     *
     * @return DrivingSchoolEntity
     */
    public function addStudentEntity(\DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity)
    {
        $this->StudentEntity[] = $studentEntity;

        return $this;
    }

    /**
     * Remove studentEntity.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentEntity(\DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity)
    {
        return $this->StudentEntity->removeElement($studentEntity);
    }

    /**
     * Get studentEntity.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentEntity()
    {
        return $this->StudentEntity;
    }
}
