<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BannerEntity
 *
 * @ORM\Table(name="banner")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\BannerRepository")
 */
class BannerEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="banner_title", type="string", length=255, nullable=true)
     */
    private $bannerTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="banner_description", type="text", nullable=true)
     */
    private $bannerDescription;


    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    private $bannerImage;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bannerTitle.
     *
     * @param string|null $bannerTitle
     *
     * @return Banner
     */
    public function setBannerTitle($bannerTitle = null)
    {
        $this->bannerTitle = $bannerTitle;

        return $this;
    }

    /**
     * Get bannerTitle.
     *
     * @return string|null
     */
    public function getBannerTitle()
    {
        return $this->bannerTitle;
    }

    /**
     * Set bannerDescription.
     *
     * @param string|null $bannerDescription
     *
     * @return Banner
     */
    public function setBannerDescription($bannerDescription = null)
    {
        $this->bannerDescription = $bannerDescription;

        return $this;
    }

    /**
     * Get bannerDescription.
     *
     * @return string|null
     */
    public function getBannerDescription()
    {
        return $this->bannerDescription;
    }

    /**
     * Set bannerImage.
     *
     * @param string|null $bannerImage
     *
     * @return Banner
     */
    public function setBannerImage($bannerImage = null)
    {
        $this->bannerImage = $bannerImage;

        return $this;
    }

    /**
     * Get bannerImage.
     *
     * @return string|null
     */
    public function getBannerImage()
    {
        return $this->bannerImage;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Banner
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
}
