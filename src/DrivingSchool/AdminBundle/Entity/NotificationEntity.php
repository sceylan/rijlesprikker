<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * NotificationEntity
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\NotificationRepository")
 */
class NotificationEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="notification")
     * @ORM\JoinColumn(name="event_school_id", referencedColumnName="id", nullable=true)
     */
    private $school;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="notification")
     * @ORM\JoinColumn(name="event_student_id", referencedColumnName="id", nullable=true)
     */
    private $student;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="InstructorEntity", inversedBy="notification")
     * @ORM\JoinColumn(name="event_instructor_id", referencedColumnName="id", nullable=true)
     */
    private $instructor;

    /**
     * @var string
     *
     * @ORM\Column(name="package_name", type="string", length=255)
     */
    private $packageName; 

    /**
     * @var string
     *
     * @ORM\Column(name="training_session_name", type="string", length=255)
     */
    private $trainingSessionName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_startdate", type="datetime")
     */
    private $eventstartdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_enddate", type="datetime")
     */
    private $eventenddate;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="UserTypeEntity", inversedBy="notification")
     * @ORM\JoinColumn(name="user_type", referencedColumnName="id", nullable=true)
     */
    private $userType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="action", type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @var int
     *
     * @ORM\Column(name="school_read_status", type="integer")
     */
    private $schoolReadStatus;

    /**
     * @var int
     *
     * @ORM\Column(name="instructor_read_status", type="integer")
     */
    private $instructorReadStatus;

    /**
     * @var int
     *
     * @ORM\Column(name="student_read_status", type="integer")
     */
    private $studentReadStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notification_type", type="string", length=255, nullable=true)
     */
    private $notificationType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    public function __construct()
    {
        $this->creation = new \DateTime();
        $this->modified = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set school.
     *
     * @param int|null $school
     *
     * @return DrivingSchoolEntity
     */
    public function setSchool(DrivingSchoolEntity $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school.
     *
     * @return int|null
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Add school.
     *
     * @param \DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school
     *
     * @return NotificationEntity
     */
    public function addSchool(\DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school)
    {
        $this->school[] = $school;

        return $this;
    }

    /**
     * Remove school.
     *
     * @param \DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school
     */
    public function removeSchool(\DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school)
    {
        $this->school->removeElement($school);
    }

    /**
     * Set student.
     *
     * @param int|null $student
     *
     * @return StudentEntity
     */
    public function setStudent(StudentEntity $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return int|null
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Add student.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $student
     *
     * @return NotificationEntity
     */
    public function addStudent(\DrivingSchool\AdminBundle\Entity\StudentEntity $student)
    {
        $this->student[] = $student;

        return $this;
    }

    /**
     * Remove student.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $student
     */
    public function removeStudent(\DrivingSchool\AdminBundle\Entity\StudentEntity $student)
    {
        $this->student->removeElement($student);
    }

    /**
     * Set instructor.
     *
     * @param int|null $instructor
     *
     * @return NotificationEntity
     */
    public function setInstructor(InstructorEntity $instructor = null)
    {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Add instructor.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor
     *
     * @return NotificationEntity
     */
    public function addInstructor(\DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor)
    {
        $this->instructor[] = $instructor;

        return $this;
    }

    /**
     * Remove instructor.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor
     */
    public function removeInstructor(\DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor)
    {
        $this->instructor->removeElement($instructor);
    }

    /**
     * Set packageName.
     *
     * @param string $packageName
     *
     * @return NotificationEntity
     */
    public function setPackageName($packageName)
    {
        $this->packageName = $packageName;

        return $this;
    }

    /**
     * Get packageName.
     *
     * @return string
     */
    public function getPackageName()
    {
        return $this->packageName;
    }

    /**
     * Set trainingSessionName.
     *
     * @param string $trainingSessionName
     *
     * @return NotificationEntity
     */
    public function setTrainingSessionName($trainingSessionName)
    {
        $this->trainingSessionName = $trainingSessionName;

        return $this;
    }

    /**
     * Get trainingSessionName.
     *
     * @return string
     */
    public function getTrainingSessionName()
    {
        return $this->trainingSessionName;
    }

    /**
     * Set eventstartdate.
     *
     * @param \DateTime $eventstartdate
     *
     * @return NotificationEntity
     */
    public function setEventStartDate($eventstartdate)
    {
        $this->eventstartdate = $eventstartdate;

        return $this;
    }

    /**
     * Get eventstartdate.
     *
     * @return \DateTime
     */
    public function getEventStartDate()
    {
        return $this->eventstartdate;
    }

    /**
     * Set eventenddate.
     *
     * @param \DateTime $eventenddate
     *
     * @return NotificationEntity
     */
    public function setEventEndDate($eventenddate)
    {
        $this->eventenddate = $eventenddate;

        return $this;
    }

    /**
     * Get eventenddate.
     *
     * @return \DateTime
     */
    public function getEventEndDate()
    {
        return $this->eventenddate;
    }
    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return NotificationEntity
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userType.
     *
     * @param int|null $userType
     *
     * @return NotificationEntity
     */
    public function setUserType(UserTypeEntity $userType = null)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType.
     *
     * @return int|null
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set action.
     *
     * @param string|null $action
     *
     * @return NotificationEntity
     */
    public function setAction($action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set schoolReadStatus.
     *
     * @param int $schoolReadStatus
     *
     * @return NotificationEntity
     */
    public function setSchoolReadStatus($schoolReadStatus)
    {
        $this->schoolReadStatus = $schoolReadStatus;

        return $this;
    }

    /**
     * Get schoolReadStatus.
     *
     * @return int
     */
    public function getSchoolReadStatus()
    {
        return $this->schoolReadStatus;
    }

    /**
     * Set instructorReadStatus.
     *
     * @param int $instructorReadStatus
     *
     * @return NotificationEntity
     */
    public function setInstructorReadStatus($instructorReadStatus)
    {
        $this->instructorReadStatus = $instructorReadStatus;

        return $this;
    }

    /**
     * Get instructorReadStatus.
     *
     * @return int
     */
    public function getInstructorReadStatus()
    {
        return $this->instructorReadStatus;
    }

    /**
     * Set studentReadStatus.
     *
     * @param int $studentReadStatus
     *
     * @return NotificationEntity
     */
    public function setStudentReadStatus($studentReadStatus)
    {
        $this->studentReadStatus = $studentReadStatus;

        return $this;
    }

    /**
     * Get studentReadStatus.
     *
     * @return int
     */
    public function getStudentReadStatus()
    {
        return $this->studentReadStatus;
    }

    /**
     * Set notificationType.
     *
     * @param string $notificationType
     *
     * @return NotificationEntity
     */
    public function setNotificationType($notificationType)
    {
        $this->notificationType = $notificationType;

        return $this;
    }

    /**
     * Get notificationType.
     *
     * @return string
     */
    public function getNotificationType()
    {
        return $this->notificationType;
    }

    /**
     * Set creation.
     *
     * @param \DateTime $creation
     *
     * @return NotificationEntity
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation.
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return NotificationEntity
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }
}
