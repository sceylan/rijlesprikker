<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColorEntity
 *
 * @ORM\Table(name="color")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\ColorRepository")
 */
class ColorEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="color_code", type="string", length=255)
     */
    private $colorCode;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colorCode.
     *
     * @param string $colorCode
     *
     * @return color
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;

        return $this;
    }

    /**
     * Get colorCode.
     *
     * @return string
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }
}
