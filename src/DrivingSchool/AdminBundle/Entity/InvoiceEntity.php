<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceEntity
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\InvoiceRepository")
 */
class InvoiceEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="invoice")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="invoice")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=true)
     */
    private $studentid;
    
    /**
     * @var int
     *
     * @ORM\Column(name="invoice_number", type="integer", nullable=true)
     */
    private $invoiceNumber;

    /**
     * @var text|null
     *
     * @ORM\Column(name="invoice_detail", type="text", nullable=true)
     */
    private $invoiceDetail;

    /**
     * @var text|null
     *
     * @ORM\Column(name="invoice_content", type="text", nullable=true)
     */
    private $invoiceContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_total", type="string", length=255, nullable=true)
     */
    private $invoiceTotal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return Invoice
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set studentid.
     *
     * @param int|null $studentid
     *
     * @return Invoice
     */
    public function setStudentid(StudentEntity $studentid = null)
    {
        $this->studentid = $studentid;

        return $this;
    }

    /**
     * Get studentid.
     *
     * @return int|null
     */
    public function getStudentid()
    {
        return $this->studentid;
    }

    /**
     * Set invoiceNumber.
     *
     * @param int|null $invoiceNumber
     *
     * @return Invoice
     */
    public function setInvoiceNumber($invoiceNumber = null)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * Get invoiceNumber.
     *
     * @return int|null
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set invoiceDetail.
     *
     * @param text|null $invoiceDetail
     *
     * @return Invoice
     */
    public function setInvoiceDetail($invoiceDetail = null)
    {
        $this->invoiceDetail = $invoiceDetail;

        return $this;
    }

    /**
     * Get invoiceDetail.
     *
     * @return int|null
     */
    public function getInvoiceDetail()
    {
        return $this->invoiceDetail;
    }

    /**
     * Set invoiceContent.
     *
     * @param text|null $invoiceContent
     *
     * @return Invoice
     */
    public function setInvoiceContent($invoiceContent = null)
    {
        $this->invoiceContent = $invoiceContent;

        return $this;
    }

    /**
     * Get invoiceContent.
     *
     * @return int|null
     */
    public function getInvoiceContent()
    {
        return $this->invoiceContent;
    }

    /**
     * Set invoiceTotal.
     *
     * @param string|null $invoiceTotal
     *
     * @return Invoice
     */
    public function setInvoiceTotal($invoiceTotal = null)
    {
        $this->invoiceTotal = $invoiceTotal;

        return $this;
    }

    /**
     * Get invoiceTotal.
     *
     * @return int|null
     */
    public function getInvoiceTotal()
    {
        return $this->invoiceTotal;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Invoice
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this->created;
    }

    /**
     * Get created.
     *
     * @return \DateTime|null
     */
    public function getCreated()
    {
        return $this->created;
    }
}
