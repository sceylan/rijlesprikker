<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogEntity
 *
 * @ORM\Table(name="blog")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\BlogRepository")
 */
class BlogEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="blog_name", type="string", length=255, nullable=true)
     */
    private $blogName;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    private $blogImage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="blog_description", type="text", nullable=true)
     */
    private $blogDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set blogName.
     *
     * @param string|null $blogName
     *
     * @return Blog
     */
    public function setBlogName($blogName = null)
    {
        $this->blogName = $blogName;

        return $this;
    }

    /**
     * Get blogName.
     *
     * @return string|null
     */
    public function getBlogName()
    {
        return $this->blogName;
    }

    /**
     * Set blogImage.
     *
     * @param string|null $blogImage
     *
     * @return Blog
     */
    public function setBlogImage($blogImage = null)
    {
        $this->blogImage = $blogImage;

        return $this;
    }

    /**
     * Get blogImage.
     *
     * @return string|null
     */
    public function getBlogImage()
    {
        return $this->blogImage;
    }

    /**
     * Set blogDescription.
     *
     * @param string|null $blogDescription
     *
     * @return Blog
     */
    public function setBlogDescription($blogDescription = null)
    {
        $this->blogDescription = $blogDescription;

        return $this;
    }

    /**
     * Get blogDescription.
     *
     * @return string|null
     */
    public function getBlogDescription()
    {
        return $this->blogDescription;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return parentCategory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }


}
