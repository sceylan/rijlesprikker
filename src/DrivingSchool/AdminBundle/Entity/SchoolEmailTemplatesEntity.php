<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SchoolEmailTemplatesEntity
 *
 * @ORM\Table(name="school_email_templates")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\SchoolEmailTemplatesRepository")
 */
class SchoolEmailTemplatesEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string|null
     *
     * @ORM\Column(name="top_content", type="text", nullable=true)
     */
    private $topContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bottom_content", type="text", nullable=true)
     */
    private $bottomContent;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

     /**
     * @ORM\OneToMany(targetEntity="SchoolEmailContentEntity", mappedBy="template", cascade={"remove"})
     */
    protected $SchoolEmailContent;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return EmailTemplates
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subject.
     *
     * @param string|null $subject
     *
     * @return EmailTemplates
     */
    public function setSubject($subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string|null
     */
    public function getSubject()
    {
        return $this->subject;
    }  

    /**
     * Set topContent.
     *
     * @param string|null $topContent
     *
     * @return EmailTemplates
     */
    public function setTopContent($topContent = null)
    {
        $this->topContent = $topContent;

        return $this;
    }

    /**
     * Get topContent.
     *
     * @return string|null
     */
    public function getTopContent()
    {
        return $this->topContent;
    }

    /**
     * Set bottomContent.
     *
     * @param string|null $bottomContent
     *
     * @return EmailTemplates
     */
    public function setBottomContent($bottomContent = null)
    {
        $this->bottomContent = $bottomContent;

        return $this;
    }

    /**
     * Get bottomContent.
     *
     * @return string|null
     */
    public function getBottomContent()
    {
        return $this->bottomContent;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Vehicle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->SchoolEmailContent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add schoolEmailContent.
     *
     * @param \DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent
     *
     * @return SchoolEmailTemplatesEntity
     */
    public function addSchoolEmailContent(\DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent)
    {
        $this->SchoolEmailContent[] = $schoolEmailContent;

        return $this;
    }

    /**
     * Remove schoolEmailContent.
     *
     * @param \DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSchoolEmailContent(\DrivingSchool\AdminBundle\Entity\SchoolEmailContentEntity $schoolEmailContent)
    {
        return $this->SchoolEmailContent->removeElement($schoolEmailContent);
    }

    /**
     * Get schoolEmailContent.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchoolEmailContent()
    {
        return $this->SchoolEmailContent;
    }
}
