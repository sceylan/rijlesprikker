<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RolesAndPermissionEntity
 *
 * @ORM\Table(name="roles_and_permission", indexes={@ORM\Index(name="drivingschool_id", columns={"drivingschool_id"})})
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\RolesAndPermissionRepository")
 */
class RolesAndPermissionEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="rolesAndPermission")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="InstructorEntity", inversedBy="rolesAndPermission")
     * @ORM\JoinColumn(name="instructor_id", referencedColumnName="id", nullable=true)
     */
    private $Instructor;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="RolesModuleEntity", inversedBy="rolesAndPermission")
     * @ORM\JoinColumn(name="rolesmodule_id", referencedColumnName="id", nullable=true)
     */
    private $rolesModule;

    /**
     * @var int|null
     *
     * @ORM\Column(name="listrole", type="integer", nullable=true)
     */
    private $listrole;

    /**
     * @var int|null
     *
     * @ORM\Column(name="addrole", type="integer", nullable=true)
     */
    private $addrole;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updaterole", type="integer", nullable=true)
     */
    private $updaterole;

    /**
     * @var int|null
     *
     * @ORM\Column(name="deleterole", type="integer", nullable=true)
     */
    private $deleterole;

    /**
     * @var int|null
     *
     * @ORM\Column(name="acceptrole", type="integer", nullable=true)
     */
    private $acceptrole;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rejectrole", type="integer", nullable=true)
     */
    private $rejectrole;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return RolesAndPermission
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set Instructor.
     *
     * @param int|null $Instructor
     *
     * @return RolesAndPermission
     */
    public function setInstructor(InstructorEntity $Instructor = null)
    {
        $this->Instructor = $Instructor;

        return $this;
    }

    /**
     * Get Instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->Instructor;
    }

    /**
     * Set rolesModule.
     *
     * @param int|null $rolesModule
     *
     * @return RolesAndPermission
     */
    public function setRolesModule(RolesModuleEntity $rolesModule = null)
    {
        $this->rolesModule = $rolesModule;

        return $this;
    }

    /**
     * Get rolesModule.
     *
     * @return int|null
     */
    public function getRolesModule()
    {
        return $this->rolesModule;
    }

    /**
     * Set listrole.
     *
     * @param int|null $listrole
     *
     * @return RolesAndPermission
     */
    public function setListrole($listrole = null)
    {
        $this->listrole = $listrole;

        return $this;
    }

    /**
     * Get listrole.
     *
     * @return int|null
     */
    public function getListrole()
    {
        return $this->listrole;
    }

    /**
     * Set addrole.
     *
     * @param int|null $addrole
     *
     * @return RolesAndPermission
     */
    public function setAddrole($addrole = null)
    {
        $this->addrole = $addrole;

        return $this;
    }

    /**
     * Get addrole.
     *
     * @return int|null
     */
    public function getAddrole()
    {
        return $this->addrole;
    }

    /**
     * Set updaterole.
     *
     * @param int|null $updaterole
     *
     * @return RolesAndPermission
     */
    public function setUpdaterole($updaterole = null)
    {
        $this->updaterole = $updaterole;

        return $this;
    }

    /**
     * Get updaterole.
     *
     * @return int|null
     */
    public function getUpdaterole()
    {
        return $this->updaterole;
    }

    /**
     * Set deleterole.
     *
     * @param int|null $deleterole
     *
     * @return RolesAndPermission
     */
    public function setDeleterole($deleterole = null)
    {
        $this->deleterole = $deleterole;

        return $this;
    }

    /**
     * Get deleterole.
     *
     * @return int|null
     */
    public function getDeleterole()
    {
        return $this->deleterole;
    }

    /**
     * Set acceptrole.
     *
     * @param int|null $acceptrole
     *
     * @return RolesAndPermission
     */
    public function setAcceptrole($acceptrole = null)
    {
        $this->acceptrole = $acceptrole;

        return $this;
    }

    /**
     * Get acceptrole.
     *
     * @return int|null
     */
    public function getAcceptrole()
    {
        return $this->acceptrole;
    }

    /**
     * Set rejectrole.
     *
     * @param int|null $rejectrole
     *
     * @return RolesAndPermission
     */
    public function setRejectrole($rejectrole = null)
    {
        $this->rejectrole = $rejectrole;

        return $this;
    }

    /**
     * Get rejectrole.
     *
     * @return int|null
     */
    public function getRejectrole()
    {
        return $this->rejectrole;
    }
}
