<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\MediaBundle\Entity\Media;

/**
 * StudentNotesEntity
 *
 * @ORM\Table(name="student_notes")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\StudentNotesRepository")
 */
class StudentNotesEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="studentNotes")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="studentNotes")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=true)
     */
    private $studentid;

    /**
     * @var text|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $desc;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    private $notesImage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return StudentNotes
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set studentid.
     *
     * @param int|null $studentid
     *
     * @return StudentNotes
     */
    public function setStudentid(StudentEntity $studentid = null)
    {
        $this->studentid = $studentid;

        return $this;
    }

    /**
     * Get studentid.
     *
     * @return int|null
     */
    public function getStudentid()
    {
        return $this->studentid;
    }

    /**
     * Set desc.
     *
     * @param string|null $desc
     *
     * @return StudentNotes
     */
    public function setDesc($desc = null)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc.
     *
     * @return string|null
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set notesImage.
     *
     * @param Media|null $notesImage
     *
     * @return StudentNotes
     */
    public function setNotesImage($notesImage = null)
    {
        $this->notesImage = $notesImage;

        return $this;
    }

    /**
     * Get notesImage.
     *
     * @return Media
     */
    public function getNotesImage()
    {
        return $this->notesImage;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return StudentNotes
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this->created;
    }

    /**
     * Get created.
     *
     * @return \DateTime|null
     */
    public function getCreated()
    {
        return $this->created;
    }
}
