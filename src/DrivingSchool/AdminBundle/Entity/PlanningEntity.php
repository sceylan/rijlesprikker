<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanningEntity
 *
 * @ORM\Table(name="planning")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\PlanningEntityRepository")
 */
class PlanningEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $enddate;

    /** 
     * @var bool
     *
     * @ORM\Column(name="is_reservation", type="boolean", options={"comment":"0 = Aproved Reservation, 1 = Pending Reservation"})
     */
    private $isReservation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="planning")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=true)
     */
    private $school;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="planning")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=true)
     */
    private $student;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="VehicleEntity", inversedBy="planning")
     * @ORM\JoinColumn(name="vehicle_id", referencedColumnName="id", nullable=true)
     */
    private $vehicle;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="InstructorEntity", inversedBy="planning")
     * @ORM\JoinColumn(name="instructor_id", referencedColumnName="id", nullable=true)
     */
    private $instructor;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="PriceAndPackagesEntity", inversedBy="planning")
     * @ORM\JoinColumn(name="price_package_id", referencedColumnName="id", nullable=true)
     */
    private $pricePackage;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="TrainingSessionsEntity", inversedBy="planning")
     * @ORM\JoinColumn(name="training_session_id", referencedColumnName="id", nullable=true)
     */
    private $trainingSession;


    public function __construct()
    {
        $this->creation = new \DateTime();
        $this->modified = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return PlanningEntity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set location.
     *
     * @param string $location
     *
     * @return PlanningEntity
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set startdate.
     *
     * @param \DateTime $startdate
     *
     * @return PlanningEntity
     */
    public function setStartDate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate.
     *
     * @param \DateTime $enddate
     *
     * @return PlanningEntity
     */
    public function setEndDate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->enddate;
    }

    /**
     * Set isReservation.
     *
     * @param bool $isReservation
     *
     * @return Planning Entity
     */
    public function setIsReservation($isReservation)
    {
        $this->isReservation = $isReservation;

        return $this;
    }

    /**
     * Get isReservation.
     *
     * @return bool
     */
    public function getIsReservation()
    {
        return $this->isReservation;
    }

    /**
     * Set creation.
     *
     * @param \DateTime $creation
     *
     * @return PlanningEntity
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation.
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return PlanningEntity
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set school.
     *
     * @param int|null $school
     *
     * @return DrivingSchoolEntity
     */
    public function setSchool(DrivingSchoolEntity $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school.
     *
     * @return int|null
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Add school.
     *
     * @param \DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school
     *
     * @return PlanningEntity
     */
    public function addSchool(\DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school)
    {
        $this->school[] = $school;

        return $this;
    }

    /**
     * Remove school.
     *
     * @param \DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school
     */
    public function removeSchool(\DrivingSchool\AdminBundle\Entity\DrivingSchoolEntity $school)
    {
        $this->school->removeElement($school);
    }

    /**
     * Set student.
     *
     * @param int|null $student
     *
     * @return StudentEntity
     */
    public function setStudent(StudentEntity $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return int|null
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Add student.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $student
     *
     * @return PlanningEntity
     */
    public function addStudent(\DrivingSchool\AdminBundle\Entity\StudentEntity $student)
    {
        $this->student[] = $student;

        return $this;
    }

    /**
     * Remove student.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $student
     */
    public function removeStudent(\DrivingSchool\AdminBundle\Entity\StudentEntity $student)
    {
        $this->student->removeElement($student);
    }

    /**
     * Set vehicle.
     *
     * @param int|null $vehicle
     *
     * @return PlanningEntity
     */
    public function setVehicle(VehicleEntity $vehicle = null)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle.
     *
     * @return int|null
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Add vehicle.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle
     *
     * @return PlanningEntity
     */
    public function addVehicle(\DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle)
    {
        $this->vehicle[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle.
     *
     * @param \DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle
     */
    public function removeVehicle(\DrivingSchool\AdminBundle\Entity\VehicleEntity $vehicle)
    {
        $this->vehicle->removeElement($vehicle);
    }

    /**
     * Set instructor.
     *
     * @param int|null $instructor
     *
     * @return PlanningEntity
     */
    public function setInstructor(InstructorEntity $instructor = null)
    {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Add instructor.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor
     *
     * @return PlanningEntity
     */
    public function addInstructor(\DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor)
    {
        $this->instructor[] = $instructor;

        return $this;
    }

    /**
     * Remove instructor.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor
     */
    public function removeInstructor(\DrivingSchool\AdminBundle\Entity\InstructorEntity $instructor)
    {
        $this->instructor->removeElement($instructor);
    }


    /**
     * Set pricePackage.
     *
     * @param int|null $pricePackage
     *
     * @return PlanningEntity
     */
    public function setPricePackage(PriceAndPackagesEntity $pricePackage = null)
    {
        $this->pricePackage = $pricePackage;

        return $this;
    }

    /**
     * Get pricePackage.
     *
     * @return int|null
     */
    public function getPricePackage()
    {
        return $this->pricePackage;
    }

    /**
     * Add pricePackage.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $pricePackage
     *
     * @return PlanningEntity
     */
    public function addPricePackage(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $pricePackage)
    {
        $this->pricePackage[] = $pricePackage;

        return $this;
    }

    /**
     * Remove pricePackage.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $pricePackage
     */
    public function removePricePackage(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $pricePackage)
    {
        $this->pricePackage->removeElement($pricePackage);
    }


    /**
     * Set trainingSession.
     *
     * @param int|null $trainingSession
     *
     * @return PlanningEntity
     */
    public function setTrainingSession(TrainingSessionsEntity $trainingSession = null)
    {
        $this->trainingSession = $trainingSession;

        return $this;
    }

    /**
     * Get trainingSession.
     *
     * @return int|null
     */
    public function getTrainingSession()
    {
        return $this->trainingSession;
    }

    /**
     * Add trainingSession.
     *
     * @param \DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession
     *
     * @return PlanningEntity
     */
    public function addTrainingSession(\DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession)
    {
        $this->trainingSession[] = $trainingSession;

        return $this;
    }

    /**
     * Remove trainingSession.
     *
     * @param \DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession
     */
    public function removeTrainingSession(\DrivingSchool\AdminBundle\Entity\TrainingSessionsEntity $trainingSession)
    {
        $this->trainingSession->removeElement($trainingSession);
    }
}
