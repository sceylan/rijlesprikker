<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * HolidayEntity
 *
 * @ORM\Table(name="holiday", indexes={@ORM\Index(name="drivingschool_id", columns={"drivingschool_id"})})
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\HolidayRepository")
 */
class HolidayEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="Holiday")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Name should not be blank.")
     */
    private $name;

    /**
     * @var date|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var date|null
     *
     * @ORM\Column(name="until", type="date", nullable=true)
     */
    private $until;
    
    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="InstructorEntity", inversedBy="holiday")
     * @ORM\JoinTable(name="instructor_holiday",
     *      joinColumns={@ORM\JoinColumn(name="holiday_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="instructor_id", referencedColumnName="id")}
     *      )
     */
    private $instructor;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    public function __construct()
    {
        
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return Vehicle
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Holiday
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date.
     *
     * @param date|null $date
     *
     * @return Holiday
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return date|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set until.
     *
     * @param date|null $until
     *
     * @return Holiday
     */
    public function setUntil($until = null)
    {
        $this->until = $until;

        return $this;
    }

    /**
     * Get until.
     *
     * @return date|null
     */
    public function getUntil()
    {
        return $this->until;
    }

    /**
     * Set instructor.
     *
     * @param int|null $instructor
     *
     * @return Holiday
     */
    public function setInstructor(InstructorEntity $instructor = null)
    {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Add instructor
     *
     * @return InstructorEntity $instructor
     */
    public function addInstructor(InstructorEntity $instructor)
    {
        $this->instructor[] = $instructor;

        return $this;
    }

    /**
     * Remove instructor
     *
     * @param InstructorEntity $instructor
     */
    public function removeInstructor(instructorEntity $instructor)
    {
        $this->instructor->removeElement($instructor);
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Vehicle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
}
