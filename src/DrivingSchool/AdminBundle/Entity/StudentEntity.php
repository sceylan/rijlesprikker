<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Sonata\UserBundle\Entity\BaseUser as User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * StudentEntity
 *
 * @ORM\Table(name="student")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\StudentRepository")
 * @UniqueEntity(fields={"username"}, message="Username already taken !")
 * @UniqueEntity(fields={"email"}, message="Email already registered with us !")
 */
class StudentEntity extends User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="StudentEntity")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="PriceIconsEntity", inversedBy="StudentEntity")
     * @ORM\JoinColumn(name="icon_id", referencedColumnName="id", nullable=true)
     */
    private $icon;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="InstructorEntity", inversedBy="StudentEntity")
     * @ORM\JoinColumn(name="instructor_id", referencedColumnName="id", nullable=true)
     */
    private $instructor;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="PriceAndPackagesEntity", inversedBy="student")
     * @ORM\JoinTable(name="pricepackage_student",
     *      joinColumns={@ORM\JoinColumn(name="student_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="priceandpackages_id", referencedColumnName="id")}
     *      )
     */
    private $educations;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "First name should not be blank.")
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Last name should not be blank.")
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="initials", type="string", length=255, nullable=true)
     */
    private $initials;

    /**
     * @var string|null
     *
     * @ORM\Column(name="student_residence", type="string", length=255, nullable=true)
     */
    private $studentResidence;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="house_number", type="string", length=255, nullable=true)
     */
    private $houseNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="place", type="string", length=255, nullable=true)
     */
    private $place;

    /**
     * @var string|null
     *
     * @ORM\Column(name="birth_place", type="string", length=255, nullable=true)
     */
    private $birthPlace;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bsn", type="string", length=255, nullable=true)
     */
    private $bsn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="drivers_license", type="string", length=255, nullable=true)
     */
    private $driversLicense;

    /**
     * @var string|null
     *
     * @ORM\Column(name="candidate_number", type="string", length=20, nullable=true)
     */
    private $candidateNumber;

    /**
     * @var text|null
     *
     * @ORM\Column(name="instructors_note", type="text", length=255, nullable=true)
     */
    private $instructorsNote;

    /**
     * @var text|null
     *
     * @ORM\Column(name="note", type="text", length=255, nullable=true)
     */
    private $note;

    /**
     * @var string|null
     *
     * @ORM\Column(name="student_age", type="string", length=255, nullable=true)
     */
    private $studentAge;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    private $studentImage;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="authorization", type="boolean", nullable=true)
     */
    private $authorization;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="health_information", type="boolean", length=255, nullable=true)
     */
    private $healthInformation;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="too_good", type="boolean", length=255, nullable=true)
     */
    private $tooGood;

     /**
     * @var string|null
     *
     * @ORM\Column(name="debtor_number", type="string", length=255, nullable=true)
     */
    private $debtorNumber;

     /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_name", type="string", length=255, nullable=true)
     */
    private $invoiceName;

     /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_surname", type="string", length=255, nullable=true)
     */
    private $invoiceSurname;

     /**
     * @var string|null
     *
     * @ORM\Column(name="billing_address", type="string", length=255, nullable=true)
     */
    private $billingAddress;

     /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_zip_code", type="string", length=255, nullable=true)
     */
    private $invoiceZipCode ;

     /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_place", type="string", length=255, nullable=true)
     */
    private $invoicePlace;

     /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_email", type="string", length=255, nullable=true)
     */
    private $invoiceEmail;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialEntity", mappedBy="studentid", cascade={"remove"})
     */
    protected $studentFinancial;

    /**
     * @ORM\OneToMany(targetEntity="StudentFinancialHistoryEntity", mappedBy="studentid", cascade={"remove"})
     */
    protected $studentFinancialHistory;

    /**
     * @ORM\OneToMany(targetEntity="InvoiceEntity", mappedBy="studentid", cascade={"remove"})
     */
    protected $invoice;

    /**
     * @ORM\OneToMany(targetEntity="StudentNotesEntity", mappedBy="studentid", cascade={"remove"})
     */
    protected $studentNotes;

    /**
     * @ORM\OneToMany(targetEntity="PlanningEntity", mappedBy="student", cascade={"remove"})
     */
    protected $planning;

    /**
     * @ORM\OneToMany(targetEntity="NotificationEntity", mappedBy="student", cascade={"remove"})
     */
    protected $notification;

    /**
     * @ORM\OneToMany(targetEntity="ScoreCardNotesEntity", mappedBy="student", cascade={"remove"})
     */
    protected $ScoreCardNotes;

    /**
     * @var array
     */
    protected $roles;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     */
    protected $password;
    
    /**
     * The salt to use for hashing.
     *
     * @var string
     */
    protected $salt;

    /**
     * @var string
     */
    protected $username;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;

    
    /**
     * @var string
     */
    protected $enabled;

    public function __construct() {
        parent::__construct();
        $this->roles = array();
        $this->enabled = true;
        $this->setStudentAdmin(true);
    }

    
    /**
     * @return string
     */
    public function __toString()
    {
         return (string) $this->getFirstName() . " " . $this->getLastName();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return Student
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set icon.
     *
     * @param int|null $icon
     *
     * @return Student
     */
    public function setIcon($icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return int|null
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set instructor.
     *
     * @param int|null $instructor
     *
     * @return Student
     */
    public function setInstructor(InstructorEntity $instructor = null)
    {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get instructor.
     *
     * @return int|null
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Set educations.
     *
     * @param int|null $educations
     *
     * @return Student
     */
    public function setEducations(PriceAndPackagesEntity $educations = null)
    {
        $this->educations = $educations;

        return $this;
    }

    /**
     * Get educations.
     *
     * @return int|null
     */
    public function getEducations()
    {
        return $this->educations;
    }
   
    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return Student
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return Student
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set initials.
     *
     * @param string|null $initials
     *
     * @return Student
     */
    public function setInitials($initials = null)
    {
        $this->initials = $initials;

        return $this;
    }

    /**
     * Get initials.
     *
     * @return string|null
     */
    public function getInitials()
    {
        return $this->initials;
    }

    /**
     * Set studentResidence.
     *
     * @param string|null $studentResidence
     *
     * @return Student
     */
    public function setStudentResidence($studentResidence = null)
    {
        $this->studentResidence = $studentResidence;

        return $this;
    }

    /**
     * Get studentResidence.
     *
     * @return string|null
     */
    public function getStudentResidence()
    {
        return $this->studentResidence;
    }

    /**
     * Set postalCode.
     *
     * @param string|null $postalCode
     *
     * @return Student
     */
    public function setPostalCode($postalCode = null)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode.
     *
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set houseNumber.
     *
     * @param string|null $houseNumber
     *
     * @return Student
     */
    public function setHouseNumber($houseNumber = null)
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber.
     *
     * @return string|null
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set place.
     *
     * @param string|null $place
     *
     * @return Student
     */
    public function setPlace($place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place.
     *
     * @return string|null
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set birthPlace.
     *
     * @param string|null $birthPlace
     *
     * @return Student
     */
    public function setBirthPlace($birthPlace = null)
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    /**
     * Get birthPlace.
     *
     * @return string|null
     */
    public function getBirthPlace()
    {
        return $this->birthPlace;
    }

    /**
     * Set bsn.
     *
     * @param string|null $bsn
     *
     * @return Student
     */
    public function setBsn($bsn = null)
    {
        $this->bsn = $bsn;

        return $this;
    }

    /**
     * Get bsn.
     *
     * @return string|null
     */
    public function getBsn()
    {
        return $this->bsn;
    }

    /**
     * Set driversLicense.
     *
     * @param string|null $driversLicense
     *
     * @return Student
     */
    public function setDriversLicense($driversLicense = null)
    {
        $this->driversLicense = $driversLicense;

        return $this;
    }

    /**
     * Get driversLicense.
     *
     * @return string|null
     */
    public function getDriversLicense()
    {
        return $this->driversLicense;
    }

    /**
     * Set candidateNumber.
     *
     * @param string|null $candidateNumber
     *
     * @return Student
     */
    public function setCandidateNumber($candidateNumber = null)
    {
        $this->candidateNumber = $candidateNumber;

        return $this;
    }

    /**
     * Get candidateNumber.
     *
     * @return string|null
     */
    public function getCandidateNumber()
    {
        return $this->candidateNumber;
    }

    /**
     * Set instructorsNote.
     *
     * @param text|null $instructorsNote
     *
     * @return Student
     */
    public function setInstructorsNote($instructorsNote = null)
    {
        $this->instructorsNote = $instructorsNote;

        return $this;
    }

    /**
     * Get instructorsNote.
     *
     * @return text|null
     */
    public function getInstructorsNote()
    {
        return $this->instructorsNote;
    }

    /**
     * Set note.
     *
     * @param text|null $note
     *
     * @return Student
     */
    public function setNote($note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return text|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set studentEducation.
     *
     * @param string|null $studentEducation
     *
     * @return Student
     */
    public function setStudentEducation($studentEducation = null)
    {
        $this->studentEducation = $studentEducation;

        return $this;
    }

    /**
     * Get studentEducation.
     *
     * @return string|null
     */
    public function getStudentEducation()
    {
        return $this->studentEducation;
    }

    /**
     * Set studentAge.
     *
     * @param string|null $studentAge
     *
     * @return Student
     */
    public function setStudentAge($studentAge = null)
    {
        $this->studentAge = $studentAge;

        return $this;
    }

    /**
     * Get studentAge.
     *
     * @return string|null
     */
    public function getStudentAge()
    {
        return $this->studentAge;
    }

    /**
     * Set studentImage.
     *
     * @param string|null $studentImage
     *
     * @return Student
     */
    public function setStudentImage($studentImage = null)
    {
        $this->studentImage = $studentImage;

        return $this;
    }

    /**
     * Get studentImage.
     *
     * @return string|null
     */
    public function getStudentImage()
    {
        return $this->studentImage;
    }

    
    /**
     * Set authorization.
     *
     * @param bool|null $authorization
     *
     * @return Student
     */
    public function setAuthorization($authorization = null)
    {
        $this->authorization = $authorization;

        return $this;
    }

    /**
     * Get authorization.
     *
     * @return bool|null
     */
    public function getAuthorization()
    {
        return $this->authorization;
    }

    /**
     * Set healthInformation.
     *
     * @param bool|null $healthInformation
     *
     * @return Student
     */
    public function setHealthInformation($healthInformation = null)
    {
        $this->healthInformation = $healthInformation;

        return $this;
    }

    /**
     * Get healthInformation.
     *
     * @return bool|null
     */
    public function getHealthInformation()
    {
        return $this->healthInformation;
    }

    /**
     * Set tooGood.
     *
     * @param bool|null $tooGood
     *
     * @return Student
     */
    public function setTooGood($tooGood = null)
    {
        $this->tooGood = $tooGood;

        return $this;
    }

    /**
     * Get tooGood.
     *
     * @return bool|null
     */
    public function getTooGood()
    {
        return $this->tooGood;
    }

    /**
     * Set debtorNumber.
     *
     * @param string|null $debtorNumber
     *
     * @return Student
     */
    public function setDebtorNumber($debtorNumber = null)
    {
        $this->debtorNumber = $debtorNumber;

        return $this;
    }

    /**
     * Get debtorNumber.
     *
     * @return string|null
     */
    public function getDebtorNumber()
    {
        return $this->debtorNumber;
    }

    /**
     * Set invoiceName.
     *
     * @param string|null $invoiceName
     *
     * @return Student
     */
    public function setInvoiceName($invoiceName = null)
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * Get invoiceName.
     *
     * @return string|null
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * Set invoiceSurname.
     *
     * @param string|null $invoiceSurname
     *
     * @return Student
     */
    public function setInvoiceSurname($invoiceSurname = null)
    {
        $this->invoiceSurname = $invoiceSurname;

        return $this;
    }

    /**
     * Get invoiceSurname.
     *
     * @return string|null
     */
    public function getInvoiceSurname()
    {
        return $this->invoiceSurname;
    }

    /**
     * Set billingAddress.
     *
     * @param string|null $billingAddress
     *
     * @return Student
     */
    public function setBillingAddress($billingAddress = null)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress.
     *
     * @return string|null
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set invoiceZipCode.
     *
     * @param string|null $invoiceZipCode
     *
     * @return Student
     */
    public function setInvoiceZipCode($invoiceZipCode = null)
    {
        $this->invoiceZipCode = $invoiceZipCode;

        return $this;
    }

    /**
     * Get invoiceZipCode.
     *
     * @return string|null
     */
    public function getInvoiceZipCode()
    {
        return $this->invoiceZipCode;
    }

    /**
     * Set invoicePlace.
     *
     * @param string|null $invoicePlace
     *
     * @return Student
     */
    public function setInvoicePlace($invoicePlace = null)
    {
        $this->invoicePlace = $invoicePlace;

        return $this;
    }

    /**
     * Get invoicePlace.
     *
     * @return string|null
     */
    public function getInvoicePlace()
    {
        return $this->invoicePlace;
    }

    /**
     * Set invoiceEmail.
     *
     * @param string|null $invoiceEmail
     *
     * @return Student
     */
    public function setInvoiceEmail($invoiceEmail = null)
    {
        $this->invoiceEmail = $invoiceEmail;

        return $this;
    }

    /**
     * Get invoiceEmail.
     *
     * @return string|null
     */
    public function getInvoiceEmail()
    {
        return $this->invoiceEmail;
    }
    
    // Remove appointment table Student
    public function remove($appointment)
    {
        $this->getAppointment()->remove($appointment);
    }

    /**
     * {@inheritdoc}
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * {@inheritdoc}
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStudentAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole("ROLE_STUDENT");
        } else {
            $this->removeRole("ROLE_STUDENT");
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        $roles = $this->roles;
        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }
        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    

    /**
     * {@inheritdoc}
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;

        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }



    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        if (13 === count($data)) {
            // Unserializing a User object from 1.3.x
            unset($data[4], $data[5], $data[6], $data[9], $data[10]);
            $data = array_values($data);
        } elseif (11 === count($data)) {
            // Unserializing a User from a dev version somewhere between 2.0-alpha3 and 2.0-beta1
            unset($data[4], $data[7], $data[8]);
            $data = array_values($data);
        }

        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->enabled,
            $this->id,
            $this->email,
            $this->emailCanonical
        ) = $data;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add education.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $education
     *
     * @return StudentEntity
     */
    public function addEducation(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $education)
    {
        $this->educations[] = $education;

        return $this;
    }

    /**
     * Remove education.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $education
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEducation(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $education)
    {
        return $this->educations->removeElement($education);
    }

    /**
     * Add studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return StudentEntity
     */
    public function addStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        $this->studentFinancial[] = $studentFinancial;

        return $this;
    }

    /**
     * Remove studentFinancial.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancial(\DrivingSchool\AdminBundle\Entity\StudentFinancialEntity $studentFinancial)
    {
        return $this->studentFinancial->removeElement($studentFinancial);
    }

    /**
     * Get studentFinancial.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancial()
    {
        return $this->studentFinancial;
    }

    /**
     * Add studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return StudentEntity
     */
    public function addStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        $this->studentFinancialHistory[] = $studentFinancialHistory;

        return $this;
    }

    /**
     * Remove studentFinancialHistory.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentFinancialHistory(\DrivingSchool\AdminBundle\Entity\StudentFinancialHistoryEntity $studentFinancialHistory)
    {
        return $this->studentFinancialHistory->removeElement($studentFinancialHistory);
    }

    /**
     * Get studentFinancialHistory.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentFinancialHistory()
    {
        return $this->studentFinancialHistory;
    }

    /**
     * Add invoice.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice
     *
     * @return StudentEntity
     */
    public function addInvoice(\DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice)
    {
        $this->invoice[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice.
     *
     * @param \DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInvoice(\DrivingSchool\AdminBundle\Entity\InvoiceEntity $invoice)
    {
        return $this->invoice->removeElement($invoice);
    }

    /**
     * Get invoice.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Add studentNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote
     *
     * @return StudentEntity
     */
    public function addStudentNote(\DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote)
    {
        $this->studentNotes[] = $studentNote;

        return $this;
    }

    /**
     * Remove studentNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentNote(\DrivingSchool\AdminBundle\Entity\StudentNotesEntity $studentNote)
    {
        return $this->studentNotes->removeElement($studentNote);
    }

    /**
     * Get studentNotes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentNotes()
    {
        return $this->studentNotes;
    }

    /**
     * Add planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return StudentEntity
     */
    public function addPlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        $this->planning[] = $planning;

        return $this;
    }

    /**
     * Remove planning.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PlanningEntity $planning
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePlanning(\DrivingSchool\AdminBundle\Entity\PlanningEntity $planning)
    {
        return $this->planning->removeElement($planning);
    }

    /**
     * Get planning.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanning()
    {
        return $this->planning;
    }

    /**
     * Add notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return StudentEntity
     */
    public function addNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        $this->notification[] = $notification;

        return $this;
    }

    /**
     * Remove notification.
     *
     * @param \DrivingSchool\AdminBundle\Entity\NotificationEntity $notification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotification(\DrivingSchool\AdminBundle\Entity\NotificationEntity $notification)
    {
        return $this->notification->removeElement($notification);
    }

    /**
     * Get notification.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Add scoreCardNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote
     *
     * @return StudentEntity
     */
    public function addScoreCardNote(\DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote)
    {
        $this->ScoreCardNotes[] = $scoreCardNote;

        return $this;
    }

    /**
     * Remove scoreCardNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeScoreCardNote(\DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote)
    {
        return $this->ScoreCardNotes->removeElement($scoreCardNote);
    }

    /**
     * Get scoreCardNotes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScoreCardNotes()
    {
        return $this->ScoreCardNotes;
    }
}
