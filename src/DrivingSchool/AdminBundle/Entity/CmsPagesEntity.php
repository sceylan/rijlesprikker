<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsPagesEntity
 *
 * @ORM\Table(name="cms_pages")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\CmsPagesRepository")
 * @UniqueEntity(fields={"urlKey"}, message="URL key is already exist !")
 */
class CmsPagesEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Title should not be blank.")
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     * @Assert\NotBlank(message = "Content should not be blank.")
     */
    private $content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url_key", type="string", length=255, nullable=true, unique=true)
     * @Assert\NotBlank(message = "URL key should not be blank.")
     */
    private $urlKey;

	/**
     * @var string|null
     *
     * @ORM\Column(name="meta_title", type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_keyword", type="text", nullable=true)
     */
    private $metaKeyword;

    /**
     * @var text|null
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return CmsPages
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return CmsPages
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set urlKey.
     *
     * @param string|null $urlKey
     *
     * @return CmsPages
     */
    public function setUrlKey($urlKey = null)
    {
        $this->urlKey = $urlKey;

        return $this;
    }

    /**
     * Get urlKey.
     *
     * @return string|null
     */
    public function getUrlKey()
    {
        return $this->urlKey;
    }

    /**
     * Set metaTitle.
     *
     * @param string|null $metaTitle
     *
     * @return CmsPages
     */
    public function setMetaTitle($metaTitle = null)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle.
     *
     * @return string|null
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaKeyword.
     *
     * @param string|null $metaKeyword
     *
     * @return CmsPages
     */
    public function setMetaKeyword($metaKeyword = null)
    {
        $this->metaKeyword = $metaKeyword;

        return $this;
    }

    /**
     * Get metaKeyword.
     *
     * @return string|null
     */
    public function getMetaKeyword()
    {
        return $this->metaKeyword;
    }

    /**
     * Set metaDescription.
     *
     * @param string|null $metaDescription
     *
     * @return CmsPages
     */
    public function setMetaDescription($metaDescription = null)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription.
     *
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }
}
