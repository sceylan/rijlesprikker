<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PriceIconsEntity
 *
 * @ORM\Table(name="price_icons")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\PriceIconsRepository")
 */
class PriceIconsEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon_title", type="string", length=255, nullable=true, unique = true)
     */
    private $iconTitle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="icon_path", type="string", length=255, nullable=true, unique = true)
     */
    private $iconPath;

    /** 
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="PriceAndPackagesEntity", mappedBy="icon", cascade={"remove"})
     */
    protected $priceAndPackages;

    /**
     * @ORM\OneToMany(targetEntity="StudentEntity", mappedBy="icon", cascade={"remove"})
     */
    protected $StudentEntity;

    public function __toString()
    {
        return strval($this->id);
    }

    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iconTitle.
     *
     * @param string|null $iconTitle
     *
     * @return PriceIconsEntity
     */
    public function setIconTitle($iconTitle = null)
    {
        $this->iconTitle = $iconTitle;

        return $this;
    }

    /**
     * Get iconTitle.
     *
     * @return string|null
     */
    public function getIconTitle()
    {
        return $this->iconTitle;
    }

    /**
     * Set iconPath.
     *
     * @param string|null $iconPath
     *
     * @return PriceIconsEntity
     */
    public function setIconPath($iconPath = null)
    {
        $this->iconPath = $iconPath;

        return $this;
    }

    /**
     * Get iconPath.
     *
     * @return string|null
     */
    public function getIconPath()
    {
        return $this->iconPath;
    }
    
    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return PriceIconsEntity
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->priceAndPackages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add priceAndPackage.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage
     *
     * @return PriceIconsEntity
     */
    public function addPriceAndPackage(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage)
    {
        $this->priceAndPackages[] = $priceAndPackage;

        return $this;
    }

    /**
     * Remove priceAndPackage.
     *
     * @param \DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePriceAndPackage(\DrivingSchool\AdminBundle\Entity\PriceAndPackagesEntity $priceAndPackage)
    {
        return $this->priceAndPackages->removeElement($priceAndPackage);
    }

    /**
     * Get priceAndPackages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriceAndPackages()
    {
        return $this->priceAndPackages;
    }

    /**
     * Add studentEntity.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity
     *
     * @return PriceIconsEntity
     */
    public function addStudentEntity(\DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity)
    {
        $this->StudentEntity[] = $studentEntity;

        return $this;
    }

    /**
     * Remove studentEntity.
     *
     * @param \DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStudentEntity(\DrivingSchool\AdminBundle\Entity\StudentEntity $studentEntity)
    {
        return $this->StudentEntity->removeElement($studentEntity);
    }

    /**
     * Get studentEntity.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentEntity()
    {
        return $this->StudentEntity;
    }
}
