<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SchoolEmailContentEntity
 *
 * @ORM\Table(name="school_email_content")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\SchoolEmailContentRepository")
 */
class SchoolEmailContentEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="SchoolEmailContent")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="SchoolEmailTemplatesEntity", inversedBy="SchoolEmailContent")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id", nullable=true)
     */
    private $template;

    /**
     * @var text|null
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     * @Assert\NotBlank(message = "Content should not be blank.")
     */
    private $content;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return SchoolEmailContent
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set template.
     *
     * @param int|null $template
     *
     * @return SchoolEmailContent
     */
    public function setTemplate(SchoolEmailTemplatesEntity $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template.
     *
     * @return int|null
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return SchoolEmailContent
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return text|null
     */
    public function getContent()
    {
        return $this->content;
    }
}
