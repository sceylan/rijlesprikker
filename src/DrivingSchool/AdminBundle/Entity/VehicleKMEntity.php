<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * VehicleKMEntity
 *
 * @ORM\Table(name="vehicle_km")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\VehicleKMRepository")
 */
class VehicleKMEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="VehicleEntity", inversedBy="vehiclekm")
     * @ORM\JoinColumn(name="Vehicle_id", referencedColumnName="id", nullable=true)
     */
    private $vehicleId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mileage", type="integer", nullable=true)
     */
    private $mileage;

    /**
     * @var date|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var time|null
     *
     * @ORM\Column(name="time", type="time", nullable=true)
     */
    private $time;

    /**
     * @var text|null
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    public function __construct()
    {
    }

    public function __toString()
    {
        return strval($this->id);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vehicleId.
     *
     * @param int|null $vehicleId
     *
     * @return Vehiclekm
     */
    public function setVehicleId(VehicleEntity $vehicleId = null)
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    /**
     * Get vehicleId.
     *
     * @return int|null
     */
    public function getVehicleId()
    {
        return $this->vehicleId;
    }

    /**
     * Set mileage.
     *
     * @param int|null $mileage
     *
     * @return Vehiclekm
     */
    public function setMileage($mileage = null)
    {
        $this->mileage = $mileage;

        return $this;
    }

    /**
     * Get mileage.
     *
     * @return int|null
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * Set date.
     *
     * @param date|null $date
     *
     * @return Vehiclekm
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return time|null
     */
    public function getDate()
    {
        return $this->date;
    }  

    /**
     * Set time.
     *
     * @param time|null $time
     *
     * @return Vehiclekm
     */
    public function setTime($time = null)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time.
     *
     * @return string|null
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set note.
     *
     * @param text|null $note
     *
     * @return Vehiclekm
     */
    public function setNote($note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return text|null
     */
    public function getNote()
    {
        return $this->note;
    }
}
