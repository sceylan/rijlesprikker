<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LessonRolesEntity
 *
 * @ORM\Table(name="lesson_roles")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\LessonRolesRepository")
 */
class LessonRolesEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var int
     * @ORM\ManyToOne(targetEntity="ModuleEntity", inversedBy="LessonRoles")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $Module;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ScoreCardNotesEntity", mappedBy="lessonRoles", cascade={"remove"})
     */
    protected $ScoreCardNotes;
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Module.
     *
     * @param int|null $Module
     *
     * @return LessonRoles
     */
    public function setModule(ModuleEntity $Module = null)
    {
        $this->Module = $Module;

        return $this;
    }

    /**
     * Get Module.
     *
     * @return int|null
     */
    public function getModule()
    {
        return $this->Module;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return LessonRoles
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ScoreCardNotes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add scoreCardNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote
     *
     * @return LessonRolesEntity
     */
    public function addScoreCardNote(\DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote)
    {
        $this->ScoreCardNotes[] = $scoreCardNote;

        return $this;
    }

    /**
     * Remove scoreCardNote.
     *
     * @param \DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeScoreCardNote(\DrivingSchool\AdminBundle\Entity\ScoreCardNotesEntity $scoreCardNote)
    {
        return $this->ScoreCardNotes->removeElement($scoreCardNote);
    }

    /**
     * Get scoreCardNotes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScoreCardNotes()
    {
        return $this->ScoreCardNotes;
    }
}
