<?php

namespace DrivingSchool\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentFinancialHistoryEntity
 *
 * @ORM\Table(name="student_financial_history")
 * @ORM\Entity(repositoryClass="DrivingSchool\AdminBundle\Repository\StudentFinancialHistoryRepository")
 */
class StudentFinancialHistoryEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="DrivingSchoolEntity", inversedBy="studentFinancialHistory")
     * @ORM\JoinColumn(name="drivingschool_id", referencedColumnName="id", nullable=true)
     */
    private $DrivingSchool;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="StudentEntity", inversedBy="studentFinancialHistory")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=true)
     */
    private $studentid;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="PriceAndPackagesEntity", inversedBy="studentFinancialHistory")
     * @ORM\JoinColumn(name="price_package_id", referencedColumnName="id", nullable=true)
     */
    private $pricePackageId;
    
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="TrainingSessionsEntity", inversedBy="studentFinancialHistory")
     * @ORM\JoinColumn(name="trainingsession_id", referencedColumnName="id", nullable=true)
     */
    private $Courses;

    /**
     * @var int
     *
     * @ORM\Column(name="course_type", type="integer", nullable=true)
     */
    private $courseType;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="float", nullable=true)
     */
    private $qty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="minutes", type="integer", nullable=true)
     */
    private $minutes;

    /**
     * @var text|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $desc;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="increase_decrease", type="integer", nullable=true, options={"comment":"0 = Decrease, 1 = Increase"})
     */
    private $increaseDecrease;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    public function __construct()
    {
        $this->creation = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set DrivingSchool.
     *
     * @param int|null $DrivingSchool
     *
     * @return StudentFinancialHistory
     */
    public function setDrivingSchool(DrivingSchoolEntity $DrivingSchool = null)
    {
        $this->DrivingSchool = $DrivingSchool;

        return $this;
    }

    /**
     * Get DrivingSchool.
     *
     * @return int|null
     */
    public function getDrivingSchool()
    {
        return $this->DrivingSchool;
    }

    /**
     * Set studentid.
     *
     * @param int|null $studentid
     *
     * @return StudentFinancialHistory
     */
    public function setStudentid(StudentEntity $studentid = null)
    {
        $this->studentid = $studentid;

        return $this;
    }

    /**
     * Get studentid.
     *
     * @return int|null
     */
    public function getStudentid()
    {
        return $this->studentid;
    }

    /**
     * Set pricePackageId.
     *
     * @param int|null $pricePackageId
     *
     * @return StudentFinancialHistory
     */
    public function setPricePackageId(PriceAndPackagesEntity $pricePackageId = null)
    {
        $this->pricePackageId = $pricePackageId;

        return $this;
    }

    /**
     * Get pricePackageId.
     *
     * @return int|null
     */
    public function getPricePackageId()
    {
        return $this->pricePackageId;
    }

    /**
     * Set Courses.
     *
     * @param int|null $Courses
     *
     * @return StudentFinancialHistory
     */
    public function setCourses(TrainingSessionsEntity $Courses = null)
    {
        $this->Courses = $Courses;

        return $this;
    }

    /**
     * Get Courses.
     *
     * @return int|null
     */
    public function getCourses()
    {
        return $this->Courses;
    }

    /**
     * Set courseType.
     *
     * @param int|null $courseType
     *
     * @return StudentFinancialHistory
     */
    public function setCourseType($courseType = null)
    {
        $this->courseType = $courseType;

        return $this;
    }

    /**
     * Get courseType.
     *
     * @return int|null
     */
    public function getCourseType()
    {
        return $this->courseType;
    }

    /**
     * Set qty.
     *
     * @param int|null $qty
     *
     * @return StudentFinancialHistory
     */
    public function setQty($qty = null)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty.
     *
     * @return int|null
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set minutes.
     *
     * @param string|null $minutes
     *
     * @return StudentFinancialHistory
     */
    public function setMinutes($minutes = null)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes.
     *
     * @return string|null
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set desc.
     *
     * @param string|null $desc
     *
     * @return StudentFinancialHistory
     */
    public function setDesc($desc = null)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Set increaseDecrease.
     *
     * @param int|null $increaseDecrease
     *
     * @return StudentFinancialHistory
     */
    public function setIncreaseDecrease($increaseDecrease = null)
    {
        $this->increaseDecrease = $increaseDecrease;

        return $this;
    }

    /**
     * Get increaseDecrease.
     *
     * @return int|null
     */
    public function getIncreaseDecrease()
    {
        return $this->increaseDecrease;
    }

    /**
     * Set creation.
     *
     * @param \DateTime $creation
     *
     * @return StudentFinancialHistory
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation.
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Get desc.
     *
     * @return string|null
     */
    public function getDesc()
    {
        return $this->desc;
    }
}
