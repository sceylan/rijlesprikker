<?php

namespace DrivingSchool\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DrivingSchoolAdminBundle:Default:index.html.twig');
    }
}
