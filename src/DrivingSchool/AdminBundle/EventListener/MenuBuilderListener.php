<?php
namespace DrivingSchool\AdminBundle\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

class MenuBuilderListener
{
    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $child = $menu->addChild('', [
            'label' => 'Dashboard',
            'route' => 'sonata_admin_dashboard',
        ])->setExtras([
            'icon' => '<i class="fa fa-bar-chart"></i>',
        ]);

        $name = $child->getName();
        $position = 0;
        $order = array_keys($menu->getChildren());
        $oldPosition = array_search($name, $order);
        unset($order[$oldPosition]);
        $order = array_values($order);
        array_splice($order, $position, 0, $name);
        $menu->reorderChildren($order);        
    }
}