<?php

namespace DrivingSchool\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;

class LoadSQLFixtures extends Fixture implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function load(ObjectManager $manager)
    {
        
        $finder = new Finder();
        $sql_fixure_path = $this->container->getParameter('sql_fixure_path');
        $sql_name = $this->container->getParameter('sql_fixture_file');
        $finder->in($sql_fixure_path);
        $finder->name($sql_name);

        foreach ($finder as $file) {
            $content = $file->getContents();
            $em = $this->container->get('doctrine.orm.default_entity_manager');
            $em->getConnection()->exec($content);
            $em->flush();
            $em->clear();
        }
    }
}
